# Import libraries 
from PIL import Image 
import sys 
import os 
from pathlib import Path
import PyPDF2
import subprocess as sb

datasetPath = Path('./PDFs/')
textPath = Path('./TXTs/')

files = datasetPath.iterdir()

for file in files:
    if file.is_file() and file.suffix == '.pdf':
        print(f'Processing {file.name}')
        ocrMyPdfBruh = f'ocrmypdf "{file.resolve()}" "{file.resolve()}"'
        textFile = Path(textPath,file.name).with_suffix('.txt')
        command = f'pdf2txt.py -t text -o "{textFile.resolve()}" "{file.resolve()}"'
        try:
            sb.run(ocrMyPdfBruh, sb.PIPE, shell=True)
        except sb.CalledProcessError:
            print("Dude something went wrong with OCR")
        try: 
            sb.run(command, sb.PIPE, shell=True)
        except sb.CalledProcessError:
            print("Dude something went wrong with pdf2text")

        # with file.open(mode='rb') as pdf:
        #     try:
        #         pdfReader = PyPDF2.PdfFileReader(pdf)
        #         # Now we can start extracting text if we can without OCR
        #         numberOfPages = pdfReader.numPages
        #         textFile = Path(textPath,file.name).with_suffix('.txt')
        #         page = pdfReader.getPage(1)
        #         if '/Font' in page['/Resources']:
        #             # TEXT FOUND, USE PDFMINER
        #             command = f'pdf2txt.py -t text -o "{textFile.resolve()}" "{file.resolve()}"'
        #             try:
        #                 sb.run(command, sb.PIPE, shell=True)
        #             except sb.CalledProcessError:
        #                 print("Something went wrong with subprocess")
        #     except PyPDF2.utils.PdfReadError:
        #         print('not valid pdf')
        #         file.rename(file.with_suffix('.del'))
        #     except OSError:
        #         print('dunno wtf went on')