HAIL MARY

"Pilot"

Written by

Brian Gallivan

Based on Braunschlag

Created by 

David Schalko

Intrigue                                          January 24, 2016
Exhibit A
Sony Pictures Television

COLD OPEN

FADE IN:

EXT. TOWN CENTER - DAY

We open on a charming little town called Bottom Heights, 
which is located near the Canadian border and which was a lot 
more charming five years ago.  As we see a festival that’s in 
full swing, we hear the confident Mayor MARY Wolf (30).  

MARY (V.O.)

Welcome to Bottom Heights.  And 
welcome to our Biannual Corduroy 
Festival.  Which takes place twice 
a month.  Not because we don’t know 
what “biannual” means, but because 
people here think it’s that fun.  

Mary makes her way through the festival.  Although she’s a 
young, female politician, she’s less of a rule-following Amy 
Poehler and more of a rule-breaking Amy Schumer.  She’s a 
charmer though, and we see that everyone in town loves her.  
She’s got the charisma of a young Bill Clinton in a body that 
young (or old) Bill Clinton would be very drawn to.  

MARY (V.O.)

(upbeat)

I’m Mary Wolf, the mayor here.      

We see her smile and wave to the residents of her town, who 
enthusiastically smile and wave back.

MARY (V.O.)

My constituents are good people, 
and pretty easy to please.

SATISFIED CITIZEN

Thanks for all your hard work!

MARY

Hey, today’s not about me, Bill!  
Today’s about pants with ridges!

Bill and his wife laugh really hard.  

MARY (V.O.)

See.  Good people.  All I did was 
describe the pants we’re 
celebrating, and they’re delighted.

BILL’S WIFE

(to herself, delighted)

Pants with ridges.

2.

Mary sees KEVIN from the bank (30s, cute but dorky) waving.  

KEVIN

Mayor Wolf!  You were supposed to 
come by the bank!  We need to talk!

She darts and weaves through the crowd to avoid him.  

MARY (V.O.)

Our town is going through a bit of 
a financial crisis right now, and 
except for Kevin from the bank, the 
citizens are feeling confident. 

We see more people waving and smiling at her.

MARY (V.O.)

They believe in me as their leader.  
Because I am full of confidence and 
optimism.  And most importantly, I 
am full of (bleeped) shit. 

(then, sincere)

Don’t worry.  I’m not full of it in 
the way most politicians are.  I 
just have a gift for telling people 
what they need to hear to feel good 
about themselves.  

MARY

(demonstrates this by 

calling off to someone)

Johnny, I never thought I’d like a 
corduroy bike short!  But you make 
it work!

Johnny (40), awkward and a little shy, blushes with pride.

MARY (V.O.)

I like to give a boost to an 
underdog, like Johnny.  And I have 
a soft spot for nerdy kids.

MARY

(to two nerdy young boys)

May the cords be with you!

(off their surprised looks)

That’s right.  The theme of this 
year’s festival is Star Wars.

The boys almost explode with nerd joy.

MARY (V.O.)

It’s not the theme, but look how 
excited they are.  

3.

MARY

(to one of the boys)

Every ridge on your corduroys is a 
light saber.

NERDY YOUNG BOY #!

Awesome!

MARY

(to the other boy)

And your ridges are the beautiful 
laugh lines on Carrie Fisher’s 
gracefully aging face.

NERDY YOUNG BOY #2

Cool!  Good for her!

Mary smiles and turns to HEIDI, who we only see from the 
back.  Heidi is setting up jars of homemade jam on a table.

MARY

Heidi, I bet folks are enjoying 
that tasty jam almost as much as 
they’re enjoying those freshly-
trimmed bangs of yours.  

HEIDI (O.S.)

I was worried they were too short.

We see Heidi and her bangs, which are disastrously short.   

MARY

(believably sells this)

Too short?  What?!  Heidi, guys may 
say they’re into boobs and asses, 
but you know what they’re really 
into?  Forehead.  And they want a 
lot of it.  You nailed it, Heidi.   

Heidi beams as Mary walks away.

MARY (V.O.)

There’s nothing Heidi can do about 
her bangs, so why not say good 
things and make her feel better?

(a little guilty)

It’s similar to our town’s dire 
economic situation.  I don’t want 
to worry anyone, so I just keep 
saying to people...

4.

MARY

(upbeat, to a passing guy)

We’re on the verge of something 
big, Joe!

PASSING GUY 

Hope it’s coming soon, Mary!

MARY (V.O.)
(feeling terrible)

They don’t know that the “something 
big” we’re on the verge of... is 
financial collapse.

Mary dodges Kevin from the bank again.  

Mary crowns Corduroy Queen OLGA (a sour-looking old woman).  

DISSOLVE TO:

MARY

Congrats,  Olga!  And thanks for 
reminding us that corduroy rompers 
are great at every age.

OLGA

(an old friend, who loves 

giving her shit)

It wouldn’t look good on you.

MARY

(loves giving it back)

Don’t make me strip you naked and 
out-romper you, Olga.  

Olga, Mary, and everyone in the crowd burst out laughing.  
Nothing’s more fun than Olga and Mary ribbing each other.

MARY (CONT’D)

(into a microphone)

Look, I want to thank you all for 
being so patient while I fix our 
town.  I know I can do it because--
and many of you have heard this 
story--because my mom died right 
after she gave birth to me.

A lot of people nod solemnly.

MARY (CONT'D)

But her last words were “Tell Mary 
she can do anything.”  Those words 
made me a very confident woman.

(smiles)

(MORE)

MARY (CONT'D)

And a bit of a wild teen, but she 
said I could do anything, right?

The crowd laughs.  HENRY (chubby, 35) calls out a question.

5.

HENRY

(too interested)

What kind of stuff did you do?  As 
a teen?

MARY

(friendly teasing)

Henry, don’t be a perv.

Everyone laughs again, including Henry.

HENRY

(good-naturedly)

Sorry!  I was being a perv.

MARY

(back to her speech)

When my dad was mayor, he made some 
not-great, unapproved, big-swing 
investments to try to help our 
economy, and I said I’d fix that--

CURIOUS WOMAN 

How’s that all going?

MARY

(nervous, into mic)

We’re on the “verge of something 
big.”

Kevin from the bank is in front of Mary now.  Mary’s trapped 
but relieved when she hears him say...

I have good news!

KEVIN

MARY

(pulls him on stage)

Kevin from the bank has good news!

Everyone cheers.

KEVIN

(taking the mic)

I thought the town would be 
officially out of money tomorrow, 
but I’ve discovered that we have 
enough money to last for a whole 
other week!

(MORE)

6.

KEVIN (CONT'D)

(clarifying)

And then we’ll be out of money.

Mary is shocked and feels horrible.  Everyone is stunned.    

MARY

I thought Kevin had better news--

DISGRUNTLED CITIZEN

(panicked, outraged)

You haven’t fixed anything?!  

MARY

I encourage you to be open minded--

HEIDI

(short bangs, short fuse)
You’re worse than a Canadian!

A LOT of people agree with this statement, which hurts.

MARY

Wow.  That is a very low blow.  
Things are not as bad as you think!  
You’re all so resourceful--

Loose jam flies through the air and lands on Mary’s face.

MARY (CONT'D)

(trying to roll with it)

You can keep selling products like 
this delicious concord grape jam.

Some liquid lands on her face.

MARY (CONT'D)

(rubbing her eye)

And our apple cider.  Our very 
acidic cider. 

She ducks as something big sails over her head.

MARY (CONT'D)

And our masonry.  Our dangerous, 
dangerous masonry.  

(pops up, confidently)

I can fix this!

A worried Mary ducks as things larger than masonry are 
thrown.

END OF COLD OPEN

FADE OUT.

7.

ACT ONE

FADE IN:

INT. THE HOUSE OF MARY’S FATHER (MR. WOLF) - LATER

Mary talks on the phone as she walks up the stairs.

MARY
(into phone)

We have to review what “good news” 
means, Kevin.  No!  Do not file for 
bankruptcy yet.  I’ll find a 
solution!  I’m at my father’s.  And 
“good news,” he’s on his deathbed.

(sarcastic enthusiasm)

I did use it incorrectly!  Great 
work, Kevin!  Teachable moment!

INT. MR. WOLF’S BEDROOM - MOMENTS LATER

NURSE WANDA (a Wanda Sykes type) updates an emotional Mary.

NURSE WANDA

I’m Wanda, the new nurse.  Just a 
heads up.  Painkillers are causing 
him some delirium.  He’s good now, 
but earlier he called me Martin 
Luther King.  I was not flattered.  

(shrugs)

But also flattered. 

MARY

He called me Nick Nolte the other 
day.  I felt the same way.

NURSE WANDA

(as gently as she can)

I’d say he has about a week left.

MARY

Nick Nolte?  Sounds about right.

(then, serious)

No, I know.  I know.

Mary sits by MR. WOLF (70, a Christopher Walken type) who was 
a charming rascal in his day, but who looks very tired now.  

MR. WOLF

(groggily excited)

Mary, drugs are fun!

8.

MARY

(delighted by him)

Yeah, I know!  Mom told me I could 
do anything, remember?  And I did!

MR. WOLF

(to Wanda, proud)

I told you she was accomplished.

Mary holds his hand and they look at each other, both aware 
that there’s not much time left.  Mary gets a little teary.

MARY

I said I’d restore your good name.  
But I haven’t--  

MR. WOLF

No.  Forget about my name.  Focus 
on the town.  I feel awful about 
what I did, Mary.  My dying wish--

(interrupts himself)

Are dying wishes still a thing?

MARY

Yes.  They’re all the rage with 
dying people.

MR. WOLF

Great!  My dying wish is to see 
Bottom Heights headed in the right 
direction before I go.  Can you 
make that happen?   

MARY

(sincerely promises 

something impossible)

I can.  I’m gonna make that happen.   

NURSE WANDA

(very doubtful)

You can? 

MARY

I can.  He played the “dying wish” 
card.  That’s pretty motivating.  

MR. WOLF

She’ll do it.  Oh and one more 
thing, Mary.  Can you pray for me?    

MARY

(voice a little too high 

and a little too hopeful)

You gettin’ sleepy, Dad?

Thankfully for her, he actually is.  Wanda sits next to Mary.

NURSE WANDA

I take it you don’t pray. 

9.

MARY

Don’t pray.  Don’t believe.  Don’t--

(sees Wanda’s serious face, 

shifts to very upbeat)

--need to talk about me so much.  
Tell me about you!  Do you pray?!  

NURSE WANDA

I do.  I come from a long line of 
people who prayed because we used 
to be bought and sold.  But if you 
don’t need it, that’s cool.  Good 
for you. 

MARY

And the winner for Best Response to 
Atheism goes to Nurse Wanda!

NURSE WANDA

Trust me.  Believers have more luck 
than non-believers.  But if you’re 
not a sinner, then you’ll be fine.  

Mary considers this statement as we...  

INT. MARY’S BEDROOM - LAST WEEK

Mary and DONALD (a depressed Craig Robinson type) have just 
finished having sex.

FLASHBACK TO:

DONALD

(gently)

I can never leave my wife.  She’s 
not a kind person, but still, it 
would just hurt her too much.

MARY

(also gently)

I’d actually like something in 
writing from you saying you will 
never leave your wife.  

EXT. A DARK ALLEY - A FEW MONTHS AGO

TREMBLAY (a Dan Aykroyd type) shakes Mary’s hand.

FLASHBACK TO:

10.

TREMBLAY

Congratulations.  You’re in 
business with the Canadian mafia.

MARY

You should just say mafia.  It 
sounds more threatening.

They both laugh.  But Mary’s laugh has some worry under it.

BACK TO:

INT. MR. WOLF’S BEDROOM

NURSE WANDA

So are you a sinner?

MARY

(looks outside, relieved)

Oh, look, my brother Christopher!  

Mary rushes out of the room.  Nurse Wanda follows her.

MR. WOLF

(opens eyes, sees Wanda)

Hello, Beyonce.

NURSE WANDA

That one at least makes sense.

EXT. MR. WOLF’S FRONT LAWN - LATER

Mary and Wanda open the front door and see CHRISTOPHER (mid 
30s, a Zach Galifianakis type) coming up the front walk.

CHRISTOPHER

(matter-of-fact, to Mary)

You haven’t slept.  There’s a big 
discrepancy in how old your face 
looks and how old you actually are.

MARY

Don’t waste those lines on your 
sister.  Save ‘em for the “clurb.” 

CHRISTOPHER

(serious)

How’s Dad doing?

Mary gives him a sad look.  He nods stoically.

CHRISTOPHER (CONT’D)

(factual)

I see.  He’ll be dead soon.

11.

MARY

Okay.  I’m getting you some 
euphemisms for your birthday.  

CHRISTOPHER

I know he’s dying.  But I also know 
that I’ve  got a taxidermy project 
that will really cheer him up. 

He holds up a family portrait from when their mom was alive. 

CHRISTOPHER (CONT’D)

I’m re-creating our family using 
miniature replicas of wolves. 

MARY

Christopher, I am not in that 
family portrait.  

(then)

And I realize that by bringing that 
up first, it seems like except for 
that, I’m totally cool with a 
re-creation of a family portrait 
using miniature replicas of wolves. 

CHRISTOPHER

I’m doing it no matter what.  I’ve 
already got the skins.

(to Wanda, matter-of-fact)

I’m not just a taxidermist.  I also 
blog extensively debunking fake 
UFOs.  So the real UFO sightings 
can get the attention they deserve.

NURSE WANDA

Thank you for that?

CHRISTOPHER

No thanks necessary.  I love facts.

MARY

Stop flirting, Christopher.

(then, emphatic)

And stop being so factual with dad.

INT. MR. WOLF’S BEDROOM - YESTERDAY

Christopher sits by his dad’s bed.

FLASHBACK TO:

12.

BACK TO:

CHRISTOPHER

(cheerful)

If you’re wondering why your skin 
looks yellow, it can very easily be 
explained by acute liver failure.

MR. WOLF

(somehow charmed by this)

Fun!  Tell me about the kidneys.

EXT. MR. WOLF’S FRONT LAWN

CHRISTOPHER

She hates when I talk facts because 
she’s a politician who distorts 
them.  Mary, we may be family, but 
if I find out you’ve done anything 
illegal, I will blog about it.

MARY

(nervous, but covering)

As long as you don’t reenact it 
with tiny woodland creatures, I’ll 
be happy.

Wanda heads inside as a car pulls into the driveway.  JANICE 
(30s, a Cecily Strong type) pops out of the passenger seat.  

JANICE

Surprise!  It’s your favorite 
sister!

MARY

(bleeped)

Fuck.

CHRISTOPHER

(bleeped)

Fuck.

Janice runs towards her siblings as her boyfriend BOBBY (a 
beautiful, dumb, cocky James Franco type) opens the trunk.  

JANICE

Well, my horrible job is over!  
We’ve now reached the seventh boss 
in two years who was so jealous of 
me that she had to fire me.   

Mary and Christopher exchange a look.

13.

JANICE (CONT’D)

But I have a lead on a job where 
I’d be leasing rubber food-storage 
containers to friends and family. 

CHRISTOPHER

(simply)

Scam.

JANICE

(simply)

Jealous.

(then, serious)

Oh, our poor, sweet Daddy.  I’m 
sorry I haven’t been here.  But 
I’ve been praying hard for him.  

MARY
(big smile)

Oh thanks.  We’ve been wiping his 
butt.  

Bobby drops two big suitcases next to Janice.  

BOBBY

I’m gonna sit in the car.

(to Mary)

Nice to see you, Mrs. Wolf.

MARY

(as he leaves, horrified)

Does he think I’m your mother?!

CHRISTOPHER

(as he heads inside)

I told you about your face.

JANICE

Oh, Mary, Bobby and I are so, so in 
love.

Just then, Bobby drives away.

JANICE (CONT’D)

(explains)

He thinks it’s more fun to sit in 
the car when it’s moving. 

(then, excited)

He always comes back though, and 
now he’ll be back permanently!  
Bobby and I are moving here! 

Janice hugs Mary and screams with joy.  Mary just screams.  

14.

JANICE (CONT’D)

And now I can really be here to 
help with Daddy!  

MARY

Oh, Janice!  What a helpful promise 
you might keep!

JANICE

I know I used to be flaky-- 

MARY

You?  No way!  Anyone can take 
their grandma out of her nursing 
home for a day and misplace her.  

JANICE

(missing the sarcasm)

I guess you’re right.  And Grandma 
did get to write an article about 
her experiences that day.  Plus, 
that was years ago.  I’m 100% 
reliable now.  So what do you need? 

MARY

(she does need her)

Well, Wanda has to leave soon.  Can 
you watch Dad for a few hours while 
I try to save the town?

JANICE

Oh, Mary.  Always the drama queen.  
Of course.  I’d love to.

(off Mary’s nervous look)

It’ll be fine.

Mary nods and opens her car door.  Janice opens Mary’s 
passenger side door.

JANICE (CONT’D)

(not kidding)

Oh, right.  I’m staying here.  
Whoops.

Janice heads inside as Mary looks very nervous.

INT. MARY’S CAR - LATER

Mary drives while talking to Kevin on the phone.

Kevin, you said we had the week.  I 
need that week to find a solution.  

MARY

(MORE)

15.

MARY (CONT'D)

Because once we declare bankruptcy, 
we lose jobs, and pensions, and 
tourists!  You know what tourists 
never say?  “Hey!  We should go to 
that bankrupt town!”

An annoyed Mary hangs up and turns on the radio.

RADIO ANNOUNCER (V.O.)

The town of Full Meadow has seen a 
huge spike in visitors because a 
nun at their church appears to have 
the power to heal people.  Money is 
pouring into the town’s economy--

Mary turns off the radio.

MARY (V.O.)

(jealously)

A nun with “superpowers.”  That’s 
genius.  Good for her.

A pile of loose jam splatters across her windshield.  

MARY

(yells with a tight smile)

Thank you, Heidi!  Looks like a 
good batch this year!  

More jam hits the windshield.  As Mary pulls over to deal 
with it, a family driving by the other way stops.  

MOM IN CAR

(from the car, kindly)

What a mess.  We’re gonna help you!

MARY

Thank you so much!

MOM IN CAR

We’re going to see the healing nun!  
We’ll ask her to pray for you!

They wave and drive away.  Mary looks shocked, then pissed.

MARY

(really loses it)

I don’t need prayers!  I NEED PAPER 
TOWELS! 

(incredulous)

Who does that?  Who prays for 
someone?!

As she yells, more jam flies into her mouth.

MARY (CONT’D)

16.

Ahhrggg.

(swallows)

God, that’s good.  

INT. TAVERN/DISCO - LATER

Mary sits at the bar owned by her friend RUTHIE (a deadpan 
Maya Rudolph type).  Ruthie pours two shots.  

RUTHIE

There’s a little jam behind your 
ear.  Big date tonight?

MARY

Everyone’s gone God crazy!  They’re 
all praying, or asking me to, or 
running off to that nun before they 
even give me some Bounty paper t’s!

RUTHIE

So... no date tonight.  

MARY

Ruthie, do you believe in God?

RUTHIE

Yes.

MARY

You do?!  Why?!  Terrible things 
happen to you all the time.  You 
haven’t had sex with your husband 
in over a year!  You’ve been run 
down by a car in a supermarket 
parking lot.  Three times.  

RUTHIE

Once at a Whole Foods.

(braggy)

(then)

I believe God judges people by how 
well they enjoy themselves while he 
throws crap at them.  I’m not 
having sex with my husband, but I’m 
in an S&M relationship with God.  
That’s pretty hot. 

Donald, co-owner of the bar, enters and plays it cool because 
his sister Ruthie doesn’t know he’s sleeping with Mary.

RUTHIE (CONT’D)

For Donald, God’s all about guilt.

DONALD
That’s not true.

Mary raises an eyebrow at him.

INT. MARY’S BEDROOM - YESTERDAY

Mary and Donald are having sex.

17.

FLASHBACK TO:

DONALD

(as he climaxes)

I’m sorry!

MARY

Who are you talking to?

DONALD

Everyone.

INT. TAVERN/DISCO

BACK TO:

DONALD

Okay.  Guilt’s involved.  But 
mostly it’s that life’s too hard 
not to believe in God.

(then, to Mary)

Also, there’s some sort of plasma 
behind your ear.

MARY

(tries to rub jam off but 

gets a lot in her hair)

But what if you’re not a believer?  
What if you tried, but you can’t?  
What if you lost your mom the day 
you were born, and you can’t 
believe in a God who would do that?

(the jam is everywhere now)

What if the only thing you truly 
believe is you have to solve your 
own problems, and for the first 
time ever, you can’t solve them? 

(really choked up)

And you promised your dad you 
would.  Before he...   

Mary puts her head down on the bar.

18.

RUTHIE

(sweetly, rubs her back)

Oh, Mary... 

(still sweetly)

There’s a ton of jam in your hair.

(then, focusing)

Look, I know how much you love your 
Dad, but no matter what, he thinks 
you’re amazing--

MARY

It’s his dying wish.
(off their looks)

It’s pretty motivating.

RUTHIE

How much money to fix all this?  

MARY

To get us out of bankruptcy, and to 
pay back the Canadian mafia...

RUTHIE

They should just say mafia.

MARY

I know!  

Mary pulls them close and whispers the amount.

RUTHIE

(bleeped)

Jesus fucking Christ.

DONALD

Oh, wow.  You need more than 
belief.  You need a miracle.

MARY

(sighs, then, her eyes 

widening with excitement)

Oh my god!  Oh YOUR god!  I know 
how we’re going to save this town.
WE’RE GOING TO FAKE A MIRACLE!  

RUTHIE

(mutters a bleeped...)

Jesus Fucking Christ. 

END OF ACT ONE

FADE OUT.

19.

ACT TWO

FADE IN:

EXT. TOWN CENTER - LATER

Mary is parked near Christopher’s taxidermy shop.  She’s 
glancing furtively at a Virgin Mary statue inside of a Party 
City bag in her trunk.  Suddenly Janice is behind her.

JANICE

What’s that?

MARY

(slamming trunk, rattled)

Nothing.  Something.  Anything I 
want it to be.  I’m a feminist.

JANICE

It looked like a Party City bag. Oh 
my god!  Are you throwing me a 
surprise welcome home party?  

MARY

(so relieved)

I can’t say.  But I’ll give you a 
clue.  I am!

Janice dances around in excitement as Mary rolls her eyes and 
then rushes inside Christopher’s shop.  Janice follows her. 

INT. CHRISTOPHER’S TAXIDERMY SHOP - CONTINUOUS

Christopher helps OLD MAN WENGERT, who has his dog with him. 

OLD MAN WENGERT

Rusty and I aren’t ready to start 
the process yet.  We’re just 
looking at ideas for fun poses. 

CHRISTOPHER
(seeing Mary enter)

You still haven’t slept, Mary.  If 
you were dead, I would stuff you 
and stretch those wrinkles out.

MARY

As any thoughtful brother would. 

JANICE

Christopher!  Mary’s having a 
welcome home party for me!  

20.

MARY

(too focused on the miracle 

to deal with Janice)

Christopher, I actually came here 
to ask if you’d help Nurse Wanda 
watch Dad tonight?

CHRISTOPHER

(suspicious)

You’ve been at Dad’s every night 
for weeks.  What are you up to?  

MARY

(setting up her alibi)

I’m not up to anything.  I’m just 
going to go to bed early, get some 
rest, and try to look less old.

OLD MAN WENGERT

I think you’ve always looked great. 
Even when we were in high school.

MARY

Thank you, classmate.

JANICE

So, Mary!  I can’t wait.  Tell me 
about the surprise party!

MARY

(suddenly realizing)

Wait!  Janice, what the hell are 
you doing here?!  You’re supposed 
to be watching dad right now!  

JANICE

(makes a fake scared face)

Oh no!  Is this how you’re going to 
get me there for the surprise?

(laughs, then, remembers)

Oh no.

INT. MR. WOLF’S BEDROOM - LATER

They find an eyes-closed, way-too-still Mr. Wolf.

SMASH TO:

MARY

(heartbroken)

Oh no.

21.

JANICE

You almost got me!  Is he gonna pop 
up as part of the surprise?

MARY
(very loud)

He may never pop up again!

Mr. Wolf pops up suddenly.  They all scream.

INT. HALLWAY OUTSIDE MR. WOLF’S BEDROOM - MOMENTS LATER

The siblings close the door behind them.

JANICE

FYI.  I only left him to go light a 
candle for him at church.  

MARY

FYI.  He could have died.  All 
alone!  FYI.  I promised him he’d 
see this town doing well before he 
dies!  FYI.  I can’t do that if I 
can’t count on you, Janice!  FYI.  
I can’t stop saying FYI!    

JANICE

Well, I’m sorry if you fail for the 
first time ever, and you’re not 
Daddy’s favorite one time!

MARY

Maybe I’m his favorite because I 
show up!  And because I never lost 
his mom in a ball pit at McDonalds!

JANICE

She got to write an article about 
it!  Fine!  I’ll watch Dad tonight!

MARY

Oh wow!  Thanks for the one night!  
Janice, I’m so tired from helping 
Dad that I look like a contemporary 
of Old Man Wengert!  So I don’t 
need your PRAYERS or CANDLES!  I 
need you to show up and DO 
SOMETHING, ANYTHING!  As long as 
it’s not strolling in asking for a 
WELCOME HOME PARTY!  There’s no 
party, Janice!  Because you don’t 
deserve ANYTHING from Party City!

22.

JANICE

That is a terrible thing to say!

Mary storms out.  Janice turns to Christopher.

JANICE (CONT'D)

Can you believe her?

CHRISTOPHER

We’re not on the same side.

JANICE

I agree.  No one’s on her side.

INT. DONALD’S GARAGE - NIGHT

We are close on the statue of the Virgin Mary.  “Ave Maria” 
plays in the background.  A teardrop falls from the statue’s 
eye.  It’s hauntingly beautiful.  Then a full on spray of 
water comes shooting out of her eye.  We reveal a drenched 
Donald, Ruthie, and Mary trying to rig up the statue. 

DONALD

(never too optimistic)

Is there a plan B?

MARY

(intense)

The Virgin will appear tonight.  
This town will become the new 
miracle hot spot.  And people will 
say the healing nun is garbage.

RUTHIE

(gives her a look)

Do we have to take down a nun?

MARY

(more intense)

Garbage!

EXT. THE HILL BEHIND TAVERN/DISCO - LATER

In the darkness, Mary, Ruthie, and Donald are sneaking up to 
the top of a hill in the woods behind Tavern/Disco.  They’re 
carrying the small statue of the Virgin Mary, a microphone, a 
speaker, and some lights from the disco. 

MARY

When Old Man Wengert does his 
nightly walk with his dog, he’ll 
have a good view of where our 
Virgin Mary will be “appearing.”  
He’ll be a good gullible witness.

23.

DONALD

You know this is a real long shot, 
right?

MARY
(vulnerable)

Yeah.  I do.  But I have to take 
one last big swing.  Plus, maybe my 
Dad will see The Virgin Mary before 
he dies.  I think he’d like that.

RUTHIE

That is the sweetest thing I’ve 
ever heard from someone planning to 
deceive an entire town about a 
sacred being.

INT. MR. WOLF’S BEDROOM - LATER

Nurse Wanda reads as Janice sits by her sleeping father’s 
bed.  The TV’s on, and Janice is a little too interested in a 
commercial for the healing power of guinea pigs.    

JANICE

(re: guinea pigs)

Good to remember if I’m ever sick.

Wanda shakes her head.  

EXT. TOWN CENTER - MOMENTS LATER

Old Man Wengert is walking his dog.  The dog looks up to the 
hill behind Tavern/Disco and starts barking.  

OLD MAN WENGERT

You can bark all you want, but 
we’re not getting any prostitutes. 

Then he looks to where the dog is looking, and he can’t 
believe what he’s seeing.  There is a beautiful light focused 
on the one and only Virgin Mary.  She looks real!  

INT. MR. WOLF’S BEDROOM - CONTINUOUS

MR. WOLF

(looks out the window)

Mary! 

JANICE

Mary’s not here Daddy.  Just me.  

MR. WOLF

(full of joy and wonder)

No!  The Virgin Mary!  She’s here!

24.

NURSE WANDA

(sees it too, stunned)

Mary?  You’re in Bottom Heights?  
Haven’t you been through enough?

JANICE

(sees it too, typical 

Janice, very dramatic)

I feel... the power.

EXT. TOP OF HILL BEHIND TAVERN/DISCO - CONTINUOUS

Our Mary, hidden behind some bushes, looks down on the town.

MARY

Old Man Wengert’s watching.  Go!

RUTHIE

(on mic, sings beautifully)

I am here for you and for others!  
Go proclaim this miracle! 

Ruthie starts riffing some angelic sounding notes.  Donald 
harmonizes, and it sounds amazing.  There’s enough cloud 
cover that Old Man Wengert’s definitely going to buy it.  
Mary’s thrilled, but then suddenly she hears police sirens 
coming closer, and she sees someone charging up the hill.  
And it’s not Old Man Wengert.  It’s her brother Christopher!

CHRISTOPHER

(as he runs up the hill)

Declare yourself!  

Mary and Donald scramble to gather equipment.  Ruthie tries 
to grab the Virgin statue, but it’s stuck between two rocks.

RUTHIE

She’s stuck!  And there’s a big 
Party City price tag they’re all 
gonna see if we leave her here!

CHRISTOPHER
(from even closer)

DECLARE YOURSELF!  

MARY

(panicked, bleeped)

HOLY SHIT!

END OF ACT TWO

FADE OUT.

25.

FADE IN:

ACT THREE

EXT. BACK SIDE OF HILL BEHIND TAVERN/DISCO - MOMENTS LATER

Mary rushes over to help Ruthie pull on the Virgin statue. 

RUTHIE

(praying)

God, please free this plastic--

MARY

Ruthie, if there is a God, he’s not 
going to help in this situation!

(yanks hard, frees it)

Now run, woman!

They all start sprinting down the back side of the hill.  

MARY (CONT’D)

What’s Christopher doing here?!  We 
don’t need any debunkers right now!  

RUTHIE

(to Donald)

Our singing was really good.  
Should we start a brother/sister 
band?  Like the Carpenters?

MARY

Ruthie!  Focus! 

(then, can’t help herself)

And, yes, you should.  You sounded 
amazing.

EXT. TOP OF HILL BEHIND TAVERN/DISCO - CONTINUOUS

On top of the hill, Christopher stands where Mary had been.  
Old Man Wengert finally reaches the top of the hill. 

OLD MAN WENGERT

(overcome)

I saw the Virgin Mary!  

He looks to Christopher.  Christopher says nothing.  

EXT./INT. MARY’S HOUSE - LATER

Mary opens her door to see OFFICER GARY and OFFICER LINCOLN 
on her front steps with Old Man Wengert and Christopher.  
Mary’s wearing pajamas and maybe playing the groggy, just-
woke-up attitude a little too much. 

MARY

(rubbing her eyes)

So sleepy.  Me.  What’s going on?

26.

OFFICER GARY

(dubiously)

Old Man Wengert claims he saw The 
Virgin Mary on the hill behind 
Tavern/Disco.

MARY

(acting just as dubious)

That sounds TOTALLY crazy.

CHRISTOPHER

Mary, wait. 

Mary looks nervous.

CHRISTOPHER (CONT'D)
It’s not crazy.  I saw it too.

Mary’s relieved but can’t tell if he’s lying to protect her.

MARY

Well, as mayor, it’s my duty to 
alert the press.  And, let’s see, 
maybe the Vatican?  Yeah, that 
sounds good.  Let’s immediately 
alert the press and the Vatican.

INT. MR. WOLF’S BEDROOM - THE NEXT DAY

A blissed-out looking Janice sits by her father, who looks 
weak, but at peace.  Wanda is on her phone.      

NURSE WANDA

I could see her.  Yes.  The Virgin 
Mary.  Of course she looked good!

JANICE

(hearing Wanda, inspired)
She looked amazing.  She was 
amazing.  She makes me want to be 
amazing.  And I’m not just gonna 
buy candles.  Or pray.  I’m gonna 
do something!  

She grabs her purse and rushes out of the room.  

NURSE WANDA

(into phone)

There’s another sister now.  
Weirder.  I’m not kidding. 

EXT. TOWN CENTER - LATER

Mary walks with a spring in her step as a van full of old 
ladies pulls up.  They roll down their windows. 

27.

LEADER OF THE OLD LADIES
Excuse me!  Do you know where the 
Virgin Mary appeared?

MARY

Yes!  Up on that hill there.  Hey, 
how’d you hear about it?

LEADER OF THE OLD LADIES

It’s on all the miracle blogs.  
You’re making that nun who heals 
people look pretty rinky-dink!

MARY

Would you call her “garbage”?

LEADER OF THE OLD LADIES

(frowns)

No.  She’s a nun.

MARY

Fair enough.

Mary grins big as they leave.  Christopher is suddenly there.  

CHRISTOPHER

Last night was pretty crazy, huh?

MARY

(nervous)

Oh!  Yeah.  The craziest.

(then)

I’m on my way to meet the 
Investigator from the Vatican.

They stare at each other.

CHRISTOPHER

I know it wasn’t the Virgin Mary.

Mary looks caught.

CHRISTOPHER (CONT’D)

I can’t prove it, but I think...

(beaming)

I think it was a UFO. 

(off her shocked look)

Or an alien.  Trying to make 
contact with me.  

MARY

Well... both seem reasonable.

28.

CHRISTOPHER

(excitedly explaining)

Right before the “Virgin Mary” 
appeared, I was working on the wolf 
replica of mom, and I was really 
missing her.  And I think in an 
effort to make contact with me, the 
aliens projected an image of what I 
needed to see most at that moment. 

(big finish)

The mother of all mothers.  

MARY

Christopher, I need to tell you--

CHRISTOPHER

I’ve been feeling so sad about Dad 
dying.  But this gave me hope.

(emotional)

And I really needed some hope.  

MARY
(conflicted)

Well, that’s... great.  I’m glad.

CHRISTOPHER

And you can still tell people it 
was the Virgin Mary.  I know that 
will be good for the town.  

MARY
(surprised)

Isn’t that “distorting the facts”?

CHRISTOPHER

Hey, it’s the brilliant cover the 
aliens chose.  They did that for a 
reason.  Isn’t it comforting to 
know that the aliens have a plan?  

MARY

I get so much comfort from that.  

CHRISTOPHER

Oh, I need to show you something!

EXT. CHRISTOPHER’S TAXIDERMY SHOP - MOMENTS LATER

She sees the finished re-creation of their family portrait 
with the miniature wolves.  It’s totally weird and adorable.

29.

MARY

You put me next to Mom.  Thank you!

(looks more closely)

You made my wolf face look so old.

CHRISTOPHER

I had to.  They’re replicas.

(then)

It may be the chemicals I’ve been 
working with, but I want you, me, 
and Janice to hang out more.  And 
to call ourselves the Wolf Pack.

MARY

It’s the chemicals.

EXT. THE CHURCH PARKING LOT - LATER

Mary is waiting at the church.  Suddenly she sees Janice 
running towards her with shopping bags on each arm. 

JANICE

Mary!  I have such a Virgin Mary 
buzz!  I feel like she looked right 
at me and said, “Get it, Janice.”  

MARY

That sounds like her.

JANICE

She’s given me the power to do 
whatever I want!  This must be what 
you feel like all the time! 

MARY

Me?  Why?

JANICE

You have those words of Mom’s.  
“Tell Mary she can do anything.”

Mary has never heard Janice talk about this. 

JANICE (CONT’D)

Do you know what Mom’s last words 
to me were?  “Janice, we don’t eat 
keys and pennies.”

MARY

(feels for Janice, but 

can’t help asking)
Were you eating both?  

JANICE

The point is I have confidence now.

30.

MARY

(surprised at her warm 
feelings for Janice)

Well, if it ever goes away, we can 
always share our final words from 
Mom.  We can pretend she said both 
things to both of us, and our joint 
motto becomes, “I can do anything.  
Except eat keys and pennies.”

JANICE
(considers it)

I like it.  Because it still allows 
you to do almost everything you 
want to do.

Mary makes a puzzled face.  Janice remembers her bags.

JANICE (CONT’D)

I forgot!  You said you didn’t need 
prayers. You needed me to do 
something!  Well, I did!  I got 
guinea pigs to heal Daddy!  I’ll 
let you know what your share is!

She runs off as Mary shakes her head.

INT. INSIDE THE CHURCH - LATER 

A crowd (including Kevin, Christopher, Ruthie, Donald, Heidi, 
and Olga the Corduroy Queen) have gathered for the arrival of 
the Investigator from Rome.  Kevin pulls Mary aside.

KEVIN

I have good news!  

MARY

Based on your track record, I’m 
guessing I have an aggressive 
cancer or the river is poisoned.

KEVIN

LOTS of Tourists are coming!  We’ve 
seen two busses today!  We might 
not need to file bankruptcy!  

MARY

Yessssss!  Now, that’s good news, 
Kevin!  That’s good news!  Oh thank 
God!  No!  Thank no one!  

31.

KEVIN

All we need now is to have the 
miracle approved.  By him, I guess.

Mary turns and sees the handsome but irritating INVESTIGATOR 
MATTEO (think Bobby Cannavale with an Italian accent).  He 
wears a stylish black suit and black shirt instead of the 
traditional clothes of a priest.

MARY

So where’s your collar, Father?

INVESTIGATOR MATTEO

And where’s your hoop skirt?

MARY

Excuse me?

INVESTIGATOR MATTEO

(annoyingly smug)

Time moves forward.  Fashion too. 

MARY

(can’t let him win)

Then why do I own four hoop skirts?

They both paste on a smile, but they do not like each other.  
Still, there seems to be a sexual chemistry between them.

CHRISTOPHER

There seems to be a sexual 
chemistry between you.

Ruthie laughs hard.  Donald, who saw the chemistry, does not.     

INVESTIGATOR MATTEO

(laughing, dismissive)

I’m sorry to laugh.  It’s just... 
Why would Mary appear in this town?

MARY

Why wouldn’t she?  She’s not super 
choosy.  She gave birth in a barn.

INVESTIGATOR MATTEO

Mayor Wolf, do you know how many 
mayors say that a miracle has 
happened in their town?  You can’t 
just say it happened.  You need 
evidence of an actual miracle.  

Just then, Mr. Wolf, who hasn’t been out of bed in months, 
walks into the church clutching two guinea pigs.  A smiling, 
teary Janice and a bewildered Nurse Wanda follow behind. 

CHRISTOPHER

(thrilled)

What is happening?! 

32.

MARY

(beyond excited)

Dad?!

MR. WOLF

These two animals have cured me.  
They’ve taken it on themselves.  Do 
you understand?  They’ve sucked the 
illness out of me.  It’s a miracle! 

Everyone’s shocked.  A wide-eyed Mary turns to Janice.  

JANICE

(still stunned)

I guess I can do anything.

MARY

(really excited)

I guess you can!  

(off Janice’s smile)

Well, except eat keys and pennies.

Janice laughs, and they rush over to their father and 
Christopher for a group hug.  Mr. Wolf can’t stop smiling.  

CHRISTOPHER
(quietly, to Mary)

Wolf Pack.

MARY

(smiles and nods, then)

Now never say it again.

MARY (V.O.)

I faked a miracle.  And now some 
crazy stuff has happened.  It can’t 
just be the guinea pigs.  So I have 
to consider the possibility that 
there are greater forces at work in 
the universe.  Forces that make 
real miracles happen.  Forces that 
I might have to start believing in. 

MARY

(hugging her dad, bleeped)

Holy shit.

END OF ACT THREE

FADE OUT.

33.

TAG

Coming up, on Hail Mary.

MARY (V.O.)

We see very short snippets of future scenes from the series.

INT. CHURCH

INVESTIGATOR MATTEO
(to Mary and a healthy Mr. 

Wolf)

I don’t believe either miracle.

EXT. DARK ALLEY

TREMBLAY

Mayor Wolf, I hope you haven’t 
forgotten about the Canadian mafia.

EXT. TOWN CENTER

MARY

I need that key.  Where is it?!

JANICE

I swallowed it!  I couldn’t resist!

INT. TAVERN/DISCO

CHRISTOPHER

(suspicious as he listens 

to Ruthie and Donald sing 
a Carpenters song)

I’ve heard this singing before.

DISSOLVE TO:

DISSOLVE TO:

DISSOLVE TO:

DISSOLVE TO:

EXT. TOWN CENTER

HEIDI (O.S.)

IS THIS WHOLE THING A LIE?!

Mary’s face gets splattered with jam.

END OF SHOW

