REALLY

Pilot episode

BIRD STRIKE

By Jay Chandrasekhar

Draft 9 (AMAZON)
January 4, 2013

Duck Attack Productions

INT. MEXICAN RESTAURANT - EVENING

JED is a typical, trouble seeking, slightly overweight 
suburban Chicago dad.  Not that it matters, but he’s also a 
neurosurgeon.  And he’s Indian-American.  He’s sitting in a 
Mexican restaurant across from his wife...LORI.

Although she lives in the Chicago suburbs, she’s from 
Philadelphia. Just ask her.  Lori is a part time corporate 
lawyer and a full time mom.  

Jed and Lori spar a lot, but it’s almost always playful.  If 
you asked, they’d both say they married the right person.

They have three opinionated children...SAMMY (6), EMMA (5) 
and CHARLIE (3).  

At the moment, Emma and Sammy are arguing, noisily, as Jed 
and Lori order from an attractive WAITRESS.  

WAITRESS
Something to drink?

LORI

Ooooh, hmmm, let’s see...

Jed waits, as he watches his wife.  He lets her stew in it.

LORI (CONT’D)

Uh, you go.  Come back to me.

JED

Are you sure? (Lori nods) Three 
lemonades and a Margarita, on the 
rocks, with a little salt. 

LORI

Really?  You couldn’t kill a little 
more time than that?

WAITRESS
I could come back.

LORI

No.  One second.  (To Jed)  So 
you’re drinking tonight?

JED

One drink.  I can have one drink.

LORI

I don’t care if you drink.  I care 
if you snore.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              2.

JED

(Smiles) Order.

LORI
Ok...let’s see.

Lori continues to read the drink menu.

JED

My wife is surprised when someone 
asks her what she wants to drink at 
a restaurant.  

Lori looks at a woman at the next table.

What is she having?

LORI

WAITRESS

A strawberry Manhattan.

LORI

Ok, I’ll try that. (Jed smiles) 
What?

JED

(Laughs) I love you.

WAITRESS

I’ll get those right out to you.

The hot waitress walks off towards the kitchen.  Jed waits a 
beat and then turns towards the bar, where a TV is mounted 
above.  ESPN is playing a college football game.  

JED

(Half hearted) Can you see what the 
score is?

Lori is now reading the food menu.  She looks up briefly.

LORI

No.

Jed pretends to watch the college football game, but he is 
really watching the waitress walk.  Wow, can she walk.  As he 
zones out on her ass, he is interrupted by...

EMMA (O.S.) 

Daddy, tell him.  Daddy...

Jed looks at his daughter, Emma (Age 5).

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              3.

JED
Tell him what?

EMMA

Boys have bigger boobs than girls, 
right?

JED

Ha!  What?  No. Shshsh.

SAMMY

See, stupid!

JED

Don’t call her “stupid.”

EMMA

Yes, they do!  Daddy, a mermaid’s a 
boy, right?  Tell him. 

Close on what Emma is pointing at.  It’s a wall mural of a 
topless Mermaid, perched on a large clamshell.  

No, hon, that’s a girl.  

JED

Mermaids are girls, dummy.

SAMMY

JED (CONT’D)

Don’t, Sammy...

Emma is not letting this one go.

EMMA

No, but boys have bigger boobs than 
girls.  Right, Daddy?  Because of 
your boobs?

JED

Shshshsh.  Stop.

LORI

What is she saying?

JED

I don’t know.  Nothing.

LORI

Honey, what’re you saying?

Emma practically begs Jed to back her up.  

EMMA

Daddy’s boobs are bigger than 
yours.  Right, Daddy?  Remember you 
said?  

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              4.

Lori looks at Jed: “Seriously?”

The title comes up on screen:  “REALLY”

INT. JED’S AND LORI’S HOUSE - HALLWAY - NIGHT

Lori walks out of Emma’s room.

LORI

Good night, honey.

EMMA (O.S.) 

Good night, Mommy.

INT. KITCHEN - NIGHT

Jed stands in the kitchen, quietly opening a bottle of 
Budweiser, as Lori enters.

LORI

(Smiles) Why is our daughter 
confused about the gender of a 
mermaid?  

Jed pours his Budweiser into a “frozen mug” from the freezer.

JED

I was in the shower...

LORI

(Smiles) Never a good way to start 
a story involving children.

JED

I was in the shower.  And she came 
in, said my name was “fat daddy” 
and then she looked at her chest 
and said that I had bigger boobs 
than she did.  Which, of course, is 
literally true.  And then she said 
I had bigger boobs than you.  

LORI

And what did you say?

JED

I don’t know.  I probably laughed, 
and said “you’re right.”

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              5.

LORI

Am I seriously dating a guy who’s 
comfortable with having bigger 
boobs than his wife?

JED

We’re married. (Lori raises an 
eyebrow) I know.  I get it.

LORI

Hon, you’re a doctor.  You’re 
supposed to be healthier.

JED

I’m going to the gym tomorrow.

Jed walks over to the two garbage cans, one is black and the 
other is white.  

LORI

Go in the morning, because we have 
an early dinner party at Fred and 
Jo’s tomorrow night.  

As he’s about to throw the empty bottle into the black can... 

LORI (CONT’D)

Oh wait.  White is recycling now.

JED
Since when?   

Jed drops the bottle into the white can and walks back over.

LORI

Well, I really like the kitchen in 
Parenthood.  And they have their 
cans like this.  So think of white 
as recycling goodness, and black as 
evil garbage.

JED

That seems kind of racist.

LORI

It does, doesn’t it?  But I married  
a brown guy, so I get a free pass.

Jed laughs as Lori leans into him and drinks a sip of his 
beer.  They kiss.  

JED

Hey.  I’m ready for that birthday 
blow job.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              6.

LORI

What?  Your birthday was last 
month.

JED

Did my present expire?

LORI

No, no. I’ll give it to you.

They keep making out.  Lori starts batting at Jed’s “tits.”

JED
Don’t do that.

LORI

It’s foreplay.  I’m going to second 
base.  

Jed drops and starts doing push ups.

LORI (CONT’D)

One, two, three, four, five 
thousand more to go!

INT. BEDROOM - NIGHT

Jed and Lori are fooling around in bed.

LORI

Happy birthday, sugar.

Lori goes down, under the covers, and her head starts 
bobbing.  We can tell by Jed’s face that he’s enjoying it.  
He closes his eyes, really enjoying life.  Then...

CHILD’S VOICE (O.S.)

I can’t sweep.

JED

AHHHHH!!!!

His eyes BOLT OPEN!  Standing there, is their 3 year old son, 
Charlie.  Lori’s head stops bobbing underneath the covers.

CHARLIE

Where’s Mommy?

JED

Uhm, I don’t know.  We’re playing 
hide and seek.

Charlie points to Lori shaped lump under the comforter.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              7.

CHARLIE

I FOUND HER!

Lori emerges from under the covers, Mommy all the way.

LORI

Oh, you found me, sweety!

I found you!

CHARLIE

Lori lifts Charlie up.

Great job!

JED

*
*
*
*

JED (CONT’D)

Don’t kiss him with that mouth.

Lori and Charlie exit, as Jed finishes his beer and waits.  
After a bit, Lori walks back in, brushing her teeth.

LORI

Do you need to finish tonight?

JED

Uh, need to?

LORI

I have to get up at 5:30.  (Sexy) I 
promise, promise, promise to give 
you a blow job tomorrow night, ok?

JED

Can you pass me my iPad?

Jed stands up to go to the bathroom.

LORI

Why?

I’m gonna take care of myself.

JED

LORI

Oh, I’d rather you didn’t.  Save it 
for tomorrow, ok?

She kisses him, and then goes into the bathroom.  Jed turns 
on a light and grabs his book.  After a beat, Lori walks back 
out and gets into bed.  She inserts foam ear plugs into her 
ears and puts a sleeping eye patch on top of her head.  

LORI (CONT’D)

You’re not gonna read now, are you?

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              8.

JED

I can get you a helmet if the eye 
mask and ear plugs aren’t enough.

LORI

What?

JED

I can get you a helmet...(Laughs)  
I always fall for that one.

Lori winks, as Jed turns off the light.

JED (CONT’D)

Good night.

On screen: “15 minutes later.”

The sound of LOUD SNORING is heard in the darkness.  Lori 
lifts her eye mask and listens.  Then, she nudges Jed.

LORI

Jed, you’re snoring.  Roll over.

Jed rolls over and stops snoring.  Lori lowers her eye mask.  
Jed starts snoring again.  Lori lifts her eye mask again.

LORI (CONT’D)

Babe...can you go sleep in the 
living room?

Jed’s done this before.  He grabs his pillow, and his eye 
drops and walks out.  He kicks the door shut.

LORI (CONT’D)

Thanks.

INT. LIVING ROOM - NIGHT

Jed throws his stuff onto the couch, and walks into the 
kitchen.  

INT. KITCHEN - NIGHT

He grabs some paper towels and pumps a couple of squirts of 
lotion onto his hand.  

INT. LIVING ROOM - NIGHT

He lays down and begins to masturbate.  His fantasy...

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              9.

EXT. PARKING LOT - NIGHT - FANTASY

Jed starts his car.  Knock Knock.  It’s the hot waitress from 
the Mexican restaurant, holding a doggie bag.  Lowers window. 

WAITRESS

You forgot your food, Doctor.

JED

Oh, thanks.

WAITRESS

Hey, any chance you can give me a 
ride home?  My shift’s over and my 
car’s in the shop.

EXT. SUBURBAN NEIGHBORHOOD - NIGHT - FANTASY

Jed’s car pulls up in front of a dark suburban house.

WAITRESS

Oh shit.  I left the lights off.  
Would you mind walking me in? 

INT. HOUSE - NIGHT -  FANTASY

They walk in.  The waitress turns on some lights, including 
the back porch light, which reveals a bubbling hot tub.  

WAITRESS

I’m gonna hop in, if you’d care to 
join me.  It’s good for sore legs.

JED

Oh, I don’t have a suit.

WAITRESS

You have boxers?  (Jed nods)  I’ll 
go in, in my panties, if you 
promise to be good.

The waitress starts to peel off her shirt, as we CUT TO...

EXT. PORCH - NIGHT - FANTASY

Jed and the waitress are in the hot tub.  The Waitress leans 
against him, grabbing two beers off the ledge, behind.  Jed 
eyes her delectable nipples, inches from his face.

Beer? 

WAITRESS

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              10.

She kisses him and then pats the side of the tub.  Jed slides 
out and sits there. Then, she pulls him out of his boxers.

WAITRESS (CONT’D)

Yum.

She takes a sip of beer, which she holds in her mouth, and 
then starts to go down on him.  Jed holds her hair to get a 
better look at...  

LORI (O.S.)

Really?

Jed opens his eyes to see his wife standing over the couch, 
holding a glass of water.

Uh,...

JED

LORI (CONT'D)

I don’t see why you have to 
do that?  It’s not like I’m 
holding out.

JED (CONT’D)

No, I know, but it’s perfectly 
natural.

LORI

(Sighs)  I just don’t want you, in 
here, fantasizing about other 
women.

JED

Babe, I was fantasizing about you.

LORI

Oh really?  Tell me.

JED

Well, we were in a hot tub.  

LORI

What hot tub?

JED

I don’t...Just a generic one...And 
you were dressed in a really 
expensive Louis Vuitton gown...

LORI
In the hot tub?

Well, you took it off...

JED

LORI (CONT'D)

Oh please.  Good night.

*
*

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              11.

And she walks off.  We hear her footsteps, and then a door 
close.  Jed spits in his hand, and starts jerking off again, 
this time with his eyes open, looking for Lori.  

TIME CUT:

INT. LIVING ROOM - NIGHT

Jed wipes up with the paper towels, and walks into the 
kitchen.  He thinks for a beat, before throwing the paper 
towels into the white can (recycling).  He walks back to the 
couch and lays down.  After a long beat, he begins to snore. 

INT. BREAKFAST ROOM -  MORNING

Lori is eating, and reading the New York Times, while Jed 
cuts a bagel.  The following conversation is totally playful.

LORI

It’s just much worse when you 
drink.

JED

I don’t really drink that much.

LORI

Yeah, but if you drink anything, 
you snore. 

JED

So, you want me to go sober?  

LORI

No.  I’m just worried that you’re 
drinking because you like sleeping 
on the couch, so you can masturbate 
about other women.

JED

How can you...?  This conversation 
is where logic went to die.

LORI

Grrrrr.

JED

What did you used to do, when we 
first started dating?  You never 
had a problem and I drank way more.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              12.

LORI

(Bats her eyes) Well, I was moon 
eyed back then.

JED

(Not meaning it) So maybe you’re 
not in love with me anymore.

LORI

Oh come here, honey.  Are you not 
getting enough love?

Lori holds her arms out.  Jed goes to her and they start 
kissing, when... 

WHAM!  

THE SOUND OF SOMETHING HITTING GLASS.

A look of annoyance crosses Jed’s face.  Lori lifts the paper 
again, as Jed walks out of the room.  The camera follows him 
to the living room, where we see a large bay window.  
Outside, is a garden and woods.  A quivering, near dead, 
Robin lays on the ground, near the window.  A few feet away, 
two more dead birds lay on the patio.  Jed exhales loudly.

JED

You got a robin this time!

LORI

If we can see glass, how come they 
can’t?

Jed walks back into the kitchen.

JED

This has to stop.  I’m putting up 
the owl sticker.

LORI

No, you’re not.  This is not a 
skateboard store.  I’m not having 
stickers on the windows.  

JED

This is the third one this month.

LORI

And how would I know that?  From 
the three dead birds sitting out  
on our patio?

Jed eyes the twitching bird.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              13.

JED

It’s still alive.  

LORI

So put it out of its misery.

JED

What?  Stomp on it?

LORI

Oh, right.  You’re afraid of birds.
Why is that again?

JED

If a human, with its big brain, can 
go crazy and shoot up a mall, why 
can’t a bird with its little brain 
go crazy and peck your eyes out?

LORI

(Poking fun) Do you really want me 
to do this for you?

JED

No.  I want to put the sticker up 
so that they stop flying into the 
glass.

The birds stops twitching.

JED (CONT’D)

I’m leaving them there so you 
understand what you’re doing.

LORI

What I understand is that I married 
a guy who’s afraid to do a man’s 
job and remove the dead birds.  

Jed huffs into the garage, as Lori goes back to reading.  
After a long beat, Jed returns, holding a large OWL STICKER.

LORI (CONT’D)

No.

JED

You’re presiding over an avian 
genocide.

LORI

It’s called Natural Selection.  
Smart birds don’t have a problem 
with it.  It’s the dumb birds who 
don’t think, and pay the price.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              14.

Jed takes the owl sticker and sticks it to the bay window. 
Then, he walks back into the kitchen.

LORI (CONT’D)

I’m just gonna take it off. (Beat) 
Hey, can you go to the grocery 
store for me?  The dinner tonight 
is pot luck.  Write this down.

He pulls out his phone and starts typing the list.

LORI (CONT’D)

I need flour, shortening, a dozen 
Macintosh Apples, cinnamon...

JED

What’re you making?

LORI

Bird pot pie. (Lori winks)

INT. GYM - MORNING

Jed is on the elliptical, at his gym, sweating, while 
watching THE WALKING DEAD on his iPad.

O.S. VOICE
What’s up, buttface?

Jed turns to see his friend, STEVE who is getting onto the 
machine next to him.  Single, Steve is a carouser.  He’s 
smart, funny, flirty and he loves, loves, loves new women.

STEVE

What’re you watching?

JED
Walking Dead.

STEVE

Oh shit!  Did you get to the part 
where the zombie kills that human 
dude and turns him into a zombie?  

JED

(Laughs) This is a great show.

STEVE

No, I know.  I gotta watch it.  
What’re you doin’ tonight?

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              15.

JED

Uh, just a dinner party.  At 
Fred’s.

STEVE

Where’s my invite?

JED

Well, it’s not my dinner party, but 
you should swing by.

STEVE

You guys don’t like me anymore?

JED

Pal, you’re always invited, but you 
never come anymore.  

STEVE

It’s the wives, right?  They don’t 
like me.

JED

They love you.  Just,...there won’t 
be any single action there.

STEVE

Can I bring a date?

JED

Pretending it’s my dinner party, 
sure.  Is it Shauna?

STEVE

Shauna?  No, no. Shauna is a gonna.  

JED

Is there a new one?

STEVE

Yeah.  She’s...I met her on a 
flight back from New York.  

JED

Don’t tell me she’s a flight 
attendant?

STEVE

Yeah.  Virgin.  I pretended I 
didn’t know how to put a seat belt 
on, and she gave me a private 
demonstration. 

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              16.

JED

Mmm, Virgin has the hottest flight 
attendants.  (British) Thank you, 
Sir Richard. 

STEVE

I don’t know if we can make it 
tonight, anyway.  We’re going 
downtown to a flight attendant 
party.  (Wistful) Just me and a 
bunch of attractive 25 year old 
girls, contemplating our futures.

JED

Is it bad to work out with a hard 
on?

STEVE

(Laughs)  You should come.  Bring 
Lori.

JED

Yeah, we’ll be there.  We just have 
to pop by a sexy school teacher 
party first, so...

Steve laughs, as Jed plops his ear phones back in.

EXT. SUBURBAN HOUSE - NIGHT

Jed and Lori walk up the walkway of a nice, suburban house.  
Jed is holding a tray of Apple Empanadas in one hand, and a 
half eaten one in the other.  They ring the bell.  

LORI

Don’t eat the empanadas.

She brushes crumbs off his face.  

JED

They’re delicious.  You’re 
delicious.

Jed pulls Lori in for a kiss.  The door opens, revealing 
FRED, Jed’s close friend.  Fred is smart and funny and an 
alcoholic, who smokes and drinks like he’s just out of 
college.  It can be a problem.  It’s his house.  

FRED

Hello, love birds!   Come in!  God, 
you’ve got a beautiful wife.

Fred kisses Lori hello.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              17.

JED

Not on the lips.  Not on the lips.

INT. HAYES’ HOUSE - NIGHT

Fred leads Jed and Lori into the kitchen, where they see 
JOANNA, Fred’s wife, putting the finishing touches on a plate 
of hors d’oeuvres.  Joanna is a knock out, whose quick wit 
sometimes cuts to the bone.  You can’t help but feel that 
Joanna may be out of Fred’s league.  Jo mostly smiles through 
her husband’s growing alcohol issues.  

LORI

Are we the first ones here?

JOANNA

Mike and Margaret are out back.  
You’re not gonna believe what 
Margaret is wearing.  Straight out 
of the preppy handbook, circa 1988.

LORI

I have some stuff in my closet that 
I think would shock you.

FRED

Who wants a drink besides me?

JED

You have vodka?

FRED

Do I have vodka?  I have nothing 
but vodka.  Lori?

LORI

Pinot Grigio.

FRED

I have that, too.

INT. LIVING ROOM - NIGHT

Fred and Jed walk into the living room, to Fred’s bar.  
Outside, is their friend, MIKE and his wife, MARGARET.  
They’re both smoking.  Mike is 6’3”, a great looking guy, but 
also sort of a dorky jock.   

From Connecticut, Margaret is a preppy dresser, and already 
owns some “Mom jeans.”  She writes a lifestyle blog.  Jed 
pops his head out and Mike smiles big, flipping him the bird.  

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              18.

MIKE
Hey, handsome.

MARGARET

Jed!  Don’t hate us for smoking.  
We’re just having one.

JED

Not at all!  I need customers.

MIKE

Smoking causes brain damage?

JED

Smoking causes everything.  

MIKE

Eh, fuck it.  I can live with a 
little dain bramage.

INT. KITCHEN - NIGHT

As they walk back into the kitchen, two more friends, HAYES 
and ALISON enter.  Hayes is a funny, bossy, know it all. 
Alison is a smart, put together therapist, who is far more 
mischievous than she looks.  Hayes and Alison have kids, but 
are not married.  She won’t do it.  

FRED

Crazy Hayes and Alisonain 
Democracy!

Hugs and kisses.

HAYES

(Re: drink) I want one of 
those!

Fred starts out of the room.

FRED (CONT'D)

On my way!

HAYES (CONT’D)

Joanna, do you have some paper 
towels?  I stepped in some shit in 
your yard.  Not sure if it’s dog or 
human.

ALISON

Might have been Fred’s.

Laughs.

I HEARD THAT!

FRED (O.S.)

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              19.

INT. DINING ROOM - NIGHT

Dinner is already in progress.  People are sitting in 
couples.  Hayes and Alison, Jed and Lori, Mike and Margaret, 
Fred and Joanna.  This is a tight group of friends.

HAYES

Reality television is an American 
disaster.  It’s a celebration of 
our dumbest and our worst and...

Oh, here we go.

ALISON

HAYES (CONT'D)

...It should be banned.

HAYES (CONT’D)

If I were in charge of television, 
Reality would be banned.  It’s 
ruining the country.

ALISON

Oh come on.  People like it.

HAYES

But it’s fake.  It’s all made up.

MARGARET

It’s not all made up.

HAYES

Remember Mark Cavanaugh?  He 
“writes” for that show, America’s 
Smartest Model.

LORI

Mark Cavanaugh?  Really?

JED

Oh god!  Mark Cavanaugh!  

Everyone looks at Jed.

JED (CONT’D)

(Re: Lori) She dated Cavanaugh.

LORI

For like a week.  (Beat) A week of 
non-stop passion.

Laughs.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              20.

MIKE

America’s Smartest Model.  That’s 
the show where the models just sit 
around, drink Chardonnay and argue, 
right?  

ALISON

Yeah.  It should really be called 
“Models Arguing.”

MARGARET

And so he, what, writes what they 
say?

HAYES

He sets up situations.  Like he’ll 
get a black model and then a 
racist, white model, and he’ll get 
them both drunk and then inform 
them that they have to live in an 
apartment together with one bed.  

FRED
Why one bed?  

HAYES

So they’ll, hopefully, hook up.

JOANNA

I saw that one.  The white one made 
a crack about Michelle Obama, and 
then the black one bitch slapped 
her, and they started wrestling.  

FRED

Mucho lesbioso, baby!

MIKE

Tell me he’s getting lucky.

HAYES

Cavanaugh?  He banged the racist 
one.  Excuse my English.

ALISON

She looks like a coat hanger.

FRED

A fuckin’ sexy coat hanger.  She’s 
a C.H.I.L.F.  

Laughs.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              21.

FRED (CONT’D)

A coat hanger I’d like to 
fuck.

We get it.

JOANNA

*

ALISON

What else would you ban?

JED

How about texting and driving? 
(Looks at Lori)  

LORI

(Smiles) I don’t text and drive.

JED

I didn’t say you did.

LORI

You looked at me.

JED

I looked at you because you’re a 
stone fox.

WOMEN

Awwwww.

MIKE

I read about this app that disables 
your phone when the car is moving 
over ten miles an hour.

LORI

Great.  Now, all we need is an app 
that disables Jed’s car after three 
drinks.  

Laughter, “oooohs and aaaaahs.”

JED

I weigh 200 pounds.  I can drive a 
car on three drinks.

LORI

Ok, Doctor.

JED

And...we’re taking a cab tonight.

FRED

I think it’s actually kind of safer 
in a weird way.  I mean everyone is 
just texting at red lights now.  

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              22.

FRED (CONT'D)

So, when the light turns green 
they’re still just sitting there, 
which prevents them from getting T-
boned by someone who’s running a 
red light.

LORI

See?  It’s good for America! (Winks 
at Jed)

FRED

Hey if you two are taking a cab, 
tonight, you might as well start 
drinking like it. (Starts pouring)

INT. HALLWAY -  NIGHT

Alison and Joanna exit a room, pulling the door shut behind. 

ALISON

(Whispered) Your kids are yummy.  

JOANNA

(Whispered) You can have them.

Laughs, as they move down the hall. 

JOANNA (CONT’D)

We went to a birthday party today 
and they were just awful.  They 
refused to leave before they got a 
crack at the damned donkey pinata.  
But the birthday boy was throwing a 
15 or 20 minute hissy fit.  So, I 
told my kids that the pinata was 
full of healthy snacks, which 
totally ruined it for them.  And 
Mama won.

ALISON

Evil mama!

Laughs, as they stop outside the guest room.

ALISON (CONT’D)

Did you redo this room?  It’s so 
cozy.

JOANNA

You and Hayes are welcome to spend 
the night?  But you’d have to sleep 
with Fred.  And he’ll probably wet 
the bed.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              23.

ALISON

He sleeps in here?

JOANNA

(Jokey) Rubber sheets and all.  
This is my life!  (Beat) I can’t 
tell if he’s drinking more or if 
his liver just stopped working.  
Does that happen?

ALISON

Why don’t you ask Jed?

JOANNA

Because Jed’s an enabler.

INT. LIVING ROOM - NIGHT

Alison and Joanna walk in to see everyone hanging around, 
drinking.  Steve is there with his date, JENNY, 24, who is 
young, hot and wearing a tight mini-dress with heels.

FRED

Look who stopped by!

Hey!  Steve! (Hugs)

JOANNA

Hey!  This is Jenny.

STEVE

*

JOANNA (CONT’D)

Great to meet you.  Why didn’t you 
come for dinner?!  We still have 
some food if you guys are hungry.

STEVE

Oh thanks.  We’re just gonna grab a 
quick drink and then head downtown 
for some sush.  We have a party 
later.

ALISON

And how did you two meet?

JENNY

On the New York/Chicago red eye.  
I’m a flight attendant.

STEVE

And the crazy thing is, I missed my 
earlier flight because of traffic.  

ALISON

(Fucking with them?) Wow!  I guess 
it was fate.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              24.

Fred downs his wine and sloppily pours some more.  

JOANNA

Honey, don’t spill on the carpet.

FRED

Never!

JOANNA

But you are!

FRED

Ahhh, club soda!

STEVE

Freddy’s goin’ big!

Fred drunk-moves towards the kitchen, flipping Steve off 
behind his back, as Joanna smiles tightly.

EXT. PATIO - NIGHT

Fred, Jed, Mike, Steve and Hayes are getting high.  Fred 
repeatedly tries to light a joint that won’t light.

FRED

Fuck this...fucking joint...

HAYES

Steve, may I say that Jenny has the 
most ridiculously perfect ass I 
have ever seen? Please tell me you 
didn’t join the Mile high club?  It 
would break my heart.

STEVE

Does a hand job under the blanket 
count?  

JED

(Wistful) Mmm, I wanna hand job 
under a blanket.

Fuck this fucking joint!

FRED

JED (CONT'D)

I have a pipe.

*
*

FRED (CONT’D)

How long were you gonna let me go?

Jed hands the pipe to Fred, who starts tearing the joint 
apart and filling the pipe.  Fred lights it, takes a monster 
hit and explodes in a fit of coughing/laughter.  He coughs so 
hard, he stumbles to a bush, where he vomits bile.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              25.

JED AND MIKE

Oooooooh!  

Get the poison out.

STEVE

*
*
*

Fred rinses him mouth with beer and spits it on the ground.  
He hands off the pipe and the other guys start smoking.

I did not get puke on it.

FRED

JED
Good man!    

Joanna exits the house, with a tray of desserts.

JOANNA

Apple Empanadas?  Lori made them.

Everyone grabs one.  Fred, who is clearly off his rocker, 
grabs 3 with one hand and grabs Joanna’s hip with the other, 
going in for a kiss.

FRED

(Sings) Tall and tan and young and 
lovely, the girl from Empanada goes 
walking...

Joanna spins, easily avoiding Fred.

JOANNA
Did you throw up?

FRED

I don’t think so.

STEVE

How’s Jenny doing in there?

HAYES

Yes!  We all want to know!

JOANNA

She’s charming us all with a master 
class on the push-up bra.

STEVE

Should I go rescue her?

JOANNA

Rescue us.  Kidding!  Kidding!

Joanna winks, and eyes the pot pipe.

Can I...?

JOANNA (CONT’D)

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              26.

Mike puts the pot pipe in her mouth and lights it.  She 
inhales and winks at Mike.

FRED

(Cracking up) Bitch loves to hit 
the pipe!

The joke falls flat.  Joanna exhales and walks back inside. 

JED

How did you get a girl that hot?

FRED

Jed, I have no idea.

Everyone laughs through the slightly odd moment.  Steve looks 
at his phone.  

STEVE

We’ve gotta book.  A whole crew of 
bisexual flight attendants awaits.  

HAYES, FRED AND JED

(Kidding) Fuck you!  Asshole!  
Diiiiiiick!

STEVE

Hey, if any of you can get a hall 
pass...

Fred, Jed and Hayes just stare, silent.

EXT. PATIO - NIGHT, LATER

Everyone is outside now, sitting around on wooden deck 
chairs.  Steve and Jenny have left.

We should go camping.  Wisconsin.

HAYES

JOANNA

Tents?

MIKE

Yeah, one tent for each married 
couple.  

FRED

And one for Steve and whoever he’s 
boning.

We’re not married, by the way.

ALISON

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              27.

Everyone groans.  They’ve heard this tiny distinction before.

JOANNA

(Smiles) Oh, how can we forget?  
You’re so free.

ALISON

(Taps mind) In here, we are.

HAYES

Don’t look at me!  I want to get 
married.

MARGARET

Why don’t we rent cabins?  They’ve 
got luxury ones on Balsam Lake.  
It’s called “glamping.”

MIKE

No.  We’re sleeping in tents.  

LORI

(To Jed) Hon, I love you to pieces, 
but I’m not sleeping in a tent with 
you.  You can sleep with Hayes.

HAYES
No, no, hell no!

ALISON

Oh, he snores?

MIKE

(Re: Jed) This man, asleep, sounds 
like a dying pig.  But the pig dies 
all night long.

FRED

And he sleeps with his eyes open.

HAYES

Oh shit, yes!  Pig zombie!

Mike, Fred and Hayes do imitations of Jed as a dying zombie 
pig with its eyes open.

JOANNA

You know, I hear there’s surgery 
for that.  Teddy Epstein did it.

LORI

Snoring surgery?  Jed?

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              28.

MIKE

They’d have to cut off his entire 
head.

FRED

HEADLESS JED!  YOU KNOW WHO YOU 
SHOULD GET TO DO THAT?  DR. PHIL!  
CAN YOU IMAGINE IF HE WAS YOUR 
SURGEON?  I MEAN, HE’S A NOT A REAL 
DOCTOR, RIGHT?!

JOANNA

Shhh.  We have neighbors.

FRED

I MEAN HE WOULD SHIT HIMSELF IF HE 
HAD TO DO A SURGERY!

LORI

Dr. Phil is a doctor.  

FRED

NO WAY!  NO FUCKIN’ WAY!  THEY 
SHOULD JUST CALL HIM “PHIL!”  

JOANNA

A little quieter, babe.

MARGARET

He has a PhD (Holds up iPhone)  
from University of North Texas.

FRED

JOANNA HAS A PhD TOO! (Off looks) 
PAPA HAS DOUGH!

Fred is the only one who laughs.

FRED (CONT’D)

(Slurring) I NEED ANOTHER EMPENADA.  
WHO WANTS ONE?  NOBODY?! 

LORI

They’re all gone.

FRED

EMPANADAS!  TONIGHT ON PHIL!

Fred turns to go inside, and WHAM!  He slams into the sliding 
glass door, and down he goes.  Spontaneous shock and 
laughter, as he pops back up, with a smashed nose, bleeding.  
He throws a fist in the air! 

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              29.

FRED (CONT’D)

I MEANT TO DO THAT!

Fred laughs and disappears into the house.  As Joanna 
follows, she eyes the smear of blood on the sliding glass 
door.  Lori looks at Jed.

LORI

(Smiles) Natural selection.

Jed laughs as his phone buzzes.  He looks at it.

JED

And that’s our cab.  Who is the 
drunk driving hero?!

They hug everyone goodbye, as the rest of the group starts to 
get up to leave.

INT. CAB - NIGHT

They get into the back of the cab.

JED

Remember, we’re picking up where we 
left off last night, right?

LORI

Oh right.  What was that, again?  A 
back rub?  Or was it a foot rub?

JED

I think it was anal.

LORI

Ew.  8307 Pine. 

The cab driver turns the meter on.

JED

Oh wait.  I left my bowl in there. 
(To Cabbie) One second.

LORI

Tell Joanna I’ll bring her tray 
back to her tomorrow.

Jed hops out and walks back up the street.  He passes Mike 
and Margaret’s car.  He leans down and raps on the hood.

JED

Hey!

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              30.

Jed notices that only Margaret is in the car.

MARGARET

Mike forgot his coat.  And I still 
vote for glamping!

Jed laughs, as he walks towards Fred and Jo’s house.

INT. JOANNA’S AND FRED’S HOUSE - NIGHT

Jed walks in, to the living room, where he finds...Fred 
passed out, on his back, snoring loudly, on the couch, dried 
blood, caking around his REALLY SWOLLEN NOSE.  Jed searches 
Fred’s pockets and finds...his pipe, grasped in Fred’s hand.  
Jed flips Fred over, so he’s face down, and then walks down 
the hallway, towards the kitchen.

INT. KITCHEN - CONTINUOUS

Jed grabs a bucket from under the sink and exits.

INT. HALLWAY - NIGHT

Jed walks back down the hallway towards the living room.  He 
hears voices.  Just then, the bathroom door creaks open a 
crack.  In the mirror Jed can see Mike (Margaret’s husband) 
sitting on the bathroom counter, while Joanna (Fred’s wife) 
gives him head.  Jed freezes for a beat, as he and Mike make 
eye contact.  Jed quickly continues moving down the hall.  
Behind him, the bathroom door opens and Mike and Joanna come 
out, disheveled, but covering.  Everyone freezes.  In the 
pregnant pause...  

Hey, buddy.

MIKE

JED

Lori is going to return your 
tray tomorrow.

*
*

JOANNA

Jed...?

But Jed just turns and walks away.  Mike follows.

INT. LIVING ROOM - CONTINUOUS

Jed walks quickly through the living room, slowing just long 
enough to place the bucket under Fred’s face, before exiting.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              31.

EXT. STREET - NIGHT

Jed walks out of the house, followed quickly by Mike. 

MIKE

Hey, dude, do we have band practice 
tomorrow?

What?!

JED

MIKE (CONT'D)

What?

JED (CONT’D)

What the fuck?!

MIKE

Dude, we were just talking!  It... 
she was crying about Freddy’s 
alcoholism, and their marriage, and 
I was just comforting her...

JED

With your dick in her mouth?!

MIKE

No!  That did not happen!

JED

I fucking saw you! 

MIKE

No, you’re mistaken.

JED

I am a guy!  Do not “deny til you 
die” with me, asshole! 

MIKE

Ok, shit.  I feel terrible.  I’m so 
sorry.  I don’t know what came over 
me...She was crying and we hugged 
and then she kind of kissed me...

JED

And then you popped your cock in 
her mouth?

MIKE

Man, my wife’s in the car.  

HONK! HONK!  They both eye Mike’s car, where they see 
Margaret, jamming along in the car, oblivious.  

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              32.

MIKE (CONT’D)

I gotta go.  I’ll call you tonight.  
I’m sorry.  I’ll call you.  

Mike gets into his car, kisses Margaret and drives off.

INT. CAB - NIGHT

Lori is still in the back seat.  Jed opens the door.

LORI

What took you so long?

JED

Uh, nothing.

LORI

Did you tell Jo about the tray?

JED

Oh, no.  I didn’t see her.

LORI
Where was she?

JED

I don’t know.  Sleeping? (To Cab 
driver) Ok.  We’re good. 

The cab pulls away, as Lori’s phone buzzes.  She reads.

LORI

It’s Joanna.  She says she had a 
great time and she loves us.

Lori texts back as Jed looks on.  Angle on text: “Will bring 
you tray tomorrow. xoxox”  Jed pulls Lori in for a kiss, but 
her phone buzzes again.  She reads Joanna’s text:  

“Jed told me.  No problem.”  Lori shows the text to Jed.

LORI (CONT’D)

What the fuck?

JED

She must have read my note.

Lori raises an eyebrow.

LORI

Why’re you acting weird?

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              33.

JED

I’m not acting weird, I’m acting 
horny.  Come here, you hot little 
creature.  

He pulls her in and they start making out.

INT. JED AND LORI’S HOUSE - NIGHT

Jed is wearing pajamas and slippers, brushing his teeth.  He 
looks worried.  Suddenly, Lori appears in the door way, 
wearing only a pair of panties.  Mmm.

LORI

You ready for that blow job?

INT. BEDROOM - NIGHT

Lori takes a sip of water, holding it in her mouth, before 
going down on Jed.  Jed closes his eyes, enjoying it.  
BZZZZZZZ.  Jed’s phone lights up on the night stand.  Jed 
reaches over and silences it.  He gets back into it... Then, 
RING! RING!  He reaches over and grabs the phone. 

LORI

Is it the hospital?

JED

No, I’m sorry.  I’m turning it off.

Jed reads the screen, as he turns the phone off.  

LORI

Who needs to reach you so badly?  

JED

It’s no one.  It’s Mike.

LORI

You’re not having an affair, are 
you?

JED

With Mike? (Small laugh) No.

He tosses the phone back onto the bed stand, and Lori 
continues to go down on him.  

JED (CONT’D)

I love you, hon.

Lori stops for a second.

REALLY - PILOT - BIRD STRIKE - DRAFT 9 (AMAZON) - 1/4/13              34.

LORI

I love you too, weirdo.

Close on Jed’s worried face.

EXT. JED’S HOUSE - NIGHT

It’s quiet, except for Crickets.  “1 hour later” comes up on 
screen.  Then, the sound of LOUD SNORING.

INT. HALLWAY - NIGHT

The door opens and Jed walks out, carrying a pillow.

INT. KITCHEN - NIGHT

Jed pours the last of a bottle of vodka into a glass of soda 
water and ice.  He mixes it with his finger and then takes a 
long pull.  He exhales, deep in thought.  Then, he throws the 
empty vodka bottle into the black garbage can.  

After a beat, he reaches in, retrieves the bottle and puts it 
into the white can.  He can’t help but feel that his whole 
world is about to change. .                

END.

