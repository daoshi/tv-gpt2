RUSSIAN DOLL

Episode 101

"Nothing In This World Is Easy"

Story by

Leslye Headland
Natasha Lyonne

Amy Poehler

Teleplay by

Leslye Headland

Goldenrod Revisions: 4/25/18
Green Revisions: 4/9/18
Yellow Revisions: 2/26/18
Pink Draft: 2/20/18
Blue Draft: 2/7/18
White Draft: 1/25/18

Paper Kite Productions
9015 Rosewood Ave
West Hollywood, CA 90048

DARKNESS.

SFX. Thumping bass of good music in a nearby room.

SFX. Water gushing like a flood. Or maybe a faucet?

SFX. A single, deafening KNOCK and--

Reflection of a WOMAN in the mirror of a home-spa style...

HARD CUT TO:

1

INT. MAXINE’S LOFT - BATHROOM - NIGHT - (LOOP A)

1

This woman is NADIA (36), troubled but one of the good guys. 
Clad in all black with a gold necklace, her style and 
attitude are the perfect marriage of feminine and masculine.

She stares at herself, eerily still. The water sound 
continues. There’s another KNOCK at the door. Over her 
indecipherable and placid face, the title card:

                     RUSSIAN DOLL

PULL OUT to reveal she is washing her hands. She turns off 
the faucet. The water sounds stop. She goes for the door.

A sculptural art piece covers the door. It resembles a portal 
or a mouth but it is just art. As she reaches out for the 
door handle (shaped like a revolver), another KNOCK pounds 
the door and it flies open. 

WHAM! Several PARTY GUESTS collide with Nadia and spill into 
the bathroom. A cacophony of music and conversation fill the 
empty space as Nadia goes down the rabbit hole into...

2

INT. MAXINE’S LOFT - CONTINUOUS - (LOOP A)

2

9PM. Sunday. A crowded, almost labyrinthine PARTY at a 
sweeping loft space in the East Village. We STEADICAM float 
with Nadia through GUESTS and FRIENDS (including the 
ATTRACTIVE LADY who is annoyed at her BOYFRIEND featured in 
ep. 103). She periodically waves hello or receives a peck on 
the cheek. Many wish her... “Happy Birthday, Nadia!” This is 
her birthday party.

SOUNDTRACK: “Gotta Get Up” by Harry Nilsson.

SFX. Snippets of the GUESTS various conversations overlap.

The loft decor is fantastical. Almost other-worldly.

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

1A.

Painters like Rene Ricard and Brett Whiteley line the walls 
with photographers like Sally Mann and Helmut Newton. 

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

2.

The high-brow art mingles with furniture smeared with stains, 
carpets ripped to shreds, and a red/white striped wallpaper. 
There are also several original art pieces scattered 
throughout the space. A chair covered in fluffy penises. A 
mirror with a manifesto scribbled across it. Etc.

We follow NADIA to...

3

INT. MAXINE’S LOFT - KITCHEN - NIGHT - (LOOP A)

3

More of an area than a separate room. MAXINE (30s), the host 
of this party, Nadia’s friend, and owner of this massive 
apartment, prepares a whole chicken for the oven.

Maxine is a successful media artist (think Bill Viola). She 
seems frenetic and unstable at first but only because she 
wears her heart on her sleeve and in her work, compounded by 
casual drug use and unresolved sexual trauma.

MAXINE

Sweet birthday baby!

(offers her a joint)

It’s laced with cocaine like the 
Israelis do it.

Nadia takes the joint and puffs. She seems a little down.

MAXINE (CONT’D)

(notices Nadia’s demeanor)

Are you having fun? 

NADIA

(shrugs)

“Fun” is for suckers, Max. Two 
minutes ago I turned thirty-six. 
Staring down the barrel of my own 
mortality always beats “fun”.

MAXINE

Don’t be morbid. It’s your party and 
I made a fucking birthday chicken! 

NADIA

And I do appreciate your efforts but 
I’ve never been good around 
concentrated affection from others.  
I love it. I hate it. It’s my 
struggle. Thank you. 

MAXINE

You’re welcome.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

3.

LIZZY (30s, lesbian, strong look with a sly smile), 
approaches. A contractor/fabricator who works with Maxine.

LIZZY

Hey, Max, the bathroom door turned 
out so killer. That revolver door 
handle? Straight fire.

NADIA

You made that door?

LIZZY

She made it.

MAXINE

She built it.

NADIA

Congrats! It’s terrifying.

LIZZY

Yeah. Guns are terrifying. Havin’ 
fun, birthday buddy?

NADIA

I’m fence-straddling. Lizzy, you’re 
a good person to ask: Do ladies have 
mid-life crises?

LIZZY

Cuz I’m dating a younger woman?

Younger?

NADIA

Dating?

MAXINE

LIZZY

Fine. Fucking a twenty-two year old. 

Does she know what 9/11 is?

NADIA

LIZZY

Does anyone? 

(hits joint)

Aren’t you a little young for a mid-
life crisis?

NADIA

I have the internal organs of a man 
twice my age. If I make it to the 
low seventies, I’ll be shocked.

BEAT. Both look at Nadia. Why is she so heavy? She sighs.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

4.

NADIA (CONT’D)

My cat’s gone.

Oatmeal? No!

MAXINE

What’d’you mean “gone”?

LIZZY

NADIA

He’s not “gone” gone but it’s been 
three days. Usually, if he wanders 
off, he’s back at my place or at the 
deli, after forty-eight hours max.

MAXINE

Do you think he’s okay?

NADIA

Fundamentally, sure, he’s a deli 
cat. Ferran found him in a trash-can 
in Seward Park. He has survived more 
than the three of us can imagine. 

Maxine gets distracted by GUESTS. Lizzy and Nadia walk 
further into the party.

LIZZY

Maybe you should consider taking the 
leap and making Oatmeal a strictly 
indoor cat. Just to be safe.

NADIA

I don’t believe in dictating the 
boundaries of a sentient being’s 
existence. For you, it’s safety. For 
Oatmeal and me? It’s a prison.

LIZZY

However old you feel, Nads, you 
definitely sound twenty-two.

JUMP CUTS: Dancing guests. Talking guests. Drunk and 
destructive guests. BACK ON: Nadia soaks in the crowd.

NADIA

All right. Let’s make some choices.

4

INT. MAXINE’S LOFT - BEDROOM - NIGHT - (LOOP A)

4

10PM. Separated from the PARTY by a partition. Decor same. 
Noise of revelry. Large exotic fish in a huge lit-up tank. 
Nadia looks at them while she cruises...

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

5.

MIKE (30s/40s), a divorcé, almost professorial in his style. 
We get the feeling this is not Nadia’s first rodeo with a 
cute, over-educated, talkative single man.

MIKE

--so when they say “working class” 
they mean, people who cannot afford 
a college education and end up 
greeting at Walmart cuz they didn’t 
learn to write code for computer 
software. They feel sidelined by the 
American Dream and resort to 
xenophobia and bigotry which the 
Right stokes with pundit bullying--

NADIA

You got kids?

MIKE

That’s your pick-up line?

NADIA

Hasn’t failed me yet.

MIKE

Yes. A son. His mom and I broke up 
last year.

NADIA

Naw. I got a cat.

Nadia reflects on this: Does she still have a cat? She had 
said it out of habit but the statement now seems like a lie. 
Mike’s phone BLIPS. He responds to a text.

MIKE

(without looking)

Your place is incredible.

NADIA

I don’t live here. It’s just my 
party. You know it used to be a 
school for Jews.

MIKE

(as if this were a joke)

Right.

NADIA

Seriously. Yeshiva students used to 
read the Talmud right where you’re 
standing. It gives this whole place 
a feeling of--

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

6.

MIKE

History?

NADIA

Creepiness?

She gets closer to him, leaning into him. She touches him. 
Mike tries to slow her down. Backs away maybe.

MIKE
What do you do?

NADIA

Software engineer. I freelance. I 
used to be at Rock-N-Roll Games.

MIKE

No shit. I play Dark Justice all the 
time. Battleground Blackout, too. 
What’s your handle?

NADIA

I don’t play.

MIKE
You don’t play.

She puts his hand down her pants.

NADIA

My place is just a few blocks away. 

MIKE

Jesus, you’re wet.

NADIA

I know, right? It's like, don't go 
chasing waterfalls.

MIKE

Gotta say Lisa ‘Left Eye’ Lopes was 
not my favorite--

NADIA

Because she burned down that 
dude’s house?

MIKE (CONT'D)

--because she burned down 
that dude’s house.

They both smile. Nice sexual tension between them. It’s on.

5

INT. MAXINE’S LOFT - ENTRANCE - NIGHT - (LOOP A)

5

11PM. Over the course of her interaction with Mike, the party 
has transformed. More of a seedier scene.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

7.

(Note: This is a good moment to establish ATTRACTIVE LADY 
having a fight with her BOYFRIEND from 103.)

Nadia, on her way out, grabs her coat from a rack. Just as... 
RUTH (60/70s) enters, a therapist and Nadia’s godmother. She 
sounds like Harvey Fierstein (thanks to a lifetime of chain-
smoking Carltons) and acts like she knows your life story.

RUTH

Those stairs almost killed me. 
Nothing in this world is easy. 
Except pissing in the shower.

NADIA

Hey, Ruth.

RUTH

Happy Birthday, pumpkin.

They embrace. Ruth reads Nadia’s body language.

RUTH (CONT’D)

You’re leaving? You can’t ghost your 
own birthday. It’s only--

(checks watch)

--Eleven. Ish. What’s going on with 
you? The oppressive love of your 
friends? The march of time?

NADIA

Ruth, you’re a great shrink but 
you’re not MY shrink. I caught some 
tail and I’m gonna split. No need to 
read into this. 

RUTH

Fair but I expect a kiss on the 
cheek and a promise you’ll come over 
for coffee this week.

NADIA

(kisses her cheek)

Enjoy the party.

RUTH

Party? On a Sunday night? I’m afraid 
not, love. This is just a loose 
gathering of soft intellectuals. 

ANGLE ON: Ruth’s POV of the party. Maxine and Lizzy in the 
crowd. Mike enters, on his phone again, responding to a text.

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

8.

RUTH (O.S.) (CONT’D)

In my day, a party was a goddamned 
party. Have I ever told you the one 
about my first husband and Hedy 
Lamar in Cancun?

(Mike joins them)

Hello...

NADIA

Mike, Ruth. Ruth, Mike. 

Since both Mike and Nadia are dressed to go, Ruth puts two 
and two together and beams at them.

RUTH

Excellent work, Nadia.

MIKE

I’m sorry?

RUTH

(takes his hand)

I’ve known Nadia her whole life. She 
is one of the good guys.

(to Nadia; in her ear)

Only our natural capacity for love 
can master sadistic destruction.

She winks at her and floats into the party. Almost swallowed 
by the entangled bodies of GUESTS. 

MIKE

Is that your mom?

NADIA

No. It’s complicated. 

Nadia grabs her coat, gloves, and iPhone. She exits.

6

EXT. MAXINE’S LOFT - NIGHT - (LOOP A)

6

East Village. Eighth Street and Avenue B. Tompkins Square 
Park. Nadia and Mike exit Maxine’s apartment. 

Nadia immediately lights up a cigarette. Across the street...

ANGLE ON: Nadia’s POV. HORSE (30s) a very handsome and  
apparently homeless oddball. He’s not wearing shoes. The kind 
of gutterpunk you see panhandling with a one-legged German 
Shepard in a bandana. He has a fair amount of face tattoos. 
He makes eye contact with Nadia. She doesn’t know him but his 
stare unnerves her. Mike notices:

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

8A.

MIKE

You okay?

RUSSIAN DOLL #101

Green Revisions (4/9/2018)

9.

NADIA

Always. Let’s get some provisions.

7

INT. DELI - NIGHT - (LOOP A)

7

ANGLE ON: Wall clock. 11:30 PM. ANGLE ON: Rows of brightly 
packaged cat food and litter. REVERSE ON: Nadia contemplates 
them. At the front of the deli, Mike hovers near the cashier, 
debating which type of condom to buy. He decides on sheepskin 
then notices there’s no one behind the counter. He looks:

His POV: The DELI PATRONS are appropriately weird for the 
neighborhood and the hour. An ancient OLD MAN with multiple 
used shopping bags. An intense WOMAN shakes plastic 
containers of nuts. Three WALL STREET TYPES stock up on 
nitrous oxide siphons from whipped cream canisters.

Nadia joins Mike, tosses cash and her items onto the counter: 
cottage cheese, crackers, prosciutto, plus the cat food.

NADIA

(notices Mike’s condoms)

Nice choice. I’m allergic to latex.

MIKE

Where’s the guy--

Almost on cue, FERRAN (20s), nice guy, rabidly introspective, 
enters and rushes behind the counter. His FRIEND stumbled in 
with FERRAN but immediately splits to the back of the deli.

NADIA

There he is. Ferran, how are you? 

FERRAN

I’ve been better. 

(in English)

Smokes?

NADIA

(nods)

You seen Oatmeal?

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

10.

FERRAN

Not for awhile. I always leave the 
Meow Mix by the Post, but nothing--

There’s a small CRASH from off-screen. Nadia turns to look. 
Her POV: Ferran’s FRIEND, obscured by an aisle or his own 
movement, picks up the products he knocked over.

NADIA
(to Ferran)

What’s up with your friend?

FERRAN

He had a rough night. 

There’s something about this comment that makes Nadia pause. 
She turns and walks toward the FRIEND. During the following, 
Mike buys all their stuff from Ferran. Back to her POV: Is he 
crying? Is he shaking? More drama than she wants to deal with 
right now. Then, a WALL STREET GUY calls out to her.

WALL STREET #1

Hey, shortie, where’s Clockwork Bar?

NADIA

Take a left at Disneyland.

WALL STREET #2

(tries to diffuse)

Come on, we’re new to the 
neighborhood. Where can we party?

NADIA

Avenue D and Eighth.

They thank her and exit. The Friend is gone. She turns back 
to Mike, who holds a plastic bag full of their stuff.

Where’d you send them?

MIKE

NADIA

Hardware store that closed in 1996. 

FERRAN

(notices Friend is gone)

Oh shit. 

Ferran goes after him. Nadia turns to Mike.

NADIA

Let’s get out of here before I end up 
with another cat.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

8

INT. NADIA’S APARTMENT - NIGHT - (LOOP A)

11.

8

1:00AM. Early Monday. Much like Maxine’s, Nadia’s apartment 
is another historic-turned-residential building in the East 
Village. Only Nadia’s is a small one bedroom/one bath.

Unlike the open space of Maxine’s loft, Nadia’s space is very 
limited. The walls feel like they’re caving in with 
overstuffed bookshelves. Overflowing ashtrays litter every 
surface. Her computer, with multiple monitors and keyboards, 
sits like a shrine on a corner desk. Many programs running. 
On the wall, a poster of William Burroughs with the phrase:

                     LIFE’S A KILLER

Nadia, post-coital, types and swipes on her iPhone. Mike 
enters, typing on his phone. 

MIKE

I don’t know why anal play is still 
so taboo for straight males. It 
seems almost parodic at this point. 
But it’s like I tell my students--

Mike notices untouched bowl of cat food and her slightly open 
window.

MIKE (CONT’D)

He got out through the window?

NADIA

Bold to assume my cat is male. I get 
it. I’m single and choose to foster 
an animal so it must be a pathetic 
attempt to fill the hole in my soul 
that would otherwise be filled by 
what? A penis? No thanks, I’m full.

MIKE

(game for a debate)

You want me to call you a “sad cat 
lady” so you can say you aren’t one?

NADIA

Being a feline lover is not and has 
never been “sad”. Have you seen the 
1982 film Cat People? It holds up.

MIKE

So you think the standard sexual 
narrative: pair bonding, marriage or 
formal partnerships-- 

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

12.

NADIA

--are just futile ways to stave off 
death and are destined to fail.

MIKE

Kinda harsh.

NADIA

‘The One’ in practical application,  
means ‘The One I’m Going to Die 
With’. To take care of me when 
you’re infirm and shit. So my move 
is gonna be to wait ‘til I’m in, 
like, my late sixties then seal the 
deal. This is assuming I don’t die 
in between then and now.

MIKE
(turned on)

Why didn’t you talk like this before 
we fucked?

NADIA

I thought you wanted me for my body.

MIKE

Would you sit on my face right now?

NADIA

I would...

Her phone makes a noise. She holds it up to show him:

NADIA (CONT’D)

...but I called you an Uber.

HARD CUT TO:

9

INT. NADIA’S APARTMENT - NIGHT - (LOOP A)

9

Later. 1:30AM. Nadia, now alone, sits at her computer. She 
writes code, listens to her headphones, and pounds Red Bull.

REVERSE ANGLE: Her computer screen. Lines of code fly across 
the black screen, pushing the previous line up and away.

Nadia pulls her last cigarette out of her pack. She searches 
for more. She crushes the empty pack. Thinks. She forgot to 
get them at the deli. She was distracted by Ferran’s FRIEND. 
She sighs, then layers up to go out.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

13.

10

EXT. STREET - FOUR WAY STOP - NIGHT - (LOOP A)

10

Avenue B. WHOOSH! A yellow cab whizzes by Nadia as she starts 
down the street back toward the deli.

SFX. A cat mewing. Meow! Meow!

Nadia freezes. Her senses heighten. All diegetic sound drops 
out. All we hear is: Meow. Meow. Nadia looks around. Calm. 
Centered. Ready to answer the call. And then she sees:

ANGLE ON: A fluffy, gray and white SIBERIAN CAT pokes his 
head out of the dark, dense night of Tompkins Square Park.

BACK ON: Nadia. Her relief floods her body. A softness, 
previously undetected in her personality, oozes from her.

NADIA

Hey there, my little one.

Oatmeal looks at her then splits! He slips back into the 
darkness of Tompkins Square Park. All the diegetic sounds of 
the East Village come roaring back, as Nadia sprints into the 
street after him and--

WHAM! A car slams into her. 

Her blood and brains splatter on windshield. Her dead body 
bounces from the car to the street. A sickening crack as she 
lands. Then stillness. It’s all over in a few seconds. A 
couple of BYSTANDERS approach. The DRIVER exits the car.

PUSH IN on: Nadia’s corpse. ECU: Nadia’s eye. Glassy. Dead.

SFX. Water. The powerful sound of a river. Or is it a faucet?

SOUNDTRACK: “Gotta Get Up” by Harry Nilsson.

11

INT. MAXINE’S LOFT - BATHROOM - NIGHT - (LOOP B)

11

We are back to where we were at the beginning of the episode.

Nadia stares at herself in the mirror and washes her hands in 
Maxine’s bathroom. There’s a loud KNOCK at the door. 

Nadia turns off the faucet. Another KNOCK pounds the door. 
She goes for the door. Then she stops. The door is the same 
as it was. Still a sculptural portal. Which is still just an 
art piece.

CLOSE ON: Nadia’s face. She registers this moment as 
significant. Maybe that she’s done this before? 

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

14.

Maybe she’s concerned with what’s on the other side? With a 
rush of anxiety, she flings the door open.

SFX. A cacophony of Sounds and Music.

12

INT. MAXINE’S LOFT - KITCHEN - NIGHT - (LOOP B)

12

A few minutes later. Maxine prepares her chicken. Nadia 
across from her. It’s all exactly as it was before. Maxine, 
in the same outfit, same spirits, same actions.

MAXINE

Sweet Birthday Baby! Havin’ fun?

Nadia takes in the scene again. JUMP CUTS: Maxine, 
overstimulated, distracted by GUESTS. Her chicken, a carcass. 

MAXINE (CONT’D)

(offering a joint)

It’s laced with cocaine like the 
Israelis do it!

NOTE: Nadia is not fully cognizant of the fact that 
she is seeing, feeling and hearing the same things 
all over again. It’s more of an uneasy feeling of 
deja vu. Not full recall.

Nadia takes the joint from her. Looks at it.

NADIA

Max--

MAXINE

Are you not having fun? You hate 
your party--

NADIA

What was I just doing?

MAXINE

Why? What do you mean? You were in 
the bathroom. You mean before that?

NADIA

Does it ever freak you out? Partying 
here? In an old Yeshiva School? 

She hands the joint back to Maxine without smoking it. 

MAXINE

Why would that freak me out?

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

15.

NADIA

It doesn’t bother you that this was 
once, I guess, a sacred place?

MAXINE

It’s New York. Real estate is 
sacred. What’s up with you?

NADIA
(remembers)

Oatmeal. He’s gone.

MAXINE

Gone? No. He always comes back.

NADIA

Maybe he gave up. Do you think it’s 
possible for pets to commit suicide? 
Or do animals just have stronger 
self-preservation instincts because 
they don’t have souls.

MAXINE

(appalled; hands covered 

in dead chicken)

Animals have souls, Nadia. Jesus.

NADIA

Oh, yeah? Like chickens?

She exits just as Lizzy joins.

MAXINE

(to Nadia)

Fuck you, it’s cage-free!

LIZZY

What’s up with the baby?

MAXINE

I don’t know. If nobody eats my 
chicken I’m going to fucking kill 
myself.

13

INT. MAXINE’S LOFT - BEDROOM - NIGHT - (LOOP B)

13

Later. Nadia and Mike. Fish tank. Mike, in the same outfit, 
same spirits, prattles as he did before. Nadia listens but 
absent-mindedly. A little like someone going through the 
motions. As she stares into the fish-tank, wondering if fish 
have souls. She’s been listening to Mike longer so he is a 
little further along than last time. 

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

16.

MIKE

(mid-monologue)

--so like John Updike said: “Every 
marriage tends to consist of an 
aristocrat and a peasant. Of a 
teacher and a learner.” Since my ex-
wife has tenure at Fordham it was 
this imbalance of--

NADIA

(absent-mindedly)

You got kids?

MIKE

That’s your pick-up line?

There’s a pause. Nadia stares at Mike, surprised. She 
remembers this dialogue. Mike does not have any sense that 
this has happened before. Mike’s phone BLIPS. He responds to 
a text.

NADIA

A son. You’ve got a son.

MIKE

Have we met before?

NADIA

I think I have amnesia. 

MIKE

But you just remembered something. 
“Amnesia” means you forgot stuff--

Nadia has already lost interest and is back to the fish.

NADIA

(re: the fish)

You know it’s a myth that fish have 
no memory. Sometimes they can 
remember months. And a channel 
catfish can remember a human voice 
announcing food five years after 
last hearing it.

MIKE

That makes sense. If what they 
remember serves an evolutionary 
purpose, contributes to survival--

NADIA

But we don’t.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

17.

MIKE

Huh?

NADIA

Human memories don’t serve 
evolutionary purposes. 

MIKE

Fire. Ouch. Next time fire. No 
thanks.

NADIA

I mean, yeah, some of them do. But 
like what about deja vu, shame or 
nostalgia? How do they help us 
survive?

MIKE

(pause, really thinks)

They don’t.

NADIA

Exactly, deja vu is something to be 
ignored.

Nadia puts his hand down her pants like she did earlier.

MIKE

Whoa.

NADIA

Like this feels wrong but also 
right, you know?

He nods, enthusiastically. They make out.

14

INT. MAXINE’S LOFT - ENTRANCE - NIGHT - (LOOP B)

14

Later. Nadia prepares to leave with Mike as she did in her 
previous loop. She passes Maxine who hands her the cocaine-
laced joint again. Feeling a little better, post-Mike, she 
takes a hit. The joint, having burned down more, is shorter.

Just as she gets her coat, Ruth enters as before...

RUTH

Those stairs almost killed me. 
Nothing in this world is easy. 
Except pissing in the shower.

NADIA

Hey, Ruth.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

18.

Happy Birthday, pumpkin.

(she hugs Nadia)

You’re leaving? You can’t be 
ghosting your birthday. It’s only--

RUTH

(checks time)

Eleven. Ish.

NADIA

(suddenly scared)

I don’t know what I’m doing. 

RUTH

What’s up, muffin?

NADIA

I was gonna go home and fuck this 
guy but now I feel profoundly empty. 
When does menopause start?

The joint burns her fingers. She drops it.

NADIA (CONT’D)

Fire. Ouch.

RUTH

Let’s sit crooked and talk straight.

They pass Mike. He looks up to see Nadia re-entering the 
party. He can take a hint. His phone BLIPS again. He responds 
to a text. He moves to the exit. 

15

INT. MAXINE’S LOFT - LIVING ROOM - NIGHT - (LOOP B)

15

12:00AM. RUTH, who is mid-story, holds court with several 
GUESTS including Nadia. Everyone eats Maxine’s roast chicken.

RUTH

--so Nadia is nine years old. It’s 
her birthday party. There are no 
nine year olds at this party. It’s 
all adults. Me, her mother and all 
our friends. And at one point, my 
cousin Jimmy Weinstock says to her 
“How come you don’t have any friends 
your own age?” And she says: “Jim, I 
don’t believe in friends.”

Everyone bursts out laughing. Nadia listens with a soft 
smile. This is obviously a story Ruth tells every year. This 
year though, it affects differently than before.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

19.

NADIA

Every time that story gets better. 
Every time I wish it were true.

RUTH

I embellish. I’m allowed.

Conversations spring up around them as Ruth and Nadia 
sidebar. Ruth lights up a cigarette. Nadia downs her drink.

RUTH (CONT’D)
Are you feeling better?

NADIA

I was having this really intense 
deja vu. Like I had done all this 
already. This night. This party.

RUTH

This conversation?

NADIA

No. This is new which makes me feel 
like I might be okay? I’m probably 
emotional because--

RUTH

This was always going to be a tough 
birthday, Nadia.

Nadia is reluctant to talk about this. She sighs and downs 
her drink. Ruth touches the gold necklace Nadia wears. It’s a 
long chain with a Krugerrand at the end of it.

NADIA

Not everything is about Mom.

RUTH

She would’ve been proud you made it 
to thirty-six.

NADIA

I don’t know. She was pretty 
competitive. She might just be happy 
I’m finally older than she ever was.

Maxine approaches, happy everyone is eating the chicken.

MAXINE

I’ll make another chicken. Will you 
guys eat another chicken?

A GUEST, three-piece suit and surprising facial hair, holds 
up an almost archaic-looking slide or video projector.

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

20.

SUITED GUEST

Hey Maxine, what’s this?

MAXINE

Oh, I found this antiquing upstate 
and created some new projections for 
it. It’s geeky but you guys might be 
into it. Yeah? Fine. Let’s get it 
going. I’m looking for a communal 
experience not a solo performance. 
This is a safe space. 

Click! Maxine’s projector lights up and shines her work onto 
a wall. Nadia, Ruth and the rest look at...

ANGLE ON: The night sky. Constellations. We weave throughout 
the solar system. Flying by planets. We get to Jupiter.

BACK ON: Nadia. She turns away from the wall. All diegetic 
sound out. She is lost. Disconnected. The night sky reflected 
on her face. Intrusive memories of her previous loop appear.

16

INT/EXT. VARIOUS - NIGHT - FLASHBACK - (LOOP A)

16

Quick cuts. Nadia and Mike chat with Ferran. Nadia is 
distracted by the FRIEND. Nadia and Mike make love. Nadia 
looks for cigarettes. Nadia killed in a car accident.

BACK TO:

17

INT. MAXINE'S LOFT - NIGHT - (LOOP B)

17

Nadia snaps out of it. She gets up suddenly and stumbles into 
the projector. It falls over and the Night Sky disappears.

18

19

NADIA

I’m sorry. I-- I think I’m gonna--

She exits to the bathroom. Ruth and Maxine watch her go.

OMITTED

18

INT. MAXINE’S LOFT - BATHROOM - CONTINUOUS - (LOOP B)

19

Nadia bursts into the bathroom. Panting. Disoriented. It’s 
almost eerie how real and tactile it all is. A LESBIAN COUPLE 
make out near the sink. It is LIZZY and her twenty-year-old 
“girlfriend”, JORDANA. (NOTE: Jordana wears a scarf that will 
play in 108.) 

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

21.

LIZZY

What the Fu-- Hey, Nads.

Nadia, disoriented, tries to piece together what’s going on. 

NADIA

I think I might throw up.

LIZZY

You okay?

NADIA

Does this bathroom seem kinda... I 
don’t know... weird to you?

LIZZY

I mean, besides the shitty 
commercial fillers trying to patch a 
leaky U-joint it’s a nice bathroom?

(takes in Nadia)
You drank too much?

JORDANA

(playfully)

Me too.

NADIA

No. No. No more than usual. 

LIZZY

Smoked too much then.

NADIA

I can’t remember the last time I ate 
but other things are so clear it’s 
like they already happened and I’m 
doing them again. I think I’m dead.

We’re all dead.

JORDANA

LIZZY

(cute gesture to Jordana)

Sees a Fellini film once.

JOHN (O.S.)

Hey. You all right in here?

JOHN (mid 40s), handsome and truly adult, inherently balanced 
with an inconvenient romantic streak.

LIZZY

Hey John. All good. Nads is feelin’ 
a little under the weather.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

22.

JOHN

I can take a hint.

NADIA

No. I’m okay. I’m okay.

ANGLE ON: Lizzy and Nadia share a look. Nadia nods like “I’m 
okay. You can leave me with him.” BRIDGETTE pokes her head 
into the door. 

BRIDGETTE

Are you kidding? People are hopping 
up and down on one leg out here!

LIZZY

Well, she’s the birthday girl and 
he’s her ex so if I were you I’d 
find a backup plan.
(to Nadia/John)

If you guys aren’t out in twenty, 
I’ll tell the line to wait longer.

Lizzy rolls out with her GIRL, closing the door behind them. 
There’s a connection with John that immediately springs up. 
Nadia’s demeanor changes. She feels more safe.

JOHN

NADIA

Hi.

Hi.

JOHN
Happy Birthday.

NADIA

Thanks.

I thought you were ignoring me.

JOHN

NADIA

I didn’t know you were here.

JOHN

Even worse. 

NADIA

Well, I didn’t invite you.  We 
haven’t talked in six months.

JOHN

I know. Maxine did.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

23.

NADIA

She’s a good friend.

JOHN

Oh yeah?

NADIA

She knew I’d need a pick-me-up.

They are close to each other. Nadia puts his hand on her.

JOHN

Jesus, you’re wet.

NADIA

I know right? It's like, don't go 
chasing waterfalls. 

(then)

Sorry I don’t know why I said that.

JOHN

You had me at Left Eye.

They kiss. It gets heavy. They’re not going to have sex in 
the bathroom but they will probably get each other off.

20

INT. MAXINE'S LOFT - LIVING ROOM - NIGHT - (LOOP B)

20

12:45AM. The party rages on. John and Nadia smoke cigarettes 
out a window. John contemplates something Nadia just said.

NADIA

Honestly I’ll feel better if you 
just tell me I sound crazy. 

JOHN

I’m just trying to understand. So 
you think you were hit by a car when 
you were chasing your cat and now 
you’re reliving your birthday.

NADIA

I don’t “think”. I’m pretty sure it 
really happened.

JOHN

Okay. Well, let’s say you were 
actually hit by a car. Look at you. 
You’re not hurt. You’re not 
suffering any consequences. So even 
if it did happen it didn’t actually 
affect you.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

24.

NADIA

Hmm, I smell a metaphor for our 
relationship creeping up on me.

JOHN

My point is: you’re okay. 

NADIA

Well, I did smoke one of Maxine’s 
joints. Me and coke are like oil and 
vinegar. I don’t have the best track 
record with mixing substances. 

JOHN

Or metaphors.

NADIA

I said “like” oil and vinegar. 
That’s a fucking simile.

JOHN

What’re you running a touché race?

NADIA

(smiles)
How are you?

JOHN

You really want to know? My divorce 
is straight-up harrowing at this 
point. The last six months have been 
a onslaught of my own failure and 
other people’s misery. 

NADIA

How’s Lucy?

JOHN

She’ll be okay. I miss you.

NADIA

Don’t use your kid to guilt me into 
getting back together.

JOHN

Don’t use my kid as a pick up line.

This is, of course, very similar to the conversation she had 
with Mike in her first loop. This unnerves Nadia.

NADIA

I lost my cat.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

25.

JOHN

Wait what happened to Oatmeal?

NADIA

I feel like that might be the thing 
I need to do. Find him. 

JOHN

Doesn’t he go out though? Into the 
park? The deli? 

NADIA

It’s been three days.

JOHN

Do you want to go look for him?

NADIA

Right now? Oh my god. That’s all I 
want to do! Let’s blow this joint.

21

21A

OMITTED

EXT. STREET - NIGHT - (LOOP B)

21

21A

JOHN

So you wanna start on this side of 
the park? Or should we go to the--

He continues talking. Nadia stops listening. She stares 
across the street at Horse, the attractive/homeless guy. He 
smiles, Cheshire-like. Seeing him unnerves Nadia this time.

NADIA

I feel like I know that guy.

(yells at Horse)

Do I KNOW you?!

HORSE

WHAT?!

NADIA

DO WE KNOW EACH OTHER?

HORSE

FUCK OFF!

JOHN

(shrugs to Nadia)

Sounds like you two have a lot in 
common.

RUSSIAN DOLL #101

Goldenrod Rev. (4/25/18)

26.

Nadia suddenly stops. She finds herself at...

22

EXT. STREET - FOUR WAY STOP - NIGHT - (LOOP B)

22

...the corner where she was hit by a car. She’s flooded with 
deja vu. Nadia looks for something across the street and 
there he is: Oatmeal. She goes after him.

JOHN

Hey!

He stops her. VROOM! The car that hit her before passes them, 
clearing her this time.

JOHN (CONT’D)

Nadia. You were almost hit by that--

(putting it together)

--car. Fuck. That’s creepy. Is this 
some sort of prank or something?

*
*

NADIA

Of course! You know how much I love 
the movie The Game so I orchestrated 
a complex and terrifying prank for 
you for my birthday. I just saw 
Oatmeal. Let’s go--

JOHN

I think I should just drop you off 
at home. You need to sleep.

NADIA

I just saw him. You’re not going to 
help me now?

JOHN

Honestly? I thought Oatmeal was just 
a line to get me to go home with 
you.

NADIA

You think I need a line to do that?

JOHN

Of course. Because this is you we’re 
talking about. If Maxine hadn’t 
texted me, I wouldn’t’ve even known 
you were having a birthday party.

NADIA

You know who gets mad about not 
being invited to birthdays? Little 
girls. Grow up.

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

27.

She dashes into the park. John calls after her but she’s 
gone.

23

EXT. TOMPKINS SQUARE PARK - NIGHT - (LOOP B)

23

JUMP CUTS: Nadia looks for Oatmeal. She mutters to herself. 
It’s deadly cold. She grows more and more concerned for him. 
Finally, she finds him (snuggling with a girl cat).

NADIA

You fucking slut.

She snuggles him. He’s placid and unfeeling. Like a cat.

24

EXT. EAST RIVER - NIGHT - (LOOP B)

24

3:30/4AM. Between the Manhattan and Williamsburg bridges. The 
sky is purple with a creeping dawn. Nadia sits on the railing 
above the East River. Oatmeal in her lap. The sight of the 
sun coming up fills her with some relief.

NADIA

We made it through the night. We did 
it. I did it. I knew I just had to 
find you--

She looks down. Oatmeal has disappeared into thin air. Nadia 
POV: Whip pans back and forth at the walkway/park.

NADIA (CONT’D)

Oatmeal?!

She stands up too quickly, twists her foot on the railing, 
loses her balance, falls, and plunges into...

25

EXT. UNDERWATER - CONTINUOUS - (LOOP B)

25

... the cold water of the East River. 

This sequence has a surreal feel to it. It’s dark and we 
can’t quite make out what’s happening. Nadia struggles to 
fight her way through the garbage-filled East River up to the 
surface. Pumps her legs. Her gold necklace, tight around her 
neck, choking her as she tries to free herself or get to the 
surface. Whichever comes first but a plastic bag floats into 
her face. We hear...

SOUNDTRACK: “Gotta Get Up” by Harry Nilsson

CUT TO:

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

28.

26

INT. MAXINE’S LOFT - BATHROOM - NIGHT - (LOOP C)

26

We are back to where we were at the beginning of the episode.

Nadia washes her hands in Maxine’s bathroom. CLOSE ON: 
Nadia’s face. Scared. Trying to piece together what’s going 
on. A loud KNOCK at the door. The same door. 

NADIA
(to herself)

You saw that cat disappear, right?

She nods at her own reflection. With a rush of anxiety, she 
flings the door open. GUESTS file in as she exits.

27

INT. MAXINE’S LOFT - KITCHEN - NIGHT - (LOOP C)

27

Moments later. Maxine prepares her chicken. Everything is the 
same as it was. Maxine behaves exactly as she has in past 
versions of this scene. 

Nadia enters. Less disoriented; more determined.

MAXINE

Sweet Birthday Baby! Havin’ fun?

(offering a joint)

It’s laced--

NADIA

The universe is fucking with me, and 
I refuse to engage.

(to the Universe)

Do you hear me?! I WON’T DO IT! I 
don’t give a fuck if you disappeared 
my cat! 

MAXINE

Nadia... you’re acting a little--

Nadia takes the joint, examines and does not smoke it.

NADIA

What’s in this joint?

MAXINE

It’s laced with cocaine like--

NADIA

No. I’ve tried cocaine many times. 
It didn’t fuck me up the way this 
did. 

RUSSIAN DOLL #101

Pink Draft (02/20/2018)

29.

MAXINE

But you haven’t smoked it yet...

NADIA

If there is a chance, any chance at 
all, that there is something in this 
joint that isn’t cocaine... I really 
need to know.

Maxine sees this is very serious for Nadia.

MAXINE

It was pre-rolled by my guy. He said 
it was laced with cocaine.

NADIA

Who’s your guy? I need to talk to 
him.

MAXINE

Now I have to share my guy with you?

NADIA

If you’re gonna roofie me, then fuck 
yeah, you have to share your guy!

MAXINE

Come on, Nads. Stop acting like such 
a victim. You’re a cockroach.

NADIA

(shocked)

I am NOT a cockroach. Don’t call me 
a cockroach. What does that even 
mean? A cockroach?

MAXINE

You can eat anything, take anything, 
do anything. It’s impossible to 
destroy you. You’ll never die.

NADIA

I’m dying constantly! OUCH!

The joint burns down to Nadia’s fingers and singes her. Nadia 
holds up the joint and stares at it. CLOSE ON: The burn on 
her fingers. Fresh. Real. 

MAXINE

Nads, if you’re gonna act like this 
then why don’t you just leave--

Nadia turns on her heel.

RUSSIAN DOLL #101

Yellow Rev. (2/26/2018)

30.

MAXINE (CONT’D)

(180 degrees)

Don’t bail on my party for you!

NADIA

If I’m a cockroach, then that makes 
you an exterminator!

She grabs her coat and is gone, taking the joint with her. 
Maxine is upset/confused.

28

INT. MAXINE'S LOFT - STAIRWELL - NIGHT - (LOOP C)

28

Mike is on the stairs, on a call.

MIKE

(into phone)

I understand. I know. Look, I’m at a 
work thing. I’ll try to come over 
after this. Okay. I love you too--

Nadia exits Maxine’s loft and runs right into him. He saves 
her from falling down the stairs. 

MIKE (CONT’D)

Whoa. You almost took us both down.

NADIA

Thank you. 

MIKE

(not gracious)

Be careful!

NADIA

Fuck you!

Off Mike’s confused look...

29

EXT. MAXINE'S LOFT - NIGHT - (LOOP C)

29

Nadia spills out onto the street. She’s breathing hard. New 
York City. Shrouded in danger. Everything is a way to die. 
Nadia’s usual devil-may-care attitude severely impaired by 
the circumstances.

NADIA

Be. Careful.

She heads off into the, to her, very dangerous night.

END OF EPISODE.

