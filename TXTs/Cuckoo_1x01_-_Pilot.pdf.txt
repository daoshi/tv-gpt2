CUCKOO

Pilot (US)

Written by

Robin French

Kieron Quirke

&

ACT ONE

EXT. THOMSON FAMILY HOME

A comfortable home in Shermer, an affluent suburb of Chicago - 
exactly the kind of liberal, NPR-listening, Prius-driving 
neighborhood that Fox News thinks is destroying America.

INT. THOMPSON FAMILY HOME, HALLWAY

PHOTOS of RACHEL on a mantelpiece. Rachel is the daughter of 
the Thompson family - a bespectacled, uptight 22-year old, 
who has just graduated college. 

Photos show her playing the trumpet at the school concert, 
posing with a bunch of nerdy boys - the computer club, 
dressed up with her friend as Holmes & Watson for 
Halloween...

KEN (mid-40s) shouts up the stairs as his wife LORI (same) 
bustles about, putting on her coat. 

KEN

Dylan! We’re super-late to pick up 
Rachel!

DYLAN (14, sarcastic, unkempt) appears, eating a sandwich.

DYLAN

Shoot, sorry. Can’t come. I’m 
eating this sandwich. Hey, but have 
fun with your golden child! 

KEN

Come on, buddy, not this again. If 
you got into Northwestern Law 
School, we’d be proud of you too. 
(A LOOK FROM LORI) I mean, if you 
achieved anything equivalent. 
Actually, I mean we’re proud of you 
anyway. You’re... (STRUGGLES) OK. 
(BEAT) Now get in the car. 

Dylan looks at him, huffs, then heads to the car. 

KEN (CONT’D)

(to LORI)

Was that bad parenting?

LORI

We can afford the therapy.

2.

EXT. INTERSTATE, NEAR CHICAGO.

Ken’s Prius drives by - an Obama sticker on the back window. 
They pass a sign reading “O’Hare airport”. 

I/E THE THOMPSON FAMILY CAR

Ken is on his cell. Lori drives while listening Salt ‘n’ 
Pepa’s “Push it” on the car stereo. Dylan plays a PSP in the 
back. Lori turns up the volume on the song. 

(very smug)

KEN

I’m not actually in the 
office, Bryan. I’m picking up 
my daughter... Yes, the law 
school one. Following in her 
father’s footsteps! Not that 
I care!.. She’s been in 
Thailand... Oh, you gotta go? 
OK, bye. 

LORI
(singing along)
Push it! Push it! 
(sings with the 
instrumental)

Rachel’s coming home. Push 
it! Push it! Rachel’s coming 
home. 

Ken hangs up, looks at Lori exasperated.  

KEN

Lori, that was a client! 

LORI

(singing to Ken)

Salt and Pepa's here, and we're in 
effect/ Want you to push it, babe
Coolin' by day then at night...

Suddenly Ken joins in with her. They both know the words.

KEN & LORI

Working up a sweat! C'mon girls, 
let's go show the guys...

In the back, Dylan looks up from his PSP, mimes an intense, 
tortured scream, then returns to his game. 

KEN & LORI (CONT’D)

Now push it! Push it real good! 

INT. O’HARE AIRPORT, ARRIVALS

Ken and Lori wait for Rachel, holding an elaborate sign 
reading, “Welcome Home Rachel!” Dylan stands nearby.  

LORI

I envy Rachel. A summer abroad. 
Adventures on the other side of the 
world.  

3.

KEN

(smiles wistfully)

The things she must have seen. The 
art. The cuisine. Oh, the 
architecture! 

DYLAN

She probably became a drug mule. 

KEN

Dylan!

DYLAN

It happens all the time in 
Thailand. Or they get sex 
trafficked. (OFF KEN’S LOOK) It’s 
what girls do, Dad, you can’t baby 
her forever! 

Dylan shrugs, puts in his iPhone headphones, wanders off. 

LORI

Ken! There she is!

Ken looks thrilled. Rachel comes out of Arrivals, grinning. 
She is TRANSFORMED - tanned, glowing, her hair in braids.

LORI (CONT’D)

Rachel! Hey!

KEN

That is her! Rachel! Over 
here!

Rachel runs up, hugs Lori. 

LORI (CONT’D)

Honey, wow! You look amazing!

KEN

(holding out his arms)

Permission to approach the bench? 

RACHEL

(a little embarrassed)

Granted, counsel!

Ken gives Rachel a big hug. His little girl is finally back! 
Then he catches sight of someone just over Rachel’s shoulder.

A YOUNG MAN, grinning. He looks totally positive, totally 
part of the group. This is CUCKOO, a thirty year-old traveler 
- attractive, intense, almost inspiring, but ultimately 
absurd. He wears a poncho, fisherman’s pants, sandals, a 
giant backpack. 

RACHEL (CONT’D)

Well...here he is! 

KEN

I’m sorry? 

4.

CUCKOO clasps Ken, looking deep into his eyes. 

CUCKOO

An honor, Ken. Just a great honor. 

KEN
(confused)

Hi! 

RACHEL

(equally confused)

Dad, this is Cuckoo. (BEAT) Didn’t 
you get my Facebook message? 

KEN

I don’t check Facebook, Rach - I’m 
47. Nice to meet you though - what 
was the name?

CUCKOO

Cuckoo. 

LORI

And how do you and Rachel know each 
other?

CUCKOO

(confident)

Oh, she’s my spouse. 

Beat.

KEN

I’m sorry?

Dad! I got married!

RACHEL

Ken and Lori freeze. Cuckoo walks up to Ken, embraces him.

CUCKOO

Thank you. She’s beautiful.

END OF ACT ONE

5.

ACT TWO

I/E THE THOMPSON FAMILY CAR

Lori and Ken in the front, still in shock. Cuckoo and Rachel 
all over each other. Dylan plays on his PSP, never looks up.  

RACHEL

So Cuckoo and I were living in this 
tiny hut... 

CUCKOO

I’m tight with one of the local 
fishermen - Ruang Sak...  

RACHEL

...and every morning I’d get up 
with the sun and run along the 
beach...

CUCKOO

I was usually asleep! (WINKS) 
Exhausted from the night before!

Rachel and Cuckoo laugh delighted. 

RACHEL

Oh my god, I have so many stories 
to tell!  

KEN

Great! How about the story where 
you went and got married?

CUCKOO

Rach, I got this. OK Ken, we were 
partying - hard - in Pom Pah. And 
we got talking about getting 
married and we were like ‘WHY 
NOT?’. 

RACHEL

Why not?! 

CUCKOO

Why the hell not?! I can’t think of 
a reason. 

Beat. 

KEN

And how long had you guys been 
together? 

6.

CUCKOO

Our souls or our bodies? Well, our 
souls are eternal, born from the 
confluence of the stars at the dawn 
of time. We’d been doing it about 
two weeks.

LORI

And Cuckoo - what do you do? Do 
you... have a job?

CUCKOO

I love your concern. Yes, Lori, 
fear not, I do have a job. I am a 
wise man. 

KEN

“Wise man”? Is that a job?

CUCKOO

It was Buddha’s job. Jesus’s job. 
Cat Steven’s job. (BEAT) Obviously, 
I’m not putting myself in the same 
category as Cat Stevens.

An awkward pause.

CUCKOO (CONT’D)

Fine, have it your way, I’ll sing 
some Cat Stevens.

(sings)

PEACE TRAIN’S A HOLY ROLLER ROLLER/ 
CLIMB ON THE PEACE TRAIN...

He sings confidently. Rachel looks at him, adoringly. Ken and 
Lori look bemused.

Dylan finally looks up from his PSP.

DYLAN

(to Ken)

I’m sorry, who is this guy?

KEN

Um, he’s your sister’s new husband.

DYLAN

(uninterested)

Cool.  

He goes back to his game. 

I/E DRIVEWAY OF THOMPSON HOME

The car pulls up outside the house. Everybody gets out. Dylan 
immediately walks in.

Ken looks at Cuckoo, who is standing in the middle of the 
drive with his eyes closed, humming to himself. 

7.

KEN

What’s he doing? 

RACHEL

Reading the energy.

Cuckoo opens his eyes - grins at the family.

CUCKOO

Oh yeah! It’s good! Repeat: the 
vibrations are all good, guys!  
Come on, Rach! I’ll carry you 
across the threshold! 

Rachel jumps into his arms. Cuckoo carries her towards the 
porch, knocking over a garden statue as they go. The happy 
couple laugh delightedly and continue in. 

Ken looks at Lori - this was not the day he expected... 

INT. THOMPSON HOME, BATHROOM

Cuckoo is in the shower, joyfully singing an Indian 
devotional lyric.

CUCKOO

(singing)

Govinda jaia jaia! Gopal jaia 
jaia...

INT. THOMPSON HOME, KITCHEN

Ken and Lori are now in the kitchen. 

LORI

She’s in love, Ken! You see the way 
he looks at her? And Rachel is 
glowing! Her acne’s gone!

KEN

Oh great! Well, as long as her 
skin’s good, no need to worry about 
the down-sides. Like her new 
husband who showed up in a poncho.

LORI

Ken, we promised ourselves we 
wouldn’t be the kind of parents who 
can’t handle their kid’s choices. 
Like our fathers.  

KEN

You mean your father. 

8.

LORI

Exactly. And if I’d listened to 
Dad, there would be no us. He 
didn’t like you at first. 

KEN

Or ever since. 

LORI

C’mon, you. She’s still your hot-
shot lawyer daughter - only now 
with a fun, funky twist.

She does a quick, playful “shoulder-roll” breakdancing move.  
Ken glares at her, unmoved. She cuddles up to him. 

LORI (CONT’D)

At least get to KNOW him...

INT. THOMPSON HOME, RACHEL’S BEDROOM

Rachel packs the contents of her shelves into a cardboard box 
- folders, schoolbooks, certificates. She replaces them with 
new books about Buddhism, ethnic statues. 

Cuckoo is unpacking many reams of paper from his rucksack. 

RACHEL

Dad didn’t look so happy. 

CUCKOO

Ken?! Are you kidding? We clicked 
instantly. And by the way - his 
aura - wow! That is one special 
guy. A LOT to work with there.

Rachel looks at a book: ‘Expert Debating’.

RACHEL

So weird being home. Just three 
months since I left and I don’t 
even remember this person. (TO 
CUCKOO) You’ve transformed me, you 
know that?

CUCKOO

You transformed yourself. I was 
just the thing that speeds up a 
change without actually being part 
of the change itself. 

RACHEL

The catalyst. 

CUCKOO

(gazing at her)

You always find the right word. 

9.

They share a beautiful moment. Suddenly... 

DYLAN (O.S.)

You haven’t changed. 

Dylan has appeared at the door.

DYLAN (CONT’D)

Underneath, you’re still the same 
nerd who cried when she got Canada 
at Model UN.   

KEN (O.S.)

Dylan, your mom wants you to set 
the table!

DYLAN
(all smiles)

Coming, Dad!

He gives Rachel a death stare. She looks momentarily rattled.

DYLAN (CONT’D)

The tan will fade. But the geek 
remains forever. 

He slinks away. Ken arrives at the door. 

KEN

Hey guys! Settling in OK?

Ken notices a Thai statue with an enormous phallus. 

CUCKOO

Like it Ken? It’s a powerful 
fertility statue. (PLACES IT) Put 
that next to the bed.  

Ken looks around, notices Cuckoo’s stack of papers.

KEN

Lotta paper there, Cuckoo. 

CUCKOO

Oh this? This is my book. 

KEN

(disbelief, then hope)

Your book? You’re an author. That’s 
impressive.   

CUCKOO

It’s about my political philosophy. 
I’d like it to be the most 
important book of the twenty first 
century. 

10.

KEN

Oh. Well, good to aim high. 

CUCKOO

Well, most of it was written high, 
so...

He laughs at Ken, super-friendly. 

CUCKOO (CONT’D)

Right now, these are just things I 
jotted down on my journeys. 
Fragments.  

He holds up one piece of paper. It says “Words are walls”. 

KEN

Words are walls? Does that... mean 
something?

CUCKOO

It might, Ken. And when I work it 
out: a new beginning. 

Ken looks stunned. The doorbell rings.  

KEN

Lovely. That will be Uncle Steve 
and Aunt Connie... 

INT. THOMPSON HOME, HALLWAY

Ken comes down the stairs, meets Lori in the hallway. 

KEN
(agonized)

Just so you know - Steve is gonna 
love this. This is a nightmare.

EXT. THOMPSON HOME, PORCH

STEVE, CONNIE and TOBY wait. Steve is a black fortysomething 
businessman. Connie is Lori’s 37 year old little sister. She 
is wheeling Toby, Lori and Connie’s elderly father. 

STEVE

This is a nightmare. Big boasting 
Ken droning on about Rachel and 
Thailand. Steve Junior went 
snorkeling in Florida last year. 
Swam with turtles! We didn’t make a 
big deal about it. 

INT. THOMPSON HOME, HALLWAY

Continuous. Lori opens the door. Connie, Steve and Toby 
enter. Big smiles and hugs all around (except from Toby).

11.

CONNIE

(to Lori)

Sorry we’re late, sweetie. Dad made 
us pick up a bucket of chicken. 

LORI

Dad, I wish you’d let us feed you.  

TOBY
(very rude)

Never!

CONNIE

So. Where’s Rachel? 

Ken and Lori look at each other. 

KEN

Rachel? Well - something you should 
probably know. We have another 
guest for dinner. 

STEVE

We weren’t informed.  

Cuckoo appears, his arms open in greeting. Inexplicably he’s 
wearing a Native American headpiece. Rachel follows. 

CUCKOO

Good evening everybody! (KISSES 
CONNIE) You must be Connie. 

CONNIE

(already putty in his 
hands)

Hi. 

Cuckoo gives Steve a warm hug. 

CUCKOO

Uncle Steve. (BEAT) Mmmm. You smell 
great!

STEVE

(to Ken, over Cuckoo’s 
shoulder)

Who’s the weirdo? 

Ken takes a deep breath.

KEN

OK, well. Cuckoo is Rachel’s new 
husband. 

12.

RACHEL

Surprise!

CUCKOO

Word up my people! 

Steve looks at Ken, his eyes full of delight. 

STEVE

Rachel - you got married?! To this 
guy?! This is fabulous news! Hey 
Ken - congratulations, my friend.  

KEN
(tightly)

Thanks Steve. Shall we go eat? 

They move towards the dining room. Connie glares at Steve, 
who is grinning too much. 

STEVE

What? I just love family. 

INT. THOMPSON HOME, DINING ROOM

EVERYBODY is tucking into dinner. Cuckoo loves his food. 

CUCKOO

You know the fishermen of Phuket 
have a saying: ‘When a woman passes 
forty, her food is her love-
making’. Very tasty, Lori. 

LORI

Thank you Cuckoo! 

STEVE

So Cuckoo. I take it you are in 
some way American.

RACHEL

Cuckoo’s been traveling the world  
for twelve years. But he was 
brought up in Portland.

CUCKOO

Yeah, had to get out of that town. 
(CHUCKLES) Way, way too corporate. 

TOBY

(insanely furious)

Where is my chicken?! WHERE IS MY 
GODDAM CHICKEN?!

LORI

(calmly)

Dad, you already had your bucket. 

13.

TOBY
(grumbling)

They fed us better than this in 
Vietnam.

CUCKOO

You’ve been to Vietnam!  Did you 
party on the beaches of Phu Quoc? I 
know right! Mindblowing!

TOBY

My best friend Phil went down on 
Ong Lan Biên. 

CUCKOO

OK, Grandpa, not at the table!

He makes a face to the company. Old people!

DYLAN
(to Cuckoo)

So, what’s with the stupid name?

KEN

Dylan! Dylan wants to know if 
Cuckoo is your real name. 

CUCKOO

(baffled)

Sure! (BEAT) Oh, you mean my birth-
name! My birth-name is Dale 
Ashbrick.

STEVE

(amused)

Dale Ashbrick? 

CUCKOO

Yes, my friend. You see, some years 
back...

RACHEL

Babe, I don’t think they’re ready 
for this story yet. 

CUCKOO

...I was boating the Moctezuma 
river with a couple of Huichol 
Indian dudes, and... 

RACHEL

Cuckoo! 

CUCKOO

Come on, your family are people of 
the world... And I’d just taken a 
triple dose of the hallucinogen 
peyote. 

14.

RACHEL
(to herself)

Oh great. 

CUCKOO

You with me Ken? (KEN NODS) 
Awesome! So I’m in the river, I’m 
buzzing like a handsaw - and that’s 
when I realize - I am not a person. 
I’m all people. 

LORI

I’m not sure I understand. 

CUCKOO

I am all people. I am Dale 
Ashbrick. But I am also you, Lori.

Beat.

He points at Lori, his hand quite close to her face.

CUCKOO (CONT’D)
(pointing accordingly)

And I am you, Dylan. And I am Steve 
and Connie. And I am you, Rachel, 
my wife. 

(pointing at Ken’s face)

And I am you, Ken.

KEN

Well, obviously not literally...

CUCKOO
(passionate)

I’m not sitting here bullshitting 
you, Ken. I am you. And I am in 
you. I am right deep inside you. 

KEN bites his tongue. Steve starts chuckling, loving this. 

CONNIE

So... if you’re all people. Why did 
you bother changing your name? 

CUCKOO

What? Oh. Well, I guess I was just 
high. 

INT. THOMPSON HOME, KITCHEN

A few minutes later. Ken brings in the dirty dishes, takes a 
moment to himself. 

Steve enters, gloating. Not the guy Ken wanted to speak to. 

15.

STEVE

I feel for you buddy. Rachel tied 
to some insane hippy who wouldn’t 
know a Corby press if it singed him 
in the face! I mean - if Steve 
Junior did this to me....but he 
never would. He idolizes his 
father.

KEN

Thanks for your concern Steve. Say, 
what is a Corby press?

Ken does not wait for the answer, but walks back to the 
living room. Steve looks irked, and turns to Connie, who has 
come in and overheard.

STEVE

He knows what it is! He’s 
belittling me!

CONNIE

Aw, honey! How could he belittle a 
man who owns four dry cleaners? He 
probably forgot - what with this 
terrible blow to his family. (BEAT) 
Can’t say I blame Rachel though - 
that guy is sex in a pair of 
sandals. 

A slightly awkward beat. Steve looks worried. 

STEVE

You know what a Corby press is, 
don’t you?

CONNIE

(doesn’t know)

Let’s go join the others!

She runs in to join the others.

STEVE

(super-pissed)

It’s an industrial press for pants!

INT. THOMPSON HOME, LIVING ROOM

Thai music on the stereo. The family stand in a circle around 
Rachel. Cuckoo plays a simple beat on an ethnic looking drum. 
Rachel dances, whirling about with two halves of a coconut, 
clapping them together in rhythm. She’s really good!

RACHEL

(grinning)

It’s called the Serng Krapo or 
Coconut Dance! 

16.

KEN

(to Lori, whisper)

Since when does Rachel dance?

LORI

Since him. 

Ken watches Rachel dancing, and begins to smile. Maybe this 
isn’t so bad. He starts to clap along. 

CUCKOO

(to Lori)

Hey mom! You take over! 

LORI

What? 

CUCKOO

Just do what I do!

Cuckoo passes Lori the drum. She takes it and drums along 
quite well. Everybody whoops and cheers. 

Cuckoo now dances with Rachel - they grind in a way that 
could just about be culturally significant, but also looks a 
bit rude. Connie watches fascinated. 

Cuckoo suddenly sings out - a weird high pitched Thai piece 
of singing. 

CUCKOO (CONT’D)

Come on everybody! Sing along with 
me!

Everybody finds themselves singing along with Cuckoo’s 
strange Thai shriek. Ken is getting into it - maybe he’s 
coming around...

INT. THOMPSON HOME, LIVING ROOM

The family stand around. Lori has filled up glasses for a 
toast. There’s a cake. Ken stands to speak. All smiles. 

KEN

OK, I guess I should say a few 
words...

RACHEL

Ooh! Actually Dad, Cuckoo was gonna 
make a toast...

Cuckoo walks forward, tapping his glass loudly with his fork. 

CUCKOO

Speech! Speech! Speech by me! Do 
you mind, Dad? 

17.

KEN

(he does mind)

Sure. Go ahead. 

Everyone stands up and holds their glasses for a toast.

CUCKOO

Hello new family! Well, I never! 
What an attractive family huh? All 
of you - so good looking. 

This pretty much wins the crowd. Everyone is a little 
flattered. Steve nods, super-vain. 

CUCKOO (CONT’D)

So I gave this speech on our 
wedding day. But now I would like 
to give it again, with clothes on, 
and my new family present.

He removes a piece of paper from his pocket. 

CUCKOO (CONT’D)

Rachel, you are my light. I don’t 
wanna be cheesy here - but you 
opened up my heart and made it sing 
like a big beautiful bird. And 
also, you complete me. 

Everybody ‘Aws’. Ken smiles - that was nice. 

CUCKOO (CONT’D)

Ken, when I look at Rachel, your 
only daughter, I feel myself 
swelling. With pride. Rachel is 
loving. She’s enthusiastic. 
She’s... adventurous: always 
seeking new things - new ways of 
doing things. This girl’s the kind 
of girl who says yes to everything. 

Everyone is struggling with this speech, particularly Ken. 

CUCKOO (CONT’D)

She’s generous - so generous! - she 
just gives and gives. And gives. 
She’s firm. She can be fiery - oh 
God - sometimes you can not hold 
her down. And I don’t need to tell 
you that a tongue-lashing from her 
is quite an experience! Rachel 
loves to be on top. But when it 
gets hard, she gets her head down, 
and she WILL NOT STOP till the job 
is done. Truly impressive. Rachel 
is open. Welcoming. Warm inside. 
And finally - and most importantly - 
grateful. To Grateful Rachel!

18.

ALL

(awkwardly)

To Grateful Rachel!

Ken swallows his discomfort, then toasts in return. 

KEN

OK, well, I too want to talk about 
Rachel. Don’t think I’ll be 
repeating anything Cuckoo has said. 
(BEAT) OK, why lie? This marriage 
was a surprise - a bombshell you 
might say...

DYLAN 

Get to the end.

Everybody ignores Dylan. Lori gives him an encouraging look. 

KEN

But, the thing is - the only thing 
that truly matters to me, to Lori, 
is that our little girl is happy. 
Rachel - wherever you go in life, I 
will be there with you. Whatever 
choices you make, I will support 
you. Always.

EVERYBODY aaahs and claps - what a lovely speech. Lori looks 
approving. RACHEL smiles up at KEN - overjoyed to hear this. 

RACHEL

Dad! I’m not going to Northwestern! 
I’m not going to be a lawyer! 

Silence. The whole family is agog. Ken drops his glass. It 
lands right in the middle of the cake. It spatters his pants. 

Opposite, Steve starts chuckling. Cuckoo chuckles along, puts 
his arm round Steve. 

CUCKOO
I know! So great!

Ken stares at Rachel. His life is falling apart. Toby looks 
up at Ken, seemingly sympathetic. 

TOBY

You deserve this. 

He winks at him. 

END OF ACT TWO

19.

ACT THREE

INT. THOMPSON HOME, KEN & LORI’S BEDROOM

Lori and Ken are in bed. 

LORI

This is Rachel’s decision. Honey, 
this is what she wants. 

KEN

You support her! Even in this you 
support her!

LORI

She’s happy! Ken, being a lawyer is 
not the only job. In fact, I was 
kind of dreading having two of you 
in the house. 

KEN

What does that mean?

LORI

Oh, you know. It’s all ‘what do you 
mean by that?’ and ‘I have two 
points’ and ‘define your terms.’

KEN

(still worked up)

OK. Two points. One - you can be 
chilled out, and fun loving and be 
a lawyer. For example - me. 

Your face is turning red.  

LORI

KEN

It’s turning red with fun! Two - 
Rachel’s not doing what she wants, 
she’s doing what he wants. I’m 
going to talk to her.  

LORI

(laughs)

Good luck with that.  

KEN

(sighs)

You’re right. She won’t listen to 
me. (BEAT) I’ll talk to him! She 
obviously believes anything he 
says. 

Lori turns off the light. She can’t be bothered with this. 

20.

LORI

You do that. Now... Sleepy time!

...and they cuddle up to go to sleep. From the next room...

CUCKOO (O.S.)

Yep. Yep. Yep. Yep. 

It can only be one thing. Out on Ken’s pained face...

INT. THOMPSON HOME, KEN AND LORI’S BEDROOM

Later. Lori is reading. Ken sits, in deep psychological pain. 
Cuckoo’s ‘Yeps’ continue from next door. 

CUCKOO (O.S.)

Yep. Yep. Yep. Yep.

KEN

Am I allowed to go and stop them? 
Is that allowed? 

LORI

Come on, Ken - she’s married. 
Remember when you and I were like 
this? 

Ken smiles. Trip down memory lane. 

KEN

Oh yes, our first vacation. That 
little hotel in Martha’s Vineyard. 
Guess I was kind of a powerhouse...

LORI

(cheerfully)

Yes, you were! Of course, it never 
lasted anything like this long!

KEN

(peeved)

Good night.

He switches the light off. 

INT. THOMPSON HOME, CUCKOO & RACHEL’S BEDROOM

Ken enters, with some coffee. 

KEN

Cuckoo! Wake up, Cuckoo. I made 
coffee. I thought we could...

Cuckoo is asleep in the bed, naked - his dignity barely 
preserved by the duvet. 

21.

KEN (CONT’D)

Cuckoo! I figured we could take the 
car, drive up and look at the lake. 
Hang out. (BEAT) Wakey, wakey!

No response. Ken creeps forward, leans over to touch 
Cuckoo...

KEN (CONT’D)

Wake up, Cuckoo...

He touches Cuckoo. Suddenly, Cuckoo sits up and stares at 
Ken. 

CUCKOO

Get out! Kuma Dadaio! Kuma Kakaio! 
Kuma Mamako!  Get. Out. Get! Out!. 
Kuma Mamako! Kuma Kakaio!

Cuckoo slaps Ken round the face. Ken looks astonished.

EXT. CHICAGO LAKEFRONT

Ken and Cuckoo have just bought Sloppy Joe’s from a cafe. 
They walk along the lakefront, eating them. 

CUCKOO

Sorry about earlier. I’m always 
like that when I’m woken. You know - 
grouchy. 

KEN

Water under the bridge. 

CUCKOO

I’m not an early morning sort of 
person. I think - because I have so 
many profound thoughts, the brain 
needs time to rebuild.

KEN

I wanted to talk to you about this 
whole ‘Rachel not being a lawyer’ 
thing. 

Cuckoo takes a mouthful of his food. 

CUCKOO

She doesn’t want to do it. Says 
it’s not her... Oh man, this is 
frickin’ delicious! What is this? 

KEN

It’s a Sloppy Joe.

22.

CUCKOO

Slop-py Joe. So who’s Joe? And why 
did the world deem him sloppy? 

KEN

I don’t know. Anyway, 
Northwestern...

CUCKOO
What’s in this? 

KEN

Oh. It’s just chili on a... burger 
bun. 

CUCKOO

When we get home - I want you to 
write down that recipe for me.

KEN

Do you not remember these from your 
childhood?

CUCKOO

Thing about me, Ken, my mind is 
fast and agile, like a supercool 
leopard. But my memory? Not so hot. 
Which is good, because I like to be 
in the present, which is so much 
harder if you’re constantly 
remembering things. 

KEN

Let’s get back on track here. If 
Rachel’s not going to be a lawyer, 
what will you do for money?

CUCKOO

OK. Rach and I have got a very 
simple philosophy on this. You 
don’t need money, when you’ve got 
love. 

KEN

Great. But, you do need some money - 
or, you can’t eat. 

CUCKOO

I ate today. I didn’t have any 
money today. 

He takes a delicious munch of his Sloppy Joe. 

KEN

Yes, because I bought you food.  

23.

CUCKOO

May good people like you buy me 
food to eat every day of my life - 
Inshallah. 

KEN

Right. So you’re staying in 
Chicago. With us. I mean is Rachel 
going to get a job? 

CUCKOO

Uh-uh. Rachel’s on a spiritual 
journey right now. She needs room 
for her soul to grow. 

KEN

So - are you going to get a job?

CUCKOO

I have a job.

KEN

Other than “being a wise man”. 

Cuckoo looks Ken in the eye. 

CUCKOO

Ken, I’m a great believer in 
society.

KEN

So am I. 

CUCKOO

Awesome. So in the most basic human 
societies, you would have hunters. 
People like you. Guys who were good 
at the basic stuff, you know. 
‘There food. There money. Go get. 
Ugh. Ugh.’ Which is tremendous! But 
then there would be the thinkers 
too. Now they weren’t good at 
hunting - like I’m not good at 
jobs. But they would eat the food 
provided by the hunters and in 
return think up something really 
cool for the future and shit. 
That’s me. (POINTS TO HIMSELF) 
Thinker. (POINTS TO KEN) Hunter. 
(RE-POINTS TO KEN) Hunter. (POINTS 
TO HIMSELF) Thinker.

He smiles at Ken, and nods. A big moment for them.

KEN

OK, two points...

INT. THOMPSON HOME, KITCHEN

Ken enters. Lori is lying on her front on the kitchen table. 
Rachel stands above her, waving her hands over Lori’s body. 
Both are  rapt in concentration. Dylan sits nearby, texting. 

24.

RACHEL

(seeing Ken, grins)

It’s Reiki. I picked up a few 
techniques in Thailand. 

KEN

Lori, you do realize Reiki is not 
medical science. It’s irrational 
superstition. It’s totally 
unscientific. 

LORI

Wrong, Ken. It’s all about energy. 
And my knee feels a lot better!

 KEN

What is happening to this family?! 
What’s next? Sacrificing a virgin? 

RACHEL

Ooo, watch out Dylan!

She and Lori laugh. 

DYLAN

(fury and embarrassment)

I’m not! I’m... I’m not!

INT. THOMPSON HOME, HALLWAY

Later. Ken stares forlornly at the photos of Rachel on the 
wall we saw earlier, including Ken with Rachel at her 
graduation - happier days.  

DYLAN (O.S.)

No way! 

Dylan and Lori enter. Dylan looks furious. 

DYLAN (CONT’D)

Dad! You have to reason with her. 

LORI

I told Dylan he had to trade rooms 
with Rachel and Cuckoo. You know 
give them their privacy - for their 
“naps”.

She raises her eyebrows to Ken. 

25.

KEN

Dylan, your mom’s right. Go and 
tell them you’re trading rooms. 
(BEAT) Maybe knock first.

DYLAN

This Cuckoo guy is ruining 
everything.  Why can’t you just pay 
him off? Send him back to Thailand. 

LORI

Dylan! 

DYLAN

You’d be doing him a favor. Why 
does he want to be here anyway - 
living with you and married to 
Rachel who, I don’t want to be rude 
but... is a massive dog. (BEAT) 
Give him some money, and he’d be 
out of here. I would be. 

Ken looks at Dylan with interest.

LORI

I’m gonna pretend you never said 
that. Cuckoo’s family now - he’s 
part of our lives. This is the man 
who will father our grandchildren, 
be there for us in old age, carry 
your father’s coffin. Tell him, 
Ken... Ken? 

Lori’s speech has only made Ken look more thoughtful...

KEN

What?... Oh yes, Dylan. That’s a  
ludicrous idea. 

INT. THOMPSON HOME, HALLWAY

The next day. Ken comes in the front door, looking furtive. 

KEN

Lori! Rach!

No answer. Ken walks up the stairs purposefully. 

INT. THOMPSON HOME, RACHEL AND CUCKOO’S ROOM.

Ken enters. Cuckoo is in deep concentration, listening to 
whale music. 

CUCKOO

Hey Ken. It’s whale music. Wait, 
wait...

26.

He smiles, satisfied. 

CUCKOO (CONT’D)

I love that part. 

He turns the music off. 

KEN

OK. (BEAT) I’ve been thinking about 
our conversation yesterday. 

CUCKOO

We argued - and in that white heat 
of emotion we discovered a lot 
about ourselves. There’s this 
energy between us, Ken. Did we know 
each other in a previous life?

KEN

I don’t think so... 

CUCKOO

Perhaps we were brothers. You were 
small, vulnerable, frightened of 
everything. I was strong. I saved 
your life. Many times. 

KEN

Cuckoo, I’ve been thinking. About 
what we said yesterday. You and me, 
we come from different worlds. 

CUCKOO

Pretty different, yeah!

KEN

Yeah! And I respect that. 
Totally... man. (BEAT) So - in my 
world - very different from yours - 
Rachel was going to Northwestern. 
She had good prospects 

CUCKOO

Ken, I love your daughter.

KEN

Sure. But I’ve been thinking about 
that very perceptive thing you 
said. You’re a thinker. So true. 
And my world - y’know - it’s not 
really a place for people who 
think. All day. And don’t do 
anything else. 

CUCKOO

You’re saying I don’t fit here. 

27.

KEN

Wow! I hadn’t made that leap. You 
put it so well. 

CUCKOO

You know me! Thinking! 

KEN

So how are we going to resolve 
this?

CUCKOO

I confess I don’t know. I mean, 
Ken, honestly, this is a 
fascinating conversation. 

Ken pushes an envelope across the table. 

KEN

Do you miss Thailand?

CUCKOO

A little...

Cuckoo opens the envelope. It is full of money. He opens his 
mouth in surprise.  

CUCKOO (CONT’D)

Woah. Ken, what is this?

KEN

I think if you took that money and 
found a different path for 
yourself, everyone would be a lot 
happier. 

CUCKOO

This is so... Does Rachel know 
about this?

KEN

Rachel’s a special girl. She needs 
a guy who can support her. Make 
this sacrifice, Cuckoo. The money’s 
just to help you along. 

Cuckoo thinks long and hard. 

KEN (CONT’D)

Just take the money. And walk out 
the door.

Cuckoo has tears in his eyes. He takes the money. 

CUCKOO

Thank you, Ken. (BEAT) I’m glad you 
said it. Hari Vishnu.

28.

He shakes Ken’s hand, walks out. Ken breathes a sigh of 
relief. 

EXT. BUS STOP, SHERMER STREETS

Cuckoo is standing at a bus stop on the streets of Shermer, 
deep in thought, almost tearful. This is epochal for him. 

KEN (V.O.)

Rachel, there’s no easy way for me 
to say this. Cuckoo told me this 
morning, that he was considering... 

RACHEL (V.O.)

What? 

INT. THOMPSON HOME, KITCHEN

Ken is talking to Rachel. Very intimate.

KEN

Leaving you. Going back to 
Thailand. Without you. 

RACHEL

No.

KEN

Yes. He said he could never feel at 
home here. He told me to tell you 
that he loved you, but that being 
with him wasn’t your...path. He 
wanted you to go to Northwestern, 
work hard, set yourself up in a 
good career, marry someone nice - a 
doctor, or another lawyer - someone 
with a steady income, local, maybe 
someone who cheers for the Cubs.  

RACHEL

Cuckoo said that?  

KEN

I may have embellished. I was 
emotional at the time. I was just 
so sad to see him go. 

Rachel breaks down in sobs. 

INT. THOMPSON HOME, LIVING ROOM

Rachel is crying. Lori and Ken comfort her. Dylan plays games 
on the TV, oblivious to the others.  

29.

LORI

It doesn’t make sense. He seemed 
like he adored her. 

KEN

I know. You think you know someone! 

LORI

Is that a bonfire in the garden?

KEN

He left some of his stuff. Couldn’t 
leave it lying around. Too painful 
for Rachel. 

(to Rachel)

You’ll be all right honey...

RACHEL

I won’t...

KEN

You will. 

RACHEL

I won’t. 

DYLAN

She might not be. 

LORI

Dylan!

DYLAN

I’m just saying. I mean, she’s been 
married now. Kind of used goods. In 
some societies, we’d probably have 
to stone her. 

Rachel cries more. Ken and Lori stare daggers at Dylan. Dylan 
shrugs.

KEN

Rachel, it might not seem like it 
now, but you’re gonna get through 
this. I promise. Because however 
bad it gets, I will do anything, 
anything to make you happy. In the 
long term.  

Rachel looks at her father, tears in her eyes. He has got 
through to her. A lovely moment. 

INT. THOMPSON HOME, LIVING ROOM

Time for bed. Ken looks out over his front drive. His kingdom 
is safe again. 

The security light comes on. Ken looks anxious. But it’s the 
cat. Ken smiles. But as the cat walks away. A figure appears 
in the security light. 

It is Cuckoo. He is pushing a dilapidated food truck. 

30.

EXT. THOMPSON HOME, DRIVEWAY

Ken runs out into the drive. 

KEN

Cuckoo, what the hell? I thought we 
had a deal. 

CUCKOO

We did, Ken. And here it is! You 
likey?

Rachel runs out the house, with Lori. 

RACHEL

Cuckoo! Oh my god! Where have you 
been? What’s this? 

CUCKOO

It’s a food truck. I’m gonna sell 
Sloppy Joes. Who knew? You can sell 
them from a truck! It’s ideal! 

LORI

You’re going to sell Sloppy Joes? 

CUCKOO

Yes, that is if you’ll honor me 
with the recipe, Ken. 

KEN

(exasperated)

It’s chili in a burger bun.

RACHEL

Wait. You’re going to work? But you 
said...

CUCKOO

I know what I said, Rach. But just 
because I’m a thinker, does that 
mean I can’t be a hunter too? I 
mean, if anything, a thinker could 
be an even better hunter than a 
hunter, because of all his cool 
thoughts. For after all, what is a 
thinker but a hunter for the 
thoughts which a hunter can think. 
Right, Ken?

Ken looks utterly confused. 

31.

CUCKOO (CONT’D)

I lost him. Anyway, pretty sweet, 
huh? Only cost ten thousand 
dollars. 

KEN

Ten thousand dollars?! You 
paid...ten thousand dollars? 

CUCKOO

Yes, Ken. Do you want to look 
inside? 

He opens the door - the door falls off, onto the driveway. 

CUCKOO (CONT’D)

Door’s a little cranky. The guy 
said it was probably worth less - 
but I gave him the lot because he 
seemed like a nice guy.  

He starts putting the door back on. 

RACHEL

I don’t understand. Cuckoo - where 
did you get this money?

CUCKOO
Dad gave it me. 

RACHEL

Dad?

LORI

(to Ken)

You gave Cuckoo ten thousand 
dollars to set up a business?

Everybody looks at Ken. There’s nothing he can do. 

KEN
Yep. Surprise!

LORI

Ten thousand dollars? 

KEN

(pushing on)

You know, because I was on him 
about getting a job. 

RACHEL

But why did you tell me he’d gone 
to Thailand? 

KEN

Did I say that?

32.

LORI

And why burn all his stuff?

CUCKOO

He burnt my stuff? Why did you do 
that, Ken?

Long, awful pause. Lori, Rachel and Cuckoo stare at Ken. 

KEN

TO MAKE IT AN EVEN BIGGER 
SURPRISE!... Come on you three - 
indoors! I’m sure I’ve got a bottle 
of champagne somewhere! 

Lori and Rachel start to go in. 

LORI

(affectionate)

You funny, funny man. 

Rachel and Lori go in, delighted. Cuckoo stops beside Ken. He 
hugs him, warmly. 

The door of the van falls off again. 

CUCKOO

You and me, Ken. You and me.

END

*

