THINGS YOU SHOULDN'T SAY PAST MIDNIGHT

 EPISODE 1

Written by

Peter Ackerman

2nd Revision
March 27, 2014

                              MIDNIGHT - One - 3.27.14 - 1.

In darkness, a first TITLE CARD is followed by a second:

TITLE CARD:  Things You Shouldn’t Past Midnight

TITLE CARD:  #1 - something horribly offensive 

We start to hear bickering as we FADE UP ON:

INT. MARK’S STUDIO APARTMENT/THERAPY OFFICE - MIDNIGHT

Ten characters (EVERYONE EXCEPT FOR DEAN BLACKMAN, PEDRO 1, 
AND GENE) are crowded into Mark’s small, but tastefully 
designed apartment.  

SUPERTITLE:  12:01 am

Each person argues with another until a subtly commanding 
chime repeats and silences them.  They turn to MARK, 30-ish, 
the thoughtful, cute, gay therapist in charge of the chime.

MARK

Hi.  Oh, that worked so well.  I’ll 
have to thank Mrs. Prufrock.  She 
gave it to me from her Malaysian 
adventure.  

(they’re looking at him)

Anyhoo.  Thank you for coming.  As 
you all know by now, I’m Mark, the 
Midnight Therapist.  I believe 
these late hours can release an 
honesty and spontaneity that you 
don’t find during the day.  And 
though I generally do private 
sessions at just 60 dollars an hour-

MRS. ABRAMSON

50 minutes.  

MARK

Thank you, Mrs. Abramson.

MRS. ABRAMSON 

(to Mr. Abramson)

See that?  He won’t even call me 
Estelle anymore because of you!

MARK

Okay, okay, tempers are high, I 
know.  That’s why we’re here.  Last 
night was traumatic for us all.  
Some of us barely even know each 
other but fate brought us together.  

(MORE)

                              MIDNIGHT - One - 3.27.14 - 2.

MARK (CONT'D)

I’m hoping that coming here tonight 
can give us an opportunity to 
understand what happened and how we 
can all deal with it.

GRACE

(thumbs up; mouths)

You’re doing great!

MARK

Thanks, Grace.  So who’s gonna go 
first?  

(no response)

Come on.  Don’t be shy.  That’s 
what Midnight Therapy is all about.  
Let the subconscious just float on 
out there. Like a raft. A dingy. 
A...

LEO

Lily pad?

MARK

A lily pad.

NANCY

I’ll go.  

MARK

Great.  Thanks, Nancy.  Why don’t 
you tell us how it started for you.

NANCY

Well some of you know, but Benjy 
and I--

BEN

Ben-- is fine.  

NANCY

Had a great evening: dinner and a 
movie, then up to the roof of his 
office at UCLA to see the stars.  
But when we went into his office--

BEN

That’s where things got a little 
... dicey.

EXT. DOOR OF BEN’S OFFICE - NIGHT

The plaque on the door reads “Professor Benjamin Perlmutter, 
Assistant Professor of English.”

                              MIDNIGHT - One - 3.27.14 - 3.

NANCY (O.S.)

Come on, slip it in!

BEN (O.S.)

I’m trying!

SUPERTITLE:  The Night Before, 12:01 am

NANCY (O.S.)

What’s the problem?

BEN (O.S.)

It’s dark.

NANCY (O.S.)

BEN (O.S.)

Let me.

I got it.

NANCY (O.S.)

I want it so bad.

BEN (O.S.)

So do I.

NANCY (O.S.)

Then put it in!

BEN (O.S.)

I am trying!

We hear a key scraping.

NANCY (O.S.)

What are you using?  A medieval 
dungeon key?

Then a key unlocks the door, which is pushed open by BEN, the 
sweet, if nerdy young Jewish professor.

NANCY, his blonde, blue-eyed, younger girlfriend, pushes him 
inside where he clicks on the desk-lamp.

NANCY (CONT’D)

I have fantasized about this since 
back when I was your student.

BEN

You have?

                              MIDNIGHT - One - 3.27.14 - 4.

NANCY

(sexy)

Yeahhh, Professor Perlmutter.  All 
over your desk.

BEN

Oh.  Wow.  Why did I ever let you 
graduate?

NANCY

‘Cause I wouldn’t be here if you 
didn’t.

BEN

Oh right.

NANCY

Now teach me some poetry.

As she closes the door on us...

BEN

But we gotta keep it down.

NANCY

Why?  The building’s empty.

BEN

I know, but still.

The door closes.  Down the dimly lit hall past closed office 
and bathroom doors, we hear a bump from the stairwell.

LEO (O.S.)

PHIL (O.S.)

Ow!

Shh!

BACK TO GROUP THERAPY AT MARK’S

Mark addresses PHIL, Asian-American, and LEO, both 19 year 
old UCLA sophomores.

MARK

So you guys were already in the 
building?

LEO

I guess.  Just past midnight?

MARK

What were you doing?

                              MIDNIGHT - One - 3.27.14 - 5.

INT. STAIRWELL, UCLA - SIMULTANEOUS

Phil and Leo wearing hoodies and face-bandannas for disguise, 
struggle to lug a large fire extinguisher on a dolly up the 
stairs.

LEO

Ow!  You almost broke my foot!

PHIL

Would you keep it down.

LEO

You said there’s no one here.

PHIL

There’s not, but just keep it down.

LEO

Well don’t drop it on my foot.

PHIL

You’re the one who’s not listening 
to the count.

LEO

When you say three, do you mean on 
or after?

PHIL
On.  1-2-three.

LEO

Okay, let’s try again.

1-2-three!

OWW!!!  

PHIL

LEO

PHIL

You didn’t lift.

LEO

I was thinking about something 
else.

PHIL

How about you count.

LEO

Okay, ready?

                              MIDNIGHT - One - 3.27.14 - 6.

Yes.

1-2...

PHIL

LEO

Beat.  Phil lifts.  Leo doesn’t.

LEO (CONT’D)

OWW!!!  That was two!

PHIL

You paused!

LEO

I thought I was gonna sneeze.

PHIL

Let’s count together and not so 
loud.

LEO

You said there’s no one here.

PHIL

There’s not, but just in case.

BACK TO MARK’S IN THE PRESENT

DAWN 

I was there.

MARK

Okay.  So, Dawn, where were you?

DAWN

In the basement.

MARK

And you were doing what exactly?

INT. BATHROOM, SAME BUILDING - SIMULTANEOUS

DAWN, the significantly-sized Security Guard, uses her 
nightstick to act out her own version of Travis Bickle in the 
mirror of a cramped basement bathroom.

DAWN

You wanna piece of me?  Yeah?  I’ll 
bet you do, big boy.  How about 
this?  

(fierce hand karate chop)

And this!  

(MORE)

                              MIDNIGHT - One - 3.27.14 - 7.

DAWN (CONT'D)

(high karate kick)

You like that, don’t you, ‘cause of 
the--

(re: her open groin area)

How about this.

(grabs through to her ass)

I bet you want a piece of that, yes 
you do.  That’s a good piece.  You 
can put your drainpipe in that 
piece.  I can drain that drainpipe.  
Ooh, yeah.

(undoing belt and zipper)

Won’t even be a drainpipe anymore.  
It’ll just be a pipe and then I’ll 
pipe it!  How about these, huh?

(grabs breasts)

Thing 1 and Thing 2?  Lefty and 
Righty?  Lefty and Lucy?  Lucy and 
Desi?  Let’s give ‘em some air, 
shall we?

(unbuttons shirt)

Which one you like better?  Tick?  
Tock.  Wanna run your little mouse 
up this clock?  I bet you do.  How 
about if I go like ...

(shakes boobs with hands)

Oohbiddy, oohbiddy, ooh--

CRASH!  Dawn freezes, shirt unbuttoned, belt and zipper open, 
holding her breasts in her bra, but alert, on the job.  
Something has fallen upstairs.

INT. BEN’S OFFICE - SIMULTANEOUS

Nancy, in a bra, no shirt, still wearing her skirt, has 
knocked over Ben’s desk lamp, the only light in the room.

Sorry.

It’s okay.

NANCY

BEN

Ben’s shirt is half-unbuttoned, his belt undone.

NANCY
It’s not broken.

BEN

Just turn it off.

                              MIDNIGHT - One - 3.27.14 - 8.

NANCY

No, I like it.  It’s like an 
interrogation.

BEN

Oh.  Okay.

NANCY

Now where were we?

BEN

I think you were cleaning my 
esophagus with your tongue.

NANCY

Oh right.

They start kissing, groping, getting hot.

NANCY (CONT’D)

Now turn me around.

BEN

Huh?

NANCY
Turn me around.

BEN

Well can’t you just--

Turn me around, Benjy!

NANCY

BEN

Okay!

Ben turns her around so she’s facing the desk.  He’s behind 
her, squeezing, kissing.

BEN (CONT’D)

Oh, you were right.  Turned around 
is good.

NANCY

Now lift my skirt.

I know.

Lift it!

BEN

NANCY

He flips up her skirt.  

                              MIDNIGHT - One - 3.27.14 - 9.

NANCY (CONT’D)

Oohhh ...

BEN

Told you I knew.

NANCY

Now --

BEN

I know.  I have Wikipedia.

He rips down her panties.

NANCY
Oohhh, Benjamin.

BEN

That’s my name, don’t wear it out.

NANCY

Come on, come on, come on.  

BEN

I’m coming.

NANCY

(stops)

Wait.  You --

BEN

No, no.  I just-- 

Oh.

He plunges in.

NANCY

BEN AND NANCY

PHIL AND LEO (V.O.)

Ahhhhhh...

Unghh..

INT. STAIRWELL, UCLA - SIMULTANEOUS

Phil and Leo lug the fire extinguisher up the steps in synch, 
grunting rhythmically with each step, INTERCUT with Ben and 
Nancy:

BEN AND NANCY

Ohhhhhh...

                              MIDNIGHT - One - 3.27.14 - 10.

PHIL AND LEO

BEN AND NANCY

PHIL AND LEO

BEN AND NANCY

PHIL AND LEO

BEN AND NANCY

Unghh...

Aaaaahh...

Ohhhhhh...

Unghh...

Aaaaahh...

Ohhhhhh...

INT. BASEMENT BATHROOM, UCLA - SIMULTANEOUS

Dawn briskly finishes buttoning up in front of the mirror.

DAWN

Ohhkay, Dawn Hightower.  No one’s 
messin’ with you, girl.  Not 
tonight.  You screw me up, I screw 
you worse.  Like a butterfly to a 
board.

(her buttons are wrong)

Wait.  You go there.  You there.

(she’s set now)

Now.  

(her Dirty Harry line)

Time for Dawn to come a’risin’.

Her eyes glitter psychotically.  She turns sharply to march 
out, but - CRASH - trips over a mop pail and goes flying.

BACK TO MARK’S APARTMENT IN THE PRESENT

Dawn explains to everyone:

DAWN

That’s my Dirty Harry line.

MARK

Great.  So just to complete the 
picture, at that same time, Grace 
was having her session here with 
me.  Remember what we were talking 
about, Grace?

                              MIDNIGHT - One - 3.27.14 - 11.

GRACE

Ugh.

INT. MARK’S STUDIO APARTMENT/THERAPY OFFICE - LAST NIGHT

GRACE lounges back on the elegant divan.

GRACE

I can’t live like this, Mark.  It’s 
been ten years: making excuses, 
working around it, saying it’ll 
change.  It won’t.  It doesn’t.  
It’s the same thing every day.  I 
wake up and deal with it.  Go to 
bed and deal with it.  It’s like a 
weight I can’t get off my chest.  A 
stone.  I can’t breathe.  Can’t 
inhale.  I can’t even smoke.  
Something has to be done.

MARK (O.S.)

Have you tried Design Within Reach?

REVEAL:  MARK listening to his favorite patient. 

GRACE

The room’s a square, Mark.  Their 
nicest table is a rectangle.

MARK

How about Pottery Barn?

GRACE

Uch.  Are you kidding?  That’s not 
sexy.  I need sexy.

MARK

Then why’d you get Crate and Barrel 
to begin with?

GRACE

Because it was free!  Okay?  Are 
you trying to humiliate me?  Is 
this how you’re gonna deal with 
your real clients?

MARK

We don’t call them clients.

                              MIDNIGHT - One - 3.27.14 - 12.

GRACE

I inherited it from my cousin who 
moved to Mexico when her boyfriend 
got caught embezzling money from 
her Dad’s pager business.

MARK

(jotting it down)

Pagers, interesting.

GRACE

That’s how old it is.  I have a 
dining room table from the pager 
era.

MARK

Well I don’t get what’s holding you 
back from buying a new one.

GRACE

That’s why I come to you!

MARK

Unfortunately your hour is up.

GRACE

Uch, you must be kidding me!

MARK

You shouldn’t wait until the last 
minute to bring up important 
issues.

GRACE

I thought that was the whole point 
of Midnight Therapy.  Spontaneity.

MARK

Maybe a little earlier in the 
session though.

GRACE

Can’t I just lie here?  It’s so 
cozy. 

MARK

I have another patient.

GRACE

You do?  

MARK

Don’t act shocked.

                              MIDNIGHT - One - 3.27.14 - 13.

GRACE

You never have another patient.  

MARK

Tonight I do.

GRACE

Wait, another patient?  Or a 
gentleman caller.

MARK

Why do you feel the need to ask me 
that?

GRACE

Don’t get shrinky on me.

MARK

Well I am a professional.

GRACE

Yeah and I’m a chiropodist.

MARK

A what?

GRACE

Chiropodist.  

MARK

What’s that?

GRACE

A foot doctor.

MARK

You mean a podiatrist.

GRACE

No, a chiropodist.

MARK

Wanna bet?

GRACE

Ten thousand.

MARK
How about ten?

GRACE

You’re on!

They both whip out their i-phones and search.  Mark finds ...

                              MIDNIGHT - One - 3.27.14 - 14.

MARK

Podiatrist.

GRACE

Wait, wait, wait for it ... 

(reads)

“The first society of chiropodists 
now known as podiatrists--”

MARK

Good.  We both win.

GRACE

Technically I do, because mine was 
first.  It’s older.  Like my table.  
But not my men!

She cackles, while gathering her stuff.

MARK

What’s that supposed to mean?

GRACE

What?

MARK
“Not your men.”

GRACE

It’s just a joke.

Well, Freud says--

MARK

GRACE

(stands with outerwear)
Uh, uh, uh.  My hour is up.

MARK

We still have a few minutes.

GRACE

To talk about my men but not yours?

MARK

You’re always making these little 
jokes when you leave.

GRACE

‘Cause I’m adorable.

MARK

Why do you think that is?

                              MIDNIGHT - One - 3.27.14 - 15.

GRACE

It’s just natural charm I guess.

Grace.

Mark.

MARK

GRACE

He looks at her.

GRACE (CONT’D)

I don’t know, ‘cause I don’t want 
to talk about it?

MARK

Why?

GRACE

Why does anyone like young men?
They’re young!  They’re men!

MARK

Then what’s the problem?

GRACE

I didn’t say there was one.

MARK

But.

GRACE

Oh I don’t know, I guess when they 
say Maroon 5 and I think it’s a 
paint color it’s a little weird.

MARK

Now we’re getting somewhere.

GRACE

Too late.  My hour’s up.

MARK

What’s the problem with not having 
the same reference points?

GRACE

I don’t know.  I guess sometimes it 
would be nice to be with a grown-
up, not have to make all the 
decisions myself:  choose the 
restaurants, foot the bill...  

                              MIDNIGHT - One - 3.27.14 - 16.

MARK

It gets old.

GRACE

It makes me feel like the grown-up, 
which is exactly what I don’t want 
to feel like.  And then... 

MARK

What?

GRACE

I don’t know, whatever happened to 
the John Waynes, you know?  My Dad 
was like that.  He was in the war, 
but he never talked about it.  He 
just did it.  Now you get all these 
idiots talking so much ‘cause they 
don’t actually do anything.  I used 
to sit for hours with my Dad saying 
nothing at all, but I knew I was 
with someone who’d done more than 
update his Facebook page.

MARK

There are still men like that.

GRACE

Yeah?  With more body hair than I 
have? 

I didn’t know you were so hairy.

MARK

GRACE

Like an ape.

Mark chuckles, but seems like he’s thinking something.

GRACE (CONT’D)

What?  

MARK

Well.  I’ve sort of been waiting 
for you to get to this point.

GRACE

Why?  What are you talking about?  

(off his coy smile)

Wait.  Don’t tell me you’re ... 
setting me up with someone!

RING - Mark’s phone.

                              MIDNIGHT - One - 3.27.14 - 17.

MARK

Excuse me.

GRACE

Are you kidding me?

MARK

(holds up finger)

Hour’s up.  I’ll talk to you 
tomorrow.

GRACE

Mark!

MARK

(answering phone)

Mark Dibonnio, Midnight therapist.

JUMPCUT TO MARK AND EVERYONE TONIGHT

MRS. ABRAMSON

Was that me?  Was that me on the 
phone?

MARK

Yes, Mrs. Abramson.

MRS. ABRAMSON

I knew it!  I knew that was me!

MARK

Okay, but let’s get these guys 
looped in.

He gestures to the unlikely pair of Mr. Abramson and Pedro 2.

EXT. ABRAMSON’S CARPETING WAREHOUSE - ESTABLISHING SHOT

Supertitle:  Last night, same time, elsewhere in LA, say... 
12:18 am ... ish

MR. ABRAMSON (V.O.)

Sorry to make you work this late, 
boys.

INT. CORNER OF ABRAMSON’S CARPETING WAREHOUSE - SIMULTANEOUS

We see the ends of rolled-up carpets under a banner on the 
wall that reads “Abramsons!  You got floor?  We got carpet!”

MR. ABRAMSON, 75, enters with two Mexican laborers - PEDRO 1 
and PEDRO 2 - brothers (possibly twins). 

                              MIDNIGHT - One - 3.27.14 - 18.

MR. ABRAMSON

The shipment got held up at 
customs.  There was nothing I could 
do. But you’ll get double-time.  Or 
time and a half.  Or maybe a 
quarter.  Depends on how you do.  
Could be an eighth if it’s not so 
good.  And I’m not talkin’ time and 
an eighth.  I’m talkin’ an eighth.  
What are your names?

PEDRO 1

Pedro.

MR. ABRAMSON

(to the other)

And you?

PEDRO 2

Pedro.

He looks at them.

MR. ABRAMSON

You’re both named Pedro?

(they nod)

You friends?  Amigos?

PEDRO 1

Hermanos.

Hermanos?  You mean brothers?  

MR. ABRAMSON

And you’re both named Pedro?

(they nod)

(they nod)

Jesus, what a country.  You got 
more hermanos?  Más hermanos?

PEDRO 1

Seis.

MR. ABRAMSON

Seis hermanos?!  Don’t tell me 
they’re all named Pedro.  

PEDRO 1

Sí.

MR. ABRAMSON 

Oh Jesus Christ, are you kidding 
me?  All named Pedro?!

(they nod)

Why would your mother do that?  

(MORE)

                              MIDNIGHT - One - 3.27.14 - 19.

MR. ABRAMSON  (CONT'D)

Saves on nametags.  You got 
sisters?  Hermanas?

PEDRO 1

Una.

MR. ABRAMSON

Don’t tell me her name is Pedro.

PEDRO 1

Tammy.

Mr. Abramson nods, thinks.

MR. ABRAMSON 

Eight Pedros and a Tammy.  You’re 
like a circus act.  You in the 
circus?  El Circolo?  Tightrope?  
Human cannon ball?
(blank looks)

You’ll get back to me.  Here’s what 
you gotta do. 

A cellphone starts quacking like a duck.  Mr. Abramson looks 
around, confused.

MR. ABRAMSON (CONT’D)

What?  What is that?  

Un pato.

What?

PEDRO 2

MR. ABRAMSON

PEDRO 1

Su phone.

MR. ABRAMSON

Huh?

PEDRO 1

Teléfono.  Usted.  Su cell.  

Huh?  Oh.  Oh.

(takes out i-phone)

MR. ABRAMSON

It’s my wife.  She keeps changing 
the sound.  The other day it was a 
machine gun.  I thought they were 
shooting me.  I dove under my desk.  
It took 45 minutes to get up.

He’s trying to finger-swipe his i-phone.  It keeps quacking.

                              MIDNIGHT - One - 3.27.14 - 20.

MR. ABRAMSON (CONT’D)
What is this?  What’s happening?

Pedro 1 helps him.

MR. ABRAMSON (CONT’D)

Oh.  Thank you.

(into phone)

Hello?

We hear a woman screaming at him.

MR. ABRAMSON (CONT’D)

Too loud.  

(more screaming)

Just talk.  I’m right here.  The 
phone comes all the way to my ear.

(beat)

Yes I’m at the warehouse!  Of 
course I’m at the warehouse.  Where 
the hell else would I be?

(she’s screaming)

I’m gonna take my hearing aid out 
if you don’t stop screaming.  

(she speaks more calmly)

No, that’ll disconnect us.

(beat)

Oh for Chrisssake, hold on.

(to Pedros)

Can you make this so I can see her?  
She says there’s a button.  Do you 
know what I’m talking about?  
Comprendo?  Button?  Face to face? 

Pedro 1 takes the i-phone, pushes a button, and hands it back 
to Mr. Abramson.  Mr. Abramson looks at his i-phone and sees 
MRS. ABRAMSON.

MR. ABRAMSON (CONT’D)

Oh.  Look at that.  

MRS. ABRAMSON (IN I-PHONE)

Show me the warehouse.

MR. ABRAMSON

What?

MRS. ABRAMSON 

The warehouse.  I wanna see it.

MR. ABRAMSON

Why?

                              MIDNIGHT - One - 3.27.14 - 21.

MRS. ABRAMSON

Just show me the warehouse so I 
know you’re there, Donald!

MR. ABRAMSON

Oh for chrissake.

He holds up the phone and waves it so she can see everything.

MR. ABRAMSON (CONT’D)

There?  Are you happy?  

MRS. ABRAMSON

Who are they?

MR. ABRAMSON

Pedro and Pedro.  

MRS. ABRAMSON
They have the same name?

MR. ABRAMSON

They’re brothers.

MRS. ABRAMSON

What?

MR. ABRAMSON

Like the Flying Wallendas.

MRS. ABRAMSON
Wallenda is a last name.

MR. ABRAMSON

What can I tell you?  This is what 
you needed to see my face for?  So 
I can tell you about the Flying 
Pedros?  They got six other Pedros 
and a Tammy.

MRS. ABRAMSON

You missed your appointment.

MR. ABRAMSON

What?

MRS. ABRAMSON

With the Midnight Therapist.

MR. ABRAMSON

Oh for...

MRS. ABRAMSON

You promised.

                              MIDNIGHT - One - 3.27.14 - 22.

MR. ABRAMSON
We had a late shipment.

MRS. ABRAMSON

At midnight?

MR. ABRAMSON

You think I’m here for my jollies?

MRS. ABRAMSON

You’re certainly not here for mine.

MR. ABRAMSON

Don’t start that, Estelle.  
Something came up.

MRS. ABRAMSON

Something never comes up here.

Mr. Abramson, embarrassed, turns to the Pedros, who are 
staring at him.

MR. ABRAMSON

Would you excuse me for a moment?

He turns his back on them to talk to his wife, supposedly 
quietly, but not really.

MR. ABRAMSON (CONT’D)

What’s the matter with you?  You 
want to embarrass me in front of 
the Pedros?

MRS. ABRAMSON
I am a woman, Donald.  

MR. ABRAMSON

(mutters)

That’s debatable.

MRS. ABRAMSON

I heard that.  No matter how old 
you think I am--

MR. ABRAMSON
I know how old you are.

MRS. ABRAMSON

--I have needs that have not been 
met for far too long.  And I am 
unwilling to go to my grave in this 
fashion.  

(MORE)

                              MIDNIGHT - One - 3.27.14 - 23.

MRS. ABRAMSON (CONT'D)

Now either you step up and hit the 
ball that I have been waiting to 
catch or so help me god I will go 
on J-Date!

Mr. Abramson turns the phone over and gives it the finger.  
We hear Mrs. Abramson:

MRS. ABRAMSON (O.S.) (CONT’D) 

What.  What am I looking at?  Is 
that the floor?  What’s happening?

Mr. Abramson turns the phone right-side up and tries to be 
nice.

MR. ABRAMSON

Estelle ...

MRS. ABRAMSON

Don’t Estelle me.  He’s a smart 
young man who works late to 
accommodate schedules just like 
yours.  And he says the late hours 
facilitate the subconscious.  

MR. ABRAMSON

I’ll bet.

MRS. ABRAMSON

I just talked to him.  He’s 
finishing up with a patient --

MR. ABRAMSON  

Now?!

MRS. ABRAMSON

He can see you next.  Pico and 
Crescent Heights like I wrote in 
your book.

MR. ABRAMSON

It’s after midnight!

MRS. ABRAMSON

It’ll take fifteen minutes to get 
there.  And maybe when you get 
home, for the first time in a long 
while, I won’t need artificial 
sweetener to go to sleep.  You know 
what I mean by that, Donald? 
Artificial sweetener?  

Mr. Abramson looks at the Pedros who look at him.

                              MIDNIGHT - One - 3.27.14 - 24.

MRS. ABRAMSON (CONT’D)
Don’t look at the Pedros.  I see 
you looking at them.  This has 
nothing to do with the Pedros.

MR. ABRAMSON

Lucky Pedros.

MRS. ABRAMSON

We are not dead yet, Donald.  I am 
a living, breathing human being.  
But this anger you are trapping us 
in is killing us.  So go to the 
nice young man, like you promised 
and I’ll stop hukkin’ ya’.  Now I 
gotta go.  There’s a re-run of 
Oprah.  They’re talking about young 
people and how sexual they are.  

She’s out. Mr. Abramson turns to the Pedros, who look at him.

INT. BEN’S OFFICE - SIMULTANEOUS

Nancy leans against the desk while Ben takes her from behind.

NANCY

Do me, do me, do me.

BEN

Yeah, yeah, yeah.

NANCY

Do me, do me, do me.

BEN

Yeah, yeah.

Do me.

Yeah.

Do me.

Yeah.

NANCY

BEN

NANCY

BEN

NANCY

Do me, do me, do me, do me, do me.

                              MIDNIGHT - One - 3.27.14 - 25.

INT. TOP OF STAIRWELL - SIMULTANEOUS

Leo and Phil successfully lug their contraption to the top 
step.

LEO

Yeah!

INT. BOTTOM OF STAIRWELL - SIMULTANEOUS

Dawn hears the loud “Yeah!” and throws herself back against 
the wall, nightstick out, panting.

DAWN

Stay calm.  This is what you’re 
paid for.  You’ll take care of it.  
Everything’s fine.

(looks up stairwell)

You want to get it on, perp?  Let’s 
get it aowhnnn.

She leaps to race up the stairs, but SLIPS!

DAWN (CONT’D)

Ahh!

She flips over, the nightstick out in both hands!  She looks 
down, sees what she slipped on, picks it up: a receipt.  She 
holds it up like a clue in a suspense thriller.

Starbucks.

DAWN (CONT’D)

INT. BEN’S OFFICE - SIMULTANEOUS

Ben and Nancy are frozen in their humping.  Ben, in 
particular, is listening alertly.

BEN

You didn’t hear that?

NANCY

No.

BEN

It was like a “yeah!”

NANCY

I think you said yeah.

BEN

No, from outside.

                              MIDNIGHT - One - 3.27.14 - 26.

NANCY

The building?

BEN

No, the room.  In the building.

NANCY

I didn’t hear, Benjy, but if we 
stop now I’m gonna explode.

BEN

Oh, okay.

Ben starts moving again.

NANCY

Oh yeah, baby, do me.  Do me ...

EXT. DEAN BLACKMAN’S OFFICE - CONTINUOUS

Leo and Phil have wheeled their fire extinguisher to the big 
door marked “Dean Dean Blackman.”  They are unaware that far 
behind them, at the other end of the hall, light seeps out of 
the bottom of Ben’s door. Phil puts a key in the Dean’s door.

INT. DEAN BLACKMAN’S OFFICE - NIGHT

Phil and Leo enter, pulling the large fire extinguisher on 
the dolly into the room, lit only by outside light spilling 
through the large windows.  Leo flips on the lights.

PHIL

No!  Stop!  Turn it off!  

Phil jumps to turn off the light, then closes the door, then 
the curtains, before turning the lights back on.

PHIL (CONT’D)

We can’t have lights on in the 
Dean’s office after midnight.  Are 
you crazy?  What if someone sees 
from outside?

LEO

But why close the door?  You said 
we’re the only ones in the 
building.

PHIL

We are, but just in case.

                              MIDNIGHT - One - 3.27.14 - 27.

INT. BEN’S OFFICE - SIMULTANEOUS

Ben and Nancy are peaking.

NANCY

Yeah oh yeah oh yeah oh yeah oh--

BEN

Huh.  Huh.  Huh.  Huh.

NANCY

Do me, do me, do me--

BEN

Yes, yes.

NANCY

Do me, do me--

Yes.

Do me.

Doing.

Do me!

Doing.

BEN

NANCY

BEN

NANCY

BEN

NANCY

DO ME YOU HOOK-NOSED JEW!!!

Ben freezes.

BEN

What?

INT. MARK’S STUDIO APARTMENT/THERAPY OFFICE - PRESENT

Everyone looks at Nancy.

MARK

I think we should take a short 
break.

Music.  End-credits.

END OF EPISODE ONE

