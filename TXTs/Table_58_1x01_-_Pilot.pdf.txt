TABLE 58

Pilot

Written by

May Chan

04/11/2014

COLD OPEN

FADE IN:

INT. CAFETERIA - DAY

CLOSE ON kids’ hands shepherding molded plastic lunch trays 
down the lunch line rail.  One after another, until a tray is 
set down with a pair of over-sized, foam GREEN HULK HANDS.  

WIDEN to reveal they belong to 13-year-old LOGAN DAVIS (All-
American, athletic).  

TRACK Logan as he struggles to balance his tray with his Hulk 
hands.  Kids shoot him ODD LOOKS as he moves past the tables, 
each with their own NUMBER CENTERPIECE.  

TABLE 1: It’s the only table with a white tablecloth and 
flower vase.  CHEERLEADER/princesses in trendy party dresses 
talking, texting and OMG-ing.  JOCKS in jerseys.   

TABLE 2: Big looming BULLIES in stained flannel shirts 
cracking their knuckles. 

TABLE 3.14: NERDS in glasses and argyle sweater vests, each 
with an open laptop in front of them. 

TABLE 13: It’s empty.  The number is upside down and 
vandalized with a circle back-slash symbol.  Wait!  Eyes peek 
out from UNDER the table -- the OUTCASTS.  Logan quickly 
moves on. 

TABLE 44: SLACKERS napping.  They raise their heads to eye 
Logan, then return to a group sleep. 

Logan stops at TABLE 58 which has a kid from each clique: 

BEBE WHITMAN - The Princess.  Self-centered, condescending, 
and trendy.   

DULLBERT FINKELSTEEN - The bully.  Big, tall, and seemingly 
dumb as a stump.  Wreck-it Ralph incarnate in flannel. 

OLIVE DaCOSTA - The Nerd.  Type A, organized, go-getter.  Her 
argyle sweater matches her headband.  Laptop in front of her; 
also has an argyle cover.

JANE CHOY - The Outcast.  Sarcastic and MEAN!  Oversized 
military fatigues all say “stay away.”  Literally, they say 
“stay away.”  

FEENEY - The Slacker.  Laid-back and carefree.  His ironic 
faded tee says “Procrastinate Now.”   

TABLE 58 - "Pilot" - 4/11/2014                         2.

LOGAN
(struggling)

Hey guys, how ‘bout a hand? 

Olive, Bebe, Feeney, Jane, and Dullbert all raise their hands 
-- REVEAL: They’re all wearing HULK HANDS.   

BEBE

(icily to Logan)

All.  Your.  Fault.

MAIN TITLES

CUT TO:

TABLE 58 - "Pilot" - 4/11/2014                         3.

ACT ONE

INT. MILTON MIDDLE SCHOOL - HALLWAY - MORNING

QUICK SHOTS of the morning grind: KIDS spilling in the front 
door, chatting by their lockers, etc.

CHYRON: YESTERDAY MORNING

FIND Logan (sans Hulk hands) wearing SUNGLASSES, strutting 
John Travolta “Stayin’ Alive”-style.  He nods and waves to 
STUDENTS as he passes.  They stare at him confused.     

LOGAN

Hey bro!  What up, dawgs?  Good 
morrow, Medieval dudes!  

He passes THEATER KIDS dressed in Shakespearean garb and 
genuflects to them in that weird medieval way. 

Nearby, PIERCE BROWN (13, jock, wears a crown) narrows his 
eyes at Logan, sniffs air with disdain. 

PIERCE

(sotto)

New Kid.  Fresh out of the box. 

INT. FRONT OFFICE - RECEPTIONIST AREA - SAME TIME

PRINCIPAL FRIDAY (40s, failed improv comedian) stands at the 
receptionist counter with the PA mic in front of her.  VICE-
PRINCIPAL MONTY (30s, fastidious, Friday’s self-appointed yes-
man) is nearby. 

PRINCIPAL FRIDAY

Good morning, Milton Middle School!  
Principal Friday here with today’s 
rundown-fundown!  

INT. HALLWAY - SAME TIME

BEBE’S LOCKER: All sparkles and glitter, with a retractable 
closet extended out.  Standing in front of a vanity mirror, 
Bebe rummages through her make-up kit and pulls out a BLACK 
AND WHITE COW-PATTERNED HEADBAND.  She looks at it longingly 
then shoves it back into her kit.   

PRINCIPAL FRIDAY (O.S.)

Football game tomorrow.  Go 
Interrupting Cows!  Moo, four, six, 
eight, moo do we moo-ppreciate?  
Moo. 

TABLE 58 - "Pilot" - 4/11/2014                         4.

CAMERA WHOOSHES TO -- OLIVE’S LOCKER: Immaculate and lined 
with alphabetized books.  Olive pulls out books one by one 
and piles them into her library-style book cart.   

PRINCIPAL FRIDAY (O.S.) (CONT’D)

Our Mathletes will be heading into 
their first academic decathlon of 
the year.  Can I get a moo-moo? 

WHOOSH TO -- FEENEY’S LOCKER: A mess of shredded paper.  A 
possum wanders out.  

FEENEY

(to possum)

Morning, Lazarus.

Feeney shuts his locker with the papers still sticking out.  

PRINCIPAL FRIDAY (O.S.)

Last thing, Cowpokes.  We’re 
getting reports of poison oak on 
the quad.  Stay away or you’ll be 
herding yourselves to the nurse’s 
office for a healthy does of cow-
limine lotion.  

WHOOSH TO -- JANE’S LOCKER: She dials the combo and swings 
open the locker door.  We don’t see what’s inside, but it 
glows, a la the briefcase in “Pulp Fiction.”  The JANITOR 
passes by, sees whatever it is, SCREAMS. 

WHOOSH TO -- DULLBERT’S LOCKER: Empty except for a single 
bent spoon.  Dullbert is nearby, holding a KID by his ankles 
over a trash can. 

PRINCIPAL FRIDAY (O.S.) (CONT’D)

As always, I will sign off with 
some humor. 

(clears throat)

What did one marine biologist-
waiter say to the other marine 
biologist-waiter?

VICE-PRINCIPAL MONTY (O.S.)

(reading, stiff)

What, Principal Friday?

PRINCIPAL FRIDAY (O.S.)

We all serve a porpoise!  

WHOOSH TO -- LOGAN’S LOCKER: It’s empty -- a blank slate.  
Logan slams it shut.  He spots COACH PETERSON (40s, not in 
fighting shape) in front of his office door with a huge key 
ring trying to find the right key for the lock. 

TABLE 58 - "Pilot" - 4/11/2014                         5.

LOGAN
Coach Peterson?

Logan whips out his WALLET and flashes his old school ID like 
a cop flashing his badge.  CLOSE ON: The ID picture -- Logan 
in full football gear, pointing to himself with both thumbs.

LOGAN (CONT’D)

Who has four thumbs and is your new 
starting quarterback? 

PULL BACK to reveal: Logan posing like his picture.  

COACH PETERSON

(confused)

You have four thumbs?

LOGAN

I was counting the ones in the 
picture and adding them to the 
total...

(beat)

Logan Davis, new transfer.  I’d 
like to try out for the team. 

COACH PETERSON

(focused on keys)

And I’d like a magic straw which 
keeps me from gaining weight each 
time they roll out those minty 
green milk shakes. 

LOGAN

Taking that as a “yes”!

COACH PETERSON

Open try-outs are next month. 

Coach Peterson enters his office, shuts door in Logan’s face.

SFX: BELL RINGS

Vice-Principal Monty enters, blowing on a whistle. 

VICE-PRINCIPAL MONTY

You are all now assembling 
unlawfully!  Get to class or 
it’s...

(like a drill sergeant)

De-ten-tion!

Kids head off to class.  Dullbert pulls the kid out of the 
trash, stands him upright.  Turns out he was helping the kid. 

TABLE 58 - "Pilot" - 4/11/2014                         6.

KID

Thanks, Dullbert.  Can’t believe I 
dropped my favorite pocket 
protector in the trash. 

DULLBERT

If we can’t protect our pockets, 
we’re lost as a culture.  

INT. CAFETERIA - LATER

SFX: BELL RINGS

LUNCH LADY SNEED (30s, pleasant) doles out pizza squares to 
KIDS.  Logan, with his tray of food (still no Hulk hands), 
scans the cafeteria.  

His eyes stop on TABLE 1 -- the jocks and cheerleaders, 
including Pierce and PORTIA LEE (pretty Asian girl) seated in 
thrones, both wearing crowns. (NOTE: The cheerleaders are 
wearing cow-patterned headbands like the one Bebe had.)  

The room goes dark -- a SINGLE PIN LIGHT shines on the table.  
ANGELS SING “AAAHHHHH.”  It’s surreal, until...  

LUNCH LADY SNEED

Maintenance, light bulb’s out!  
Hey, Glee kids, keep it moving! 

The lights flicker back on.  The Glee Club is in line -- our 
“singing angels.”

LOGAN

(re: Table 1)

My people.

Logan STRUTS towards Table 1.  Suddenly, TREMORS shake the 
room.  Water glasses vibrate, a la “Jurassic Park.”  In SLOW-
MOTION, ZIGGY McCALLISTER (13, big, tall thug) stomps in with 
his posse of other big, tall thugs, THE MEANIES.  Kids part 
like the Red Sea.  

Ziggy’s POV, a la “The Terminator,” he scans the room, 
labeling each potential victim: “SCREAMER” “CRIER” “FAINTER.”  
Ziggy zooms in on “NEW KID.”  It’s Logan.  “TARGET ACQUIRED.”

Ziggy knocks Logan’s milk carton to the ground, then tosses a 
business card on his tray.  Logan reads the card. 

LOGAN (CONT’D)

“Collect ten for a free wedgie.”
What is this?

TABLE 58 - "Pilot" - 4/11/2014                         7.

ZIGGY

A punch card.

Ziggy promptly punches him in the stomach... then takes 
Logan’s card, uses a HOLE-PUNCH on it, and throws it back on 
his tray.  

ZIGGY (CONT’D)

Nine more to go, Turd-burger.

LOGAN

(in pain)

Guess there’s no use in asking if 
there’s a return policy. 

Ziggy and his gang clear.  Logan collects himself and turns 
to Table 1.  TWO JOCKS cross baseball bats like swords, 
creating a barricade.  Pierce points his scepter at Logan’s 
chest.

PIERCE
No Turd-burgers.

Logan looks around -- Now where will he sit? 

ANGLE ON TABLE 58

The room goes dark -- a SINGLE PIN LIGHT shines on the table. 
This time, we hear the “WHA-WHA” SAD TROMBONE SOUND EFFECT. 

LUNCH LADY SNEED (O.S.)

Move along, band kids.

The lights pop on.  A TROMBONE PLAYER (in marching band 
uniform) crosses.  Holding his tray, Logan approaches 58.  He 
takes a deep breath, collects himself, re-sets.  

LOGAN

Hey.  

BEBE

You’re photo-bombing my selfie. 

Logan sits down, moving out of Bebe’s picture. 

BEBE (CONT’D)

(offers pinky, Logan 
shakes it)

Bebe Whitman.  Saw you trying to 
weasel your way into Table 1.  It’s 
for jocks and cheerleaders only.  
Hashtag: creme de la creme. 

TABLE 58 - "Pilot" - 4/11/2014                         8.

ANGLE ON Table 1 -- the jocks are wearing helmets and ramming 
their heads into each other.

OLIVE

(nose buried in laptop)

Talk about a few microfilaments 
short of a eukaryotic cell...

Jane picks sundried tomatoes off her pizza and flings it up 
to the ceiling. 

JANE

(re: Table 1)

Bunch of snobs.  They call 
themselves the “Royals.” 

BEBE

Take their name out of your filthy 
mouth, Jane. 

(ignores her, to Logan)

The Royals are wonderful, popular 
young adults.  They’re always 
nominated for King and Queen on the 
royal court. 

FLASHBACK TO:

INT. GYM - NIGHT

CHYRON: LAST MONTH

Principal Friday and Royal Portia Lee stand on the stage next 
to two THRONES identical to the ones at Table 1. 

PRINCIPAL FRIDAY

Our Homecoming Queen is... once 
again Portia Lee! 

Principal Friday ceremoniously takes the tiara off Portia’s 
head and then puts it right back on. 

PRINCIPAL FRIDAY (CONT’D)

And the King is...

The Former King, wearing a crown, moves forward expecting the 
same treatment, but:

PRINCIPAL FRIDAY (CONT’D)
Shocker!  Pierce Brown, get on over 
here! 

Pierce shoves the Former King aside, approaches.  

TABLE 58 - "Pilot" - 4/11/2014                         9.

Principal Friday rips the crown off Former King’s head and 
places it on Pierce.  The Former King fades into the shadows, 
gone forever. 

DISSOLVE TO:

INT. CAFETERIA - PRESENT DAY 

Our gang is as we left them. 

FEENEY

We haven’t seen the Former King 
since.  Rumor is he ran away and 
lives in an abandoned food truck.  

OLIVE

Or his father transferred jobs and 
he resides in New Jersey.  

BEBE

Either way.  Ew. 

JANE
(to Logan)

Look around, nobody sits outside of 
their own group.    

FEENEY

True.  Look at Table 4, French 
Club.  They never mingle with Table 
4-eh -- French-Canadian Club. 

FIND Table 4: students are dressed in black turtle necks and 
berets.  Next to it is Table 4A, dressed in hockey sweaters.

LOGAN

So how’d you guys end up together?

DULLBERT

Destiny is not a matter of chance.  
It is a matter of choice.  Of which 
we have none.

BEBE

Table 58 is slightly better than 
eating alone in the bathroom.    

(thinks)

I take that back.  It’s the same. 

FEENEY

(gestures to table)

Barrel bottom.  Us. 

TABLE 58 - "Pilot" - 4/11/2014                         10.

LOGAN

Well, I belong at Table 1.  I was 
QB at my old school.  They called 
me “Logan the Laser.” 

FEENEY

(gets it)

Because you had corrective eye 
surgery. 

LOGAN

Because I could throw a pass with 
pinpoint precision. 

FEENEY

Yeah, sorry, don’t follow water 
polo. 

OLIVE

I believe Logan is speaking of that 
inane sporting event where 
synaptically-challenged alphas turn 
a dirty lawn into a concussion 
factory.

LOGAN

Yeah, football!  It’s awesome!

(gets into it)

It’s you and your teammates acting 
as one unit fighting against the 
opposing team, trying to move the 
ball, uh... 

(realizes, deflated 
mumbling)

...down a dirty lawn.  Let me try 
that again.

Too late.

SFX: BELL RINGS

Lunch is over.  A RED CARPET rolls out and the Royals exit 
first.  STUDENTS follow them, taking pictures with their cell 
phones and asking for autographs.  

Logan watches longingly.  He catches eyes with Portia.  She 
half-smiles at him behind Pierce’s back.  Logan is instantly 
smitten.  

Then Ziggy walks by, sneers at Logan.  Holds up HOLE-PUNCH 
and squeezes it like a hand grip exerciser.    

TABLE 58 - "Pilot" - 4/11/2014                         11.

FEENEY

(to Logan)

Ziggy McCallister.  Leader of The 
Meanies.  The only bully at Milton 
with a rewards program. 

The gang, except for Dullbert, all hold up their Ziggy punch-
cards.  

INT. FRONT OFFICE - RECEPTIONIST AREA - MOMENTS LATER

Principal Friday enters.  Lunch Lady Sneed opens a large 
package, pulling out GREEN FOAM HULK HANDS.  The box is 
filled with them.  

PRINCIPAL FRIDAY

Why did you order Hulk hands? 

Sneed looks equally confused but then:

LUNCH LADY SNEED

I said I wanted bulk hams!  

Sneed pushes the box toward Principal Friday but Friday 
shoves it back. 

PRINCIPAL FRIDAY

I will not have waste at my school, 
Lunch Lady Sneed.  Find a use.  
Everything serves a porpoise.    

LUNCH LADY SNEED

Yeah, about that.  What the heck’s 
a marine-biologist waiter? 

PRINCIPAL FRIDAY

It’s a-- just find a use!

Principal Friday exits. 

INT. HALLWAY - SAME TIME

Over at his locker, Logan sees Pierce and the jocks pass by.  
He jumps in front of them.  

LOGAN

Hey, Royal Brosephs -- Ro-sephs.  
Looking forward to sharing the 
gridiron together this year. 

(holds up fist)

Knuckles.  Pound it. 

TABLE 58 - "Pilot" - 4/11/2014                         12.

PIERCE

The only person you’ll be playing 
football with is my Nana because 
she’s slow and old and smells like 
yeast.  

(chokes up)

But she’s still a great nana, so--

(points scepter at Logan’s 
chest)

Stay away from Nana.

Pierce and the jocks brush past Logan.  A kid everybody calls 
SWEATPANTS (wears hoodie and sweatpants) accidentally bumps 
Pierce.  (NOTE: With the hoodie over his head, we NEVER see 
Sweatpants’ face.)    

PIERCE (CONT’D)

Watch it, Sweatpants.

The Shakespearean theater kids pass Logan who is still 
holding his fist up for a bump.  They dramatically admonish 
him.  

THEATER KID #1

I scorn you, scurvy companion. 

THEATER KID #2

Away, you moldy rogue, away!

Having witnessed all this, Bebe slams her locker shut and 
presses on her key chain; it BEEPS like a car alarm.

BEBE

O-M-G.  Even the theater geeks
snubbed you.  Embarrassed much?

LOGAN

Small hurdle but completely jump-
able.  Mark my words, I’ll be with 
the Royals by tomorrow.  

Logan sees Coach Peterson at his office door fumbling with 
his keys again. 

LOGAN (CONT’D)

Coach Peterson!  

(runs over)

Who’s got two thumbs and wants to 
try out for the team today?  I 
can’t wait a month.   

COACH PETERSON

And I can’t clip a toenail without 
taking out my wife’s eye.

TABLE 58 - "Pilot" - 4/11/2014                         13.

Thinking fast, Logan takes the pair of sunglasses out of his 
backpack and hands it to Coach. 

LOGAN

Have her wear these.  

Coach snatches the glasses from Logan, takes a moment to 
consider them:

COACH PETERSON

Stylish frames, safety rated 
lenses... I like your moxie, kid.  
Three-thirty, practice field.  

As before, Coach enters his office, shuts the door in Logan’s 
face.  But Logan’s on cloud 9, fist pumping as he clears. 

ANGLE ON Pierce.  He saw all that and doesn’t like it one 
bit.  He catches eyes with Ziggy, gives him a villainous nod.  
Ziggy nods back.  The two continue to nod back and forth 
until they both get creeped out and exit.  

INT. LIBRARY - LATER

Logan enters.  The only empty seats are at the front table 
where Olive has her books strewn everywhere.  Just then, TWO 
ARGYLE NERDS enter.  Olive jumps in front of them. 

OLIVE

Greetings, Table three-point-one-
four!  I have procured this area so 
that we Mathletes may study as a 
single hive mind. 

Without a word, the nerds brush past her.  Ouch. 

LOGAN

Uncool.  I thought all you, uh, 
“types” got along.

OLIVE

“Types”?

LOGAN

Uh, yeah, you know... the 
“cerebrally well-endowed”.

OLIVE

Nerd.  You can say it.  It 
describes an individual who is 
smart, gifted, and bound to achieve 
greatness -- oh yeah, we took the 
word back!

TABLE 58 - "Pilot" - 4/11/2014                         14.

Beat.  Bebe, Jane, Feeney, and Dullbert enter separately.

LOGAN

(relieved)

Hey, guys!  You have Study Hall 
this period too?  Cool, let’s sit 
together. 

FEENEY / BEBE / JANE / DULLBERT

Nah. / Ick. / Pass. / Nay.

Vice-Principal Monty enters, clapping. 

VICE-PRINCIPAL MONTY

Anyone not in a seat in the next 
four seconds gets de-ten-tion!  
One, four! 

Logan, Feeney, Jane, Bebe and Dullbert scramble for the 
closes seats.  All at Olive’s table.  She grudgingly moves 
her books over just a tad.  

INT. HALLWAY - SAME TIME

Again, Coach Peterson tries to find the right key for his 
office.  Lunch Lady Sneed approaches with a box.  

LUNCH LADY SNEED

I tried.  Braised them for an hour.  
Turned my pots green and they still 
taste like foam.  Friday says to 
find its porpoise. 

Lunch Lady Sneed dumps the box of Hulk hands next to Peterson 
and walks away.

COACH PETERSON

Why do I have so many keys?! 

INT. LIBRARY - LATER

SFX: BELL RINGS

Study hall is over.  Students get up and exit.  Logan nudges 
Feeney awake.  Our gang stands and immediately topples over.  

LOGAN

What happened?

OLIVE

Our foot protection!  They’ve been 
compromised!

TABLE 58 - "Pilot" - 4/11/2014                         15.

REVEAL: their shoelaces have been tied together.  There’s a 
card stuck to Logan’s shoe.  It’s Ziggy’s PUNCH CARD -- now 
with two punches.  

FEENEY
Eight more to go!

Each of our gang starts to walk in different directions, 
getting no where.   

LOGAN

Whoa, whoa, whoa!  We have to go in 
the same direction! 

OLIVE
(struggling)

It’s Polynomials Day in Algebra, 
and I refuse to be tardy.

BEBE

(pulling)

Locker: stat.  If I don’t curl my 
eyelashes now.  They will sag. 

FEENEY

(pushing)

I gotta feed Lazarus or he gets 
bitey.

LOGAN

Stop!

(points)

The librarian’s desk probably has 
scissors.  Follow my lead.  Left 
foot, right foot, left foot, right 
foot...

The gang moves toward the desk, following Logan’s commands.  
When they get there, Logan tugs on the drawers -- locked. 

OLIVE

Fail. 

LOGAN

(thinks, gets an idea)

I saw a janitor’s closet next door.  
Maybe he has something we can use.

JANE

(nods)

A pair of nickel-plated number 8 
bolt cutters on the third shelf 
behind the institutional-sized box 
of Little Morty urinal cakes.

(MORE)

TABLE 58 - "Pilot" - 4/11/2014                         16.

JANE (CONT'D)

(off their looks)

What?

INT. JANITOR’S CLOSET - MOMENTS LATER

The door is propped open.  

LOGAN (O.S.)

Left foot, right foot, left foot, 
right foot...

Our gang, moving as one unit, make their way in.  Logan, 
bringing up the rear, pulls the door. 

JANE

Don’t shut the--

Door shuts, click.  

JANE (CONT’D)

It locks from both sides. 

Dullbert tugs on the door knob -- confirms that it’s locked 
with a solemn nod.

JANE (CONT’D)

(re: empty shelf)

And the bolt cutters aren’t even in 
here.

OLIVE

(despairs)

We’re failing at a one-to-one 
ratio! 

BEBE

Don’t get your argyles in a bunch.  
I’ll simply use my--

(searching her purse)

Where’s my phone?!  

OLIVE

Why isn’t it permanently affixed to 
your cranium?!

Everybody looks around for Bebe’s blinged-out phone... except 
Feeney who’s holding it, playing a game.  Even he starts to 
look --

FEENEY

(without irony)

It’s got to be around here 
somewhere.

TABLE 58 - "Pilot" - 4/11/2014                         17.

Logan snatches it, checks it --

LOGAN

...and the battery’s dead.

BEBE

Great!  We’re gonna die in here!  
Come spring, they’ll find our 
skeletons: five gross ones and one 
cute one.

Logan sees a vent on the top of the wall. 

LOGAN

Dullbert, that air vent.  If we 
found a screwdriver, do you --

Dullbert beats him to the punch, PULLS the vent off the wall!

LOGAN (CONT’D)

Awesome.  Olive, where does this 
vent go?

Olive wets her finger, holds it up.  

OLIVE

It splits into two sub-channels and 
judging by the in-flow versus the 
out-flow --

JANE

You don’t need to show your work!

OLIVE

(quickly)

Dead end to the right.  Left goes 
to the front office.  But if you’re 
aiming to crawl through, judging by 
your height and shoulder width, 
you’ll never fit.  None of us will.  

Feeney inserts himself --

FEENEY

I got this.  I will summon Lazarus. 

LOGAN

And we need a possum why?

FEENEY

Comfort.  Morale.  Bonding.

Jane looks around, then grabs a URINAL CAKE from the box.  

TABLE 58 - "Pilot" - 4/11/2014                         18.

JANE

I’ve got an idea.

INT. FRONT OFFICE - RECEPTIONIST AREA - SAME TIME

Starting on the VENT, we pull back to see FACULTY working. 
Clank!  Clank!  Clank!  What the heck?!  Everyone looks up at 
the vent.  CLANK!

INT. JANITOR’S CLOSET - SAME TIME

Bebe uses a bedazzled compact mirror to reflect light on to a 
chalk “X” just inside the vent.  Jane hands Logan a urinal 
cake.  

JANE

Last one.  Hope someone can hear 
us.  

LOGAN

I got this.  

Logan takes a moment, cocks his arm --

DULLBERT

Laser.  Laser.  Laser.

The rest join in, causing Logan to smile.  He resets, cocks 
his arm but -- The door opens!  

LOGAN

It worked!  We did it!

The gang cheers!  Until they see it’s Vice-Principal Monty.

VICE-PRINCIPAL MONTY

Loitering after the bell has rung! 
De-ten-tion!  After school!  

What? 

OLIVE

No!  

AUSTIN

Vice-Principal Monty stalks off. 

LOGAN

Vice-Principal Monty, I have try-
outs after--

Logan moves towards the door to chase Monty, but is snapped 
back toward the gang because of their shoelaces.  

TABLE 58 - "Pilot" - 4/11/2014                         19.

INT. HALLWAY - CONTINUOUS

Coach Peterson turns the corner, carrying the box of Hulk 
hands.  Notice Peterson’s hand is bandaged.  He shoves the 
box to Monty. 

COACH PETERSON

You find its porpoise.  Here’s a 
hint: boxing gloves they ain’t. 

Monty exits with the box.  Logan runs into frame, dragging 
FIVE PAIRS of SHOES behind him. 

LOGAN

Vice-Principal-- Oh, Coach 
Peterson, perfect.  Who’s got 
thumbs-- whatever, I want to try 
out for the team right now!

COACH PETERSON
And I want-- Wait, what? 

Thinking fast, Logan grabs a greasy, drippy LUNCH BAG out of 
a trash can. 

LOGAN

Go long, I’ll throw you a pass!  
The Laser never misses!

Logan throws it at the Coach who doesn't budge.  It hits him 
in the chest. 

COACH PETERSON

Kid, there’s a fine line between 
moxie and a psychopath.  Now 
normally when people throw garbage 
at me in public, I divorce them.  
But I did see a tight spiral in 
there so I’m giving you one more 
chance.  See you on the field. 

LOGAN

Yeah, about that... See funny 
thing, something’s come up--

COACH PETERSON

(holds up hand, stops him)

I had to rearrange practice just to 
let you try-out today.  Don’t let 
me down. 

Coach Peterson enters his office door which has been propped 
open.  Door slams.  Dejected, Logan shuffles away, dragging 
the shoes behind him.

TABLE 58 - "Pilot" - 4/11/2014                         20.

EXT. QUAD - LATER

The gang stands by an open tool shed.  Their shoes are back 
on, but their shoelaces have been cut.  Logan is pestering 
Vice-Principal Monty, handing him a slip of paper.  

LOGAN

An official Logan Davis I.O.U.  I 
promise to do double detention 
tomorrow. 

(Boy Scout salute)

Scout’s honor. 

Monty promptly rips up the I.O.U. and turns to the group.

VICE-PRINCIPAL MONTY

Attention, delinquents!  See those 
unruly leaves?  They’re a metaphor 
for your unruly behavior.  So 
you’re gonna rein it, contain it, 
and throw it away. 

Logan despairs at seeing Coach Peterson and the jocks doing 
drills.  He looks at his watch.

LOGAN
(muttering)
Twenty minutes. 

He does a double take -- Is Pierce talking to Ziggy? 

LOGAN (CONT’D)

Hey, Bebs, I thought everybody kept 
with their own at this crazy 
school.

BEBE

Uh-doi.

LOGAN

Well, Pierce and Ziggy seem to be 
buds. 

Bebe looks for herself but Ziggy is gone. 

BEBE

Keep this up and you’ll be sitting 
at Table 12.

SMASH CUT TO:

TABLE 58 - "Pilot" - 4/11/2014                         21.

INT. CAFETERIA - DAY

Seated in chairs around TABLE 12 are five giant bags of 
POTATOES and one student: Sweatpants (again with a hoodie 
over his head -- we DON’T see his face). 

CUT TO:

EXT. QUAD - PRESENT DAY

Our gang grabs bags and rakes from the tool shed. 

VICE-PRINCIPAL MONTY

Grab a rake, a bag and gloves.  
Detention is over when you have rid 
the quad of all leaves.    

Monty locks the tool shed and walks away.  The gang looks up. 
The quad is covered with leaves.

LOGAN
(dumbstruck)

Geez, did the leaves text all their 
friends to flash mob the quad? 

BEBE

We forgot gloves.

Tool shed’s locked. 

LOGAN

Skip ‘em.  The sooner we clear the 
leaves the sooner we can go.  

Feeney lays on a bench, playing with his tablet device. 

LOGAN (CONT’D)

Feeney!

FEENEY

This candy ain’t gonna crush 
itself, bro.

(off looks, explains)

Detention’s only an hour.  Monty 
can’t make us stay here all night.  
Once time is up, he has to let us 
go.  Leaves or no leaves.  

JANE

A detention loophole? 

FEENEY

You’re in my world now. 

TABLE 58 - "Pilot" - 4/11/2014                         22.

BEBE

Shoot, I can wait an hour.  Bebe 
out. 

Dullbert, Bebe, and Jane throw down their rakes and join 
Feeney.  

LOGAN

I don’t have an hour.  I’m meeting 
Coach in twenty minutes.  You gotta 
help me. 

JANE

Why?  So you can sit with the 
Royals?  You need to build a bridge 
and get over it.  Everyone at Table 
1 is awful.  Why do you want to be 
friends with them?

Logan considers this until he sees Portia walk by in the 
distance -- there’s his reason.    

OLIVE (O.S.)

Forget them.  Tick-tock! 

REVEAL: Olive’s already filled two garbage bags.  Logan 
starts raking. 

LOGAN

Thanks, Olive.  You’re a good 
friend--

OLIVE

See that bus?  It departs for the 
Academic Decathlon in twenty
minutes.  I will not let this 
derail my plans.  This is the first 
competition since last year’s 
disastrous Super Quiz.

INT. GYM - DAY

“NATIONAL SUPER QUIZ” banner hangs on the wall.  On stage is 
Olive and the TWO ARGYLE NERDS.   

FLASHBACK TO:

PRINCIPAL FRIDAY

Here’s your last equation.  

A complicated math equation is projected on the screen.  The 
nerds get in a huddle, but Olive shoves them aside, grabs the 
marker and starts scribbling away, “A Beautiful Mind”-style.  
After a beat, she buzzes in.

TABLE 58 - "Pilot" - 4/11/2014                         23.

PRINCIPAL FRIDAY (CONT’D)

Interrupting Cows?

A JUDGE checks Olive’s work, nods.

PRINCIPAL FRIDAY (CONT’D)

Winner, Milton Middle School!

Friday holds up Olive’s arm in victory.  Off on the nerds 
scowling enviously...  

DISSOLVE TO:

EXT. QUAD - PRESENT DAY

Logan and Olive are as we left them, raking as they talk. 

OLIVE

The next day I was excommunicated 
from my table.  It seems, jealousy, 
unlike a compounding integer, has 
no limits. 

LOGAN

So that’s why you’re at Table 58. 

OLIVE

I’ll be back in the metaphorical 
fold once those nerds realize I am 
the team’s X factor.  

(explaining)

The variable in a given situation 
that could have the most 
significant outcome.

LOGAN
Yes, I got it. 

OLIVE

If I’m absent today, the team will 
have cause to permanently terminate 
me.  And without Academic 
Decathlons, I... well, simply put, 
I’d have no porpoise. 

LOGAN

Like me and football.  Royals 
aside, I just belong on that field. 
It’s my porpoise.  

Overhearing all this, Jane sighs, picks up a rake and helps.  

JANE

This doesn't mean we’re friends. 

TABLE 58 - "Pilot" - 4/11/2014                         24.

Dullbert and Feeney follow suit. 

DULLBERT

Common ground and parallel goals.

FEENEY

Word.  Let’s show these leaves 
who’s boss.

Bebe doesn’t budge.  Jane snatches Bebe’s phone out of her 
hand.  

JANE

If you don’t help, I’ll send a 
piece of this to you in the mail 
every week for a year.  

Bebe grudgingly joins in.  In DOUBLE SPEED, the gang rake and 
bag leaves like crazy.

EXT. QUAD - LATER

Vice-Principal Monty scans the area.  It’s leaf-free.  The 
gang stands before garbage bags of leaves.  

FLIP TO:

VICE-PRINCIPAL MONTY

(disappointed)

Fine.  You’re dismissed. 

OLIVE

(to Logan)

With one minute to spare. 

LOGAN

Yeah, girl!  Knuckles!

OLIVE

No time.

Olive dashes off, leaving Logan hanging.  

LOGAN

(calling after)

There’s always time for knuckles!

VICE-PRINCIPAL MONTY

Halt!

REVEAL: some of the garbage bags are empty, leaves are 
everywhere.  On Logan’s face: How did that happen?!

TABLE 58 - "Pilot" - 4/11/2014                         25.

VICE-PRINCIPAL MONTY (CONT’D)

(fist pump)

Detention’s back on!  

Olive is about to exit the gate.  Logan looks from Olive to 
Coach Peterson.  Back and forth.  Until --   

LOGAN

It’s my fault!  All me!  I forgot 
to tie up my bags.  I’ll clean it 
up.

VICE-PRINCIPAL MONTY

Figures.  It’s only your first day, 
and you’ve already managed to get 
on my bad side.  Your efficiency is 
noted. 

(to the others)

The rest of you may go. 

Olive mouths a “thank you” to Logan and exits.  Monty clears.  
Logan watches as Coach Peterson exits the field -- his 
opportunity lost.  

Then he sees Pierce and Ziggy walk by with LEAF BLOWERS 
strapped to their backs.  They are in cahoots!  Pierce 
catches Logan’s eyes -- shrugs with a smug smile “oops.”  

DULLBERT

Logan the Laser, your lack of self 
would make the Dalai Lama smile.

LOGAN

Olive needed this more than I did. 

FEENEY

So you’re actually going to wait a 
month for try-outs?

LOGAN

Heck no.  I plan on bugging Coach 
every day until he gives me another 
shot.  Wait ‘til he sees me spiral 
his breakfast burrito tomorrow.  
Don’t worry about me, I’ll make the 
team.  

FEENEY

(gestures to group)

Until then, the bottom of the 
barrel welcomes you. 

CUT TO:

TABLE 58 - "Pilot" - 4/11/2014                         26.

INT. CAFETERIA - DAY

Continuing from the Cold Open, Logan stands before Table 58 
wearing HULK HANDS.  Olive, Bebe, Feeney, Jane, and Dullbert 
all raise their hands -- REVEAL: They’re all are wearing HULK 
HANDS.    

BEBE

(icily to Logan)

All.  Your.  Fault.

Olive pushes up the bridge of her glasses with her Hulk 
hands.

OLIVE

Let’s not lay blame on Logan.  He 
is as much an innocent victim as we 
all are.

LOGAN

Yeah, how was I supposed to know 
there was poison oak on the quad?

Vice-Principal Monty walks by. 

VICE-PRINCIPAL MONTY

I told you to wear gloves.  Maybe 
next time you’ll follow directions.

BEBE

Ugh, green is so not my color. 

VICE-PRINCIPAL MONTY

Tough.  Nurse said your hands must 
be kept warm for twenty-four hours 
and these keep the calamine from 
dripping.  You’re lucky we had 
them.  

PRINCIPAL FRIDAY
(rushes over, excited)

They serve a porpoise!  

VICE-PRINCIPAL MONTY

Thanks to yours truly!

FEENEY

Hey, Principal Friday, what’s a 
marine-biologist waiter?

PRINCIPAL FRIDAY

(ignoring him)

I love happy endings!

TABLE 58 - "Pilot" - 4/11/2014                         27.

Our gang looks miserable as the adults cross off.  Pierce and 
the jocks walk by, pointing and laughing at 58.  Logan turns 
to his table-mates.

LOGAN

Team huddle.  Bring it in.  I’ve 
been thinking.  Table 58’s
different than those other tables.  
We each have our own strengths.

BEBE

Yeah, Jane, giant pores are a 
terrible thing to waste. 

LOGAN

I’ve got a new game plan.  First, I 
get in with the Royals.  Then I 
dethrone Pierce from the inside.  
Olive, you want back in with 3.14?  
Dullbert, The Meanies?   

Olive nods.  Dullbert shrugs. 

FEENEY

And I need to be with people who 
know the difference between a pre-
lunch nap and a pre-nap lunch. 

LOGAN

I say we help each other get back 
to our tables.  Work together to 
tear this group apart. 

FEENEY

How deliciously ironic. 

JANE

I don’t hate this idea.

OLIVE

Statistically speaking, it would 
increase our chance of succeeding.

DULLBERT

Or perhaps we could just enjoy 
where we currently are and 
appreciate each other’s company.

They consider it, but:

ALL

Nah. 

Feeney grins and holds up his Hulk hand.   

TABLE 58 - "Pilot" - 4/11/2014                         28.

FEENEY

Knuckles?

LOGAN

(perks up)

Pound it!

Logan, Jane, Olive, Feeney, Dullbert fist bump with their 
Hulk hands.  Bebe doesn't join.   

BEBE

Uh, hello?!  “Working together” led 
to THIS!  Ugh, last month I was 
living it up with the Royals.  Now 
I’m slumming it with the Boils. 

LOGAN

YOU used to sit at Table 1?  So you 
know all the bros.  I can use that 
info.  What happened?  

Bebe purses her lips -- a story for another day. 

FADE OUT.

TABLE 58 - "Pilot" - 4/11/2014                         29.

INT. GYM - DAY

TAG

“CAREER DAY” banner hangs on the wall.  Seated on the stage 
in full uniform is a DOCTOR, a FIREFIGHTER, and a POLICE 
OFFICER.  

Camera then FINDS a MAN wearing a typical WAITER UNIFORM 
(white shirt, black pants, white waist apron, black tie), 
scuba gear, and flippers.  Standing by the sidelines is the 
faculty, including a very smug Principal Friday. 

LOGAN

(raising his hand)

So do you serve fish or do you 
serve fish?

END OF PILOT

FADE TO BLACK.

