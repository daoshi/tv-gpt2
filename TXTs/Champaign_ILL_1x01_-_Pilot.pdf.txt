NOW WE HERE

PILOT

Written by

Jordan Cahan

David Caspe

&

&

&

Daniel Libman

Matthew Libman

96 BULLS TV
FANFARE
SONY TELEVISION 

8/21/15

EXT. FOREST HILL, CANADA - MIDDLE OF THE NIGHT

A quiet suburb outside Toronto.  This is the place everyone 
leaves to go chase their dreams.   

CHYRON: 2003

EXT. HIGH SCHOOL SOCCER FIELD - SAME

THREE 18-YEAR-OLD BEST FRIENDS, in open graduation gowns, lie 
on their high school soccer field, staring up at the stars 
and dreaming... 

RONNIE BORIS (Jewish, skinny, nerd, Yale sweatshirt) sparks a 
joint.  

RONNIE

I’m really gonna miss you guys, but  
I’m getting pumped for Yale.  And 
real life.  Not to be a puss, but I 
feel like I can make a difference.  
Fuck getting rich.  I’m gonna start 
a non-profit, do some good.  

(takes a huge hit)

It only takes one person to change 
the world, right?  Why not me?   

He passes the joint to ALFONSO “ALF” BELL (black, handsome, 
hopeless romantic).

ALF

No doubt, Ronnie Ron.  I’m pumped 
for the next step, too.  I know 
people say going to college with 
your girlfriend is like bringing 
sand to the beach.  But I’ve been 
in love with Courtney since 3rd 
grade.  So fuck the beach.    

(takes a huge hit)

That girl’s gonna be my wife.  

He passes the joint to AUBREY GRAHAM (half Jewish, half black, 
all charm, everybody loves him).

AUBREY

Cool, cool.  So, um, I actually 
have some crazy news.  One of those 
mixtapes I made got to Young Money 
Records and... they wanna fucking 
sign me!

Holy shit!

ALF

Dude, that’s insane!

RONNIE

2.

AUBREY

Right?  They got me a penthouse in 
Miami to record the album, they’re 
talking about me opening for Lil’ 
Wayne!  So I was gonna ask if you 
guys wanted to take a year off and 
come, like, be my crew.  But after 
hearing how committed you are to 
your dreams, I realized I was being 
selfish.  It’s not like you’re 
gonna walk away from all your plans 
to just, like, party with me.

Alf and Ronnie look at each other, they did not expect that 
offer.  Beat.

ALF

...Of course not.

RONNIE

Yeah... it’s like, as if.  I mean, 
thank you for the generous offer--

ALF

So sweet of you--

RONNIE

But as I said, I am very committed 
to changing the world--

QUICK CUT TO:

INT. RAGING HIP HOP PARTY - PRESENT DAY

Ronnie (now 30 years old, huge beard, shirtless, out of shape, 
with a pet ALBINO PYTHON draped around his shoulders) pulls 
his face out of a mound of cocaine at a RAGING HIP HOP PARTY.  

RONNIE
Fuck the world!  

Ronnie claps his hands creating a CLOUD OF COKE a la LeBron. 

RONNIE

I’m Lebron Cocaines!

CUT BACK TO:

EXT. HIGH SCHOOL SOCCER FIELD - BACK IN 2003 

BACK ON the quiet scene of the boys on their high school 
soccer field 12 years earlier.  

3.

ALF

And as I said, Courtney is the love 
of my--

QUICK CUT TO:

INT. RAGING HIP HOP PARTY - PRESENT DAY

Alf (now 30 years old, super high flat-top with “Alf” shaved 
on the side) fucks a STRIPPER while she makes out with ANOTHER 
STRIPPER in the middle of THE SAME RAGING HIP HOP PARTY.

ALF

Giddyup, giddyup, giddyup-- Uh oh. 

(slowing himself down)

Grandma, baseball, Roger Ebert’s 
jaw... Ebert’s jaw...   

(back in control)

Whew, there we go.  That was a 
close-- I’M CUUUUUMMMING!

EXT. HIGH SCHOOL SOCCER FIELD - BACK IN 2003 

BACK ON the quiet soccer field again.  Aubrey stands up.

CUT BACK TO:

AUBREY

I totally get it, guys.  Sorry, I 
guess I just had this fantasy of us 
getting one more year together, 
and, like, partying at the highest 
level possible.  But you’re right, 
it’s time to grow up.  
(takes a huge hit)
Let’s get some Wendy’s.

Aubrey starts to walk away.  Alf and Ronnie look at each 
other again.  Beat.

RONNIE

You know... after having a little 
time to mull it over--

ALF

I’m in.  

RONNIE

I am also in.

4.

AUBREY
(turns back)

For real?!

I was in at Lil’ Wayne!

RONNIE

I was in even earlier! 

ALF

They run over and jump on Aubrey, celebrating!

AUBREY

Fuck yeah!

ALF

This is gonna be the sickest year 
ever!

RONNIE

Then right back to Yale!

ALF
And Courtney!

RONNIE

Without missing a beat!

The boys start to walk off the field together, so excited...

ALF

Now, not to get ahead of ourselves, 
but what are we thinking name-wise?  
Because I would not listen to a 
rapper named Aubrey.

RONNIE

God no.  I would listen to a 
realtor named Aubrey.

ALF

Oh yeah, she’s got a good feel for 
the market.

RONNIE

And big Jewish tits. 

ALF

Aubrey is a big-titted, Jewish 
realtor’s name.  

RONNIE

It just is.

AUBREY

(laughs, then)

Yeah, I was actually thinking of 
going by my middle name...

5.

SMASH CUT TO:

INT. DARK BEDROOM - PRESENT DAY

WHISPERING VOICE (O.S.)

...Drake.  Drake.  Drake.

Aubrey (now 30 years old, sleep mask, headphones) wakes up in 
a big bed.  He removes his sleep mask to REVEAL: AUBREY IS 
HIP-HOP SUPERSTAR DRAKE.  Drake squints up at his manager, 
CRAIG (45, suit and tie, dork).

DRAKE

Cool man, I’m up.

Craig goes and opens the shades: REVEAL we’re 30,000 feet in 
the air, in a luxurious bedroom on DRAKE’S PRIVATE PLANE.  As 
Drake gets ready, Craig gives him a quick rundown:

CRAIG

Wheels down in ten.  And just a 
heads up, the Nike reps are already 
at the venue, they need you to sign 
off on the new Air Jordan x Drake 
collab; you’ve got a photo-op with 
the French Prime Minister - 
apparently, his mister is a big fan 
- apparently, mister is male for 
mistress; and you still haven’t 
tasted the latest revision of your 
signature Pringle.  We’re already 
four hours behind schedule.  So, it 
would be really super helpful if we 
could just try to maybe avoid 
any... distractions?

DRAKE

Craig, don’t worry.  You know me, 
I’m all business.

CRAIG

Yeah, it’s not you I’m worried 
about...

Craig opens the bedroom door: REVEAL THE RAGING HIP-HOP PARTY 
ALF AND RONNIE ARE AT IS ALSO ON DRAKE’S PLANE.  Ronnie 
stands up from the table with a fistful of sushi.

6.

RONNIE

Drizz!  Thank god you’re awake.  
Not to talk shit, but whoever 
hooked up this sushi - Craig - does 
not respect our crew from a toro 
standpoint.  

On a couch nearby, Alf untangles himself from the 2 Strippers 
he had sex with as he pulls on Vuitton-print leather joggers.  

ALF

Chill brah, we’ll just have Craig 
send one of his Craigs to pick up 
L’Atelier on the way to Le Grand 
Monceau.  Feed this shit to Jake...  

Alf grabs the gorgeous sushi platter, holds it up to the snake 
around Ronnie’s neck.  Ronnie throws the platter in the trash.

RONNIE

Bro, Jake “The Snake” The Snake 
Roberts is a fuckin’ endangered 
albino Burmese python.  He eats 
gophers and shit, not janky toro.  
And honestly, L’Atelier again?  
It’s like every time we’re in 
Paris, bro.  How bout Sacree Fleur?  
Or fucking La Cordonnerie?   

ALF

One: Robuchon is a friend, and I 
like to support Joël when I can.  
And two: tell me that the la boule 
de boeuf at L’Atelier is not bomb.  
Try to tell me that la boule de 
boeuf is not bomb! 

RONNIE

That la boule de boufe is not bomb!

Drake watches them go back and forth like a tennis match.

ALF

Well now I’m talking to a mental 
patient.  But I knew that because 
you’re dressed in a fucking snake.  
Drake, will you please settle this 
la boule de boufe beef? 

They turn to Drake for a judgement.  Beat.

7.

DRAKE

Guys.  Listen to yourselves.  
You’re arguing over which dish from 
which Michelin star restaurant we 
should get - as take-out - on our 
way to the dopest hotel in Europe?

(the boys are ashamed)

The answer is all them from all 
them, mothafucka!

Drake high-fives his boys!  Craig sighs.

DRAKE

You know our crew is bout dem 
multiple entrees!

ALF

Of course!  And then we should do a 
blind taste test before the show--

RONNIE

And the loser has to shit on Jim 
Morrison’s grave! 

ALF

Yes!  My boy’s a genius!

CRAIG

Actually, we’re on a pretty tight--

DRAKE

I fucking love this!  Craig, can 
you make it happen?

CRAIG

(beat, forces a smile)

Of course!  Sounds fun!  

(glares at Ronnie/Alf)

Anything for you, Drake.

Fuck yeah!

RONNIE

This is gonna be so fun!

ALF

All three boys pop champagne bottles and spray each other like 
a championship celebration.  Craig gets drenched, so annoyed, 
as WE PULL OUT through the jet window...

RONNIE (O.S.)

Now Craig, this taste test’s gotta 
be perfectly orchestrated-- 

ALF (O.S.)

Cause if the dishes don’t arrive 
simultaneously--

8.

RONNIE (O.S.)

Then the temperatures are all 
fucked up--

ALF (O.S.)

And now we’re talkin’ apples and 
oranges.

...the jet is wrapped in a PHOTO OF DRAKE.  It descends over 
the EIFFEL TOWER as Drake’s “Started from the Bottom” KICKS 
IN for the LIFE WITH DRAKE MONTAGE:

-- Drake, Ronnie, and Alf sit front row at FASHION WEEK.  As 
a MODEL struts by, the boys make it rain hundreds, laughing.

-- Drake performs on stage at THE GRAMMYS.  Ronnie and Alf 
cheer in the audience.  Alf grinds up on BARBRA STREISAND.  

DRAKE (INTO MIC)

Started from the bottom now we 
here... 

-- Drake presents Ronnie and Alf with matching SNOWBOARDS.  
CUT TO: The boys jump from a chopper, HELI-BOARDING THE ALPS. 

-- Drake gets out of a Lamborghini, joining Ronnie and Alf at 
a packed VEGAS CLUB.  As a BOUNCER ushers the boys to the VIP 
area, HOT GIRLS swarm them.  DRAKE LOOKS DIRECTLY INTO CAMERA:

DRAKE

I didn’t always have this much 
swag.  As a kid, I was insecure... 
because of my skin.

QUICK CUT TO A PHOTO: 13-YEAR-OLD DRAKE, with acne and a ‘fro.

DRAKE

But then I discovered Proactiv.  
And now my skin game, and my game 
with the ladies, is on fleek.  

It’s a PROACTIV COMMERCIAL.  Drake turns back to Ronnie and 
Alf as they all do a big, poorly acted laugh!

-- Drake performs at an APPLE CORPORATE EVENT.  Off to the 
side, Alf and Ronnie greedily fill their backpacks with Apple 
Watches, iPads, and laptops, as Craig shakes his head.

DRAKE (INTO MIC)

Started from the bottom now the 
whole team fucking here...

9.

-- Alf and Drake laugh their asses off as they watch... 
Ronnie struggling to take a shit on JIM MORRISON’S GRAVE!  
Flashlights in the distance!  The boys run off, giggling!

-- Drake presents Ronnie and Alf with matching ATVs.  CUT TO: 
The boys race up sand dunes past the EGYPTIAN PYRAMIDS.

-- HOT GIRLS wait in a long line inside an ATLANTA MANSION 
PARTY.

ALF

Is this the line for the bathroom?

HOT GIRL

No, it’s the line for Drake’s room.

ALF

(smiles)

I know a shortcut...

And Alf walks off with a Hot Girl on each arm.

-- Drake passes a blunt to PAUL MCCARTNEY in a studio, who 
passes it to JERRY SEINFELD in a Maybach, who passes it to 
Ronnie, who passes it to STEPHEN HAWKING in the Rose Garden 
during the Correspondents Dinner, who passes it to MICHAEL 
JORDAN on the 18th hole at Augusta National, who passes it to 
Alf, who passes it to TUPAC’S HOLOGRAM, who performs with...

-- Drake at the NBA ALL-STAR GAME.  

DRAKE

...No new niggas, nigga we don't 
feel that!  Fuck a fake friend, 
where my real friends at!?

Drake points to Ronnie and Alf, both wearing “NO NEW FRIENDS” 
JERSEYS.  Alf dances with Hologram Tupac, while Ronnie twerks 
in front of Toronto Raptors star, DEMAR DEROZAN, who laughs.

-- Drake presents Ronnie and Alf with matching DUCATI 
MOTORCYCLES.  CUT TO: The boys tear through the Hollywood 
Hills at night, kings of the world.  Drake yells to them:

DRAKE

It doesn’t get any better than--

A COYOTE bolts into his path.  Drake swerves, loses control, 
flies over the handle bars, and SLAMS HEADFIRST INTO A 
RETAINING WALL!  The song ABRUPTLY CUTS OUT. 

The boys stare at the accident, IN COMPLETE SHOCK.  Beat.  
They ditch their bikes and run to Drake, hysterical!

10.

RONNIE

OH MY GOD OH MY GOD OH MY GOD!

ALF

HELP!  HELP!  SOMEBODY PLEASE!

But Drake is DEAD.  Ronnie vomits.

TITLE CARD: NOW WE HERE 

EXT. HOTEL BALCONY - DAWN

CLOSE ON: a coffee table littered with DEVICES (iPads, 
phones, laptops) BUZZING/RINGING/DINGING incessantly.   

Ronnie and Alf sit silently, ignoring them all, eyes red from 
crying all night.  So sad.

RONNIE

Fuck.  I mean... fuck.  

ALF

I can’t-- I can’t believe he’s 
gone.  It’s just so... weird.   

RONNIE

I didn’t even know we could die 
yet, right?

ALF

(beat, destroyed)

Totally.  

Fuck.

They both stare ahead, IN SHOCK, as the devices continue to 
buzz.

INT. HOTEL SUITE - LATER

Alf and Ronnie try to somehow pack, so dazed.  They pass each 
other without a word.  Craig enters, broken up.

CRAIG

Hey boys.  How we holdin’ up?

Shitty.  

ALF

Super fucking shitty, Craig.

RONNIE

CRAIG

Yeah...  So the funeral’s gonna be 
tomorrow in Forest Hill.

11.

RONNIE

God.  The funeral.  
(wipes his eyes)

Alright, tell the pilot we’ll be 
ready like 3 or 4-ish?

ALF

(trying to be helpful)

And as far as lunch on the plane 
goes, just whatever’s easiest.  

RONNIE

We don’t wanna put anyone out. 

CRAIG

Oh, um, well there’s not gonna be 
any lunch on the plane cause-- 

RONNIE

Of course.  Cause if it’s wheels up 
at 4, we’re sorta in that no man’s 
land between lunch and dinner--

ALF

(wipes his eyes)

It is a bit of a tweener--

RONNIE

And who could even eat now, anyway?

CRAIG

No, guys, there’s no lunch on the 
plane, because there is no plane.  
The label chartered it for Drake’s 
tour.  And now that Drake is...

ALF

(realizing)

Oh shit.  Of course, sorry, we’re 
totally cool flying commercial. 

RONNIE

(chokes up)

All we care about is getting home 
as soon as possible to be there for 
Drake’s family.  So even if they 
don’t have first class, coach is 
great.  But not coach coach.  
What’s that other one called?    

ALF

Business.  

12.

RONNIE

Thank you.  So just to recap: aisle 
in first is our top choice.  Then 
window in first, then if you do 
have to go business, a few seats a 
piece is preferable.  After that, I 
guess...

ALF

Maybe like a row or two each in 
coach would feel like first?

RONNIE

Well not first first, but maybe-- 
remind me what it’s called again?

ALF

Business.  

RONNIE

Right!  Sorry, I must have, like, 
grief brain.  Is that a thing?  
Google that real quick, Craig. 

CRAIG

(snaps at them)

I don’t work for you!  

(softens)

I mean, it’s not my job to do stuff 
for you guys anymore.  It never 
really was. 

Beat.  Then Ronnie just SLOW CLAPS...

RONNIE

Well, well, well.  Congratulations, 
Craig.  How long have you been 
waiting for this moment?

ALF

Yeah Craig, are you happy now, 
Craig?

CRAIG

What?  No, I’m not happy.  My 
friend just died too--

RONNIE

I thought we were family, dog.  I 
gave you my sister’s number.

CRAIG

She didn’t even call me back--

13.

ALF

(bursts into tears)

It’s not our fault you’re the only 
guy that can’t close Ron’s sister!

Alf sits down, overcome by everything.

RONNIE

I think what Alf’s trying to say 
is, in this time of mourning, it’s 
essential that we-- you-- continue 
to do our-- your-- jobs-- job.  

CRAIG

Wow.  Okay.  Try to hear me here, 
guys.  All this... everything... 
was for Drake.  Not you.  So all 
the handouts--

RONNIE

Whoa whoa bro, handouts?

ALF

This motherfucker said handouts?    

RONNIE

You really wanna do this now, 
Craig?  

ALF

We work for salaries from the many 
successful companies we started.

CRAIG

With Drake.  Look, the business 
manager froze all the credit cards 
and everything until he sorts out 
the estate.  Should only be a 
couple days, but you’re gonna need 
to take care of yourselves this 
week.

ALF

Okay, then how bout you just do us 
a favor, as a dear friend who I 
have hooked up with multiple 
buttfucks over the years that you 
could not have gotten on your own--

RONNIE

You have been fucking butts way 
above your station, Craig--  

14.

ALF

And please just spot us a coupla 
tickets to OUR GODDAMNED-- 

(voice cracks)

BEST FRIEND’S FUNERAL!?     

Alf KICKS a lamp, overcome by grief again.  Ronnie comforts 
him, GLARING at Craig.  Craig sighs and takes pity on these 
idiots:

CRAIG

Fine, I’ll get you to the funeral. 

Thank you!

ALF

RONNIE
Was that so hard?!

Exasperated, Craig heads out, but stops at the door--

CRAIG

Hey guys, can I give you a little 
piece of advice-- 

RONNIE

Nah, we’re good, bro.   

ALF

Just the tickets. 

Craig shakes his head and leaves as A Tribe Called Quest’s 
“Can I Kick It” begins for the GOING HOME MONTAGE:

-- Alf (harem shorts, Christmas sweater, flip-up shades) and 
Ronnie (hooded tank top, Toronto Raptors warm-up pants, 
Dracula cape) stand on a CRAMPED AIRPORT SHUTTLE, unaccustomed 
to public transpo.  PASSENGERS stare.  A LADY accidentally 
rolls her bag over Ronnie’s new Jordans, leaving a big mark.

-- The boys struggle to carry a huge Vuitton trunk and dozens 
of other bags (promotional totes, garbage bags, shoe boxes, a 
terrarium with Ronnie’s python, etc.) toward the AIRPORT.  
They’ve never had to carry their own bags/pack efficiently.   

-- The boys stand at the AIR CANADA CHECK-IN COUNTER, 
surrounded by all their luggage. 

CHECK-IN LADY

Okay, so... checking 23 bags at 50 
dollars a bag... is 1150 dollars.

The guys look at each other.  QUICK CUTS: they shove 
everything into the Vuitton trunk; jump on it to close it; 
then lift it onto the scale with the help of two big SKYCAPS.  

15.

CHECK-IN LADY

Okay, so... 767.5 pounds at 75 
dollars for every 50 pounds... is 
1150 dollars and 50 cents.  Huh.  

The guys look at each other.  QUICK CUTS: they throw out their 
superfluous shit: a basketball, a case of Vitamin Water, 
unopened iPads, two more basketballs, a set of dumbbells.

-- The boys ride in a PACKED AIRPORT TRAM, miserable.  Ronnie 
checks the DEPARTURE MONITOR: “DELAYED”.  Alf yawns just as 
an OLD MAN sneezes in his mouth!  Alf dry heaves.  

-- The boys stand on a MOVING WALKWAY as it rolls past a wall 
of TVs showing coverage of Drake’s death.  They’re so sad.

-- The boys wait in a LONG SECURITY LINE, then notice a DRUG 
DOG nearby.  They look at each other.  SMASH CUT TO:

-- Alf holds a jewelry box filled with DRUGS (pills, coke, 
weed, etc.) over the toilet in a BATHROOM STALL.  He’s about 
to dump it when Ronnie stops him.  They look at each other.  
QUICK CUTS: The guys DO AS MUCH OF THE STASH AS THEY CAN, 
dumping only what they absolutely cannot ingest.

-- The boys, HIGH AS FUCK, ALL SMILES, enter the JET BRIDGE.  
Maybe flying commercial isn’t so bad...  SMASH CUT TO:

-- They’re passed out on the EMPTY PLANE (it arrived 30 
minutes ago).  Ronnie is drenched in sweat, nose bleeding.  A 
STEWARD pokes them awake with an umbrella.  Alf stumbles up, 
his shorts are dark with piss.  MONTAGE ENDS.       

EXT. TORONTO AIRPORT - LATER

IT’S GREY AND SNOWING.  The boys stand on the curb, 
shivering, way under-dressed for their hometown.  

RONNIE

That was... horrible.

ALF

Do you remember when we visited the 
camps in Poland?  Now I’m not 
saying that flying commercial is on 
the same level-- 

RONNIE

Tough area.

16.

ALF

I’m just saying that I now have a 
greater understanding of their 
catchphrase, “Never Again.”

RONNIE

Just calling it a catchphrase 
displays a staggering lack of 
understanding.  But if we’re 
already in that area... at least 
those trains ran on time!

ALF

Our plane was forty minutes late!

A 90’s Dodge Caravan pulls up.  GAYLE (Ronnie’s suburban mom, 
very Canadian), gets out holding her beloved dog, TAFFY.  

RONNIE

Hey Ma.

ALF

Hi, Mrs. B.  Whatup Taffy? 

She hugs Ronnie and Alf.   

Welcome home, boys.  I’m so sorry.

(re: the snake terrarium)

GAYLE

Snake’s not coming in the Caravan, 
Ron.

RONNIE

This snake costs more than your 
life, Ma!  

GAYLE

So get it a snake limo, why 
don’tcha?

RONNIE

Get yourself a snake limo!

GAYLE

Why?  I already have a nice snake-
free car right here, and it’s gonna 
stay that way.

Ronnie and Gayle stare at each other.  Alf is uncomfortable.   

17.

RONNIE

Well, then it looks like Jake and I 
will just have to find our own-- 

RONNIE HOLDS HIS SNAKE OUT THE WINDOW as the minivan drives 
down the highway.  Barbra Streisand’s “Memory” plays on the 
car stereo as they enter...

SMASH CUT TO:

EXT. FOREST HILL - CONTINUOUS

The boys watch their past roll by out the window.  Forest 
Hill looks worse for the years.  The boys look at each other.

RONNIE

Feels weird to be home.  Without 
him.

Alf nods, then sees the STREISAND ALBUM COVER on the floor of 
the dirty mini-van.  They’re a long way from the Grammys.  

EXT./INT. ALF’S HOUSE - LATER

Alf gets out of the Caravan in front of his small childhood 
home, waves goodbye, and walks inside.  

ALF

Hello?  

He glances at an old FAMILY PHOTO: 8 YEAR-OLD ALF surrounded 
by his BEAUTIFUL MOTHER and VERY IN-SHAPE FATHER on a beach.  

ALF

Anybody home?  Dad?  

He rounds the corner into the living room to find his dad, 
LAFONSO (500 lbs., “Friends” t-shirt, cheerful), in a bed 
where the couch should be.  He’s surrounded by 2 MINI-FRIDGES, 
TOASTER OVEN, MICROWAVE, COMPUTER, PRINTER/COPIER, etc.     

LAFONSO

Hi kiddo!  Welcome home!  

(then sweetly)

Hey, I’m so sorry about Aubrey.

Lafonso opens his arms wide for a hug, without getting up.  
Alf leans down and gives him an odd, seated hug.   

ALF

Thanks, Pop.  So... did you do some 
remodeling?  

18.

LAFONSO

Oh that’s right, you haven’t seen 
the ol’ live/work command center!  
Finally moved my bed into the TV 
room.  Along with my office and 
some essential appliances.  Did you 
know the average Canadian wastes 
almost 3 years of their life going 
up and down stairs?  Work smarter, 
not harder.  Cool, eh?  

ALF
Yeah.  Cool.  

LAFONSO

And there’s no one to say boo, 
since your mother abandoned us.   

ALF

Fifteen years ago.

LAFONSO

Wow, has it been that long?  Welp, 
time flies when you’re having fun.  

(the microwave dings)

Uh oh!  Who wants dinner?  Pull up 
a study pillow.  

Lafonso uses a GRABBER to all-too-expertly take out a 
microwavable lasagna, strip the plastic and serve it to 
Alf... who is horrified at this depressing tableau.     

INT. RONNIE’S PARENTS’ HOUSE - LATER

Ronnie opens the door to his room to REVEAL it has been 
converted into Taffy’s room: dog beds, dog toys, “Hotel For 
Dogs” movie poster, etc.  Ronnie shouts off:

RONNIE

Ma!  What the fuck?!  Where’s my 
room?  

GAYLE (O.S.)
It’s Taffy’s room now! 

Taffy runs past Ronnie, hops on his over-sized doggy bed, 
then stares at Ronnie, GROWLING.

RONNIE

What kinda bullshit move is that?  

19.

GAYLE (O.S.)

You always stay at the Four Seasons 
when you’re in town! 

RONNIE

Well I’m not now!  

GAYLE (O.S.)

So sleep on the air mattress.  

(then)

Burt!  Get the air mattress!  

BURT (O.S.)

GAYLE (O.S.)

For who?

Ronnie!

BURT (O.S.)

Ronnie’s home? 

GAYLE (O.S.)

Yeah! 

WE STAY ON RONNIE’S EXASPERATED FACE as he listens to his 
Jewish parents yell from different rooms of the house.  

BURT (O.S.)

Why isn’t he at the Four Seasons? 

GAYLE (O.S.)

Cause Aubrey died!  

BURT (O.S.)

What!?

GAYLE (O.S.)

Aubrey died!

BURT (O.S.)

What!?

Ronnie can’t believe he has to hear this over and over.

GAYLE (O.S.)

AUBREY DIED!

BURT (O.S.)

Oh no.  Brie who?

GAYLE (O.S.)

No, Aubrey.  Ronnie’s best friend 
since second grade!  He died in a 
horrific motorcycle accident!

20.

Ronnie shakes his head, exasperated.

BURT (O.S.)

Oh no, he was so young.  Why am I 
the last to know everything?    

GAYLE (O.S.)

Well you wouldn’t be if you joined 
Facebook!   

BURT (O.S.)

I’M NOT JOINING FACEBOOK, GAYLE!  

BURT (retired from work and life, his wife runs the show) 
pops into Ronnie’s room, compassionate.  

BURT

Heyyyy, son.  I’m so sorry about 
Aubrey.  How you holdin’ up?   

RONNIE
Not good, dad.  

BURT

Yeah.  I was crushed when I heard.  

He hugs Ronnie, then sits.  Beat.  

BURT

Good to see you, good to see you...

He loves his son, but he’s not a talker.  Burt hands Ronnie 
the AIR MATTRESS BAG and starts out, but stops at the door. 

BURT

The pump’s in the bag. 

Hey listen, son... 

(then)

INT. ALF’S DAD’S HOUSE - LATER

Alf walks down the hall.  The WALL PHOTOS tell a story as he 
goes: his family is a HAPPY THREESOME when he’s young, then 
his mom disappears, then Lafonso gets BIGGER and BIGGER...

Alf peeks into his dad’s old BEDROOM.  The carpet is LIGHTER 
where the bed used to be.  

He spots a bunch of UNOPENED BOXES OF WORKOUT EQUIPMENT 
(Soloflex, Ab Roller, etc.) with gift bows still on them.  
Alf sadly reads the CARDS: “Happy Birthday Dad! Love Alf”; 
“Happy Father’s Day! Love Alf”; “Just Cause... Love Alf”.

21.

INT. TAFFY’S ROOM - LATER

Ronnie inflates the air mattress with the VERY SLOW, VERY 
LOUD pump, as Taffy BARKS at it non-stop.  Ronnie’s IN HELL.

INT. ALF’S OLD ROOM - LATER

Alf enters his old bedroom, it’s UNCHANGED since high school: 
red silk sheets, black lacquer, and a lot of dragons - like 
an Asian sex dojo designed by a 16 year-old.

Alf opens his suitcase.  Packed on top is a framed MADISON 
SQUARE GARDEN TICKET STUB.  He stares at it, nostalgic...

CUT TO:

INT. MADISON SQUARE GARDEN - FLASHBACK

CHYRON: 2012

Ronnie, Alf, and Drake smoke a blunt in the wings of Madison 
Square Garden as the packed stadium chants, “Drake! Drake! 
Drake!”.  The boys just marvel at the enormous crowd, in awe.

RONNIE

The fucking Garden.  

ALF

You did it, Aubrey.  

DRAKE

Fuck that.  We did it.  I never 
coulda gotten here without you 
guys.  We gotta remember this 
moment.  Right now, everything 
about our lives is perfect.  Except 
Ronnie’s vest.  

REVEAL Ronnie is wearing a BULLETPROOF VEST.

RONNIE

What?  This thing is tight.     

ALF

Exactly dog, too tight.  Your love 
handles are spilling out the sides.

Drake laughs.

RONNIE

It’s a medium.  I’m a medium.  

22.

DRAKE

When did you get big, though?

RONNIE

I don’t know what to tell you guys, 
I am, and have always been, a 
bulletproof vest medium.

ALF

The only way you could be a bullet-
proof vest medium is if you had the 
ability to communicate with dead 
bulletproof vests.

Drake and Alf crack up.

RONNIE

Hilarious!  Who invited Chris 
D’Elia!?  Is he opening for you!?

DRAKE

We’re just saying, maybe your look 
needs a re-think, Ron.  The snake, 
the vest - you wore a lion’s head 
to the VMA’s.

ALF

You looked like King Jaffe from 
Coming to America!  

Now Drake dies laughing, high-fiving Alf.

RONNIE
(vulnerable)

I like to take big swings.

Ronnie finally gives in, laughing at himself, too.  Then the 
house lights dim and the crowd explodes.  Drake looks out, 
swallows, nervous now...

DRAKE

The fucking Garden.

RONNIE

(slaps his back)

Yo.  You got this, Aubrey.

ALF

You fucking got this.

Drake nods, steely now.  Then he runs out on stage...

CUT BACK TO:

23.

INT. ALF’S ROOM - PRESENT DAY

Alf smiles at the memory, sadly.  Then he calls “Ronnie”... 
but it won’t connect.  He tries again.

AUTOMATED VOICE

Your account has been disconnected.

Alf is confused.  Then he hears a CRACKLE from his desk:

RONNIE (O.S.)

Stormshadow?  Stormshadow, do you 
copy?  This is Pubes.  Over.

More confused, Alf opens his desk drawer to find his 
childhood WALKIE-TALKIE.  He picks it up:

ALF (INTO WALKIE)

...This is Stormshadow.  Over?

INT. TAFFY’S ROOM - SAME

Ronnie talks on his walkie-talkie as he looks through a BOX 
OF HIS OLD HIGH SCHOOL STUFF that he pulled from the closet.  
INTERCUT AS NECESSARY.

RONNIE (INTO WALKIE)

Yes!  Whatup, Stormshadow? 

ALF (INTO WALKIE)

Wow, I forgot about these.  I was 
trying to call you to talk about 
how depressing it is being home, 
but I think my phone’s broken.

RONNIE (INTO WALKIE)

Naw, mine’s off too.  We must’ve 
been on Drake’s Framily Plan.    

ALF (INTO WALKIE)

Fuck, I can’t believe his funeral’s 
tomorrow.  

RONNIE (INTO WALKIE)

Right?  It’s so fucked.

Ronnie finds his OLD YALE SWEATSHIRT in the box.  As he pulls 
it out, he spots some OLD PHOTOS OF THE THREE BOYS GROWING UP 
TOGETHER: 3rd grade soccer practice, Aubrey’s bar mitzvah,  
dressed up for prom, etc.  Ronnie leafs through them, sadly.

24.

RONNIE (INTO WALKIE)

Not to be a puss, but I really miss 
him, man.

ALF (INTO WALKIE)

Yeah.  Me too.  

RONNIE (INTO WALKIE)

Puss. 

They laugh a little, sad.  Ronnie lights a cigarette, 
exhaling into a toilet paper roll/dryer sheet thing to hide 
the smell from his parents, like a kid.      

RONNIE (INTO WALKIE)

My parents gave my room to Taffy.  
And are still insane people.   

ALF (INTO WALKIE)

My dad is officially talk show fat.  
But the fucked up part is I can’t 
tell if he’s the saddest guy ever, 
or if he’s got it all figured out.     

RONNIE (INTO WALKIE)

Thank god we only gotta be home a 
few more days.

ALF (INTO WALKIE)

Once they straighten out the 
financial shit, we’re outta here.   

RONNIE (INTO WALKIE)

The fuck out.

ALF (INTO WALKIE)

Hey, til then though, we should 
probably watch our spending a 
little, right?  I only have 500 
bucks left.

RONNIE (INTO WALKIE)
Yeah.  I have like 650, but I 
haven’t checked all my pants.  

ALF (INTO WALKIE)

We can totally make that last.   

RONNIE (INTO WALKIE)

ALF (INTO WALKIE)

Totally.  

Totally. 

RONNIE (INTO WALKIE)

Totally.

25.

SMASH CUT TO:

EXT. ALF’S DAD’S HOUSE - THE NEXT DAY

Ronnie, IN A TEAL LAMBORGHINI, honks in the driveway.   Alf 
walks out, confused, as Ronnie hops out of the car, proud.  

ALF

Whoa, whose is that?

RONNIE

Ours.  Til midnight.  

ALF

Tell me you didn’t spend all your 
cash renting a teal ‘Ghini.     

RONNIE

I did not.  I spent all my cash on 
the deposit, and will require all 
your cash upon its return.

(off Alf’s look)

Bro, Drake would not want us to 
roll up to his funeral looking like 
idiots.

ALF

(beat)

Such a good call!
 

SMASH CUT TO:

EXT. FOREST HILL SYNAGOGUE - LATER

Snow falls as SOMBER MOURNERS (bundled in big coats, gloves, 
hats) file into the synagogue, when... the Lamborghini SLOWLY 
SLIDES AND SLIPS around the corner, PRECARIOUSLY MANEUVERING 
the iced-over street.  

The funeral goers are stupefied as the engine REVS wildly and 
the tires SPIN hopelessly.  It takes A LONG TIME.  

Ronnie and Alf finally make it up into the parking lot, when 
the Lamborghini BOTTOMS OUT HORRIBLY-- it SCRAPES the ground 
for 30 long, embarrassing, expensive seconds, til they park.  

The boys get out of the car and give a sad knowing nod to the 
crowd, when... 

26.

Behind them, the Lamborghini (parked on a slight incline) 
SLIDES SLOWLY SIDEWAYS ACROSS THE PARKING LOT, AS PEOPLE STEP 
ASIDE TO AVOID IT, UNTIL IT SLAMS INTO A CURB.  

Surprised, the boys turn back.  Beat.

ALF

It’s actually a much better 
spot.  

RONNIE

I tried to put a little 
English on it.

The crowd just shakes their heads and resumes entering the 
funeral, when a PASSING WOMAN CATCHES ALF’S EYE...

ALF

Whoa, Courtney Riley?

She stops, sighs, then turns around... it’s Alf’s high school 
girlfriend, COURTNEY (30, even in a big parka she’s GORGEOUS).  
Courtney is not happy to see Alf... is an understatement.

ALF

Wow.  Hey, Court.  It’s me, your 
first love, Alf.  

(winks)

And your first other stuff.  

COURTNEY

Alfonso Bell.  I knew you’d be 
here. 

ALF

...which is why you came?

COURTNEY

Or I came to pay my respects to an 
old friend who passed away 
tragically.  

ALF
Sure you did.

COURTNEY

Okay, I’m gonna head in.  Sweet 
car.  Really appropriate for a 
funeral.  In a blizzard.

(coldly)

Ronnie.

She walks away.  The boys admire her as she goes...

RONNIE

Damn.  Shit stayed together.

27.

ALF

Right?  And is it me, or did it 
feel like she was just about to 
come around on old Alfy?

RONNIE

And the car!

CRAIG (O.S.)

Tell me you guys didn’t buy that 
thing.

They turn to see Craig.

RONNIE

Uh, we’re not idiots, Craig.  It’s 
a rental.

ALF

Yeah, Craig.  God, Craig.

CRAIG

Look, you guys really need to 
conserve your capital.  ...was the 
advice I was trying to give you the 
other day.  Cause I spoke with the 
business manager, and since Drake 
was so young, he never did any 
estate planning.  Everything is 
going to his mother.  

ALF

Good.  She deserves everything.  
Like we said, we’ll be fine, Craig.
Our salaries are more than enough.  

RONNIE

Yeah, our piece of the OVO clothing 
line alone--

ALF

Not to mention our royalties on 
Drizz Body Sprizz--

CRAIG

No, all the beauty and apparel has 
been operating at a loss for years.

(breaking it to them)

Guys, look, they took me through it 
all.  None of Drake’s side 
businesses were profitable.  Your 
“salaries” were coming out of his 
pocket.  He was just paying you 
to... be you.  

The boys are thrown, but try to recover.

28.

RONNIE

Okay, so we’ll just sell that condo 
building we all went in on in Kauai--

CRAIG

Underwater.  From a mortgage 
perspective, and from a bottom two 
floors look like an aquarium due to 
Hurricane Sandra perspective.  

ALF

But... we’re still the co-CFO’s of 
Drake’s Flakes, right?

CRAIG

I would not advertise that too 
loudly.  They’re being recalled.   

ALF

(spinning out)

Recalled by cereal historians as a 
gangster way to start your day?

Craig shakes his head no.  It starts to finally sink in for 
the boys. 

RONNIE

(desperate)

...My Beats by Drake idea?

CRAIG

Was never pursued.  Because of 
Beats by Dre.  Which is where you 
got the idea.  While stoned. 

ALF

So what exactly are you saying, 
Craig?

RONNIE

Yeah, I am not following, Craig.

CRAIG

There’s nothing more coming to you.  
Nothing.  Unless you wanna ask 
Drake’s mom for money...

He nods toward DRAKE’S MOM, SO DESTROYED as she gets out of a 
limo, leaning on the RABBI for support.  Alf and Ronnie GLARE 
at Craig.  Craig’s been waiting for this a long time:

29.

CRAIG

But I’m sure you guys are gonna be 
fine.  Judging by the fact that you 
rented a supercar, it’s obvious 
you’ve saved and invested your 
money wisely over the years.  
Right...?

Alf and Ronnie look at each other, then--

SMASH CUT TO:

INT. SYNAGOGUE - LATER

ALF AND RONNIE CRY THEIR EYES OUT UNCONTROLLABLY in the front 
row of the funeral.  Ugly crying.  They can’t catch their 
breath.  Their lives are fucked on every possible level.    

At the podium, Nicki Minaj begins SINGING “Shema Yisrael” in 
Hebrew.  It’s heartbreaking.  

As Ronnie and Alf wonder what the fuck they’re gonna do now...

CUT TO BLACK.

