FAST LAYNE

"Pilot"

Written by
Travis Braun

Second Draft
10/6/17

TEASER

INT. LAYNE’S HOUSE/LAYNE’S ROOM - MORNING

We PAN through a meticulously arranged room -- color-coded 
closet, alphabetized bookshelf, a poster that reads “Don’t 
Panic, Organize!” and land on a girl, asleep, not a wrinkle 
on her bed. She even sleeps neat.

LAYNE (V.O.)

You ever feel like there just 
aren’t enough hours in the day?

Her SMARTWATCH beeps. It’s 5:00am. Up pops LAYNE REID (12 
going on 35), ready to tackle the world. She beelines for her 
computer and pulls up her CALENDAR. A rainbow of colors. 
Every moment scheduled. Layne smiles, satisfied.

LAYNE (V.O.)

Not me. I think there are exactly the 
right amount.

INT. LAYNE’S HOUSE/FOYER - MOMENTS LATER

Layne heads for the door as her parents, ROB and CHERYL (40s, 
sweater vests, glasses, smartwatches, total tech junkies) 
help pack her up. In a choreographed sequence, they hand 
Layne her bookbag, volleyball gear, science diorama, flags 
for color guard. It’s all methodical. Precise. Planned.

LAYNE (V.O.)

As long as you’re scheduled, 
organized and have an adequate 
supply of energy bars...

They give her a kiss and a handful of energy bars.

LAYNE (V.O.)

...I firmly believe that you can do 
it all...

QUICK CUTS OF LAYNE

--At math club. Layne solves a giant equation on a white board.

--At choir practice. Layne hits an impossibly high note.

--At Model UN. Layne gives a speech in Japanese wearing a 
kimono. She bows.

--As a crossing guard. Layne helps a KID cross the road.

--Under a banner that reads “Clogging Society”, Layne clogs 
next to an OLD MAN and a LITTLE BOY.

EXT. SCHOOL COURTYARD - DAY

We FIND Layne at a podium, and realize this is an address to 
an assembly of her sixth-grade class.

2.

LAYNE

You might even say ‘scheduling’ is 
my jam.

Layne giggles, to herself. No one reacts except for PRINCIPAL 
MAURICE MUGBEE (40s), who claps loudly.

Layne’s SMARTWATCH vibrates. The voice reminds her --

SMARTWATCH

Time for closing remarks.

LAYNE

So...vote for me and I promise to 
be an organized, dependable Student 
Body Supervisor.

(slogan)

With Layne Reid you get no surpri--

A ROARING ENGINE cuts Layne off. Kids look around just as a 
high-tech SUPER CAR shoots past on the street behind Layne. 

LAYNE (CONT’D)

(repeating)

You get no surpri--

Another ROARING ENGINE, as a BLACK SUV rockets past in HOT 
PURSUIT of the super car, interrupting her again.

LAYNE (CONT’D)

(yells)

NO SURPRISES!!!

This time it’s quiet. Layne’s voice booms over the speakers 
with a SCREECH OF FEEDBACK. Everyone grabs their ears.

LAYNE (CONT’D)

(politely)
...thank you.

Off Layne, slightly embarrassed, we transition to our MAIN 
TITLES.

END OF TEASER

ACT ONE

3.

INT. LAYNE’S HOUSE/KITCHEN - DAY

The kitchen has been turned into a full-on Layne Reid 
campaign headquarters. Posters, flyers, buttons, hats. We 
FIND Layne, Rob and Cheryl at the island bar, sorting t-
shirts with the slogan “Layne Reid: No Surprises!”

LAYNE

And then, out of nowhere, these two 
cars roared through and messed up 
the ending.

CHERYL

Oh, sweetie. I’m sure your speech 
was great.

ROB

Absolutely. And you can’t plan 
everything.

On cue, all three of their smartwatches go off.

SMARTWATCHES

It is 6:00pm. Time to start dinner.

ROB/CHERYL/LAYNE
(to their smartwatches)

Thanks, Alonzo.

CHERYL
(with a smile)

Falafel Friday.

Cheryl heads to the stove. Rob pulls out the food processor. 
Layne sets the table. Efficient. Just like their morning.

A STRANGE NOISE comes from next door. They look around...

ROB

Do you guys hear that?... Sounds 
like someone’s stepping on a goose.

Layne peers out the window and spots ZORA CONLEY (12) sitting 
on her roof, playing a DIDGERIDOO. She’s as bizarre as her 
mismatched PJs.

LAYNE

Oh. It’s Zora.

CHERYL
(shaking head)

That’s what happens when you home 
school.

4.

LAYNE

I hope she’s not playing that all 
weekend. I’ve got a lot to do.

CHERYL

About this weekend... your father 
has something to tell you.

Rob and Cheryl exchange nervous glances.

ROB

Yes, well. I um... I know you don’t 
like surprises, but we have to go 
out of town tomorrow.

LAYNE

What? Where?

CHERYL

Your father’s been selected to be 
the Keynote Speaker at Transistor 
Con.

ROB

It’s a big electronics convention. 
Don’t worry, it’s just for the 
weekend, and we called Aunt Bet --

LAYNE

No, no, no, no. Not Aunt Betty.

CHERYL

What’s wrong with Betty?

LAYNE

He’ll be glued to me all weekend.

It’s not Betty, it’s cousin Mel.

(exasperated)

INT. LAYNE’S HOUSE/LAYNE’S ROOM - FLASHBACK

Layne, focused, types on her computer. We hear a CHEWING 
NOISE. She slides her chair back, revealing MEL (10, devious, 
a shirt that reads Thug Lyfe) eating a hamburger.

Do you have to eat there?

LAYNE

MEL

It’s a free country.
(squirts ketchup)

You’re out of ketchup.

5.

INT. LAYNE’S HOUSE/KITCHEN - DAY (BACK TO PRESENT)

As before.

CHERYL

Look, they’re family. It’s only two 
days. We know you’ll make the best 
of it.

Layne takes a breath, sucking it up.

LAYNE

Okay, I’ll figure it out. Any more 
surprises I need to know about?

ROB

No, honey. We promise. No more 
surprises.

Right then, Rob SWITCHES on the food processor. The LID shoots 
off. Falafel ingredients fly everywhere.

INT. LAYNE’S HOUSE/LAYNE’S ROOM - THE NEXT DAY

We FIND Layne at her desk, gluing campaign labels to bottles 
of water as her printer churns out flyers. Suddenly, a TAP! 
TAP! TAP! echoes through her room. Layne looks up and spots --

Zora rapping on the window. Layne crosses over and opens it. 
They talk through the window.

ZORA

Good morning.

LAYNE

Uh, hi...Zora. It’s 2pm. And most 
people use the front door, but this 
is... memorable.

I’ve got something big to tell you.

ZORA

LAYNE

Is this about that dead lizard you 
found?

ZORA

Bigger. I don’t want to say life 
changing, but it’s up there. I was 
listening to my police scanner, and 
a truck just jackknifed on Highway 
83.

LAYNE
That’s horrible.

6.

ZORA

(solemn)

Yeah... I know.

(then, bright)

But completely awesome because the 
truck is from Duke’s Bakery and was 
full of red velvet cupcakes that 
are now everywhere. Let’s go scoop 
‘em up!

Zora holds up a snow shovel.

LAYNE

Zora, I appreciate you thinking of 
me. And I do love red velvet, but 
I’ve got a full schedule.

ZORA

Of course. You’re always busy. No 
time for fun.

LAYNE

(defensive)

Uh, my life is very fun, thank you. 

SMARTWATCH

Time to reload the printer.

ZORA

Riiigght. I’ll uh... just go by 
myself.

(chuckles)

Don’t get too crazy in there.

Zora turns to leave. Suddenly, from in the distance we hear --

MUFFLED VOICE (O.S.)

Hey?!

Zora stops. The girls share a look.

ZORA

You hear something?

MUFFLED VOICE (O.S.)

Anybody out there?!

We SNAP ZOOM to a dilapidated SHED next door.

MUFFLED VOICE (O.S.) (CONT’D)

Help! Help me! 

ZORA

It sounds like someone needs help.

7.

LAYNE

What gave that away?

EXT. NEIGHBOR’S HOUSE - DAY

Layne and Zora stare at the shed in the backyard.

ZORA

There’s someone in there.

LAYNE

(re: the house)

Nobody’s lived over here for years.

ZORA

You gonna get your parents?

LAYNE

They left this morning for a 
convention. And my aunt won’t be 
here for an hour.

A loud BANG comes from the shed.

MUFFLED VOICE (O.S.)

(in pain)

Agrrrhh!

ZORA

Somebody should go in there.

LAYNE

Yeah somebody should.

ZORA
You’re taller.

Off Layne, steeling herself --

INT. SHED - MOMENTS LATER

The door CREAKS open. Layne gazes into the darkness. She 
flips on the light switch. Nothing happens.

LAYNE
(to herself)

I hate creepy sheds.

Zora pops in next to Layne.

ZORA

Did you ever see the movie Creepy 
Shed?

LAYNE

Is this really the time?

8.

ZORA

You’re right. Sorry.

(then)

They made like seven of them. But 
the guy with no face who kills 
people with a pitchfork was only in 
the first three.

They look around. Nothing but old lawn equipment. Just then, 
a loud metallic BANG! comes from below them. Layne and Zora 
both jump back, freaked out. 

LAYNE

Uh, Zora, are you holding my hand?

ZORA

Just making sure you’re okay.

Zora pulls her hand away. Layne crouches down over an OIL 
STAIN, listening. As she does, her hand CATCHES the EDGE of 
the stain. It’s actually a STICKER. She peels it back, 
revealing a GLOWING LED SWITCH.

LAYNE

Don’t touch that.

Without hesitation, Zora flips the switch. A loud BUZZER 
wails, frightening them.

LAYNE (CONT’D)

I asked you not to touch it.

ZORA

Home school teaches you to be an 
independent thinker.

THE WHOLE FLOOR SUDDENLY STARTS TO DESCEND LIKE AN ELEVATOR. 
Down... Down... Down...

LAYNE
(wigged out)
Oh my goodness!

This happened in Creepy Shed 4!

ZORA

The floor finally comes to a STOP. They’re now in --

INT. UNDERGROUND WORKSHOP - CONTINUOUS

An impeccably tidy WORKSHOP, complete with STAINLESS STEEL 
BENCHES, shelves of HIGH-TECH HARDWARE. 

In the middle of the workshop is a hulking OBJECT covered by 
a tarp. Layne and Zora enter, cautious.

9.

ZORA

Man...we’ve been living next to a 
secret underground workshop?

LAYNE

Hello?... Hello...?

No answer. Layne and Zora look around.

ZORA

Nobody’s down here but us.

LAYNE

Then whose voice did we hear?

Something from under the tarp shifts. Layne and Zora exchange 
a nervous glance. Layne slowly approaches. She grabs the 
corner of the cover. Then bravely YANKS it off, revealing a 
STATE-OF-THE-ART SUPER CAR.

Suddenly, the car comes to life. The LED headlights blink 
like EYES. The high-tech grill moves like a MOUTH.

CAR

What took ya guys so flippin’ 
long?!

LAYNE/ZORA

AHHH!!!

Off the girls, screaming --

END OF ACT ONE

ACT TWO

10.

INT. UNDERGROUND WORKSHOP - SAME TIME

As before. Layne and Zora stare at the car.

LAYNE

The car... Just talked to us.

ZORA

(way too loud)

Hello futuristic speaking car!

CAR

She’s funny. Name’s Rev.

Rev’s grill moves as he talks. He’s enthusiastic, full of 
bravado, a total smart ass.

Layne and Zora take in Rev’s wings, scoops, high-tech 
sensors.

REV

(then)

Go ahead. Take your time. I know 
I’m a lot to drink in.

And while you’re at it, you mind 
poppin’ that boot off? Man, it’s 
itchy!

Reveal: Rev has an ORANGE BOOT bolted to his left rear wheel. 
He drives back and forth, trying to get it off. BANG! BANG!

LAYNE

Zora, I’m serious this time, don’t 
touch anything. We’re getting outta 
here.

REV

Hold on! You can’t leave me down 
here! Who knows when they’ll be 
back to unlock me.

ZORA

Who’s ‘they’?

REV

The two dudes who built me.

LAYNE

This house is vacant... This makes 
no sense.

11.

ZORA

No, I’ve heard about this. Dudes 
using abandoned sheds to hide 
secret cars. It’s a thing.

LAYNE

It’s not a thing... I’m calling my 
parents.

Layne pulls out her phone. Dials. As she waits, Zora moves 
her finger back and forth in front of Rev. His headlights 
follow it.

ZORA

(to Rev)

So do you have a tongue in there?

LAYNE (INTO PHONE)

Mom. Dad. Something really weird’s 
going on next door. Call me ASAP! 
Luv you bye.

Layne hangs up, flustered.

REV

Alright, I’ll make you two a deal. 
Pop off my boot, and I’ll take you 
both for a cruise.

ZORA

(excited)

You drive, too?

REV

Yep! So how ‘bout it? Wind in our 
hair. Well, your hair. I don’t have 
hair. Check out this carpool lane I 
keep hearing about. Who’s in?

ZORA

(to Layne)

Hey, he can take us to the cupcake 
truck!

REV

Totes! Cupcake truck. Carpool lane. 
Hit up a drive-thru. Let’s just 
wing it.

LAYNE

We’re not wingin’ it!

ZORA

(to Rev)

She’s not a ‘winger’.

12.

LAYNE

Zora, this isn’t our car. We 
shouldn’t even be in here!

REV

Uh-oh.

Just then, Rev’s headlights start to fade.

ZORA
What’s ‘uh-oh’?

REV

My solar panels haven’t charged my 
CPU... Battery dangerously low... 
Need sun...

ZORA

Layne, we gotta do something. Rev’s 
dying!

REV

(gasping for air)

Please... green switch...

Zora looks over. Spots a green switch on the wall.

LAYNE

Zora, don’t! It could be a trick!

Too late. Zora flips the switch. A loud BUZZER wails. Rev’s 
boot UNLOCKS with a CLUNK! The whole floor starts to raise up.

Off Rev’s grill, his faded lights turning into a devious 
smile --

EXT. NEIGHBOR’S HOUSE - MOMENTS LATER

We FIND Rev rolling out of the shed, flanked by Layne and 
Zora. He CRANKS his engine. Blasts the throttle. VROOM!!!!

REV

Woooohooo!!!

ZORA

Man, he totally tricked us.

LAYNE

Us?

REV

(to Zora)

Don’t beat yourself up. You’re 
dealing with the best. 

(MORE)

13.

REV (CONT'D)

Next stop, carpool lane! I hear 
it’s super exclusive.

LAYNE

No way. I’m not gonna be 
responsible for letting you loose. 
We’re shutting you down.

Layne opens the driver’s door and climbs in --

INT. REV - CONTINUOUS

Layne stares at the DASHBOARD. It’s blank. No steering wheel. 
No ignition. No pedals. 

LAYNE
Where’s the key?

Zora climbs into the passenger seat next to her.

ZORA

Where’s the steering wheel?

REV

(scoffs)

You’re inside the most 
sophisticated driving machine in 
the world.

Seat belts FLY out around Layne and Zora.

REV (CONT’D)

Cue tire squeal!

On cue, Rev PEELS OUT, tires squealing. Their doors SLAM shut.

I/E. REV (MOVING) - MOMENTS LATER

Rev cruises down the street. Layne tries not to panic. Zora 
is having a blast.

LAYNE

Okay, this is crazy!

ZORA

(excited)

I know, right?!

LAYNE

Rev, I’m going to ask this 
nicely...please turn around right 
now. I’ve got flyers to print, 
posters to paint, and my Aunt Betty 
is gonna be here in exactly...

(MORE)

LAYNE (CONT'D)

(checking watch)
Twenty-one minutes.

REV

So I’ll have you back in nineteen! 
Boom!

14.

They pass a MAN watering his lawn with his back to us. Zora 
leans out the window, excited.

ZORA

(yelling, excited)

Hey we’re 12 and we’re drivin’!

The man turns around. Layne sees that it’s Principal Mugbee. 
She leans out next to Zora.

LAYNE

Principal Mugbee! She means the car 
is driving! It’s all very safe! 
Have a nice day!

(to Rev, concerned)

We are safe, aren’t we?

REV

‘Course we’re safe. Relax. I’ll put 
on some jams. Whadda ya like?

ZORA

Daz Duckworth and the Mighty Cheedo 
Band!

LAYNE

(rolling eyes)

Oh can’t wait to hear this.

A soft, sweet song starts playing on Rev’s speakers (think 
Jason Mraz).

LAYNE (CONT’D)

(to herself)
Oooh. Not bad.

Layne taps her hand to the music -- actually kind of enjoying 
herself.

Rev pulls to a stoplight, next to a souped-up MUSCLE CAR. The 
DRIVER (30s, ponytail, sunglasses) shoots her a weird look.

LAYNE (CONT’D)

(nervous smile)

Oh, hello. Beautiful day, huh?

PONYTAIL DRIVER

(scoffs)

Nice golf cart.

The driver REVS his engine. Rev HITS his throttle, 
responding.

15.

LAYNE

(to Rev)

What are you doing?

ZORA

I think he wants to race.

REV

Probably trying to beat me to the 
carpool lane!

(then, to driver)

Ain’t gonna happen, Ponytail!

PONYTAIL DRIVER

(glares at Layne)

What’d you say to me?

LAYNE

Uh, nothing. All good here.

Ponytail Driver revs his engine again.

PONYTAIL DRIVER

You’re on!

LAYNE

No, we’re definitely not ‘on’.

OUTSIDE: The light changes and Rev and the muscle car PEEL 
OUT! They STORM down the street, swapping positions... 

INSIDE: Layne and Zora hold on tight.

LAYNE (CONT’D)

Rev, slow down. I’m law abiding. An 
honor student! A hall monitor!

Ponytail Driver pulls next to Layne’s window and scowls.

PONYTAIL DRIVER

You’re goin’ down, little girly!

LAYNE

(offended)

Little girly? Excuse me?

The muscle car pulls ahead. Insulted, Layne calls after him --

LAYNE (CONT’D)

We’re not finished here, Mr. Rude!

(annoyed)

Rev. Can you go any faster?

16.

Pfft! Strap in!

(then)

REV

Always wanted to say that.

OUTSIDE: Rev WHIPS from side-to-side, looking for a way past 
the muscle car. Finally seeing an opening, he goes FULL 
THROTTLE. He ROCKETS past. Ponytail Driver stares out his 
windshield, stunned.

INSIDE: Layne and Zora look back at him out the rear window. 
Layne turns around, satisfied.

Well, he got what he deserved.

LAYNE

REV

Yeah, I blew his doors off!

ZORA

Yeah you did! Now can you pull over 
so I can barf?

Off Zora, looking queasy --

EXT. SERVICE STATION - MOMENTS LATER

Rev sits in a parking spot. Layne waits beside him.

SMARTWATCH

Aunt Betty arriving in fifteen 
minutes.

LAYNE

Thank you, Alonzo.

REV

Wow. Alonzo. I’d love to party with 
that guy on a Friday night.

LAYNE

In spite of your abrasive 
personality, your programming is... 
pretty remarkable. And you have no 
idea what you were built for?

REV

To be honest. They won’t tell me. 
But I have a feeling it’s really 
big!

LAYNE

Like what?

CODY (O.S.)

Hey, what’s goin’ on here?

Reveal CODY CASTILLO (12, ripped jeans, greasy hands, million-
dollar smile) approaching from a garage bay behind them.

17.

LAYNE

(covering)

Oh, hi, nothing.

CODY

From over there, it looked like you 
were talking to this car.

LAYNE

Talking to a car? That’s a good one. 
Only a weirdo would talk to a silly 
car.

Layne giggles nervously. A little too long. Then stops, 
awkwardly.

CODY

O-kay...

Cody peers into Rev’s cockpit. Sees the blank dash.

CODY (CONT’D)

Is this one of those self-driving 
cars? Increíble...

REV
Gracias amigo!

Layne gives Rev a kick. Cody looks at Layne -- who said that? 

LAYNE

(covering)

Yes, uhhh, gracias for checking on 
me, amigo. But really, everything’s 
fine here. 

ZORA
(walking up)

Whew, sooo much better. I feel for 
whoever’s gotta clean up that 
bathroom, though.

Layne smiles, awkwardly.

MR. CASTILLO (O.S.)

Cody! Baño limpio! Rápido!

MR. CASTILLO (60) waves for Cody to come back to the shop.

CODY

18.

(sighs)

Grandpa wants me. But if you ever 
need help with this car, lemme 
know. Name’s Cody.

LAYNE

I’m Layne. This is Zora.

CODY

Nice to meet you.

(one last look at Rev)

Muy caliente!

Cody walks off, impressed. Layne breathes a sigh of relief. 
Zora’s eyes stay fixed on Cody.

ZORA

Speaking of muy caliente... Think 
he’s into home-schoolers?

I/E. REV (MOVING) - DAY

Rev back on the road. Layne and Zora in the front.

ZORA

Okay, next stop -- cupcake heaven! 
I’m starved!

LAYNE

Sorry, Zora, this little 
adventure’s over. We’re goin’ home. 
My day’s way off schedule.

ZORA

You and your schedule! It’s 
ridiculous. Tell her, Rev.

I’m stayin’ out of this.

REV

ZORA

Seriously, I’ve lived next to you 
for a year it’s like every second 
has to be planned.

LAYNE

Well I’ve lived next to you for a 
year, and every day’s weirder than 
the next. You know, there’s nothing 
wrong with a little structure in 
your life. As my dad says, busy 
hands are happy hands.

19.

ZORA

Well my dad says, when in doubt, 
hang it out.

REV

Oh, that’s awesome! What’s it mean?

LAYNE

I thought you were staying out of 
it.

Just then, an ALERT flashes on Rev’s display: “DANGER”.

LAYNE (CONT’D)

Uhh, Rev, what’s this danger light 
mean?

REV

Not sure, but I’m guessing... 
danger.

ZORA

(looking in mirror)

Guys... Look...

The BLACK SUV from the Teaser approaches, driven by a hulk of 
a man, RIGGINS (42, scruffy, denim jacket). Next to him is 
DEAN (36, tailored suit, Rolex. Think Ryan Gosling).

The SUV suddenly RAMS Rev. The girls scream.

LAYNE

(worried)

I guess this little adventure isn’t 
over.

As Rev hits the gas --

END OF ACT TWO

ACT THREE

20.

EXT. STREETS - DAY

Rev RACES past with the SUV on his bumper. The SUV RAMS him.

INT. REV (MOVING) - DAY

Layne and Zora are thrown forward in their seats.

REV

Hang on! I know these guys and 
they’re not here to give us a fruit 
basket.

LAYNE

You know these guys?

REV

I was out yesterday and they tried 
to steal me. It was a mess, they 
chased me all through town.

LAYNE

(figuring it out)

That was you! You ruined my speech!

REV

I ruined a lot of things. A bar 
mitzvah. Two weddings. A funeral. 
These guys really want to get their 
hands on me.

ZORA

Why?

The SUV rams Rev again!

LAYNE

I’m sure it’s his AI. It’s gotta be 
worth a fortune.

Rev hangs a sharp RIGHT into an alley. The SUV barrels past, 
missing the turn.

INT. SUV (MOVING) - SAME TIME

Riggins slams on the brakes.

DEAN

That car‘s not getting away this 
time. Back up!

21.

RIGGINS

Would it kill ya to say ‘please’?

DEAN

Please... stop being so sensitive 
and drive! 

Frustrated, Riggins throws the car in reverse --

I/E. REV (MOVING) - SAME TIME

Rev blasts through the alley and out onto another street. 
Layne and Zora look out the rear window. No sign of the SUV.

ZORA

Looks like we’re in the clear.

Layne glances out the side window as they pass a beat-up 
Camry. She spots AUNT BETTY (30s) in the driver seat. Her 
son, Mel, is in back playing a video game on his cellphone.

LAYNE

It’s my Aunt Betty!

Layne ducks, pulling Zora down with her.

INT. AUNT BETTY’S CAMRY - SAME TIME

Aunt Betty drives, oblivious, with her EAR BUDS in at full 
volume. Mel glances over at the futuristic car passing them. 
There seems to be no one inside.

Mel stares closer at the car. Spots the back of Layne’s head. 
A BLUE BARRETTE in her hair. Off Mel, wheels turning --

I/E. REV (MOVING) - SAME TIME

INSIDE: Layne and Zora pop up as they clear the Camry.

LAYNE
That was close.

Dean and Riggins PULL back onto the road behind them.

ZORA

Scary dudes are back.

REV

I’ll handle this!

OUTSIDE: Rev makes a sharp turn onto a DIRT ROAD. The SUV 
follows them, then SLAMS INTO REV’S BUMPER AT FULL SPEED!

INSIDE: Layne and Zora scream. The interior lights FLICKER. 
An ALARM sounds.

22.

LAYNE

Rev, get us outta here!

Rev veers dangerously back and forth across the dirt road.

Rev? What are you doing? Rev...?

ZORA

REV

(suddenly, singing)

Figaro! Figaro! Figaro!

LAYNE

Oh no, I think he’s shorting out.

REV

(shorting out)

I’ll take three cheeseburgers, a 
diet soda and -- Yigè dà yúmiáo! -- 
Figaro! Figaro! Figaro!

Just then, a SMALL DOOR slides open in the center console, 
revealing a HAND SCANNER. An ALERT appears on Rev’s display: 
“Manual Override Requested.”

LAYNE
Manual override?

Zora tries her hand. The scanner BEEPS and flashes RED.

ZORA

Doesn’t like me.

(to Layne)

Your turn.

LAYNE

Zora, it’s not gonna work! See!

Layne puts her hand on the scanner to prove it. Surprisingly, 
it flashes GREEN. Layne and Zora exchange glances as --

A high-tech STEERING YOKE and PEDALS suddenly appear in front 
of Layne. A panel of GLOWING BUTTONS, SWITCHES and READOUTS 
appear in front of Zora. The girls take in the cockpit, 
stunned.

LAYNE (CONT’D)

Okay, that’s weird. Why would my 
handprint unlock all this?

ZORA
I dunno. Drive!

23.

LAYNE

I can’t drive, I’m in sixth grade!

ZORA

You don’t have a choice. You’re 
gonna have to wing it!

Zora motions out the window. They’re headed straight for a 
FENCE!

LAYNE

(grabbing steering yoke)

I told you I’m not a winger!

Layne YANKS the yoke, swerving just in time to miss the 
fence. Whew! But now they’re headed straight for an 
embankment --

OUTSIDE: Rev drifts onto the embankment, turning onto two 
wheels!

INSIDE: Both girls scream as they’re squished, shoulder-to-
shoulder! Layne JERKS the yoke, steering them back onto four 
wheels and onto the road. They sit there for a beat, stunned.

ZORA

So cool!
(then)

Please don’t do it again.

OUTSIDE: The SUV pulls right behind them. Up ahead, a 
construction barricade blocks off the remains of a WOODEN 
BRIDGE spanning over a creek.

INSIDE: Layne and Zora exchange nervous glances.

LAYNE

The bridge is out.

ZORA

(re: the gap)

We can’t stop. They’re on our tail. 
We’re gonna have to jump it.

Layne glances at the speedometer, then eyes the bridge. 
Shakes her head.

LAYNE

Jump it? Are you crazy? We’re not 
going fast enough.

ZORA

I think I can help with that.

Zora motions to a switch on the dash labeled “BOOSTERS”.

LAYNE

Boosters? Zora, wait!

Zora freezes. We push in on Layne, what to do?

24.

ZORA

...give me the word.

LAYNE
(action star)

Do it!

Zora flips the switch. Layne and Zora are THROWN back in 
their seats as Layne works the yoke.

REV

FIGARO! FIGARO! FIGAROOOOO!

OUTSIDE: Rev SMASHES through the barricade, hitting the 
bridge and LAUNCHING into the air. We go SLO-MO as Rev SOARS 
over the creek. His rear wheels clear the gap BY AN INCH. 
Behind them, Dean and Riggins are forced to come to a 
screeching halt just before the bridge.

INSIDE: Rev SLAMS back onto the ground HARD, skidding to a 
stop. ON Layne, chest rising, hands clenched tight on the yoke.

ZORA

You okay?

Suddenly, a broad smile creeps onto Layne’s face.

LAYNE

That was awesome!

Rev’s interior lights stop flickering.

REV

Hey dudes. What’d I miss?

ZORA

Rev. You’re back! Layne ditched the 
scary dudes! It was incredible.

Layne smiles, then spots something out the window.

LAYNE

Zora. Look...

OUTSIDE: We REVEAL that they’re parked next to Highway 83! 
The overturned cupcake truck lies off in the distance. 
Cupcakes are strewn everywhere!

(drooling)

The cupcake truck.

ZORA

(MORE)

ZORA (CONT'D)

(to Layne)

Whadda ya say? We have two minutes 
to grab some red velvets?

25.

LAYNE

I think I can fit that into my 
schedule.

The girls share a smile. As they hop out and rush off towards 
the cupcakes, we linger on Rev. He spots a sign that reads 
CARPOOL LANE.

REV

Hey...the carpool lane!

Rev looks around. Nothing but pavement for miles.

REV (CONT’D)

Wait. What kind of pool has no 
water?!

(then, realizing)
Ahhhh, I’m an idiot.

I/E. LAYNE’S HOUSE - DAY

Aunt Betty impatiently rings the doorbell. Mel sits on the 
steps, playing a video game on his phone.

The door flings OPEN. It’s Layne, out of breath and sweaty.

LAYNE
Aunt Betty! Hi.

AUNT BETTY

Where have you been? I was about to 
call your parents.

LAYNE

Sorry. Upstairs... studying.

Aunt Betty eyes her for a beat, suspicious.

AUNT BETTY

Why are you drenched in sweat?

LAYNE

Intense session. You know how I get.

AUNT BETTY

Yes. We know. Well, Mama needs an 
herbal bath and a bucket of wings.

Aunt Betty heads inside. Layne turns. Mel is standing right 
there. He eyes her blue barrette.

26.

MEL

I saw you in that car.

LAYNE

What? What are you talking about?

MEL

I don’t know what you’re up to. But 
I’m gonna find out. And then I’m 
gonna crush you.

LAYNE

Nice to see you, too, Mel.

Mel smiles menacingly and heads off. Layne slips outside --

INT. UNDERGROUND WORKSHOP - DAY

Layne and Zora roll Rev back into his stall.

REV

Whoo-eee, that was a pretty solid 
day. But I say tomorrow we ratchet 
it up a bit, huh?!

Layne straps on Rev’s boot. CLUNK!

REV (CONT’D)

What are you doing?

LAYNE

Putting you back where you belong. 
We had our fun. And I’ve had enough 
surprises for a lifetime.

As she fastens the boot, we see Zora staring at Rev’s tail.

ZORA

Uh, Layne, you’re gonna want to see 
this. I think Rev hurt his butt.

Layne peers over and sees Rev’s exhaust dangling off.

LAYNE

(noticing)

Oh, he’s leaking fluid.

REV

I am?! Rob and Cheryl are not gonna 
be happy about this!

This hits Layne like a bolt of lightning.

LAYNE
Rob and Cheryl?

27.

REV

Yeah, I’m their baby.

LAYNE

Wait. Rob and Cheryl are the two 
dudes that built you?

Rev SHINES his headlights at a PHOTO on a workbench. Layne 
heads over and finds a SELFIE of Rob and Cheryl on either 
side of Rev. Only, their sweater vests and glasses have been 
replaced with LABCOATS and GOGGLES.

Layne stares at the photo, frozen.

ZORA

Your parents built Rev? That means 
you guys are kinda like brother and 
sister!

LAYNE

(ignoring Zora)

My parents run a tiny electronics 
store. Why would they hide 
something like this from me?

ZORA

Well, where are they anyway?

LAYNE

Some convention called Transistor 
Con.

Off Layne, suspicious --

INT. LAYNE’S HOUSE/LAYNE’S ROOM - MOMENTS LATER

Layne is at her computer, running a search. Zora stares over 
her shoulder.

LAYNE

(realizing)

...There’s no such thing as 
Transistor Con.

ZORA

This is gettin’ freaky.

Just then, Layne’s phone rings. She checks it.

LAYNE
It’s my parents.

Layne and Zora share a look, unsure what to do. She finally 
answers.

28.

LAYNE (INTO PHONE) (CONT’D)

(stunned)
Hey, Mom...

CHERYL (ON THE PHONE)

Hi, sweetie. We got your message. 
We just made it to Transistor Con. 
Everything okay with you?

LAYNE

(stunned)

Ah yeah, fine.

CHERYL (ON THE PHONE)

Great. Well, we’re running into a 
seminar right now. Call you 
tomorrow. Love you!

LAYNE

Love you, too.

Layne hangs up, speechless.

ZORA

I don’t think your parents are who 
you think they are.

LAYNE

(nodding)

Until I figure this out, this stays 
between us.

We PUSH IN on Layne, realizing that the mystery, and the 
adventure, is just getting started --

INT. ND HALLWAY - SAME TIME

Cheryl hangs up the phone. Rob is next to her. 

ROB

Everything okay?

Cheryl nods. Rob smiles. They head down the hallway to a white 
door. A RETINA SCANNER scans their eyes. The door RISES up.

Rob and Cheryl walk into a BUSTLING LABORATORY filled with 
STATE-OF-THE-ART TECHNOLOGY. A LAB TECH approaches them.

LAB TECH

Agent Reid. Agent Reid. Welcome back.

As the white door lowers back down, closing us off from this 
world, and Layne’s parents, we SMASH TO BLACK.

END OF EPISODE

