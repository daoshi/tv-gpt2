The Brainy Bunch

"Pilot"

Lizzie Molyneux & Wendy Molyneux

by

Imagine Television
20th Century Fox Television

2nd Revised Network Draft
  January 23, 2015

ALL RIGHTS RESERVED.  COPYRIGHT © 2015 TWENTIETH CENTURY FOX 
FILM CORPORATION. NO PORTION OF THIS SCRIPT MAY BE 
PERFORMED, PUBLISHED, REPRODUCED, SOLD OR DISTRIBUTED BY ANY MEANS 
OR QUOTED OR PUBLISHED IN ANY MEDIUM INCLUDING ON ANY WEBSITE, 
WITHOUT THE PRIOR WRITTEN CONSENT OF TWENTIETH CENTURY FOX FILM 
CORPORATION. DISPOSAL OF THE SCRIPT COPY OR REMOVAL OF THIS NOTICE 
DOES NOT ALTER ANY OF THE RESTRICTIONS SET FORTH ABOVE.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

1.

FADE IN:

COLD OPEN

INT. BLACKHAWK HELICOPTER - DAY

Sound of chopper blades. We’re C/U on a guy in his 40s.

KIP (V.O.)

Greetings, America. That sweet 
piece of tail right there is yours 
truly, Special Forces Operations 
Sergeant Kip Mitchell. 

Kip rappels out of the Blackhawk.

KIP (V.O.)

Until recently, this was my life. 

INT. MAKE-SHIFT ARMY BASE

A training exercise. JENKINS sneaks around a barricade and 
surprises Kip. He raises his hands and sets his weapon down, 
then suddenly springs up, takes Jenkins down and disarms him.

JENKINS

But you surrendered!

KIP

Incorrect. I pretended to 
surrender!

KIP (V.O.)

As part of our nation’s defenses, 
I’ve been here and here and here.

On a MAP, arrows point to Iraq, Darfur, and the Yukon.

KIP (V.O.)

That one was to rescue some Caribou 
that got stranded on an ice floe. A 
fearsome beast in the wild, the 
Caribou can be a real idiot if 
removed from his home turf.

Kip floats in a rescue boat alongside several Caribou tied to 
a post. Kip removes his cap and hangs it on an antler.

KIP

But no matter where I’ve been on 
God’s green marble, I’ve always 
taken my right hand man with me.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

2.

EXT. AFGHANI QALLA

An American lady in her 40s feeds a goat in a courtyard. 

KIP (V.O.)

There she is. MonaLisa Mitchell, my 
wife. She’s got a mysterious smile, 
a Ph.D. in literature, and a world-
class vajayjay, which may explain 
the fact that we have seven kids. 
And through some combo of genetics 
and MonaLisa’s teaching style, they 
all turned out super-smart.

INT. HUT IN SUDAN - SOMETIME IN THE PAST

Christmas. Kip and MonaLisa unwrap a huge contraption.

MATTHEW

It’s a Turing Machine. We only made 
one so you’ll have to share.

KIP (V.O.)
We’ve got the twins.

INT. QALLA

MARISSA and MATTHEW. She’s beautiful, he’s a chubby nerd. 
They jury-rig a computer from a TV, old typewriter, and a hot 
dog phone.

Phoebe. Our budding psychiatrist.

KIP (V.O.)

PHOEBE, 11, in an armchair, addresses an unseen patient.

PHOEBE

I’d say that your mother was a 
classic narcissist, and so you 
overcompensate by focusing on your 
children to the exclusion of your 
own needs. Does that ring true?

We reverse and discover the “patient” is her mom, Monalisa. A 
BEAT, then MonaLisa bursts into tears.

PHOEBE (CONT’D)

It’s okay, Mom. I’ll give you a hug 
once our session is over.

INT. QALLA - GREGORY’S ROOM

Flipping through a Forbes, GREGORY, 9, spins in an Aeron 
chair among stacks of sneaker boxes, talking on a cell.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

3.

GREGORY

Here’s what you should do, Hassan. 
Take that deal, turn it sideways, 
and shove it up your-

KIP (V.O.)

That’s Gregory. He terrifies me. 
Thunder’s into building stuff.

EXT. AFGHANI VILLAGE

THUNDER, 4, completes an actual well as villagers look on.

INT. QALLA

ANGLE ON: Two pictures in frames. A girl, 20 and a boy, 19.

KIP (V.O.)

We’ve also got Sue. She’s already a 
surgeon. And my boy, Marcus. I 
can’t really talk about what he 
does. (WHISPER) He’s in the CIA. 

INT. QALLA GREAT ROOM - NIGHT

Everyone’s doing projects. Kip looks on admiringly.

KIP (V.O.)

And even though I rarely understand 
what the hell my kids are saying... 

MARISSA

Dad, I’m making a list of 
tautologies in Beatles lyrics. Do 
you think “There’s nothing you can 
do that can’t be done” qualifies?

KIP

Oh hey, look, it’s snack time!

KIP (V.O.)

...We’re a pretty great team. But 
life is a funny animal...

EXT. DESERT NEAR FIELD TENT

Kip digs a trench. Jenkins exits the supply tent.

JENKINS

Sarge, I think my rifle’s jamme-

BANG. Kip gets shot in the butt.

END COLD OPEN

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

4.

ACT ONE

INT. MITCHELL VAN - PRESENT DAY

Kip sits on a big hemorrhoid donut as MonaLisa drives the 
family-sized van. They pass by a sign that welcomes us to 
Dune Point, Orange County. All of the houses are almost 
exactly the same - mini-mansions with perfect lawns.

KIP (V.O.)

And lo, my butt was forced into 
retirement even though it had so 
much work yet to do. Happily, 
someone back home offered us a free 
place to live. Unhappily, it was 
MonaLisa’s mother, Celeste.

EXT. MITCHELL HOUSE

Waiting as the Mitchells pull up is CELESTE, 60s, a former 
pageant queen with a permanent fake cheerfulness. 

KIP 

She used to be in pageants, but you 
can hardly tell.

Celeste walks down the driveway, waving exactly like a 
pageant queen, holding freshly picked flowers like a bouquet. 

CELESTE

Welcome, children! (SPEAKING TOO 
LOUDLY) My. Name. Is. Grandma. 

KIP

They all speak English, Celeste.

CELESTE

Oh good, because my foreign is not 
great! Well, everybody around back 
to get hosed off! And if anyone has 
lice, I have razors and wigs!

KIP

(SOTTO) Keep driving, M.L. It’s not 
too late.

INT. KITCHEN - MORNING - A FEW DAYS LATER

Kip, dressed casually but neat as a pin, whistles as he puts 
a final clean plate in the drying rack. MonaLisa enters.

MONALISA

How you feeling about today? 

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

5.

KIP

Great!

MONALISA

Really? You were stress-eating a 
lot of sausage at breakfast...

KIP

Well, we always talked about me 
taking over at home when I retired, 
but now that it’s here... I’m used 
to tough guys, not geniuses.

MONALISA

They are really smart, but they’re 
still just kids who need their dad.

KIP

I’ve got some pretty big shoes to 
fill though. You’ve always done an 
amazing job.

MONALISA

Well, I believe everyone on Earth 
is born with a special gift. Some 
people are dancers, some chemists, 
some just have really big ding 
dongs. I didn’t find the kids’ 
gifts for them. I just sort of got 
out of their way. I’m sure you did 
that same thing with your men.

KIP

Nope. I’d just scare them into 
submission with a combination of 
screaming and physical punishment. 

MONALISA

And here I thought that was just 
your bedroom style. 

He swats her butt as she leaves, then opens the trash can, 
finds a half of a sausage patty and stress-eats it.

INT. MITCHELL LIVING ROOM - MOMENTS LATER

The whole family is gathered in the living room. Gregory’s in 
a suit, and Marissa and Matthew are wearing matching capes. 
MonaLisa sits on the floor holding a huge yellow feather.

MONALISA

Welcome to Morning Meeting. 
Gregory, please end your call.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

6.

GREGORY

(IN JAPANESE) Well, I can’t make 
that deal, Hoshi. Call me back when 
you grow up. (HANGS UP)

KIP

Okay, everybody. Lots of changes 
today. Mom’s gonna have her first 
day at work in twenty years, 
teaching women to read --

MONALISA

Teaching a feminist poetry course.

KIP

Right. You kids are going to a real 
school for the first time instead 
of having mom as a teacher. And my 
first day as a stay-at-home-dad. 
All right, now, who has questions 
for me about their first day of 
school?

A BEAT, no one says anything.

KIP (CONT’D)

No one wants to ask me anything?

Phoebe raises her hand.

MONALISA

Oh, here we go.

MonaLisa hands her the feather.

PHOEBE

I just want to say I know that we 
all have busy schedules now, so I 
will be extending my counseling 
hours. Just remember that if you 
don’t cancel twenty-four hours in 
advance I do have to charge you.

Marissa takes the feather.

MARISSA

And we just want to say that, 
anthropologically, we’re very 
excited to go to school and meet 
teens and learn their ways. 

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

7.

MATTHEW

Marissa and I have been working on 
some general small talk to engage 
them. We’ve categorized our 
conversation starters as 
Provocative Puzzlers --

MARISSA

“So, what do you all think about 
Net Neutrality?”

MATTHEW

And Gags and Giggles. 

MARISSA

(FROM BORAT) “My wiiiiife!”

MONALISA

Enticing! And tell us the story of 
your capes.

MATTHEW

We designed and sewed them based on 
our favorite PlanesWalkers. In 
Magic: The Gathering. 

MARISSA

If there’s a Magic: The Gathering 
community at the school we want 
them to know we’re interested.

I have a feeling they will.

KIP

THUNDER

Can I please go to school?

KIP

No can do, bud. Starting age is a 
hard five. So you get to stay home 
with me and play.

THUNDER

That sounds terrible. No offense. I 
like you, Dad, but we haven’t read 
any of the same books.

CELESTE (O.S.)

If you want to read a good book, my 
bestie Sonja self-published a tell-
all about her manicure injury!

We find Celeste in the doorway, with a big gift basket.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

8.

MONALISA

Mother! I thought we decided that 
you’d knock before you came in from 
the guest house, and we’d do the sa-  

CELESTE

I wish I could have, MonaLisa, but 
I was carrying this big basket of 
goodies for all of you. For the 
girls, self-tanner. And self tanner 
for the boys. And for you, 
MonaLisa, self-tanner. Kip...

KIP

Self-tanner?

CELESTE

A picture of me when I won Mrs. 
Married California 1989. That’s 
Patrick Duffy’s elbow there. Later 
that night, he tried to put it in 
my-

MONALISA

Mother!

CELESTE

I was going to say “drink.”

MONALISA

Mom, you can stay for our meeting 
but you can’t share your feelings 
unless you have the feather. 

CELESTE

Can I tell the kids how I felt when 
Peepaw left me for a stewardess? 

MARISSA

We say Flight Attendant now.

CELESTE

Oh, those are all just fancy words 
for whore. Doesn’t matter, though. 
I got the house and that sky slut 
still has to live on a stupid jet.

KIP

Pretty sure that’s not where they 
live. 

MONALISA

Greg, what’s in the briefcase?

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

9.

GREGORY

Money. 

Gregory opens a silver briefcase filled with candy bars. 

GREGORY (CONT’D)

Boom, the cocaine of the kid world! 

KIP 

No way, cowboy. I respect your 
entrepreneurial American spirit, 
but schools have rules.

Kip takes the case and puts it on a high shelf.

KIP (CONT’D)

Okay, everybody, fifteen minutes 
til wheels up! 

CELESTE

Just enough time to get that self-
tanner on.

MONALISA

Mom, no.

CELESTE

Fine, but they look like smart 
little ghosts.

INT. VAN/EXT. DUNE POINT ACADEMY - MOMENTS LATER

Kip peers through binoculars at the students gathering around 
the flagpole. 

KIP

Conditions look a-okay for 
insertion, Mitchells. Let’s ro-

The kids are already out of the car. 

KIP (CONT’D)

Right, okay. You saw an opening and 
you took it. Good thinking. 

The kids set off. Through the binoculars, Kip watches as the 
kids blend into the rush of students. After a moment, Kip 
clocks Gregory pulling another stainless steel briefcase out 
of his backpack.

KIP (CONT’D)

Uh oh. Thunder, sit tight. I’m 
going in.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

10.

EXT. DUNE POINT SCHOOL

Kip hustles towards Gregory. Gregory sees Kip and runs into 
school. Kip tries to go in but SHARON, an officious school 
security guard, blocks him.

SHARON

Sir, what’s your business?

KIP

Oh, uh, I need to get in there. 
Don’t worry. You can trust me. I’m 
with Special Forces. I mean, I was-

SHARON

And I’m Mrs. Jonathan Stamos. 

Kip tries to get past her.

KIP

Gregory, my-

SHARON

Uh oh. Looks like I gotta call in 
my partner, Alexis.

She extracts a taser from her belt.

KIP

You call your taser Alexis?

SHARON
That’s her name.

KIP

Okay, look, this is just a 
misunderstanding. I just need to 
get in there and grab a kid--

SHARON
Grab a kid, huh?

Sharon fires up the taser.

KIP

Whoa, hold your fire. Hold it. 
Please don’t tase-- Agh! Alexis!

Sharon tases Kip, who goes down hard. 

END ACT ONE

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

11.

ACT TWO

INT. PRINCIPAL’S OFFICE - A LITTLE LATER

Kip, joined by MonaLisa, sits across from PRINCIPAL RICHARD 
FAUVER, 30s, who looks way too young for his job. In the 
corner, Thunder does and undoes a Rubik’s cube over and over 
again. The confiscated briefcase sits on the desk.

PRINCIPAL FAUVER

So, I’m sorry we have to meet in 
such a tense situation, huh? Most 
of the parents - and the students, 
honestly - think of me as more of a 
cool friend than a Principal, Mr. 
and Mrs. Mitchell.

MONALISA

Please, Kip and MonaLisa.

MR. FAUVER

MonaLisa! What a fascinating name! 
Is it based on something?

MONALISA

Uh, the painting.

MR. FAUVER

I’ll have to Google it. Anyhoward, 
thank you for confirming that this 
is your husband. 

KIP

I’m sorry for causing a kerfuffle. 
My Gregory was going to attempt to 
sell contraband on your property.

MONALISA

Kip’s last job before Dad was 
Special Forces Sergeant, so he 
might need some time to adjust! 

MR. FAUVER

Well, I will admit that Sharon also 
has a bit of an itchy taser finger. 
During the school play, she tased 
Peter Pan when he started to fly. 

KIP

Well, from here on in, I’ll follow 
the rules in this terrific handbook 
you’ve provided. 

He slaps the Dune Point Student and Parent Rule Book.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

12.

MR. FAUVER

Cool. Well My Instagram is Rico-
Fauv-ay so find me and then I’ll 
follow you back, okay?

KIP

I don’t know what those words mean.

MR. FAUVER

I feel like that’s what’s gonna 
make us great friends. You make me 
tough, I make you cool. We’re like 
an American Eagle and Rihanna.

KIP

Still not following.

EXT. DUNE POINT SCHOOL - MINUTES LATER

MonaLisa and Kip stand by the van.

KIP

Well, so far, not so great! 

MONALISA

If it’s any consolation, I couldn’t 
get past the first line of the 
first poem with my first class. 

KIP

What was the line? I’m sure I’ll 
love it.

MONALISA

Hope is the thing with feathers-

KIP

No sorry you lost me.

MONALISA

It’s all right, hon. 

KIP

Okay, Thunder, my boy, what would 
you like to do? Go play catch?

INT. DUNE POINT DEPARTMENT OF WATER AND POWER - A LITTLE 
LATER.

Thunder excitedly unfurls blueprints of the town’s water 
system. 

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

13.

THUNDER

I bet you didn't know that anyone 
can come in and look at blueprints, 
huh? It's really exciting! Sorry, 
I'm completely hogging these. Don't 
be shy, dad, grab one and dive in. 

INT. DUNE POINT ACADEMY - CAFETERIA - NOON

Marissa and Matthew walk towards an empty lunch table, 
confidently nodding and smiling at everyone as they go.

MARISSA

Hello, hello. Good afternoon.

MATTHEW

Hello. Greetings. Enjoy your lunch!

MARISSA

It’s weird how no one says hi here.

MATTHEW

It’s like their moms didn’t have a 
Mandatory Manners Minute every day.

MARISSA

Her new students are so lucky.

INT. COMMUNITY COLLEGE CLASSROOM - LATER

MonaLisa stands in front of a small group of young women, 
none of whom look up from their phones..

MONALISA

“Hope” is the thing with feathers/
That perches in the soul/ And sings 
the tune without the words-” Does 
anyone want to guess what Dickinson 
means by “sings the tune without 
the words”?

One of the girls laughs.

MONALISA (CONT’D)

Is something funny?

GIRL ONE

South Beach Gigolos. It’s so good. 
Jose got pushed into the fountain 
at his niece’s quinceanera. 

MONALISA

Why don’t we put our phones down 
and read the rest of the poem?

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

14.

The girls hesitantly set their phones down. MonaLisa closes 
her eyes, lost in the poem, reciting from memory.

MONALISA (CONT’D)

I’ve heard it in the chillest land/ 
And on the strangest Sea/ Yet - 
never in Extremity,/It asked a 
crumb - of me.” (SIGHS) Isn’t that-

A series of beeps, and all the girls pick up their phones.

GIRL TWO

Sorry, Lindsay Lohan just tweeted. 
She sprained her pelvis on Billy 
Bush’s boat in Ibiza.

MonaLisa looks defeated.

INT. CAFETERIA - A LITTLE LATER

A girl wraps up an announcement on a hand-held mic.

MIC GIRL

...to get your homecoming dance 
tickets. And Principal Fauver 
wanted me to remind you that even 
though the theme is “Bottle 
Service,” there are no alcoholic 
beverages permitted! Even though 
he’s cool. Go Dune Point Dentists!

Phoebe sets her tray down on the twins’ table.

PHOEBE

We’re the Dune Point Dentists?

MATTHEW

Yes, our new town was founded by a 
rogue Mormon dentist in 1875.

MARISSA

Huh. So, who are you guys going to 
Homecoming with? I have eleven 
invitations so far. 

PHOEBE

This boy Louis asked me, but I told 
him I’m still in my latency period.

MATTHEW

I asked four girls from that group.

Matthew waves confidently at a table full of cheerleaders. 

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

15.

MATTHEW (CONT’D)

They all had other plans that 
evening, unfortunately. One was 
taking a shower with her dog. 

MARISSA

I wonder if Gregory got a date yet.

PHOEBE

I doubt it. He told me he’s working 
through lunch.

WE SEE A QUICK POP: of Gregory at the vending machines. He’s 
buying everything inside. A boy waits behind him.

BOY

Can I just get some Cheetos?

GREGORY

In about five minutes, and I tell 
you what, I’ll give you a really 
good deal.

BACK TO SCENE

PHOEBE

So, how will you choose who you’re 
going with, Marissa?

MARISSA

I already said no to the boy who 
asked me by throwing a gummy worm 
at my face. But that leaves ten. 

MATTHEW

I wish I had more to go on. It 
would be nice to find someone who 
shared my interests in multiverses.

MARISSA

Right, like, a guy could be 
physically attractive but not 
understand algorithms or know all 
the lines from Monty Python and The 
Holy Grail and then what are we 
supposed to talk about? 

MATTHEW

We are the Knights who say Ni!

MARISSA

Ni!

They crack up, then...

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

16.

MARISSA (CONT’D)

It’s too bad there isn’t a simple 
algorithm for choosing a dance 
date.

Matthew looks up sharply.

MATTHEW

I think this is the moment in a 
movie where I say “wait, say that 
again” and then we both realize 
that the idea is staring us right 
in the face.

MARISSA

Wait, say that again.

MATTHEW

Oh, uh, I think this is the 
moment...

MARISSA

No, I’m doing that thing you just 
said. I know what you mean. We can 
write our own algorithm!

INT. COMPUTER LAB - A LITTLE LATER

Matthew and Marissa type furiously on adjacent computers.

MATTHEW

It’s crazy how open the school 
network is. Every password is “Rico 
Fauv-ay.”

MARISSA

I’ve created links to everyone’s 
social networking profiles. How’s 
the algorithm look?

MATTHEW

I was able to shift code from our 
Magic the Gathering Ally App to the 
categories that Phoebe said would 
make the best romantic pairings.

PHOEBE

Recreational interests, optimism 
versus pessimism, and attachment 
styles. You can really tell if 
someone wasn’t breast-fed enough by 
how many selfies they’ve taken!

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

17.

MATTHEW

Okay, this app should tell me 
exactly who to ask to the dance, 
solve Marissa’s dilemma, and likely 
make us heroes amongst our peers.
So... all that’s left is to email a 
link to download the app to 
everyone, then sit back and watch 
the sparks fly!

INT. CAFETERIA

A little bit at first, and then in droves, kids start 
checking their smartphones. A cheerleaders nudges her friend. 
The cheerleader’s phone shows the Let’s Dance App. It says 
“calculating your perfect match,” then shows a boy’s face. 
She looks across the room at the boy it shows. He has two 
chicken nuggets up his nose.

CHEERLEADER

Sick! Or, maybe. No, sick!

Across the cafeteria, a GOTH GIRL yells at her Goth friend.

GOTH GIRL

That’s my boyfriend, you dick!

Within moments, a general melee has broken out. Principal 
Fauver gets on the mic, but is almost totally drowned out.

PRINCIPAL FAUVER

Everyone, please-

A LUNCH LADY sprints towards him, her smart phone in hand. 
She grabs him, dips him, and kisses him.

PRINCIPAL FAUVER (CONT’D)

Helen, no!

INT. DEPARTMENT OF WATER AND POWER - A LITTLE LATER

Kip’s cell phone rings.

INT. PRINCIPAL FAUVER’S OFFICE - LATER

Phoebe, Marissa and Matthew sit in the Principal’s Office 
with Kip, who looks livid.

MATTHEW

...and it’s only really hacking if 
the network is protected. 

KIP

Matthew.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

18.

MARISSA

It’s true, Dad. I know you don’t 
know anything about computers but 
what we did was perfectly legal.

PRINCIPAL FAUVER

Still, it’s against school policy 
to make an app telling everyone in 
school who they should (WHISPERING) 
go all the way with.

PHOEBE

We didn’t design the app to   
suggest intercourse. It’s to help 
everyone determine their best 
pairing for the homecoming dance.

MARISSA

Also, I memorized the handbook 
before school started and this 
isn’t expressly forbidden, so it’s 
not legitimate to punish us.

PRINCIPAL FAUVER

I have five kids in the infirmary 
and one who’s locked himself in the 
eyewash booth in a chemistry 
classroom and is, ironically, 
crying his eyes out! Also, I was 
personally sensually assaulted.

MARISSA

Including faculty in our search 
engine was our only mistake.

KIP

Guys, we need to fix this.

PRINCIPAL FAUVER

Exactly. I know I’m usually super 
chill, and I was voted funniest 
Principal in my improv class, but I 
need the app deactivated.

MATTHEW

PRINCIPAL FAUVER

No.

I’m sorry?

MARISSA

We’re providing a service. The 
science is sound, right, Phoebe?

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

19.

PHOEBE

People may find the notion of 
ending their relationships in favor 
of the ones we’ve recommended sad 
now, but they’ll be happier in the 
long run.

PRINCIPAL FAUVER

Guys, this isn’t up for debate.

MATTHEW

Everything should be up for debate.

KIP

I am ordering you to take it down.

MATTHEW

Have you talked to mom about this?

KIP

No! Mom is working!

MARISSA

Dad, I’m happy to explain--

KIP

I don’t need you to explain, I need 
you to do what I say!

MATTHEW

Sorry Dad, but on this one I think 
we know better.

PRINCIPAL FAUVER

Then you are all suspended until 
you change your minds. Sorry bros. 

Just then, the door opens. The secretary brings Gregory in.

SECRETARY

Sir, this student purchased 
everything from all the vending 
machines and was re-selling it.

PRINCIPAL FAUVER

Let’s just throw him in with the 
others, shall we? 

GREGORY

I’d like to speak to my lawyer.

END ACT TWO

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

20.

ACT THREE

EXT. MITCHELL HOUSE - LATER THAT DAY

The sound of a WHISTLE!

KIP 

Go around again!

All of the kids except Thunder take laps around the yard in 
sweats. Celeste looks on. Thunder reads a Dostoevsky novel 
while floating in a giant inflatable swan in the pool.

KIP (CONT’D)

Everybody, drop and give me twenty.

MARISSA

Twenty what?

KIP

Really? Push ups!

They start doing the worst push-ups you’ve ever seen. 

MONALISA

(O.S.) Helloooo - anyone...? Oh, 
hey. What’s happening here?

CELESTE

Hot scandal alert! The kids got 
suspended for stealing and making 
sex apps.

MONALISA

Oh my god!

KIP

No no no! (THEN, SOTTO) Don’t 
panic, M.L. I’ve seen this a 
million times in the military. I’m 
a new leader. They don’t respect me 
yet, but they will. How’d the rest 
of class go today? 

MONALISA

Great! Bianca showed me a dick pic 
her boyfriend sent her on accident.

KIP

Sounds like we are both already 
killing it.

MONALISA

Yup.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

21.

KIP

I am earning their respect!

CHYRON: THREE DAYS LATER

INT. HALLWAY - NIGHT

KIP

Lights out everyone! We’re up at 5 
AM for a fun run!

INT. BEDROOM - CONTINUOUS

Kip enters to find MonaLisa.

KIP

Welp, they do not respect me.

MONALISA

Honey, I haven’t wanted to 
interfere, but I think maybe they 
are just used to doing things my 
way. So if you tried...

KIP

I’ll try anything. 

MONALISA

What about a sweat lodge 
meditation? When we let toxins out 
of the body it helps flush out deep 
emotions as well. I’ve used it to 
resolve lots of conflicts with the 
kids. I think they are challenging 
you for a reason. Find out why and 
you might solve the whole problem.

KIP

I hope you’re right, M.L. Now, how 
‘bout we turn this bed into a sweat 
lodge?

MONALISA

Is that a pick up line?

KIP

That depends, is it working?

They start making out.

EXT. YARD - MORNING

The kids, dressed to run, walk into the yard where Kip has 
set up a camping tent with a bunch of space heaters inside. 

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

22.

MARISSA

What is this?

KIP

It’s a good ol’ fashioned sweat 
lodge. We’re gonna sit in there and 
sweat until we resolve our issues.

PHOEBE

Dad, while I admire your 
enthusiasm, this is not how a sweat 
lodge is usually done. 

MATTHEW

Traditionally, the heat is created 
through the warming of rocks. And 
the entrance should face east to 
signify the rising of the sun.

KIP

I think it’s close enough.

GREGORY

Also, Dad, most sweats are preceded 
by a full-day fast, avoiding 
caffeine, alcohol and other 
unhealthy substances. I see you’ve 
got a Jimmy Dean sausage muffin and 
a large coffee so I would advise--

KIP

Just get in the tent. 

INT. SWEAT TENT - A LITTLE LATER

Kip and the kids are all sweaty. No one’s talking.

KIP

Well, now that we’ve got sweat all 
over us, let’s talk about the 
problem at hand. Kids, I love you 
very much, but your actions are 
upsetting me. Can you tell me why 
you won’t listen to me?

MARISSA

Well, we...

KIP

Yes?

PHOEBE

We don’t want to hurt your 
feelings, Dad.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

23.

KIP

Guys, I once survived in an 
Estonian cave for two weeks on just 
a half can of taco-flavored Spam 
and my own urine. I think I can 
survive your words.

MATTHEW

Okay, we aren’t listening because, 
we don’t think...

MARISSA

You’re...

Everyone goes quiet.

KIP

My own urine, guys. And, full 
disclosure, I also ate a little of 
my poop.

MATTHEW

(BLURTING)You don’t have anything 
to teach us Dad.

A beat. It actually stung.

KIP

Wow.

PHOEBE

Don’t get us wrong! We think you’re 
great at some parts of fatherhood. 
Like driving us places. And...

MARISSA
Driving us places.

PHOEBE

I said that. Um, paying for 
groceries.

KIP

Nothing to teach you?

MATTHEW

It’s just we think - we all think - 
that it’d be better to leave the 
school stuff to us.

A BEAT.

MARISSA

Okay?

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

24.

Kip’s still quiet.

GREGORY

Now, if this is all wrapped up, I 
gotta Skype with a Russian Oligarch 
before he’s totally drunk, so-

KIP

Sit down!

GREGORY

What?

KIP

Sit. Down.

Gregory sits. Kip looks really upset.

KIP (CONT’D)

All right, listen. I know you kids 
are very smart, but you haven’t 
been alive very long, and I have. 

MATTHEW

Dad, are you crying?

KIP

No! I’m sweating out of my eyes!

PHOEBE

Dad, try to stay calm.

KIP

No! You can’t just go around 
breaking the rules all the time! It 
won’t get you anywhere!

MARISSA

It got Steve Jobs somewhere!

MATTHEW

And Galileo.

GREGORY
And Bernie Madoff.

PHOEBE

And Ghandi.

KIP

You are not Ghandi! You are my 
eleven year old daughter and I-

Kip tries to rise, but is woozy.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

25.

MARISSA

Uh oh, dad, you’re dehydrated.

PHOEBE

Get him out of here!

KIP

I’m fine!

He’s not fine. 

MARISSA

Get him out of the tent!

The kids pull Kip out of the tent and lay him on the lawn. 

KIP

(DELIRIOUS) Oh look, a bird! Hi 
Steve.

He tries to sit up, then faints.

MATTHEW

Call mom!

INT. BEDROOM - A LITTLE LATER

Kip lays on the bed, sipping from a juice box as MonaLisa 
enters. He has a plate of sausage next to him on the bed.

KIP

Hi. You didn’t have to come home.  

MONALISA

Oh, yes I did. I threw a book at 
one of the gals when she asked me 
what rhyming meant.

KIP

(OFFERING) Sausage?

She takes one.

MONALISA

Oh, Kip. This isn’t working. 

KIP

I’m sorry. I know. It’s just. They 
really don’t need me. And this is 
the last of the sausage.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

26.

MONALISA

I’m not just talking about you. No 
one in my class is even remotely 
interested in learning anything. 
The kids worshipped me! All my 
students care about is reality TV. 

KIP

We’re Caribou on a goddamned ice 
floe, Mona! We’re out of our depth! 
I know what I should do. I’ll re-
enlist. I can take a desk position.

MONALISA

You’d hate that.

KIP

I’d get used to it! I can sit in a 
chair! Look. (HE SITS UP) See!? I’m 
sitting.

MONALISA

Oh, Kip. We’ve done so many hard 
things. It’s crazy that the kids 
making a silly dating app is the 
thing that’s tearing us apart.

Kip looks up sharply.

KIP

Wait, say that again.

MONALISA

The kids making a silly dating app 
is tearing us apart.

KIP

(LIGHTBULB) Oh my god. MonaLisa. 
That’s it. I have to pretend to 
surrender. MonaLisa, I’ve been 
forgetting who I am!

MONALISA

I don’t know what you’re talking 
about but it seems exciting!

KIP

The kids may have read a lot of 
books, but I’m Kip goddamn 
Mitchell. I’m one of the best 
strategists in the Forces. I may or 
may not have helped figure out the 
plan to get Bin Laden. Alledgedly! 
I know what to do!

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

27.

He picks up MonaLisa and gives her a big sexy kiss, then she 
slaps his face, then kisses him.

MONALISA

Sorry, I just got excited.

KIP

Do it again!

EXT. BACKYARD - LATER

Kip and MonaLisa stand in the yard in long flowing robes, 
looking somber. A large white candle, held by Celeste, 
flickers between them. The kids walk out of the house.

MATTHEW

Mom? Dad? What’s going on?

KIP

Kids, first of all, we want you to 
know that you were right. We tried 
your app, and it really is amazing. 
We no longer want you to take it 
down, and we support you one 
hundred percent.

MATTHEW

See, Dad? You just had to trust us.

MONALISA

And we’ve asked you out here to 
witness the extinguishing of our 
wedding candle. A symbol of mine  
and your father’s life together. 

KIP

Once it is out, our relationship 
will be gone with it.

Celeste sobs.

MARISSA

Uh, what are you guys talking 
about.

KIP

Like I said, we tried your app, and 
it said mom and I are incompatible. 
So we will no longer be attending 
the dance of life together. 

PHOEBE

What? No. I haven’t even given you 
couples counseling.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

28.

MONALISA

I will keep custody of the girls 
and the boys will live with Dad.

THUNDER

Dammit.

GREGORY
Can I get a condo?

MARISSA

But you and mom were meant to be 
together. You were a nineteen year 
old soldier who wandered into a bar 
in Dresden in a rainstorm and you 
saw a beautiful American woman...

KIP

..reading the dictionary.

MONALISA

Madame Bovary.

KIP

Either way: boring. I know, 
Marissa, I thought we’d be together 
forever, but the app...

A beat. Celeste breaks down in wracking sobs.

MARISSA

(REALIZING) Ohhhh, I see what you 
did there.

PHOEBE

Smart. Good play, mom.

MONALISA

It was your Dad’s idea.

PHOEBE

(DISBELIEVING) Really?

KIP

Guess old Kit isn’t so dumb after 
all. I just called my self Kit, 
didn’t I?

MONALISA

You did.

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

29.

KIP

So I’m not the smartest man in the 
world, but there is something I 
know. Love - real love - can’t be 
predicted by a computer. A computer 
wouldn’t match me with your mom. 
And I doubt a computer would say 
I’d be good at being your dad.

PHOEBE

Dad, we’re-

KIP

Phoebe, wait. But, I am your dad. 
And I don’t have to be smarter than 
you to be good at that. 

MATTHEW

Dad, we’re sorry. We should have 
listened to you. We’ll take it 
down.

KIP

Thanks buddy. I may not know 
anything about building a sweat 
lodge or how to spell diarrhea 
without googling it, but I do know 
this; high school is just - hard. 
And there are going to be times 
when a guy like me - a guy with a 
keen sense of strategy who made my 
high school my bitch for four 
glorious years - may be of help. 

MATTHEW

Okay, can you tell me why the 
cheerleaders I asked to the dance 
said no? 

KIP

Because you’re a nerd, son. At 
least they think so. They want to 
go with the tall, not as smart 
football captain. Let’s just call 
him Kit. 

MATTHEW

But don’t they don’t realize how 
successful I’ll be later in life?

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

30.

KIP

Not yet. But here’s a tip from a 
guy who dated his share of Pom-Pom 
Ponies - try asking the smartest 
girl on the debate team instead . 
(HE WINKS AT MONALISA) She’s the 
one who’s gonna turn out to be a 
real firecracker.

CELESTE

(STILL CRYING) So, wait, you guys 
aren’t splitting up?

INT. KITCHEN - THE NEXT MORNING

Kip eats a bowl of fruit. MonaLisa enters in a vintage dress.

KIP

Hey, what’s with the get-up?

MONALISA

You inspired me to try something 
different with my class today. 
What’s with the fruit salad?

KIP

I could actually feel the sausage 
in my heart.

INT. MONALISA’S CLASSROOM - A LITTLE LATER

MonaLisa enters the classroom in the sexy dress, holding a 
martini. The girls actually look up from their phones.

MONALISA

Oh hello there. My name is Sylvia 
Plath.  From the cast of The Real 
Housewives of London. Tomorrow, I’m 
going to stick my head in the oven.

GIRL ONE

(GASPS) What did he do to you girl?

MONALISA

My husband, Ted Hughes, cheated on 
me with my friend Assia Wevill! 

GIRL TWO

Wevill is evil! You should tweet 
that.

MONALISA

I did something even better. I 
wrote a series of poems about it...

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

31.

INT. DUNE POINT SCHOOL - CAFETERIA - SATURDAY NIGHT

Kip and MonaLisa are there wearing chaperone sashes. Gregory 
sells shirts with Principal Fauver’s Rico Fauv-ay Instagram 
handle and picture on them. The Principal buys one, then high-
fives him. Marissa asks the DJ for the mic. The room quiets.

MARISSA

Hi. I’m Marissa. My siblings and I 
made the app that caused so much 
trouble. We know now that an app 
can never tell you who to love. 
That’s why I came to the dance 
tonight not with my match, but with 
the coolest guy I know: my brother, 
Matthew. I’m proud to say: my 
brother is my date!

The crowd looks weirded out. 

MONALISA

We probably should not have let her 
say that, huh?

KIP

Nope.

Mr. Fauver heads to the floor and starts awkwardly dancing. 
The Mitchells join him under...

KIP (V.O.)

A smart woman once said, “Hope is 
the thing with feathers,” which I 
don’t really get. But, a smart man 
once said, “We all have big changes 
in our lives that are more or less 
a second chance.” That man was 
Harrison Ford. He’s a goddamn 
treasure, and he’s right. This is a 
second chance for the Mitchells. A 
chance for us to redefine 
ourselves. Like Harrison Ford did 
when he made Six Days Seven Nights 
with that crazy blonde lady. It was 
terrible, but hell if he didn’t 
try. 

FADE OUT:

END ACT THREE

THE BRAINY BUNCH   "PILOT"

Network Draft 1/23/15        

32.

INT. MITCHELL HOUSE - LIVING ROOM

TAG

Close up on each family member in turn as they listen raptly.

CELESTE (O.S.)

“And then the Doctor turned to 
Sonja, and said, ‘It’s not just 
that I can’t save the nail, Sonja, 
you might lose the whole hand.’”

GASPS from the family.

MARISSA

Oh no!

PHOEBE

Not the whole hand!

CELESTE

Shh! Do you want me to keep reading 
or not?

REVEAL Celeste is reading from “Manicure Massacre,” The Sonja 
Rockwell Story.”

On, the cover, blood spills out of a nail polish bottle.

KIP

Yes, woman, keep reading! (THEN, 
SOTTO TO MONALISA) She was right, 
this is a really good book.

MONALISA

Mmm hmm.

CELESTE

“Oh no,” gasped Sonja, “Not my 
hand. I need it for hand j-”

MONALISA

Mother!

CELESTE

Oh, cool it. I was going to say 
hand jives.

FADE OUT:

END OF SHOW

