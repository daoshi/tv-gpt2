SOUTH SIDE

Written by

Bashir Salahuddin, Sultan Salahuddin & Diallo Riddle

Comedy Central
Emerald Street Entertainment
The Riddle Entertainment Group
(C) 2017

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 1 

INT. NEWS STUDIO - DAY

An ANCHOR talks to camera. An OTS graphic reads: Tragedy In 
Englewood.

ANCHOR

Another senseless act of violence 
that has left authorities baffled. 
We go now to the scene were April 
Scott has more. April?

EXT. KENNEDY KING COMMUNITY COLLEGE - EXIT DOOR - DAY

A reporter APRIL SCOTT (30s) talks to a news camera near an 
field where recent graduates cheer and hug family members. 

REPORTER

Behind me, you see the happy 
graduates of Kennedy King Community 
College. But last night, this 
intersection was no place for 
celebration-- 

SIMON JAMES (30s) and his chubby best friend KAREEM “K” ODOM 
(30s) see the camera and rush it.

SIMON

We did it! Your boys graduated!

REPORTER

Excuse me, sir, you’re in my shot--

SIMON

Excuse me, but we’re celebrating. 
It’s a beautiful day. South Side!

VOICES O.S. echo, “South Side.”

SIMON (CONT'D)

It’s your man, Simon James here. 
Business Administration ya dig! Got 
the piece of paper, ya dig! Look at 
the pin on my lapel, ya dig! And my 
man K...

K

“Remote Planetary Studies.” I’m 
going to be an astronaut. 

SIMON

You gon’ make a great astronaut, K.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 2 

REPORTER

Sir!

SIMON

The world is ours!

MUSIC CUE: “AWW MAN” BY LIL BIBBY

What follows is a 60 second MONTAGE of YouTube clips uploaded 
by irate Rent-To-Own customers (similar to the sizzle reel).

A YOUNG MAN IN HIS CAR, RECORDING A VIDEO ON HIS CELL PHONE.

YOUNG MAN

Hey what’s up. This is just a quick 
rant, or my thoughts and 
expressions about Rent-To-Own. 

A GIRL IN GLASSES TO THE CAMERA, STREET INTERVIEW STYLE.

GIRL IN GLASSES

Rent-To-Own is a place where you 
can rent couches and television 
sets and computers and whatnot.

AN OLD GUY GRILLS STEAKS ON HIS BACK PORCH.

GUY

I got a lot of stuff from RTO. It’s 
easy. They don’t ask for credit.

A HOMEOWNER SURREPTITIOUSLY FILMS FROM HIS FRONT PORCH AS AN 
RTO COLLECTION TEAM ARGUE WITH HOMEOWNERS ACROSS THE STREET.

HOMEOWNER

RTO, doin’ what they do...sellin’ 
people furniture for over the top 
rates then repo’in it. Because 
these people are the same people 
that can’t afford to buy it. 

END OF MONTAGE.

EXT. RED LOBSTER - PARKING LOT - NIGHT

A Rent-To-Own van sits with the other cars. 

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 3 

INT. RED LOBSTER - NIGHT

Simon and K still wear their cap and gowns as they sit with  
Simon’s four kids (ages 6 - 10), his GRANDMOTHER (60s), his 
UNCLE SPIKE (50s) who wears an army jacket, and others.

SIMON

I want to thank my family and 
everybody from my block for sharing 
this momentous occasion with me.

(seeing)

My grandmother Ernestine, my Uncle 
Spike.

UNCLE SPIKE

I’m proud of you, nephew. Now that 
you got that paper, you gon’ have a 
fine class of women trying to 
juggle them family jewels.

SIMON

Thank you.

UNCLE SPIKE

I got a connect at the VA if you 
ever need any viagra.

SIMON

While I appreciate the offer, in 
front of my kids, tonight is about 
more than hours of sex. It’s about 
finally following my dreams. 

K

And finally getting a new job. 

SIMON

It’s going to feel like slow-
grinding with a big booty freak in 
a summer basement party when I quit 
Rent-To-Own. Speaking of, thank you 
Q, for letting us come in late to 
work tomorrow.

Sitting next to K is his twin brother QUINCY “Q” ODOM who 
wears his “Branch Manager” Rent-To-Own fleece.

Q

I never said that. 

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 4 

K

I want to say something. The 
universe is a big and mysterious 
place. Darkly we go into it. 
Unknowing yet unafraid. Carl Sagan.

SIMON

My man loves science. I wanna thank 
my man Chase AKA the one white guy 
on my block for talking to the 
hostess when she couldn’t find the 
reservation.

A slightly crunchy-looking white guy, CHASE NOVAK (think Ike 
Barinholtz) nods graciously.

CHASE

I was happy to help. I’m proud of 
you, Simon.

SIMON

I wanna thank Travis and Tha Capone 
Boys for falling through.

Teen gang members and their leader TRAVIS (19) applaud.

SIMON (CONT’D)

I know y’all activities are mainly 
at night so this means a lot that 
you would take the time to be here.

(a la Tupac)

“You are appreciated.”

TRAVIS

We love you, bruh.

Travis hands Simon a Nike shoe box. 

TRAVIS (CONT’D)

A little something we decided to 
come up on, for ya, joe.

Simon opens the box. It’s a brand new pair of Jordans. 

SIMON

Negro. Are these even out yet?

TRAVIS

Hey... for you? They are.

SIMON

Even though y’all need to quit that 
street life like I did, I love 
y’all. No pressure. 

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 5 

The boys nod respectfully.

SIMON (CONT’D)

But we do what we can to survive. I 
understand that. We come from a 
brutal place. In some ways, I feel 
like the South Side’s been trying 
to kill me my whole life.

CUT TO:

EXT. FOSTER PARK - DAY [FLASHBACK]

YOUNG SIMON (9) rides his bike past lots of scary gang 
members. Once he gets across the street, he’s hit by a 1987 
Tan Buick Skylark driven by a MAN with wild hair. As Simon 
flies through the air, we...

MAN WITH WILD HAIR

My bad, joe.

CUT BACK TO:

INT. RED LOBSTER - DAY 

Simon addresses the dinner party.

SIMON

The hunt for that Tan Buick 
continues.

Wait staff arrive with appetizers.

K

Cheddar biscuits are here, we 
should wrap this up.

Two COPS enter the restaurant, unseen by Simon.

SIMON

Like Rick Ross says in the song 
“Buy Back The Block,” we must 
control our own financial destiny. 
That’s what I plan to do. And to my 
kids. I promise you I’m going to 
work my ass off. And one day, when 
daddy’s money is right, you’re all 
going to come live with me in a big 
fancy house!

The kids swarm Simon for a group hug. The cops approach 
Simon, pulling the kids off. A few Capone boys quietly leave.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 6 

COP #1

Simon James?

SIMON

What’s this about?

COP #1

You’re under arrest for failure to 
pay child support.

SIMON

There must be some mistake, 
officer. I’m very prompt in my--

Simon takes off running, knocking over tables, trying to 
shake the cops but they eventually grab him.

INT. COURT HOUSE - DAY

A PUBLIC DEFENDER confers with a JUDGE. Then he heads to the 
defendant box and speaks confidentially with Simon.

PUBLIC DEFENDER

You need to plead “no contest.”

SIMON

What? But I’m innocent. I pay Tasha 
and Limiko every week. I give my 
baby mommas cold hard cash.

PUBLIC DEFENDER

Limiko and Tasha say you don’t.

SIMON
They’re lying. 

PUBLIC DEFENDER

Maybe they are. But it doesn’t 
matter, Simon. There’s no proof you 
pay them. No paper trail.

SIMON

My word don’t count?

PUBLIC DEFENDER

Bruh. I’m from Englewood. I’m being 
real with you. This the best you 
gon’ do.

SIMON

Ok...What does “no contest” entail? 

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 7 

PUBLIC DEFENDER

It’s no biggie. The court is just 
gonna take a lot of your paycheck. 
I’ll be honest, they’re gonna take 
most of it.

SIMON

I won’t have enough to live on! Can 
you tell the judge--

PUBLIC DEFENDER

I’m a public defender, not a 
miracle worker. You saw me up 
there. I spoke to the judge for 
what, ten minutes? Long minutes. 
Now I really want to discuss your 
case, but it's three o clock and I 
also really want to beat traffic.

Simon is stunned.

PUBLIC DEFENDER (CONT’D)
Maybe we continue this discussion 
another day and both beat traffic?

SIMON

Fine.

PUBLIC DEFENDER

(to the judge)

Your honor...

JUDGE

Mr. James, how do you plead?

SIMON

No contest.

INT. RENT-TO-OWN TRUCK - DAY

Simon and K park in the Rent-To-Own loading dock.

SIMON
My life sucks. 

K

Not to be devil’s advocate, but you 
brought it on yourself. You the 
only nigga I know to get two first 
cousins pregnant. 

They both seduced me!

SIMON

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 8 

K

You wanted it too.

SIMON

Whatever. I just have to stay 
focused. Make enough bread so I can 
have my babies come live with me.

K

You think their moms will like 
that?

SIMON

If I make enough money they won’t 
have a choice. Never should have 
messed with those women.

In reaction, K breaks in to “Poison” by Bell Biv Devoe. Simon 
can’t help but join in for a few bars. 

SIMON (CONT’D)

No more young girls. From now on 
I’m only dating chicks over sixty 
who can’t get pregnant.

INT. RENT-TO-OWN - DAY

The store looks like a smaller-scale version of Sears: TVs, 
living room sets, washer dryers, Xbox’s. The patrons are 
mostly black and brown folks with a few white people mixed 
in. At the front counter, Q sees Simon and K enter.

Q

Welcome home, Shawshank. 

INT. RENT-TO-OWN - BREAK ROOM - MOMENTS LATER

People mill around before the staff meeting. Simon, K, STACY 
(20s) who chats with KEISHA (30s), RUBEN (20s) a Panamanian 
“player” who barely speaks English, ESMERELDA (20s) Latina 
and goth, and Simon’s arch-rival BISHOP (30s). Q enters to 
start the meeting. Everyone takes their regular seats.

Q

I was supposed to be congratulating 
my brother and Simon for graduating 
last week, but then Simon had to do 
a bid.

SIMON

Bid? I was in jail for ten hours.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 9 

Q

Ten hours more than the rest of us. 
Anyway, Simon, K, good job. 

BISHOP

It only took you eight years to 
graduate from community college. 
Congratulations. Let’s clap for 
retardation, everyone.

SIMON

Thank you, Bishop, I can always 
count on you to push hateful words 
out of those ball-kissers you call 
lips.

Q

Damn, Bishop, you took that like a 
man. 

SIMON

I have a question...

Simon points to black cops OFFICER GOODNIGHT (late 30s) and 
his laid back superior officer SERGEANT CHANDRA TURNER (early 
30s) who stand by a wall.

SIMON (CONT’D)

Why are they here?

GOODNIGHT
Is there a problem?

SIMON

I don’t know, is there?

GOODNIGHT

I don’t know. Is there?

SIMON

I don’t know.

GOODNIGHT

Neither of us know. Nobody knows.

SIMON

Then good.

Q

(to the cops)

Sorry, officers, he’s “fresh out.”

(to Simon)

They’re not here for you, Cool Hand 
Luke. 

(MORE)

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 10 

Q (CONT'D)

They’re here because I listen to 
you guys. I know it’s dangerous out 
there. To that end, I’d like to 
introduce officers Turner and 
Goodnight. They patrol our major 
delivery zones. They’ll be on the 
ready if we need help with any 
uncooperative clients. 

(as he types)

I’m group-texting you their numbers 
now.

(to the cops)

Thanks, guys.

TURNER

Glad to be of service. Don’t 
hesitate to call us, guys. I’m from 
here. I was born right over on 
Seventy-sixth and Damon. Went to 
Hirsch. I know how grimy these 
streets can be. I’m one of you. And 
we’re all in this together.

The cops leave.

Q

Chicago cops ain’t cheap. That’ll 
be ten bucks each everyone.

Everyone groans. Bishop goes to each worker collecting money.

Q (CONT’D)

Everybody stop acting all brand 
new. You know the deal. 

SIMON

This is so wrong.

Q

Those cops are supposed to be 
fighting big crimes. So if we need 
help, they need a small honorarium.

SIMON

Then you pay it! Or the store! 

Q

Fine, don’t pay. You won’t get 
protection.

SIMON

Good.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 11 

Q

And you know what else, Mr. Smart 
Pants? Since you got so much to 
say, I think you and K should go 
talk to Michael Owens...

Q references a cork board with pictures and “crimes” of their 
worst customers, connected by lines, like an FBI board. 
MICHAEL “SHAW” OWENS, 30s, at the top of the pyramid.

Q (CONT’D)

You might know Michael as “Shaw”.

(to Bishop)

How long has he had that Xbox now?

BISHOP
About two years.

Q

That’s two years too long. I want 
you and K to go get it back. 
Without the cops.

SIMON

Shaw’s too dangerous. He made Greg 
quit. And Greg was a marine. 

CUT TO:

EXT. CHICAGO FOREST PRESERVE - DAY [FLASHBACK]

A muscular guy, GREG, sweaty and desperate runs through the 
park. An arrow pierces his leg. He falls. Shaw and his goons 
emerge from the brush, Shaw toting a compound bow.

SHAW

Bull's-eye, nigga.

Whip pan to a pair of white joggers. They’re shocked.

SHAW (CONT’D)

There’s parks on the north side!

CUT BACK TO:

INT. RENT-TO-OWN - BREAK ROOM - DAY

Simon faces off against Q in front of the staff.

SIMON

I ain’t going over there.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 12 

Q

Go get that Xbox, or you can 
discuss your cowardice with 
strangers in the unemployment line.

SIMON

I don’t care about this rat bastard 
job. I just graduated fool! My 
stock is all the way up. We quit. 

(to K)

Come on, K. 

K

This is foolish. But you my boy.

Simon and K leave.

INT. COP CAR - SAME

Turner and Goodnight finish up a box of Harold’s chicken.

TURNER

It’s like, if I’m being honest, 
there’s just so many fake people on 
the force. 

GOODNIGHT

Oh yeah?

TURNER

All the lies. All the little smart-
ass looks. The comments. It’s like 
when I’m off the clock, and Phillip 
or Wendy or any of those guys call 
me to get a beer? No thank you. I’d 
rather hang with the people I grew 
up with. 

Turner sees a HOT GUY pushing his son in a stroller. 

TURNER (CONT’D)

Oh, he is too cute. I should go 
give him a baby to go with that one 
he’s pushing.

(then)

I don’t even know why I put up with 
this dumbass job anymore. 

GOODNIGHT

To clean up these streets.

Waste of time. 

TURNER

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 13 

GOODNIGHT

Nah. 

TURNER

Did they tell you what happened to 
my last partner? 

GOODNIGHT

He got hit by a car chasing a perp 
on foot.

TURNER

No. He got held down and repeatedly 
run over by a group of fifteen year 
olds. Then they buried him in snow. 

GOODNIGHT

I ain’t pressed. This place, these 
people, just need a firm kick in 
the ass. That makes folk listen. 

TURNER

And what you don’t understand is 
that this is a savage community and 
nobody is gonna listen to you. Your 
little Downer’s Grove--

GOODNIGHT

Buffalo Grove.

TURNER

Your Buffalo Grove mentality is 
going to get you killed.

Simon and K exit the store. Simon scowls at Turner.

SIMON

You know you ain’t right! 

TURNER

Keep it moving, sir! 

INT. R.R. DONNELLY & SONS - OFFICE - DOWNTOWN - DAY

Simon is being interviewed by MARTIN, 30s.

SIMON

I’m not leaving here without a job 
today. Sir? Marty? Can I call you 
Marty? 

Go for it.

MARTIN

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 14 

SIMON

Marty, I’m gonna be the hardest 
working bastard you got. I’m gonna 
be the earliest bastard here in the 
morning and I’m gonna be the last 
bastard to leave each night.

MARTIN

Ok, ok. 

SIMON

Pardon my language. 

MARTIN

No, it’s fine. I was actually born 
on sixty second and Kedzie.

SIMON

Oh, so you know the deal?

MARTIN

I know a few things. 

SIMON

Well, I’m from the wild hundreds. 
This job is my chance to turn the 
lemons life threw at me into 
lemonade. That’s why I got my 
degree. 

MARTIN

How fast can you type? 

SIMON

I minored in typing. Seventy words 
a minute. Watch.

Simon stands and goes to Martin’s computer. Opens a blank 
document and types incredibly fast.

MARTIN

Wow. What are you writing? 

SIMON

Everything we’ve said in this 
meeting so far. 

Martin is impressed.

EXT. STREET - SOUTH SIDE - NIGHT

Simon and K walk through a neighborhood. 

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 15 

SIMON

Victory!

K

Corporate America. You did it. 

SIMON

We’re a team. We both did it.

Simon and K approach the door of a run-down house.

K

Not me. 

CUT TO:

INT. ADLER PLANETARIUM - DAY [FLASHBACK]

K chats with a CURATOR.

CURATOR

Kennedy King College? Where is that 
located? 

K

You know where that Walgreens is? 
On Halsted? 

CURATOR

No.

CUT BACK TO:

EXT. STREET - SOUTH SIDE - NIGHT

Simon and K walk. 

K

Why won’t someone hire me on this 
space shit? My dreams are like the 
stars. Right in front of my eyes, 
but light years away.

They arrive at a house. Simon does a secret knock. A doorman 
appears. 

SIMON

Got two cowboys for the rodeo, 
chief.

The doorman lets them in.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 16 

INT. NEIGHBORHOOD HOUSE - SOUTH SIDE - NIGHT

This house is a ghetto strip club. Guys throw ones at “around 
the way” girls. Simon chats up an older stripper MYRNA (50s) 
who sits on his lap. Uncle Spike enters and greets the gang. 
Myrna orders from a passing waitress.

MYRNA

Double Hennessy straight. 

(to Simon)

I’m more fun after Hennessy.

SIMON

I’m gon’ do some nasty shit to you 
later-- 

(seeing)

What’s Q doing here?

K

Oh, he texted me he was bored so I 
told him he should fall through.

Q enters and joins them.

Q

What’s up, Spike? What’s up, 
entrepreneurs? How’s business 
going? 

SIMON

It’s going great. I start tomorrow 
in the administrative assistant 
floater pool at a huge corporation 
downtown. I won, asshole. 

K

I got nothing.

You didn’t win shit.

Q

SIMON
I believe I did.

Q

Whatever, you’ll come crawling 
back. 

(to K)

Where’s my girl Diamond?

K

She was boxing some girl then just 
boned out.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 17 

SIMON

You ready to get out of here, 
Myrna?

MYRNA

Yes. Let’s go to my place so I can 
see if your python will choke me.

Spike offers Simon a viagra.

SPIKE

Here ya go, nephew. Like the Boy 
Scouts say, “always be prepared.”

SIMON

Don’t need it, unc.

SPIKE

Take it. I got plenty.

SIMON

Thanks, but no thanks.

Simon and Myrna head to the door. 

SIMON (CONT’D)

Wait, how old are you, baby?

MYRNA

Fifty eight.

SIMON

That’s younger than I’m looking 
for. But I’m in a good mood so I’ll 
give you an A.A.R.P. pass on these 
nuts tonight.

INT. R.R. DONNELLY & SONS - OFFICE - DAY

Simon delivers coffees to his co-workers, office to office.

SIMON

Miss Lisa, your mocha double with 
splenda. And you can tell Charmaine 
that I’ll have two boxes of those 
Thin Mints, thanks.

(to another)

Chai latte with half and half for 
my man Kevin. Go Bulls.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 18 

SIMON (CONT’D)

Now, Bill, I couldn’t remember if 
you said you wanted a green tea or 
a mint tea. Got ya both!

Martin approaches looking stern.

MARTIN

Simon, can we chat?

INT. R.R. DONNELLY & SONS - OFFICE - DAY

Martin goes over a printout with Simon.

MARTIN

I don’t mean to be the evil white 
guy, but goddamn. Your background 
check was a nightmare! 

SIMON

No. Really?

MARTIN

(reading)

Paternity suits. Multiple 
evictions. Illegal fireworks. 
Improper disposal of hazardous 
waste materials--

SIMON

One of my side-hustles is doing oil 
changes for cars and tractor-
trailers.

MARTIN

And why didn’t you tell me you were 
recently arrested?

SIMON

Oh, right.

INT. MUSEUM OF SCIENCE AND INDUSTRY - ENTRANCE - DAY

Saturday at this iconic Chicago museum. Crowded. Simon talks 
to a museum MANAGER while K hangs back with Simon’s kids. The 
kids all wear matching black t-shirts. 

SIMON

I appreciate you understanding, 
man. These kids only have about a 
month to live. They all love you 
now, man.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 19 

The kids and K follow Simon inside.

INT. MUSEUM OF SCIENCE AND INDUSTRY - NASA EXHIBIT - DAY

Simon and K watch over the kids who love the exhibits.

K

All this NASA crap bums me out.

SIMON

Yeah I know.

K

So how bad was your background 
check?

SIMON

Oh, man, it had shit on there from 
high school. Remember that time our 
baseball team fought the team from 
Calumet?

K

That was on there? 

SIMON

I didn’t think those arrests would 
count. Corporate America might be 
off my list. How’s your job hunt 
going?

K

It’s sucking Columbian donkey 
balls. I’m back at RTO.

SIMON

I can’t blame you. I may have to go 
back there myself. 

K

Can’t you tough it out a little 
longer? At least one of us should 
win.

SIMON

My prospects are grim. I even asked 
Chase and them for help.

CUT TO:

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 20 

INT. CHASE’S HOUSE - BASEMENT - DAY [FLASHBACK]

In his home studio, Chase works the sound board as Travis 
raps for a group of onlookers. Chase is feeling it the most.

TRAVIS

...fuck yo white daughter / Kill yo 
white father... 

CHASE

Yes!

Simon enters.

SIMON

Guys, guys, I need your help!

INT. CHASE’S HOUSE - BASEMENT - DAY [FLASHBACK]

Chase, Travis, Simon and the other sit in a circle. 

SIMON

Alright, let’s go. Brainstorm 
session. I need a job. Travis, what 
you got? 

TRAVIS

Well--

SIMON

That’s legal.

TRAVIS

You’re tying my hands, Joe.

INT. MUSEUM OF SCIENCE AND INDUSTRY - NASA EXHIBIT - DAY

Simon and K with the kids

CUT BACK TO:

SIMON

I just don’t understand why the 
universe is kicking me in the ass 
again.

K

I’d love to tell you, but they 
won’t let a nigga even visit the 
universe.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 21 

SIMON

Why can’t niggas have anything?

K stares at a lunar orbiter in catatonic awe.

INT. RENT-TO-OWN - DAY

Q does paperwork at the front register. Simon approaches.

Q

May I help you, sir?

SIMON

I need my job back.

Q

Need. A small word. An important 
word. We all have needs. I knew 
your sweet and sour, paper cup soft 
ass was going to come back. 

K emerges from the back area to man a register.

K

Q, stop acting like a hoe and give 
him his job back. Damn. 

Q

Enough out of you, employee.

SIMON

I apologize for the way I quit. Now 
come on, man.

Q

We all have needs, Simon. You need 
a job. I need that Xbox from Shaw. 
The new 2K is out. You remember 2K 
don’t you?

SIMON

I taught you how to play it.

Q

You taught me well. But every 
minute I’m not playing, I get 
weaker and them young guys online 
get stronger. Don’t you see what 
I’m going through? For once think 
about somebody other than yourself.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 22 

SIMON

I should have let them Disciples 
kick yo ass in high school.

Q

Maybe you should have. But it’s too 
late for that. If you want your job 
back, get that Xbox back from Shaw.

EXT. STREET - SOUTH SIDE - DAY

Simon and K sit on the back of a truck in their Rent-to-own 
uniforms smoking a blunt. They pass it back and forth. 

SIMON

You know what hurts the most?

K
What’s that?

SIMON

Those four hours at R.R. Donnelly 
were the best hours of my life.

CUT TO:

INT. R.R. DONNELLY & SONS - DAY [FLASHBACK]

Slow motion shots of Simon around the office.

SIMON (V.O.)

Everybody was so nice. The break 
room was spotless. The microwave 
worked. The plants were alive. So 
alive. The bathrooms were 
immaculate. No wet toilet paper. 
They had doughnuts. I had my own 
email address. “S dot James at RRD 
and sons dot com.” All one word. It 
was heaven.

Simon poses for an office selfie.

CUT BACK TO:

EXT. STREET - SOUTH SIDE - DAY

Simon and K finish their blunt.

Let’s get this over with it.

SIMON

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 23 

Simon and K head toward a front porch, where Shaw speaks to a 
street hustler, SECONDS, while his goons look on.

SHAW

My man in Atlanta said you good 
people, so you gon’ love it here. 

SECONDS

Already do, Shaw. Finna get my 
hands dirty, ya feel me?

Simon interrupts them.

SIMON

Mr. Shaw, how are you? We’re from 
Rent-To-Own.

SHAW

Never heard of it.

SIMON

Ok. Well, we believe that you are 
in possession of an Xbox. One of 
ours. 

SHAW

Nah, you wrong. 

A few of the gang members stand.

SIMON

Ok. You take care of yourself.

Simon and K walk back to the truck.

SIMON (CONT’D)

It’s over.

It’s not over until I say it is. 

K

K pulls out his cell phone and makes a call.

EXT. SOUTH SIDE - CORNER - EVENING

Simon and K talk to Goodnight and Turner.

TURNER

Sixty dollars?! You said you had a 
hundred.

I was mistaken.

K

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 24 

GOODNIGHT

Let’s go.

K

Wait. That’s all I have. Please 
help us!

Turner starts to drive, K gets in front of the car.

K (CONT’D)

Guys, I know at some point, a long 
time ago, in a galaxy far away, 
when you joined the force, you 
wanted to do good.

SIMON

This is a waste of time, they don’t 
care. 

GOODNIGHT

I agree. We don’t.

K

(to Turner)

Didn’t you say you was from around 
here? You know how it is for us. 
Have a heart, sister... Black Lives 
Matter?

TURNER

Don’t start with that shit...Ugh. 
Fine.

EXT. SHAW’S HOUSE - FRONT PORCH - EVENING

Shaw and his boys face off with Turner. Simon and K stand 
behind her. Goodnight waits impatiently in the car. 

TURNER

You’re going to let these guys come 
in and get their merch.

SHAW

Come on, officer.

TURNER

I said, you’re going to let these 
guys come and get the X-Box.

Shaw takes a step away from his door, rolling his eyes. Simon 
and K walk past him, entering the house.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 25 

SIMON

Sorry, man.

K

Excuse us.

Shaw stares at Turner.

INT. SHAW’S HOUSE - BASEMENT - NIGHT

Simon and K enter. They’re shocked to see Shaw has 
surprisingly middle-class tastes. Everything is suburban 
nice. They see the Xbox and start to disconnect it.

EXT. SHAW’S HOUSE - FRONT PORCH - CONTINUOUS

Shaw is still staring at Turner. Then he nods, realizing...

SHAW

How much you need?

TURNER

A hundred.

Shaw pulls out his large bill-fold and give her a hundred.

SHAW

And so these dudes in my house--

TURNER

What dudes?

Turner leaves. Shaw and his boys go inside.

INT. SHAW’S HOUSE - BASEMENT - NIGHT

Shaw enters with a few of his goons who surround Simon and K.

SIMON

We’re almost done. Is everything 
ok?

K

(to Offscreen)

Officer Turner!

SHAW

Oh, she left you in my care. 

(to one of his goons)
Get me Lucille Muhammad.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 26 

A goon, BLUTO, hands Shaw a baseball bat with nails hammered 
into it. 

SHAW (CONT’D)

Y’all watch The Walking Dead?

K

Oh God.

SHAW

Oh God is right. I told y’all not 
to touch my xbox. Y’all ready to 
get fucked up over some 
merchandise?

We push in on Simon, his moment of choice. Suddenly, a plan 
forms in his mind.

SIMON

Keep the damn xbox.

(to K)

He probably can’t play 2K anyway. 

SHAW
What’d you say?

SIMON

Probably best you do hurt us so you 
don’t have to get whupped on the 
sticks. In front of all your boys.

The goons erupt at this direct challenge. We see Shaw 
weighing his options. Simon stares back in defiance.

TIME CUT: Shaw and Simon are playing NBA 2K17 and shit 
talking each other.

SIMON (CONT’D)

To the net... COOOKIES!

Simon Dunks on Shaw. We see the replay. Simon is in his 
element, no fear.

TIME CUT: Shaw hits a 3 pointer.

SHAW

(in Simon’s face)

Eat some of these nuts 
motherfucker!

TIME CUT: Simon and Shaw continue to play, watching the 
screen.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 27 

SHAW (CONT’D)

(to his goon)

Bluto? Make me two Remy’s and bring 
these hoes some sex on the beaches.

(to Simon)

So when you was a little boy you 
dreamed about growing up and 
harassing black people for 
appliances?

SIMON

I’m doing the best I can.

SHAW

Well you ain’t doing enough. I’m 
like Diddy, my hustle is beyond 
your imagination. For instance, 
most people have one or two things 
from RTO. But look around... 

Simon looks around the room.

SHAW (CONT'D)

All this shit is RTO. And I haven’t 
paid for a thing in sight. I’ve 
gone beyond what was expected. I 
keep blowing the world’s mind. 
That’s why I win.

This registers with Simon.

EXT. SHAW’S PORCH - NIGHT

Simon and K are leaving. Shaw is in the doorway.

SIMON

So even though I won, you ain’t 
giving me the xbox?

SHAW

I’m giving you yo’ life. 

SIMON

Well, thanks. Truthfully, I 
appreciate the second chance.

SHAW

You ain’t off the hook completely.
I’ma need them Jordans.

Simon looks down at his Jordans one last time.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 28 

INT. MYRNA’S HOUSE - NIGHT

Simon smokes weed in bed with Myrna and her girlfriend PORSHA 
(60s). They just had a threesome. 

SIMON

Ladies, I appreciate you so much. 
You made me feel like a Marlboro 
Man. I almost got killed on the job 
today but your delicious sex has 
helped me reach a place of 
serenity.

MYRNA

Anything for you, Daddy. 

SIMON

Call me Grandson.

PORSHA

We’ll always be here for you, 
Simon.

SIMON

Always? Yeah right. 

MYRNA

You got that young pole that stays 
up and you know how to work it. 
Guys our age are useless. And 
Viagra too expensive. Now all we 
have is a bunch of limp dicks and 
saggy balls.

SIMON

You don’t say.

INT. UNCLE SPIKE’S APARTMENT - DAY

Simon and K sit with his uncle Spike. He hands them two 
cartons of Viagra.

SIMON

Shaw was right, I have to take my 
hustle to the next level.

SPIKE

So how’s this work again?

SIMON

Easy, you keep getting these little 
blue boys at the VA. I’ll sell them 
on the Rent-To-Own route.

South Side - Salahuddin/Riddle -  PILOT - 4/30/17    p. 29 

K

First thing we gotta do is buy my 
brother an Xbox so you keep that 
route.

SIMON

Thank you, unc. The world dealt me 
some shitty cards, but I’m about to 
play a royal flush. 

K

We about to get paid!

Simon and Spike high five. Simon holds aloft two vials.

SIMON

(a la New Jack City)

Damn, Viagra.

(then)

To Entrepreneurship!

K

To Infinity and Beyond!

We push past them to the living room window. Outside, the 
1987 Tan Buick Skylark passes by ominously, driven by a now 
older MAN with wild hair. 

END OF SHOW.

