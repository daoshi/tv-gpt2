 

“Pilot” 

Directed by 

Pamela Fryman 

 
 

 

 

 

 

 

 

 

 

 

 

Written by 

Dana Klein & Mark Feuerstein 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 

Production Drafts 

040617  
040717  
040917  
041217  
041217  
041317  
041317  
041417  
041617  
041717  
041717  
041817  
041817  
041817 
041917 

041917 
 

Table Draft  
Table Draft 1st Rev. (BLUE) 
Table Draft 2nd Rev. (PINK) 
Table Draft 3rd Rev. (YELLOW) 
Table Draft 4th Rev. (GREEN) 
Table Draft 5th Rev. (GOLDENROD) 
Table Draft 6th Rev. (SALMON) 
Shooting Draft 
Shooting Draft 1st Rev. (BLUE) 
Shooting Draft 2nd Rev. (PINK PAGES – 12, 12A, 27) 
Shooting Draft 3rd Rev. (YELLOW PAGES – 24) 
Shooting Draft 4th Rev. (GREEN PAGES – 6, 27, 45) 
Shooting Draft 5th Rev. (GOLDENROD PAGES – 6) 
Shooting Draft 6th Rev. (SALMON PAGES – 1, 4, 9, 11, 16-19, 24, 26, 28, 45) 
Shooting Draft 7th Rev. (CHERRY PAGES – 1, 2, 2A, 3-5, 19, 28, 29, 31-38, 45, 
46) 
Shooting Draft 8th Rev. (TAN PAGES – 2, 2A, 4, 28, 32) 

 

Copyright 2017 CBS Studios Inc.  All Rights Reserved. 
This script is the property of CBS Studios Inc., and may not be copied or distributed without the express written permission of 
CBS Studios Inc. This copy of the script remains the property of CBS Studios Inc.  It may not be sold or transferred and it must 
be returned to CBS Studios Inc. promptly upon demand. THE WRITING CREDITS MAY NOT BE FINAL AND SHOULD NOT 
BE USED FOR PUBLICITY OR ADVERTISING PURPOSES WITHOUT FIRST CHECKING WITH THE TELEVISION LEGAL 
DEPARTMENT. 

 

CONFIDENTIALITY NOTICE 

You are being given a copy of this document for a particular permitted purpose, and may only use it for that purpose. Except as 
may be directly necessary to your proposed or actual duties, you may not make physical or digital copies of this document or 
share a copy of it or the contents of it (or a summary of the contents) with others. This document is protected by the laws 
governing copyright and confidential information. CBS Studios Inc. (“Producer”) has strict policies with respect to protecting our 
scripts, plot lines, plot twists and related Production materials and/or information (the “Confidential Information”).  Producer and 
the  applicable  network  or  other  exhibitor  derive  independent  value  from  the  Confidential  Information  not  being  leaked  in 
advance to the public, the media or anyone not part of the core production team.  A condition of your access to the Confidential 
Information is that you must keep it confidential.  It is crucial to Producer that you not make any unauthorized disclosure, use, 
reproduction, sale and/or distribution of the Confidential Information.  Your failure to comply could result in court action and 
monetary damages. 

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

1.
C.O.

COLD OPEN

FADE IN:

INT. JOSH'S APARTMENT - MORNING (D1)
(JOSH, ANDREW, HARRY, JUDY, EVE, WYATT)

HARRY AND JUDY ROBERTS ARE LOOKING DOWN AT SOMEONE SPECIAL.

HARRY

Would you look at this gorgeous baby?

JUDY

I just wanna eat him and squeeze him 

and chew on his squishy little tushie.

REVEAL: JOSH IN BED. 

JOSH

What are you guys doing?

JUDY

Pretend we’re not here. We just want 

to watch. 

HARRY

We've been waiting twelve years for 

you to leave LA and move back to New 

York!

JUDY

Did you see what we put on the wall?

REVEAL A GIANT POSTER FOR THE TV SHOW "BLIND COP." 

1

2

4

5

6

7

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

2.
C.O.

JOSH

Yes, amazingly I did manage to catch 

that.

JUDY

Detective Mike Cross. You were the 

best thing on television. 

HARRY

How was the flight?

You hungry?

Thirsty?

Hot?

Cold?

JUDY

HARRY

JUDY

HARRY

JOSH

I’m good, I’m good. Again, and I can’t 

stress this enough I’m only staying 

here temporarily. 

We’ll see.

JUDY

HARRY

You are exactly where you should be.

8

9

10

11

12

13

14

15

16

16A

*

*

*

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

2A.
C.O.

JUDY

16B

*

Surrounded by your loving family in the 

apartment we kept ‘cause we knew one 

day you’d get divorced from that cold 

woman who didn’t know a good thing.  

ANDREW BURSTS IN. 

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

3.
C.O.

ANDREW

Well look who fell off the poster! 

(IMITATING JOSH) “A prenup? I don’t 

need a prenup! This marriage is gonna 

last forever!” 

HE STARTS LOVINGLY MAULING JOSH.

JOSH

Hello, Andrew.

ANDREW

Now she's got your money, your house 

and you're living next to your parents 

like a ten-year-old!

JOSH

You are also living next to our 

parents like a ten-year-old.

ANDREW

Just until the renovations on the Park 

Avenue duplex are done. It’s gonna be 

sick. TV's and heated toilets in every 

bathroom! 

JUDY

Ooh, do the toilets have the water 

that shoots up? If you aim right, it 

gives your business a juzj.  

EVE ENTERS WITH BABY WYATT.

EVE

Welcome home, Uncle Josh!

17

18

19

20

21

22

23

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

4.
C.O.

JOSH

Hey, Eve. What’s up, Wyatt.  

JUDY

Eve, does the baby’s head look a 

little--  

EVE

His head’s not flat, Judy. It’s fine.

JUDY

Did you ask a pediatrician?

EVE

Yes. Me. I asked myself. Because as 

you may recall, I am a pediatrician!

JUDY

Susan Stein told me you have a 4.8 out 

of 5 rating on Yelp. (TO BABY) What 

happened to the point two?

HARRY

Do pediatricians know anything about 

baby toes? Because mine looks strange.

24

25

26

27

28

29

30

HARRY IS EXAMINING HIS TOE. EVE LOOKS OVER THEN QUICKLY AWAY. 

EVE

...And I just saw your father’s balls 

again.

ANDREW

It’s like looking at the ghost of 

Christmas future.

31

31A

*

*

*

*

*

*

*

*

*

*

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

5.
C.O.

JOSH

Okay, well, you’re gonna see mine if 

you don’t leave. I have to get 

dressed.    

HARRY, ANDREW AND EVE LEAVE. JUDY SITS ON THE BED. 

JUDY

So? Tell me... 

JOSH

You too, Ma.  

JUDY

Please.  I gave birth to you.  Those 

little balls have been inside me. 

JOSH

Good to be home. 

34

35

36

37

38

FADE OUT:

END OF COLD OPEN

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

6.
I/A

ACT ONE

FADE IN:

A     INT. LOBBY – LATER (D1)

(JOSH, NICK, IAN)

NICK IS AT HIS STATION. IAN SITS ON THE COUCH, READING. JOSH 
ENTERS WITH COFFEE.

NICK

There he is! Welcome back, Josh! 

JOSH

Hey, Nick, what’s going on? 

NICK

Question. You know Pattie DeFina in 

8K? 

JOSH

She and her husband are just below me.

NICK

(CONFIDING) Well last week she was 

below me. It's been going on for 

months. Then a couple days ago, she 

tells me she's leaving Victor. I'm 

like don't leave Victor! 

JOSH

You know how sometimes people assume a 

familiarity they haven't yet earned? 

39

40

41

42

43

44

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

7.
I/A

NICK

Tell me about it! My cousin has 

colitis, every time I see him it's 

like, "Yo, Nick I got the runs--”

JOSH

Yep, you get it! You get what I mean. 

NICK

Josh, this is my man Ian, 5A. Ian, you 

now live in the same building as a 

real live TV star!

IAN

I don’t watch TV. I prefer film. Got 

any movies coming out?

JOSH

Not at the moment, no. My plan is to 

get back into theater, that’s why I 

moved back to New York.

NICK

Angie in 3J heard it was because you 

lost all your money in the divorce and 

are staying with your parents for 

free. 

IAN

You live with your parents? 

JOSH

Not with, next to! 

45

46

47

48

49

50

51

52

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

8.
I/A

Same thing.

IAN

JOSH

No it’s not! It’s not the same thing 

at all! 

IAN

Alright, guy, cool, you live "next to" 

your parents.

JOSH

Thank you.

53

54

55

56

HAVING LAID DOWN THE LAW, JOSH HEADS FOR THE ELEVATOR. IAN 
SHOOTS NICK A LOOK.

CUT TO:

B     INT. 9TH FLOOR HALLWAY – MOMENTS LATER (D1)

(JOSH, JUDY)

JOSH HAS BARELY GOT HIS KEY IN THE LOCK WHEN JUDY WHIPS OPEN 
HER DOOR, STARTLING HIM. 

Hi, Joshy.

JUDY

JOSH

That was good timing...

JUDY

I got your favorite black and white 

cookies. Wanna come in for a visit? 

JOSH

I don’t know, Ma, I have to unpack and-- 

JUDY

I’ve been waiting twelve years.

57

58

59

60

61

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

9.
I/A

JOSH

Alright, just for a little.

62

CUT TO:

C     INT. HARRY AND JUDY’S APARTMENT – AN HOUR LATER (D1)

(JOSH, HARRY, JUDY)

JOSH SITS AT THE TABLE, SURROUNDED BY STACKS OF MAGAZINES AND 
NEWSPAPERS, CIRCA 1978-2017, AS JUDY RATTLES ON.

JUDY

...And then I ran into Lynn Howard. 

David's company went public. You were 

always in the higher math group. You 

win. 

HARRY (O.C.)

Judy, what did you do with the number 

for the podiatrist? 

63

64

REVEAL HARRY BEHIND THEM, STARING AT THE WALL, WHICH IS 
COVERED FLOOR-TO-CEILING IN WEATHERED YELLOW POST-IT NOTES.

JUDY

65

I hid it in the freezer just to 

torture you. I have no idea, Harry. 

(THEN, TO JOSH) Oh and Helen’s 

daughter Marci is single...

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

10.
I/C

Ma.

JOSH

JUDY

What, you picked the first one, now 

it’s my turn.

JOSH

(STANDING) And I’m out. 

HARRY

Josh, I need a favor. I had an idea to 

promote the firm's estate planning 

business. We're going to make a viral 

video! 

JOSH

You can't make a viral video, Dad. It 

just goes viral or it doesn't.

HARRY

Okay, well this one does! And it would 

be terrific if you would star in it.

JOSH

Oh, you know, I'd love to but I’m 

really busy. 

HARRY

What are you so busy with?

JOSH

You know, I have scripts to read, I’m 

meeting with theater agents... 

67A

67B

68

69

70

71

72

73

74

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

11.
I/C

HARRY

75

No problem. Forget I asked.

JOSH TRIES TO TOLERATE THE DISCOMFORT OF SAYING NO. BEAT.

I'll do it.

JOSH

HARRY

You're not doing it.

JOSH

I want to do it.

HARRY

I don't want you to do it.

JOSH

Please, Dad, let me be in this viral 

video for you!

HARRY

Okay, if it means that much to you. 

Thank you.

JOSH CROSSES OUT.

JOSH

JUDY

76

77

78

79

80

81

82

83

Such a good boy. (TO HARRY) Marci’s 

gonna love him.

CUT TO:

D     EXT. CENTRAL PARK – LATER (D1)

(JOSH, ANDREW, CHRISTINA, MAN, EXTRAS)

ANDREW AND JOSH ARE COOLING DOWN, POST JOG.  

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

12.
I/C

ANDREW

The word is no. Say it with me, 

nooooo.

JOSH

I am perfectly capable of saying no to 

them.

ANDREW

You let mom bathe you til you were 

ten. 

JOSH

I had a broken leg and there were 

bubbles covering everything!

A MAN WALKS BY AND RECOGNIZES JOSH.

MAN

Oh snap, it's the blind cop! (GUN 

WRONG WAY) "I got him, Chief. I got 

him!" That show sucked!

JOSH

Thank you so much. 

AS THE MAN MOVES OFF, ANDREW CALLS AFTER.

ANDREW

Hey, it was on for two seasons and the 

NY Times called it “a tv show!”

CHRISTINA APPROACHES.

CHRISTINA

Oh my god. Josh Roberts?

84

85

86

86A

87

88

89

90

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

12A.
I/D

ANDREW

91

Yes. Blind Cop sucked. (TO ALL) We all 

know it sucked. 

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

13.
I/D

JOSH

No, Andrew this is Christina Jones. We 

went to Michigan together. (TO 

CHRISTINA) This is crazy! Hi! You look 

amazing!! 

ANDREW

I’ll let you guys catch up. I gotta 

get back to the hospital. (TO 

CHRISTINA, RE: SELF) Surgeon. 

92

93

ANDREW CROSSES OFF. JOSH AND CHRISTINA TAKE EACH OTHER IN.

CHRISTINA

I don’t think I’ve seen you since Nude 

Olympics. You streaked across campus 

and did push-ups outside my dorm.

JOSH

For the record, it was mid-January and 

the push-ups were into snow, so... 

just food for thought.

CHRISTINA

(LAUGHS) So how’ve you been Josh? 

JOSH

Well I got divorced and my show got 

cancelled-- two things that sound 

really bad but could maybe be good. 

How ‘bout you? 

CHRISTINA

I also just went through a breakup. 

94

95

96

97

98

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

14.
I/D

99

100

101

CUT TO:

102

103

104

105

JOSH

That’s awesome! I mean, how are you 

doing with that?

CHRISTINA

It was for the best. His family was a 

nightmare. So overbearing. 

JOSH

Huh. I cannot relate to that at all. 

E    INT. LOBBY - LATER (D1)

(JOSH, NICK, IAN)

NICK IS THERE WITH IAN. JOSH ENTERS. 

JOSH

What’s up, Fellas? Things are looking 

up for the kid!

IAN

Did you get an audition?

JOSH

No, I did not. But I did get a date 

with Christina Jones. 

IAN

That’s not gonna pay the bills. (THEN) 

My friend Billy's father's producing 

Paul Feig's follow-up to Bridesmaids. 

It's called Groomies-- has some 

problems in the second act, but 

they’ll find it. 

(MORE)

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

IAN (CONT'D)

15.
I/E

You should ask your agent about the 

role of the doctor. Get that Blind Cop 

stank off ya!  

JOSH

Thank you, Ian, but I don’t need 

career advice from a twelve-year-old. 

106

SMASH CUT TO:

H     INT. BUILDING STAIRWELL - MOMENTS LATER (D1)

(JOSH)

JOSH CLIMBS THE STAIRS ON HIS CELL.

JOSH 

107

It’s called Groomies. The role of the 

doctor. ... Yes, Danny, it would be a 

great job for me to get, that’s why 

I’m calling you, my agent to go and 

get it. 

JOSH HANGS UP AND CONTINUES UP THE STAIRS.

J     INT. 9TH FLOOR HALLWAY – MOMENTS LATER (D1)

(JOSH, JUDY)

JOSH TIPTOES OUT OF THE STAIRWELL AND DOWN THE HALL. AS SOON 
AS HIS FINGERS GRAZE HIS DOORKNOB, JUDY WHIPS OPEN HER DOOR.

CUT TO:

Hi Josh.

JUDY

JOSH

How?!  How do you always know?

JUDY

We used to be one body. 

108

109

110

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

16.
I/H

JOSH SIZES HER UP AS HE ENTERS HER APARTMENT.

JOSH

(SUSPICIOUS) Okay, okay.

111

CUT TO:

K     MONTAGE - 9TH FLOOR HALLWAY - OVER THE NEXT FEW DAYS

-JOSH MAKES HIS WAY DOWN THE HALL FLAT AGAINST THE WALL LIKE 
A NINJA. BEFORE HIS KEY IS IN THE LOCK. JUDY OPENS HER DOOR.

-JOSH CRAWLS ON ALL FOURS PAST HIS PARENTS' DOOR AND REACHES 
UP FROM THE FLOOR FOR HIS DOORKNOB. JUDY OPENS HER DOOR. 

-JOSH SPRINTS FROM THE ELEVATOR TO HIS DOOR. BEFORE HIS KEY 
MAKES IT INTO THE LOCK, JUDY NAILS HIM.

-SURRENDERED TO THE INEVITABLE, JOSH WALKS DIRECTLY TOWARD 
JUDY’S DOOR-- SHE OPENS IT AT EXACTLY THE RIGHT MOMENT AND HE 
CONTINUES INSIDE.

M    INT. JOSH’S APARTMENT – SATURDAY AFTERNOON (D3)

(JOSH, ANDREW, HARRY)

JOSH, DRESSED UP FOR HIS DATE, TALKS ON HIS CELL PHONE. 

JOSH

112

Any word on the Paul Feig audition? 

... They don't believe I could play a 

doctor?! I look like every doctor I’ve 

ever been to! Alright, thanks for 

checking.

AS HE HANGS UP, 

HARRY (O.C.)

Josh.

HARRY STANDS IN FRONT OF THE OPEN CLOSET.

JOSH

And look who’s back in my apartment.

113

114

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

17.
I/M

HARRY

115

Costco closet. (THEN) It just so 

happens my classmate from law school, 

Eddie Aberman, represented Paul Feig's 

cousin, Barry, in his IRS audit. 

Should I ask Eddie to set up a meeting 

for you and Paul?

JOSH

Dad, one of the biggest comedy 

directors in Hollywood is not going to 

meet with me because of his cousin's 

connection to Eddie Aberman. 

ANDREW ENTERS HOLDING A PLATE WITH A BURGER.

ANDREW

Do we have ketchup?

HARRY

Do we have ketchup? 

118

119

120

HE PUTS THE KETCHUP ON THE TABLE AND ANDREW HELPS HIMSELF.  

ANDREW

121

(TO JOSH) You look nice. 

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

18.
I/M

HARRY

That is because he is co-starring in 

my viral video today. (PICKS UP TOILET 

PAPER) This should get Mommy and I 

through the winter.  

HARRY EXITS WITH THE 24-PACK OF TOILET PAPER. 

JOSH

I forgot about that stupid video. I’m 

dressed up because I have a date 

tonight.

ANDREW

That girl from the park who was 

checking me out? 

JOSH

To save time I call her Christina. 

(THEN) Don’t tell mom and dad. They’ll 

want to meet her and feed her and ask 

about her ability to have children. 

122

124

125

126

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

19.
I/M

ANDREW

Obviously. (THEN) So... (WEIGHING 

THEM) first date in eight years with 

hot girl or Dad’s sad video?

JOSH

I can do both. How long can the video 

take?

129

130

SMASH CUT TO:

P     INT. SINGER & STERLING CONFERENCE ROOM – LATER (D3)

(JOSH, HARRY, EXTRAS)

HARRY AND JOSH SIT AT A CONFERENCE TABLE. 

JOSH

Take forty-two. 

HARRY

Okay, okay, I got it this time. (IN 

CHARACTER) David! I am so sorry for 

your loss. 

JOSH

My father’s death was so sudden and 

I’m overwhelmed by all the decisions I 

have to make.

HARRY

Well, luckily your father WAS ready 

for just THIS moment and hired SINGER 

STERLING TO PREPARE THE DOCUMENTS!

131

132

133

134

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

20.
I/P

JOSH

Nailed it! Perfect, no notes, can’t 

beat it, gotta go.

HARRY

Yeah. It felt good.

JOSH BOLTS.

135

136

CUT TO:

R     EXT. NYC SIDEWALK/INT. EVE AND ANDREW’S APARTMENT – DUSK (N3)

(JOSH, MOLLY, EXTRAS)

JOSH IS HURRIEDLY HAILING A CAB WHEN HIS CELL RINGS.

SFX: CELL PHONE RINGS

JOSH

(ANSWERS PHONE) Hello?

SPLIT SCREEN/TIGHT ON MOLLY, 16, HYSTERICAL.

MOLLY

Josh? This is Molly, Andrew and Eve's 

babysitter? You need to come home.

JOSH

Oh my God! Did something happen to the 

baby? 

MOLLY

I dunno, it’s in the other room but I 

am having a crisis. My boyfriend 

hooked up with my best friend so now I 

need to go hook up with his, and 

Andrew and Eve aren’t answering their 

phones. 

137

138

139

140

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

21.
I/R

JOSH

Well I’m barely gonna make my date as 

it is. Go get my mother.

MOLLY

Your mother’s not home. You have ten 

minutes.

JOSH

No. I'm sorry, but no. This is not my 

problem.

141

142

143

S     INT. RESTAURANT - LATER (N3)

(JOSH, ANDREW, EVE, CHRISTINA, WYATT, EXTRAS)

JOSH, RESENTFUL AND MORTIFIED, ENTERS, WITH WYATT IN A BABY 
BJORN. HE SPOTS CHRISTINA AT A TABLE AND CROSSES OVER AS IF 
EVERYTHING IS NORMAL.

SMASH CUT TO:

144

145

146

JOSH

Hey, sorry I’m a little late. (SITS 

OPPOSITE HER) You look incredible. 

CHRISTINA

Thanks. (THEN) Why are you wearing a 

baby? 

JOSH

Oh I don't go anywhere without my wing 

baby. (TO BABY) Pound it out, bro. 

(THEN) No, this is my nephew, Wyatt. 

Babysitting snafu. My brother and 

sister-in-law should be here literally 

any second.

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

22.
I/S

He's cute. 

CHRISTINA

JOSH

Good thing too, because he is (COVERS 

BABY’S EARS) very self absorbed.

ANDREW AND EVE RUSH IN.

EVE

Sorry, hi, sorry. 

147

148

149

AD LIB HELLOS TO CHRISTINA AS EVE TAKES WYATT FROM JOSH.

EVE (CONT’D)

(TO JOSH) Thank you, you’re our hero. 

ANDREW

Actually I’m our hero-- I crushed that 

escape room. 

EVE

We got all the clues, the other 

couples got none. Chiropractors are so 

stupid. 

THEY HIGH FIVE.

ANDREW

Why’d you bring a baby on a date? It’s 

like the opposite of foreplay.

JOSH

It’s pronounced “thank you.”

EVE

You know, you guys make a cute couple. 

I’m hoping this works out.  

150

151

152

153

154

155

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

23.
I/S

JOSH

156

You gotta go, goodbye.

EVE AND ANDREW HEAD OUT. JOSH EXHALES, RELIEVED, AND SETTLES 
INTO THE DATE. 

JOSH (CONT’D)

157

So...  

ANDREW CROSSES BACK, GRABS SOME ROLLS FROM THE BASKET.

ANDREW

I love you so much. 

ANDREW EXITS. 

158

INTERNAL 
DISSOLVE:

S1    INT. RESTAURANT - LATER (N3)

(JOSH, CHRISTINA, EXTRAS)

DINNER CLEARED, CHECK PAID, WINE GLASSES EMPTY. JOSH AND 
CHRISTINA ARE FLIRTY. 

JOSH

If college me knew I was on a date 

with Christina Jones... Oh my god, I 

had the biggest crush on you. 

CHRISTINA

I had the biggest crush on you! 

JOSH

CHRISTINA

You did?

I did.

159

160

161

162

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

24.
I/S1

JOSH

That is information I wish college me 

had had. 

THEY LAUGH, IT’S FLIRTY, HE TOUCHES HER ARM.

CHRISTINA

So... should we maybe... get outta 

here?  

JOSH

We should definitely get out of here. 

Where do you live?

CHRISTINA

My ex got our apartment so I’m on a 

friend’s sofa for a bit. Should we go 

to your place?

JOSH

My places? Uhhhh, yeah. My place is 

great. 

ON JOSH’S CONCERN, WE...

163

166

167

168

169

FADE OUT:

END OF ACT ONE

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

25.
II/T

ACT TWO

FADE IN:

T    INT. LOBBY - LATER (N3)

(JOSH, NICK, CHRISTINA, EXTRAS)

JOSH AND CHRISTINA ENTER. NICK TAKES CHRISTINA IN.

NICK

Oh my damn. Welcome to East End Place. 

I am the Captain of this fair 

establishment so if you need anything, 

anything at all, you just holler.

JOSH

Thanks, Nick, but I don't think she's 

having a package delivered tonight.

NICK

Believe in yourself, man.

JOSH USHERS CHRISTINA INTO THE ELEVATOR AS...

NICK (O.C.) (CONT’D)

Yeah, Mrs. Roberts, he's on his--

170

171

172

173

JOSH RACES OUT OF THE ELEVATOR AS THE DOORS BEGIN TO CLOSE, 
TRAPPING CHRISTINA INSIDE. JOSH WRESTLES THE PHONE AWAY.

NICK (CONT’D)

...way up. And he's--

JOSH

What are you doing?!

NICK

Telling your mom you’re home.

174

175

176

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

26.
II/T

JOSH

(HANGING UP PHONE) Why would you do 

that?!

NICK

‘Cause of the money she gives me to 

tell her when you’re home.

JOSH

My mother pays you to tell her every 

time I come home?

THE ELEVATOR DOORS RE-OPEN, REVEALING CHRISTINA.

CHRISTINA

Everything okay? 

JOSH

Couldn’t be better! (LOUD, FOR HER 

BENEFIT) So problem solved, we’ll keep 

a cardboard box out here for the days 

my fan mail won’t all fit in the 

mailbox. (THEN) And away we go! 

HE JOINS HER IN THE ELEVATOR. 

V     INT. ELEVATOR - SECONDS LATER (N3)

(JOSH, CHRISTINA)

JOSH

So I just have to run in and say a 

quick hello to my elderly neighbor. 

(HANDS HER KEY) Meet me in 9K, there’s 

wine in the fridge. 

177

178

179

180

181

CUT TO:

182

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

27.
II/V

CHRISTINA

Babies, old people. You are such a 

good guy.  

JOSH

Well, you know, I do what I can. 

THE ELEVATOR DOORS OPEN AND HE BOLTS. 

183

184

CUT TO:

W     INT. HARRY AND JUDY'S APARTMENT - A SECOND LATER (N3)

(JOSH, JUDY)

JOSH RUNS IN AND BUMPS INTO JUDY WHO'S WAITING BY THE DOOR.

JOSH

Let's do this!

HE GRABS JUDY'S HAND AND PULLS HER OVER TO THE TABLE.

JUDY

I got some of your favorite--

JOSH

Cheese. Yum! (SITS, PULLS HER INTO 

NEARBY SEAT) So who'd you run into 

today? Harriet? Susan? Emily? 

JUDY

Yes, Emily Brown. Her son Jonathan is 

the youngest circuit--

JOSH

Court judge ever but I beat him in 

mock trial in eighth grade so I win! 

(STUFFING HANDFUL IN MOUTH) Mmm, 

delicious! Love that cheese! 

(MORE)

185

186

187

188

189

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

JOSH (CONT'D)

Alright, well, great visit, I'm 

exhausted. Going to bed.

HE STANDS AND IS GONE.

28.
II/W

CUT TO:

X     INT. JOSH’S APARTMENT – A SECOND LATER (N3)

(JOSH, JUDY, CHRISTINA)

JOSH ENTERS, SWALLOWING CHEESE. CHRISTINA IS ON THE COUCH. HE 
CLOSES THE DOOR, LOCKS IT, PUTS THE CHAIN ON, THEN TESTS THE 
DOOR. 

*
*

CHRISTINA

Is there a lot of crime in this 

building?

JOSH

You can never be too careful.

CHRISTINA

Well, I feel very safe with that guy 

guarding us. 

JOSH

That’s why he’s there! (THEN) So...

CHRISTINA

So...

190

191

192

193

193A

THEY SMILE AT EACH OTHER, START TO LEAN IN FOR A KISS.

SFX: THE DOORBELL RINGS.

JUDY (O.C.)

194

Josh?

*

*

*

*

*

*

*

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

29.
II/X

JOSH

195

It’s just my elderly neighbor. She 

probably lost her teeth again, she’ll 

find them. 

SFX: THE DOORBELL RINGS SEVERAL TIMES

JOSH (CONT’D)

196

If we stay quiet maybe she’ll go away.

A KEY TURNS IN THE LOCK. THE DOOR OPENS THREE INCHES UNTIL 
THE CHAIN CATCHES.

JUDY (O.C.)

198

(CONCERNED) Joshua? What is going on?! 

LIGHT CUE: JUDY'S HAND REACHES THROUGH THE OPENING OF THE 
DOOR AND STARTS FLICKING THE LIGHTS ON AND OFF. SHE KEEPS 
FLICKING THE LIGHTS.

CHRISTINA

Perhaps I don't quite understand your 

relationship with your neighbor...

JOSH

You are not alone. (THEN USHERING HER 

UP) Why don’t you go check out the 

terrace while I get rid of her. 

Beautiful view, especially at night.

199

200

HE USHERS HER TO THE TERRACE, RACES BACK TO THE FRONT DOOR. 

RESET TO:

X1    INT. 9TH FLOOR HALLWAY - CONTINUOUS (N3)

JOSH'S ANGRY FACE APPEARS IN THE CRACK OF THE OPEN DOOR.

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

30.
II/X1

JOSH

We had our moment! 

JUDY

Oh thank god you’re okay. 

JOSH

Of course I’m okay! I was in your 

apartment ten seconds ago!

JUDY

First, I don’t like your tone. And 

second, I forgot to give you this. 

(PUSHES NEWSPAPER ARTICLE THROUGH) 

That actor you always go up against 

got a terrible review in his new play.

JOSH

Thank-you-so-much-really-appreciate-it-

goodnight. Goodnight, goodnight, 

goodnight!

201

202

203

204

205

JOSH TAKES THE ARTICLE AND CLOSES THE DOOR THEN HEADS FOR THE 
TERRACE, GLANCING AT THE REVIEW.

JOSH (CONT’D)

(DELIGHTED) Ooh, that’s not very nice. 

206

CUT TO:

Y     EXT. TERRACE (9K) - A LITTLE LATER (N3)

(JOSH, ANDREW, HARRY, CHRISTINA)

JOSH AND CHRISTINA STAND LOOKING OUT AT THE STREETS BELOW.

CHRISTINA

207

It's really beautiful out here.

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

31.
II/X1

JOSH

Yeah. (LOOKS AT HER) It really is.

THEY LEAN IN FOR A KISS. CLOSER AND CLOSER AND... 

HARRY (O.C.)

Can you hear me now?!

208

209

HARRY, IN UNDERPANTS AND A SHIRT, HAS STEPPED ONTO THE 
ADJOINING TERRACE, CRADLING A CELL PHONE AND HOLDING A PLATE 
OF SLICED MELON. 

HARRY (CONT’D)

Hello, children! (INTO PHONE) I’ll 

call you back. I WILL CALL YOU BACK!

JOSH

And that’s the terrace! Come back in.

HARRY

(EXTENDS HAND) Hello, I'm Harold-- 

JOSH

Hey Harold! Great to see you!

HARRY

Young lady, have you ever had a 

gorgeous piece of honeydew melon?

CHRISTINA

I've had melon. It looked good. I 

don't know if it was "gorgeous"...

HARRY

Well this is a gorgeous piece of 

honeydew melon.  

210

211

212

213

214

215

216

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

32.
II/Y

CHRISTINA/JOSH

217

Oh, I, uh... / Harold, Harold, Harold, 

no! Why?!

JOSH WATCHES AS HARRY SHOVES THE MELON INTO HER MOUTH. 

JOSH

Sorry! So sorry. Why don’t you go make 

us some stronger drinks. There’s a jar 

of two thousand olives in the closet. 

SHE EXITS INSIDE. JOSH TURNS ON HARRY.

JOSH (CONT’D)

You never come out on the terrace! Why 

are you on the terrace?!

HARRY

I’m trying to get reception. How was I 

supposed to know you were out here 

giving some girl a Wowee? Mommy said 

you went to sleep.

JOSH

Can you not call her "mommy!" And what 

if she didn’t like melon?

HARRY

Then she is not the girl for you.

218

219

220

221

221A

ANDREW STEPS ONTO HIS BALCONY ON THE OTHER SIDE OF JOSH'S.

ANDREW

222

What are you morons screaming about? 

You're gonna wake the baby. (TO JOSH) 

How's the date going?

*

*

*

*

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

33.
II/Y

JOSH

Great. Dad’s melon just got to first 

base. 

ANDREW

Relax, dude. You're just nervous 

'cause you haven't gotten laid in ten 

months.

HARRY

(SHOCKED) It's been ten months since 

you've had intercourse?! I mean, Mommy 

and I aren't like we used to be, but 

we still do it on Valentine’s Day and 

on both your birthdays. 

ANDREW/JOSH

Dad, please. / Birthday ruined! 

HARRY

Ever since I had that varicose vein 

removed from my testicle, I produce a 

lot more semen so--

JOSH/ANDREW

Why? / Again with the testicle?

HARRY

Well, it's very exciting that 

intercourse is on the table. You need 

me to make a condom run?

223

224

225

226

227

228

229

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

34.
II/Y

JOSH

All I need is privacy. And, don't tell

Mommy. (QUICKLY) Mom! We call her Mom.

HARRY

Okay. Go have some nice sex, son.  

ANDREW

Think about Dad's testicle. Could buy 

230

231

232

you some time. 

JOSH DISAPPEARS BACK INTO...

Y1    INT. JOSH’S APARTMENT – CONTINUOUS (N3)

(JOSH, ANDREW, HARRY, JUDY, EVE, CHRISTINA)

JOSH IS CLOSING THE CURTAINS TO THE TERRACE AS CHRISTINA 
ENTERS WITH TWO COCKTAILS.  

RESET TO:

JOSH

So sorry about that. Where were we? 

THEY LEAN IN FOR A KISS. FROM OUTSIDE ON THE TERRACE:

JUDY (O.C.)

A girl! My son lied to me?

"Son?"

CHRISTINA

JOSH

I told her she could call me her son 

because... well... you're gonna love 

this-- I am her son. 

ANDREW (O.C.)

Ma, what are you screaming about?

233

234

235

236

237

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

35.
C.O.

Who's that? 

CHRISTINA

JOSH

My brother. He, Eve and the baby live 

on the other side. (THEN) I'm sorry I 

just, look, I really like you and I 

know my living situation isn't exactly 

a turn on. (THEN, TRYING) Unless it 

is?

It's not. 

CHRISTINA

JOSH

No, it’s not, I know it’s not.

CHRISTINA

But it's not a big deal. So you live 

next to your family. 

JOSH

Yeah, right, no biggie. They’re just 

neighbors who I happen to share DNA 

with.

EVE (O.C.)

You guys woke Wyatt!

JOSH

Let’s just pretend they're not there.

238

239

240

241

242

243

244

245

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

36.
II/Y1

THEY FINALLY KISS. 

HARRY (O.C.)

Judy, are you aware that Josh hasn't 

had intercourse in ten months?!

JOSH

Would you excuse me for a moment 

please?

246

248

JOSH FLINGS OPEN THE CURTAIN, OPENS THE DOOR, AND...

RESET TO:

Y2    EXT. TERRACES (9J/9K/9L) - CONTINUOUS (N3)

(JOSH, ANDREW, HARRY, JUDY, EVE, CHRISTINA)

JOSH APPEARS. HE IS IRATE. 

JOSH

What. Is. Wrong with you people?! This 

is my first date since the divorce and 

you're ruining it! With your loudness 

and your horrible babysitter and your 

testicle.

HARRY

It’s doing fine, God forbid anyone 

should ask.

EVE

Don’t need to ask, see them twice a 

day. 

JOSH

I am a grown man! I need space!

249

250

251

252

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

37.
II/Y1

JUDY

We give you space.

JOSH

You asked to squeeze my squishy little 

tushie!! And you pay the doorman to 

tell you every time I come up in the 

elevator!

ANDREW

You pay the doorman to tell you every 

time Josh comes up in the elevator?!

JOSH

Insane, right?

ANDREW

Why don't you pay him to tell you 

every time I come up in the elevator?! 

JUDY

You don't like to visit with me.

JOSH

Neither do I.

CHRISTINA 

Sooo, I'm gonna go...

JOSH

Can I call you tomorrow? 

ANDREW/EVE/JUDY/HARRY

Dude, read the room. / I wouldn’t. / Not 

gonna happen. / That ship has sailed.

CHRISTINA NODS AND EXITS. 

253

254

255

256

257

258

259

260

261

262

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

38.
II/Y2

263

264

265

266

267

268

269

270

JOSH

Boundaries! Boundaries, boundaries, 

boundaries!! I require a modicum of 

respect.

ANDREW

("BIG WORD") Oooh, modicum!

SFX: HARRY'S CELL PHONE RINGS

HARRY

(ANSWERING PHONE) Hello?

JUDY

(TO JOSH) You’ve always had such a 

great vocabulary, Josh. 

ANDREW

And I saved three lives this week.

JUDY

(TO JOSH) Remember, you won the fifth 

grade spelling bee.

ANDREW

Like, they were dead and then I made 

them not dead.

HARRY

(INTO PHONE) Okay thanks. (HANGS UP) 

That was Eddie Aberman. He spoke to 

Barry Feig. His cousin, the director 

Paul Feig, will meet you if you can 

get to his hotel in Soho in the next 

twenty minutes.

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

39.
II/Y2

JOSH

What?! Are you serious? That's 

amazing! And horrible! It's all the 

way downtown. I'll never make it!

HARRY

(DETERMINED) Oh you will make it. 

(THEN) Judy, get my pants! 

Z     INT. HARRY ROBERTS'S BMW - MOMENTS LATER (N3)

(JOSH, ANDREW, HARRY, JUDY)

HARRY DRIVES. JOSH RIDES SHOTGUN. 

JOSH

Waze says go right on Mott. (PASSING

MOTT) Right on Mott!

HARRY

I'm taking Houston. I know these 

streets better than some computer.

ANDREW

("WATCH OUT!") Old couple on your 

left! Old couple on your left! 

HARRY

Why are they walking there?!

JOSH

That’s the sidewalk, it’s where 

they’re supposed to be!

HARRY SWERVES. THEY SCREAM. THEN, SILENCE. BEAT. 

271

272

CUT TO:

273

274

275

276

277

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

40.
II/Y2

JUDY

So. You don't like to visit with me.

REVEAL JUDY NEXT TO ANDREW IN THE BACKSEAT.

JOSH

‘Course I do, Ma, just not every time 

I come home. (NERVOUS) I can't believe 

I'm about to meet Paul Feig.  

ANDREW

Josh, relax. Dad's law school 

classmate handled his cousin's IRS 

audit. You're a shoo-in for the lead. 

HARRY

We’re here. (STOPS CAR) Careful 

getting out-- we’re on the wrong side 

of the street. 

JOSH

See you guys at home.

ANDREW

Sit across from him, never in profile. 

JOSH BOLTS OUT OF THE CAR.

AA    INT. WATERFRONT HOTEL BAR - MOMENTS LATER (N3)
(JOSH, ANDREW, HARRY, JUDY, PAUL FEIG, EXTRAS)

JOSH SPOTS PAUL FEIG AND GOES OVER. 

JOSH

Hi, Mr. Feig, I'm Josh Roberts. I’m--

PAUL FEIG

The Blind Cop!

278

279

280

281

282

283

284

285

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

41.
II/Z

JOSH

(BUMMED) Oh. You've seen the show.

PAUL FEIG

Yes. (BEAT) It was genius. Detective 

Mike Cross didn’t have the gift of 

sight and yet he could see the 

darkness in humanity. 

JOSH

Yes! That's exactly what drew me to 

the script in the first place! (THEN) 

Mr. Feig I am such a huge fan, Freaks 

and Geeks was brilliant. Bridesmaids 

is like a perfect movie. I know it's a 

long shot, but if there's any way I 

could read for the role of the doctor 

in your new movie--

PAUL FEIG

There's no way. 

JOSH

No way.

PAUL FEIG

No way. Steve Carell’s playing that 

role. But you... were a great blind 

cop. 

PAUL FEIG STANDS UP AND OFFERS HIS HAND. 

JOSH

Well, it was an honor to meet you.

286

287

288

289

290

291

292

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

42.
II/AA

PAUL FEIG

There is the part of the klutzy band 

leader. Only a few lines before he 

falls off stage and cracks his head 

open but--

JOSH

293

294

I’ll take it! Alright. I’m leading the 

band!

PAUL FEIG CROSSES OUT. JOSH IS HAPPY. 

HARRY, JUDY AND ANDREW LEAN AROUND FROM WHERE THEY HAVE BEEN 
HIDING BEHIND A CURTAIN. 

JUDY

My baby’s in a Paul Feig movie!

ANDREW

I can’t wait to see those nostrils on 

Imax. 

HARRY

Don’t forget to write Eddie Aberman a 

nice thank you note. 

I will.

JOSH

HARRY

And you know... Morty from the racquet 

club is married to Martin Scorsese’s 

niece’s orthodontist. Would you like 

to be in a Scorsese film?

295

296

297

298

299

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

43.
II/AA

JOSH

Yeah, that’d be great, Dad. 

JUDY

Let’s go home.

JOSH

Okay, I just want to say something 

first. This has been the toughest year 

of my life with the divorce and the 

show and I just... I’m very lucky. You 

guys are always there for me. 

HARRY

And we always will be. 

Always. 

JUDY

JOSH

I know. Thank you. 

JUDY

(MORE OMINOUS) Always.

AND WE...

300

301

302

303

304

305

306

FADE OUT.

END OF ACT TWO

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

44.
TAG

AA1   INT. LOBBY - A COUPLE DAYS LATER (D4)

(JOSH, NICK, IAN)

TAG

NICK AND IAN ARE THERE. JOSH ENTERS, HOLDS UP AN ENVELOPE.

JOSH

Box seats Knicks-Warriors tonight! 

NICK

For me? For real?

JOSH

If you stop telling my mother when I’m 

coming up.

Done, son!

NICK

307

308

309

310

NICK GRABS THE ENVELOPE, JOSH CROSSES OFF. IAN LOOKS UP.

IAN

I bet the mom can do better than 

Knicks tickets. 

NICK

Looks like we got ourselves a bidding 

war up in this piece!

311

312

BB    INT. 9TH FLOOR HALLWAY - LATER (D4)

(JOSH)

JOSH HAPPILY BREEZES PAST HIS PARENTS' DOOR AND INTO...

CC    INT. JOSH’S APARTMENT - CONTINUOUS (D4)
(JOSH, ANDREW, HARRY, JUDY, EVE, WYATT)

JOSH SHUTS THE DOOR AND CLOSES HIS EYES, RELISHING VICTORY.

HARRY/JUDY

313

There he is! / Hi, Joshy.

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

45.
TAG/CC

JUDY IS SITTING ON THE COUCH. HARRY IS UNPACKING TONS OF 
COSTCO BULK ITEMS AND PUTTING THEM IN THE CLOSET.

JOSH

Why are you in here? 

HARRY

Costco run. Is that a gorgeous barrel 

of cashews!

JUDY

Helen’s daughter Marci is expecting 

your call. I already dialed the first 

nine numbers. 

SHE WAVES HER CELL PHONE AS EVE ENTERS. 

EVE

Don’t mind me, just came to get my 

breast milk from your fridge.

JOSH

Why is your breast milk in my fridge?

JUDY

(WAVES PHONE) Marci’s waiting. 

ANDREW ENTERS HOLDING THE GIANT BLIND COP POSTER. 

ANDREW 

Whoa-ho-ho, look what I found in the 

dumpster. 

HARRY

The dumpster?! How did it get there?

314

315

316

317

318

319

320

321

9J, 9K, 9L - "Pilot"
Shooting Draft 8th Rev. (TAN) 04/19/17                        

     

46.
TAG/CC

ANDREW

He must have just wandered off, being 

all blind. (TO POSTER) You get back up 

there, Officer. 

ANDREW PUTS IT BACK ON THE WALL

JOSH

Guys, if this is going to work, we 

really need to establish some ground 

rules--

JUDY

I couldn’t wait anymore I pressed it!

JOSH

(FIRM) Mom, I said-- 

SHE PUTS THE PHONE UP TO THE SIDE OF HIS FACE. 

JOSH (CONT’D)

Hey, Marci, how are you...? 

AS THE CHAOS CONTINUES AROUND HIM, WE...

322

323

324

325

326

FADE OUT.

END OF SHOW

