Alexa and Katie

Pilot Script

Written 

by

Heather Wordham

Netflix Second Draft

April 9, 2017

Netflix Second Pilot pass      April 9

1.

COLD OPEN

INT. ROOM - DAY (D1)
(Alexa, Katie)

CLOSE ON TWO 14 YEAR-OLD GIRLS, ALEXA AND KATIE WHO SIT 
SQUASHED TOGETHER IN A LARGE RECLINER WHILE THEY LOOK THROUGH 
A MAGAZINE.  ALEXA, BRUNETTE, IS AN EBULLIENT TROUBLEMAKER 
WHILE KATIE, BLONDE, IS AS SWEET AS SHE IS PRACTICAL.

ALEXA

Found it!  Our first day of school 

outfits. (POINTS) You. (POINTS) Me.

KATIE

So you're going to be wearing a Prada 

skirt and I'm going to be dressed like 

the dog in the model's bag?

ALEXA

Look at his sweater. Adorable. 

KATIE PULLS THE MAGAZINE CLOSER TO HER.

KATIE

Yeah, the sweater's cute, but he's not 

wearing pants. 

ALEXA

Think of the breeze.

KATIE

Think of my mom. (PUTS MAGAZINE DOWN) 

I don't care what we wear as long as 

we go to school together. 

Netflix Second Pilot pass      April 9

2.

ALEXA

Awww, sweet... and so not true. You 

picked out our first day of high 

school clothes when we were ten.

KATIE

Nine. Roller skates and Furby tee 

shirts. (BEAT) We may want to update. 

WE WIDEN TO SEE ALEXA AND KATIE ARE IN AN INFUSION ROOM. 
ALEXA IS GETTING CHEMO.

KATIE (CONT’D)

So, are you worried Dr. Breitwieser 

won't clear you for school? 

ALEXA

Nope. I count on you to do that for 

both of us. He'll clear me. I'm 

starting school this Monday like 

everyone else. (SERIOUS) And I don't 

want anyone at school to know. I'd 

hate it if they treated me all weird. 

KATIE

I don't want anyone to know either. 

You already get all the attention.  

ALEXA

Can you imagine if Gwenny Thompson  

knew I'd been sick?

Netflix Second Pilot pass      April 9

3.

KATIE

Yeah, you guys might start being nice 

to each other.  

ALEXA

Exactly. Four years of plotting, 

scheming, and warring against my enemy 

down the drain. My life would lack 

purpose. (SEES SOMEONE IN THE HALL) 

Oh, there he is.

KATIE

Who?

ALEXA MOVES UP TO THE EDGE OF THE RECLINER. SHE HAS AN IV IN 
HER ARM, BUT PAYS LITTLE ATTENTION TO IT.

ALEXA

The cute volunteer.  Push the call 

button. Quick, he's getting away!

KATIE

Okay, okay. (PUSHES) There.

ALEXA

When he asks me if I need anything I'm 

going to say, "Your lips on mine."

KATIE

ALEXA

You are not!

Watch me.

THE CUTE VOLUNTEER ENTERS.

Netflix Second Pilot pass      April 9

4.

CUTE VOLUNTEER

Hi.  Did you need something?

BUT HE'S TOO CUTE.

ALEXA

Uhhh... 

KATIE "HELPS" ALEXA. 

KATIE

(MOUTHS) "Your lips on mine." 

ALEXA

A glass of water?

CUTE VOLUNTEER

(SMILES) Like the one on the table 

next to you.

ALEXA

Oh, hey.  Look at that. No, I, uh, 

meant to ask you if you knew what time 

it was.

CUTE VOLUNTEER

I don't. But we could check the giant 

clock on the wall there.

ALEXA

Oh, right, right. (THEN) I have 

cancer!

CUTE VOLUNTEER

That's why you come in for chemo. I  

thought you just loved to read 

Highlights.

Netflix Second Pilot pass      April 9

5.

ALEXA LAUGHS AS CUTE VOLUNTEER EXITS.

ALEXA

Did you see that? He is so into me. 

KATIE

I must have missed that moment. 

ALEXA SETTLES BACK INTO THE RECLINER. 

KATIE (CONT’D)

When is the doctor going to actually 

let you know you're cleared? Did he 

never have a first day of high school? 

Doesn't he know what this means to 

us?!

ALEXA

He's calling my mom today at three.

KATIE

What?! Why didn't you tell me?!

ALEXA

Because I knew you'd go all Katie.

KATIE

Go "all Katie"? (THEN) I'm just 

saying, why call at three? If he 

knows, he knows. Why not one? Why not 

two? Why not-- Ohhhhh. This is "going 

all Katie." Did you just make that 

expression up. Or is that like a thing 

people say?  It's a thing, isn't it?  

(MORE)

Netflix Second Pilot pass      April 9

6.

KATIE (CONT'D)

Is everyone saying it?  Oh God! (RE: 

WHAT SHE JUST DID) "All Katie" again?

ALEXA NODS. THEY LOOK AT EACH OTHER AND THEN BURST OUT 
LAUGHING AT THE ABSURDITY OF IT. A NURSE APPEARS IN THE ROOM. 

NURSE

Who's making all this noise?

KATIE

ALEXA

Alexa. 

Katie.

NURSE

You two. I should have known. Now find 

something quiet to do or Katie won't 

be allowed to come next time. 

AS THE NURSE LEAVES, THE GIRLS FIGHT OFF LAUGHTER. ALEXA 
PICKS UP HER JOURNAL AND STARTS TO WRITE. KATIE FLIPS THROUGH 
THE MAGAZINE. 

ALEXA (V.O.)

When I got cancer my nurse told me 

writing down what I'm grateful for 

would help me stay positive.  I told 

her I'd be grateful if she'd find me a 

new nurse.  She just gave me this 

journal and said, “no such luck, 

Sweetie.”  So I did write a gratitude.  

That first day it was for purple 

popsicles.  And I am still finding 

reasons to be grateful. Like Katie.  

(MORE)

Netflix Second Pilot pass      April 9

7.

ALEXA (V.O.) (CONT'D)

I've been grateful for my best friend 

a lot in this journal.  But this time 

I'm super grateful for her.  You'll 

see…

INT. COFFEE/COMPUTER CAFE - DAY (D2)
(Alexa, Katie, Reagan, Hannah, Jack, Gwenny) 

IN AN ALL PURPOSE LOFT STYLE COFFEE SHOP, KATIE SITS AT A 
TABLE AND IS JOINED BY ALEXA AND KATIE AND THEIR FRIENDS 
REAGAN AND HANNAH, CARRYING COFFEE DRINKS. 

FADE OUT.

ALEXA

All I know is my locker better not be 

anywhere near Sarah Lopez's. She got 

crazy pretty.  

REAGAN

I know! Did you see her Instagram?! 

This summer she turned into a "woman."

HANNAH

I bumped into her last week, and 

accidentally called her ma'am. She 

called me, "who are you?" 

KATIE

Oh, my God, she's known you all your 

life.

HANNAH

I know-- What's that around your 

wrist?

KATIE HOLDS UP HER ARM. THERE IS A CORD AROUND HER WRIST. 

Netflix Second Pilot pass      April 9

8.

KATIE

(ANNOYED) I'm babysitting my brother. 

REAGAN

What?

TO DEMONSTRATE, KATIE PULLS ON THE CORD AND HER BROTHER JACK 
(9) WHO IS ATTACHED TO IT, GETS YANKED OUT FROM AROUND THE 
CORNER OF THE COUNTER WHERE HE HAS BEEN STARING AT PASTRIES. 

Hey guys!

JACK

REAGAN

You have him on a leash?!

KATIE

It's not a leash.  It's a-- No, yeah, 

it's a leash.

ALEXA

Katie, that's terrible. 

KATIE

Easy for you to say. Your brother 

drives you places and helps you with 

homework. Mine wanders off and talks 

to strangers about lizards. 

HANNAH

My dog hates his leash.

KATIE

It's either this or he sits at the 

table with us.

ALEXA/REGAN/HANNAH

It's not that bad./No!/He seems happy.

Netflix Second Pilot pass      April 9

9.

REAGAN

Hey, first day. Let's dye the tips of 

our hair the school colors.

ALEXA/HANNAH

I love that./ Yes!

KATIE

I can't wait for high school. I don't 

know what I'm the most excited about. 

ALEXA/REAGAN/HANNAH

High school guys./ Guys!/ Guys! 

KATIE

Seriously? Name anything else.

ALEXA

Going out for basketball. And theatre. 

HANNAH

And pep rallies.  

REAGAN

Pep rallies with guys. 

ALEXA

Football games! 

REAGAN

Football games with guys. 

ALEXA

HANNAH

Dances!

With guys!

Netflix Second Pilot pass      April 9

10.

ALEXA

That's how it usually works. 

KATIE REELS JACK TO HER. HE HOLDS A PLATE WITH CRUMBS ON IT. 

KATIE

Oh, my God, did you eat all the 

samples. 

JACK

(MOUTH FULL OF SAMPLES) What samples? 

(SWALLOWS) I'm weak.  You knew that 

when you brought me here. 

JACK WANDERS OFF WITH HIS PLATE. 

ALEXA

I'm so looking forward to it all 

starting. 

KATIE

(SYMPATHETIC) Especially after this 

summer. 

REAGAN

What do you mean?

*

*

*

ALEXA SHOOTS KATIE A LOOK. AT ALEXA'S INSISTENCE, KATIE'S THE  *
*
ONLY ONE OF HER FRIENDS WHO KNOWS ABOUT HER CANCER. 

KATIE

(TRYING TO SAVE IT) Bacne. She got 

horrible, horrible bacne. I couldn't 

*

*

even look at it. 

REAGAN

Eeyew. 

Netflix Second Pilot pass      April 9

11.

ALEXA GIVES KATIE A "SERIOUSLY?" LOOK. KATIE GIVES HER A 
"THAT'S ALL I COULD THINK OF!" LOOK.

ALEXA

Yeah. Thanks for sharing that, Katie. 

*

(GETS A FUNNY LOOK ON HER FACE, A 

SIXTH SENSE) Gwenny's here. 

SHE TURNS AROUND TO SEE THAT A PRETTY LITTLE 14-YEAR-OLD GIRL 
HAS ENTERED WITH TWO OTHER GIRLS. IT'S GWENNY THOMPSON AND 
HER STOOGES. THE-GOOD-THE-BAD-AND-THE-UGLY-TYPE THEME PLAYS.

ALEXA GETS UP AND THEY SLOWLY APPROACH EACH OTHER WITH THEIR 
FRIENDS HANGING BACK, EACH SIZING THE OTHER UP. 

GWENNY

Hello, Alexa. 

ALEXA

Hello Gwenny.

Long time. 

GWENNY

ALEXA

Not long enough. 

GWENNY

Ha. Wit. (THEN) Oh, hey, did they ever 

figure out what caused that horrible 

smell in your locker last year? 

SHE SMIRKS AT HER FRIENDS. SHE DID IT.

ALEXA

Yeah.  They think it was your breath.

GWENNY'S STOOGES LAUGH. SHE GLARES AT THEM AND THEY STOP.

Netflix Second Pilot pass      April 9

12.

GWENNY

Well, you were smart not to retaliate. 

You know you always lose.  Come on, 

guys. This place isn't cool anymore.

ALEXA

Bye, Gwenny. Oh, I almost forgot to 

tell you. (LEANS OVER AND WHISPERS IN 

HER EAR) Don't sleep!

A LITTLE UNNERVED BUT COVERING, GWENNY AND HER SQUAD EXIT. 
KATIE CHECKS HER PHONE.  

KATIE

We gotta go! It's almost three.  

ALEXA AND KATIE GATHER THEIR THINGS. 

REAGAN

What's at three?

ALEXA AND KATIE LOOK AT EACH OTHER. 

Uhh...   

ALEXA

KATIE

Dermatologist! 

The backne?

REAGAN

KATIE

It's a mess back there.  

THEY HURRY OUT.  

EXT. STORE DAY - CONTINUOUS (D2)

ALEXA AND KATIE PAUSE OUTSIDE.

Netflix Second Pilot pass      April 9

13.

(ALEXA, KATIE)

Nice cover.

ALEXA

KATIE

I suck at improv.

ALEXA

What if he doesn't let me go to 

school?

KATIE

Now who's the worrier? He will. But 

just in case.

SHE SHOWS HER FINGERS CROSSED.

ALEXA

Way ahead of you.

REVEALS HER FINGERS CROSSED ON BOTH HANDS.

ALEXA (CONT’D)

Me, too!

THEY HURRY OFF, BUT KATIE IS STOPPED RIGHT AWAY WHEN THE ROPE   
ON HER WRIST GOES TAUT. THEY HEAR A LOUD THUD AS JACK HITS 
THE DOOR INSIDE THE CAFE. 

KATIE

Oops.  

CUT TO:

Netflix Second Pilot pass      April 9

14.

SCENE B

INT. - ALEXA'S GREAT ROOM/KITCHEN (D2)
(Alexa, Katie, Lucas, Lori)

ALEXA AND KATIE ENTER THE COOPER'S HOUSE. ALEXA QUICKLY 
THROWS DOWN HER BAG AND SETS HER MOCHA ON THE COUNTER. 

ALEXA

Take your shoes off. My mom will be 

home any second and it has to look 

like we've been here for hours. 

ALEXA EFFICIENTLY KICKS OFF HER SHOES AND JACKET AS SHE PUTS 
HER HAIR IN A SLOPPY BUN, THEN PLOPS ON THE COUCH. KATIE 
WATCHES THIS. 

ALEXA (CONT'D)

Hurry! Kick back!

KATIE

I can't kick back under pressure!

ALEXA

I told my mom I'd come home and nap 

right after chemo. She makes such a 

big deal of it. 

KATIE

What?! Why do you do this to me? I'm 

not going to lie to your mom--

THE FRONT DOOR OPENS AND KATIE FRANTICALLY PULLS OFF ONE 
SHOE, THEN SITS ON THE COUCH TUCKING HER OTHER FOOT 
UNDERNEATH HER. LORI ENTERS LOOKING AT THE MAIL. 

ALEXA

(STRETCHING) Hey, Mom. 

LORI

Hi, honey. How was chemo?

*
*

*

*

*

*

*
*
*

*

*

*

*

*

*

*

*

*

*

*

*
*
*

*

*

*

*

Netflix Second Pilot pass      April 9

15.

ALEXA

Good. Lots of laughs. Came straight 

home. 

LORI SENSES SOMETHING'S UP.

LORI

Hi, Katie. 

KATIE STILL HOLDING HER SHOE, SMILES AT HER, UNEASY. 

KATIE

Hi.  We're kicking back.

LORI LOOKS AT ALEXA.

LORI

Did you get a nap in?

ALEXA

Mom. We had an agreement. How can you 

ask? 

LORI CROSSES TOWARD THE KITCHEN.

LORI

Is that your mocha on the counter?

ALEXA

From yesterday.

LORI PICKS IT UP. 

LORI

And, yet, it's still warm.

ALEXA

We have a microwave.

LORI

The one you never use?

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

Netflix Second Pilot pass      April 9

16.

KATIE

We hung out with friends at Wired!! 

(TO ALEXA) She's too good. There was 

so much tension in the air. 

LORI

Alexa, you know you're not supposed to 

get tired. 

ALEXA

I'm not even a little tired, Mom. Did 

Dr. Breitwieser call? 

LORI

Not yet.  We'll talk about the nap 

thing later when Katie's not around to 

hear me yell.

KATIE

We can hear you pretty good from next 

door. (REALIZES) Though we try not to 

listen.

SHE PUTS HER CELL PHONE ON THE COUNTER AND PICKS UP THE MAIL 
AGAIN. THE GIRLS SIT ON STOOLS AND STARE AT THE PHONE. 

LUCAS, 16, CROSSES INTO THE KITCHEN.

LUCAS

Hey, guys. 

KATIE LOOKS AT HIM, LOVE STRUCK.

*

*

*

*

*

*

*

*

*

*

*

*

*

*
*

*

Netflix Second Pilot pass      April 9

17.

KATIE

Great, thanks. (REALIZES) Oh, you 

didn't ask. But I am. Great, I mean.  

Not to brag, just saying. 

KATIE CRINGES.  

LUCAS

Good to hear.  

LUCAS OPENS THE FRIDGE AND LOOKS FOR FOOD. ALEXA TURNS AND 
LOOKS AT KATIE, SHOCKED.  

ALEXA

(INTENSE WHISPER) Seriously?

KATIE

(INTENSE WHISPER) What?

ALEXA

(INTENSE WHISPER) You know what.

ALEXA NODS TOWARD THE OPEN FRIDGE, WHICH LUCAS IS BEHIND,  
STILL LOOKING FOR FOOD. 

KATIE

(INTENSE WHISPER, "NO WAY") Whaaat?! 

ALEXA

(INTENSE WHISPER) Lucas?  Really?  

Gross.  You've known him since you 

were five.  Now suddenly...

ALEXA DOES A "LOVE SICK" FACE.

KATIE

(INTENSE WHISPER) I don't know what 

you're talking about.

Netflix Second Pilot pass      April 9

18.

ALEXA

(INTENSE WHISPER) You know he farts.

KATIE

(INTENSE WHISPER) He. Does. Not!

LUCAS STANDS UP, SHUTTING THE FRIDGE DOOR. 

LUCAS

We're out of milk.  

KATIE

(TO LUCAS, SINCERE) Oh, no. Milk is 

really important.  

SHE CATCHES ALEXA'S LOOK OF DISBELIEF. 

KATIE (CONT’D)

...Or whatever. If you're into that.  

(TRAILS OFF) ...Water's also good... 

LUCAS LOOKS BACK IN THE FRIDGE. ALEXA TURNS TO KATIE.

ALEXA

(INTENSE WHISPER) My best friend likes 

my brother. That makes me want to hurl 

more than chemo.

LORI HOLDS UP A LETTER SHE JUST READ. 

LORI

Alexa, look at this. The principal 

sent us a note. She says she's aware 

of your situation, and we can count on 

her for any help and support we need. 

*

*

Netflix Second Pilot pass      April 9

19.

ALEXA

The principal is writing me notes.  

That's so lame.

LORI

Well, I think it's pretty cool.

ALEXA

Mom, you've clearly lost touch with 

what cool means. Because it's not 

being besties with your principal.  

LUCAS LOOKS AT THE NOTE.  

LUCAS

Principal Trugly is horrible. I got 

detention for a week just for skipping 

home room. No one can stand her. They 

*

*

*

call her Ugly Trugly.  

ALEXA AND KATIE LAUGH. 

LORI

Oh, that's awful.

THEY STOP LAUGHING.

ALEXA

KATIE

It's mean.

Juvenile.

BUT LORI CAN'T KEEP A STRAIGHT FACE. SHE LAUGHS. 

LORI

We used to call her the same thing. 

Netflix Second Pilot pass      April 9

20.

ALEXA

How old is she?

LORI

Well, not your father. She loved your 

dad. He was such a suck-up. (TO LUCAS) 

Let's go back to you skipping class.

CELL PHONE RINGS. ALEXA LOOKS AT KATIE. 

LUCAS

I'd love to, Mom, but that sounds 

important. 

ALEXA

Doctor Breitwieser!

THEY JUMP OFF THEIR SEATS, AND ALEXA GRABS HER MOM'S PHONE.  

*

*

*

*

*

*

ALEXA (CONT’D)

(RE: PHONE) Ugh, Dad.  

LUCAS

Tell him we need--

ALEXA

(RE: PHONE) Ignore.

SHE HITS A BUTTON AND THE PHONE STOPS RINGING. 

LUCAS

--milk.

THE HOME PHONE RINGS. ALEXA QUICKLY CROSSES THE KITCHEN TO 
GRAB IT. SHE NOTICES THE CALLER ID. 

ALEXA

Ugh, Dad, again. Can't we just block 

him?

Netflix Second Pilot pass      April 9

21.

LORI

Don't hang up, just tell him--

KATIE

Lucas needs milk! 

ALEXA PICKS UP THE PHONE.

ALEXA

Waiting-for-Doctor-Breitwieser's-call-

Lucas-needs-milk-don't-call-again-see-

you-when-you-get-home-love-you-bye-

seriously, don't call again!

ALEXA HANGS UP. LORI'S CELL PHONE RINGS. SHE PICKS IT UP, 
THEN LOOKS AT THE GIRLS. 

LORI

Dr. Breitwieser. (THEN, INTO PHONE) 

Hello...

AS LORI PACES, WE FOLLOW THE CONVERSATION ON THE GIRL'S 
FACES.

LORI (CONT’D)

(ON PHONE)...good... okay... yes...

THE GIRLS LOOK HAPPY.

LORI (CONT’D)

(ON PHONE)...no, no, I understand...

THE GIRLS LOOK TROUBLED.

LORI (CONT’D)

(ON PHONE)...oh, good...

THEY BRIGHTEN.

LORI (CONT’D)

...no, no, of course not...

Netflix Second Pilot pass      April 9

22.

TROUBLED AGAIN.

ALEXA

(FIERCE WHISPER) You're killin' me, 

Mom. Yes or no?!

LORI

(INTO PHONE) Oh, sorry, doctor... (TO 

ALEXA) Yes.

THE GIRLS SCREAM LOUD AND LONG, THRILLED. ALEXA BREAKS INTO A 
SORT OF ENDZONE DANCE. 

ALEXA

(LIKE "IT'S MY BIRTHDAY") That's 

right, that's right.  Going to high 

school.  Starting Monday.  

SHE STOPS DANCING AND GOES BACK TO CASUAL.

ALEXA (CONT’D)

Not that I was ever really worried 

about it.

KATIE

You have a little spit on your chin.

LORI

(ON PHONE)...yes, I'm still here... 

LORI EXITS, STILL ON THE PHONE. ALEXA SEES LUCAS CHECKING HIS  *
HAIR IN THE REFLECTION OFF THE TOASTER. 
*

ALEXA

It's poofy in the back.

LUCAS

Where? Where?

HE CHECKS. 

*

*

*

*

*

Netflix Second Pilot pass      April 9

23.

*

*

*

*

*

*

*

*

*

*

*

*

*

LUCAS (CONT’D)

No, it's good. It's good. 

KATIE

It's perfect.

LUCAS

(TO ALEXA)  We don't joke about the 

hair. 

ALEXA

I don't know what I was thinking.

HE SMILES THEN PUTS HIS ARM AROUND HER.  

LUCAS

Hey, congratulations, Lex. I knew 

you'd get in. And any weird freshman 

who likes my little sister is going to 

have to deal with me. 

ALEXA

But you'll leave the cute ones alone? 

LUCAS

No, them, too. Same goes for you, 

Katie. Any guy bugs you at school, 

just come find me.  

KATIE

I will! Where? Where should I find 

you. Where are you usually? Like where 

do you hang out between classes? So I 

know where to find you.    

Netflix Second Pilot pass      April 9

24.

LUCAS

(BEAT) We'll work it out.

LUCAS EXITS. 

KATIE

Well, I should go. I have to crawl 

into a hole and die now.

LORI ENTERS HOLDING A SHEET OF PAPER WITH NOTES ON IT.

LORI

Good news, huh, hon?

ALEXA

Yeah, but what's that?

LORI

Let's take a moment to take in the 

good news. 

ALEXA TAKES THE LIST.

ALEXA

Come right home when school ends... no 

extracurricular activities...

LORI

It's just a few precautions.

ALEXA

No physical activities... That means 

no basketball.

LORI

You could be the team manager. Make 

the roster... do manager stuff.  

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

*

Netflix Second Pilot pass      April 9

25.

KATIE

(TRYING) You could glitter your 

clipboard! (OFF ALEXA'S LOOK) That, 

um, would be one option.

ALEXA

This also means no pep rallies or 

dances. All that leaves is going to 

class - the worst part of school. 

LORI

Or the most important part of school - 

oh, who am I kidding. It sucks, honey, 

but it's only for right now. You're 

getting better and we don't want to do 

anything to risk that. 

ALEXA

I know. I just thought once I started 

school things would go back to normal. 

LORI

You're on your way, honey. In no time 

at all you'll be lying to me about 

skipping homework instead of skipping 

naps. 

ALEXA

God, I hope so. 

LORI KISSES ALEXA ON THE HEAD AND EXITS.  

*

*

*

*

*

*

*

*

*

*

*

*

*

Netflix Second Pilot pass      April 9

26.

KATIE

You'll be able to do all those things 

really soon. 

ALEXA

Yeah.  I guess I'll just have to come 

*

up with other reasons for getting Cs 

while I don't have after-school stuff 

to blame it on. 

 KATIE LAUGHS AND GIVES ALEXA A BIG HUG. 

KATIE

We're starting high school together!

ALEXA

What if people notice I'm not doing 

any after school stuff and think 

something's wrong with me?

KATIE

Who are these people who are standing 

by and carefully watching your every 

move?   

ALEXA

They do, Katie. They just do. 

KATIE

Come on, let's celebrate!

*

ALEXA

Yes! Let's do something our moms will 

hate. 

CUT TO:

Netflix Second Pilot pass      April 9

27.

SCENE C

INT. BATHROOM - MOMENTS LATER (D2)
(Alexa, Katie)

ALEXA AND KATIE COLOR THE BOTTOM FEW INCHES OF THEIR HAIR 
WITH SCHOOL COLORS PURPLE AND GREEN. KATIE EXAMINES HERS.

KATIE

This is so cool! Now help me put it in 

a bun so my mom can't see it.

ALEXA HELPS KATIE WITH HER HAIR. 

ALEXA

Cleared for school, colored hair. Now 

all that's left is to buy some 

clothes, take down Gwenny, and I'll be 

officially ready to start high school.

KATIE

Seriously? Are you going to keep that 

stupid feud going in high school?

ALEXA

What choice do I have? I'm just 

supposed to let her win?  

KATIE

It's an endless revenge cycle. Do you 

really think anyone's ever going to 

win?

ALEXA

Yeah. Me. (THEN) Hey, let's FaceTime 

Hannah and Reagan and show them our 

hair. 

Netflix Second Pilot pass      April 9

28.

KATIE

Good idea. 

KATIE DIALS HER PHONE. ALEXA BRUSHES HER OWN HAIR THEN 
SUDDENLY STOPS.  

ALEXA

Katie, wait! Uh, let's just surprise 

them when we see them. 

KATIE

Okay. 

KATIE'S PHONE BEEPS. 

KATIE (CONT’D)

Oh, that's my mom. Better go. I'll 

call you later.

KATIE EXITS. AFTER A BEAT, ALEXA OPENS HER HAND AND LOOKS 
DOWN, TEARY-EYED, AT A CLUMP OF HAIR THAT CAME OFF HER HEAD.

INT. KATIE'S KITCHEN - NIGHT (N2)
(Katie, Jennifer, Jack) 

KATIE, JENNIFER, AND JACK ARE FINISHING DINNER. JACK RUMMAGES 
THROUGH A COOKIE TIN. 

JACK

C'mon, c'mon.  Where are you?

KATIE

Are you seriously going to touch every 

cookie?

JACK

Why does every cookie only have a 

single chocolate chip?!

Netflix Second Pilot pass      April 9

29.

JENNIFER

(HER LITTLE JOKE) Because you have a 

single parent. (NO RESPONSE) Nothing?

JACK

I hate being poor!

JENNIFER

Hardship builds character. 

JACK

I hate character.

JACK GETS UP FORM THE TABLE.

JACK (CONT’D)

I'm going next door. They've got 

Milanos. 

JENNIFER

Bring me back one. 

JACK EXITS OUT THE SLIDING GLASS DOOR. JENNIFER AND KATIE 
PICK UP DISHES OFF THE TABLE AND BRING THEM TO THE SINK.

JENNIFER (CONT’D)

How's Alexa feeling about school 

starting?

KATIE

She's ready.  

JENNIFER

High school is a big change, and she's 

already going through a lot.

Netflix Second Pilot pass      April 9

30.

KATIE

Mom, she's good. Alexa and I 

practically read each other's minds.  

I'd know if she were worried.

KATIE'S CELL PHONE BEEPS ON THE COUNTER.

KATIE (CONT’D)

Like right now. That's her texting.

JENNIFER PICKS UP KATIE'S PHONE AND LOOKS AT IT. 

JENNIFER

You're right. 

KATIE

I can even tell you what it says. 

"Don't watch Project Runway without 

me." 

JENNIFER TAPS ON THE PHONE AND READS THE TEXT.

JENNIFER

Close. It says, "Meet me in the tree 

after you know your mom's asleep."

JENNIFER HANDS KATIE HER PHONE. 

KATIE

Oh. (TEXTING) And I'm obviously 

telling her, "sorry, I'll be asleep 

myself then."

KATIE SMILES AT HER MOM AND SETS DOWN THE PHONE.

FLIP TO:

Netflix Second Pilot pass      April 9

31.

SCENE E

EXT. TREE - LATER (N2)
(Alexa, Katie, Jack)

ALEXA SITS IN A BIG TREE WHICH IS LOCATED IN THE BACKYARD 
DIRECTLY BETWEEN HERS AND KATIE'S HOUSES. WORDS, HEARTS, ETC 
SCRATCHED IN THE TREE INDICATE THEY'VE BEEN MEETING IN THIS 
TREE FOR YEARS. ALEXA WEARS DARK CLOTHES AND A DARK SKI CAP.

KATIE CRAWLS OUT HER WINDOW AND SHIMMIES TOWARD ALEXA. 

ALEXA

What took you so long?

KATIE

Don't you mean, "Katie, thanks so much 

for meeting me when you're supposed to 

be in bed, even though I know how 

uncomfortable it makes you doing 

things you could get in trouble for"?

ALEXA

It must be exhausting being you.

KATIE

You have no idea.

ALEXA

Okay, I need you to grab some dark 

clothes and as many rolls of toilet 

paper as you can. 

KATIE

Good night.

KATIE TURNS AND STARTS BACK TOWARD HER HOUSE.

ALEXA

Katie, wait! It's important.

Netflix Second Pilot pass      April 9

32.

KATIE STOPS AND LOOKS BACK AT ALEXA.

ALEXA (CONT’D)

I need to toilet paper Gwenny's house.

KATIE

(TURNS BACK) Good night. 

ALEXA

C'mon, Katie. Do you really want to 

look back at your childhood one day 

and know that you never t.p.'d 

anyone's house? 

KATIE

I'd like to look back on my childhood 

and know I never went to prison. What 

is going on with you? You're kinda 

losing your mind. 

ALEXA

How often do I ask you to do things?

Constantly.

KATIE

ALEXA

(SERIOUS) I really need this right 

now. It's a good distraction from 

everything else. (THEN) Pleeeease. 

KATIE

Oh, my gosh, fiiiine. I'll do it.  

JACK (O.S.)

This is going to be fun!

Netflix Second Pilot pass      April 9

33.

THEY TURN TO SEE JACK, IN PAJAMAS, SHIMMYING HIS WAY TO THEM.

KATIE

Jack! What are you doing?

JACK

I don't want to look back on my 

childhood and never have t.p.'d a 

house. 

KATIE

Go to bed. And keep this to yourself.

JACK 

What if I go with you and keep it to 

myself?

KATIE

What if I don't strangle you and you 

keep it to yourself? 

Pleeeeeease.

JACK

KATIE

Oh, my gosh, fiiiine. I hate you both.

JACK

(EXCITED) I'll get the toilet paper!!!

SHHHHHHH!

KATIE

ALEXA

Jack, c'mon, the yelling -- not 

helping your case. 

Netflix Second Pilot pass      April 9

34.

JACK

(WHISPERS) I'll get the toilet paper!!

AS THEY ALL START TO CLIMB OUT OF THE TREE...

CUT TO:

Netflix Second Pilot pass      April 9

35.

EXT. FRONT YARD - LATER (N2)
(Alexa, Katie, Jack) 

ALEXA AND KATIE APPROACH A HOUSE WITH A NICELY MANICURED 
FRONT YARD.  KATIE PULLS A WAGON FILLED WITH T.P. AND JACK, 
STILL IN HIS PAJAMAS.  JACK CLIMBS OUT AND GRABS A ROLL. 

KATIE

How did you know where she lives?

ALEXA

How would I not know where my enemy 

lives? It's scary how little you know 

about retaliation and vengeance.   

KATIE LOOKS AT THE HOUSE HESITANTLY.

KATIE

 Are we really doing this?

SUDDENLY A ROLL OF TOILET PAPER STREAMS ACROSS THEM LOOPING 
OVER A TREE AND LEAVING A TRAIL OF T.P. BEHIND IT.  JACK DOES 
A HAPPY DANCE ON THE SIDEWALK.  

ALEXA

Oh, we're doing it. Nice arm, kid. 

JACK GRABS ROLLS OF T.P. AND HEADS INTO THE YARD. ALEXA GRABS 
A COUPLE ROLLS AND HANDS ONE TO KATIE. SHE STOPS, THEN SITS 
IN THE WAGON FOR A SECOND. 

KATIE

Are you all right?

ALEXA

You know I hate that question. As my 

best friend, you have to not ask me 

that ever again.

Netflix Second Pilot pass      April 9

36.

KATIE

As my best friend you have to not do 

things that wear you out. 

BEHIND KATIE, WE SEE JACK JUMPING FROM TREE TO TREE, LEAVING 
A T.P. TRAIL AS HE GOES. HE'S HAVING THE TIME OF HIS LIFE. 

KATIE (CONT’D)

Wow. He's like an artist who works in 

toilet paper.  

ALEXA

Okay, I'll sit here for a minute, 

until I feel-- 

Better?

KATIE

ALEXA

Noooo. Inspired. Go do the shrubs.  

AS KATIE CROSSES OFF, JACK RUNS UP TO ALEXA AND SHE LOADS HIM 
UP WITH MORE TOILET PAPER.

JACK

This is the best night of my life.

ALEXA

You really get it, Jack. I hope I 

marry someone like you when I'm older.

JACK

Why would you say that? Married is 

gross. 

JACK HURRIES OFF WITH HIS TOILET PAPER. KATIE COMES BACK FOR 
MORE TOILET PAPER. ALEXA GETS UP. KATIE STARES AT HER HEAD.

Netflix Second Pilot pass      April 9

37.

ALEXA

I'm ready. Stop looking at me like 

that.

KATIE

I'm looking at my ski cap. I knew that 

was mine. 

ALEXA

Oh, my gosh, I'll give it back to you. 

Now will you focus.

THEY CROSS TO THE SHRUBS AND WRAP PAPER AROUND THEM.   

ALEXA (CONT’D)

This feels right.

KATIE STEPS OVER TO THE MAILBOX AND STARTS WRAPPING IT.

KATIE

You making me do something stupid and 

me being irritated by it?

ALEXA

Yes. 

SUDDENLY KATIE STOPS. SHE STARES AT THE MAILBOX.

KATIE

Alexa, this isn't Gwenny's house.

Huh?

ALEXA

KATIE

It says, Trugly. Ohmygosh, ohmygosh, 

ohmygosh. This is our new principal's 

house. 

*

*

*

*

*

Netflix Second Pilot pass      April 9

38.

KATIE RUSHES INTO THE YARD AND TRIES TO GET JACK'S ATTENTION. 

KATIE (CONT’D)

(WHISPERS) JACK! JACK! WE GOTTA GO! 

ALEXA STARTS PUTTING THE UNUSED T.P. BACK IN THE WAGON. 

KATIE (CONT’D)

Leave it! 

ALEXA

I'm almost done. 

SUDDENLY THE FRONT PORCH LIGHT COMES ON, THE DOOR OPENS AND 
PRINCIPAL TRUGLY STEPS INTO THE YARD, FURIOUS.

PRINCIPAL TRUGLY

What do you think you're doing?

KATIE AND ALEXA JUST STAND THERE, CAUGHT.

PRINCIPAL TRUGLY (CONT’D)

I'm calling your parents. This will 

not end well. 

BEHIND THE PRINCIPAL, JACK COMES INTO FRAME HANGING UPSIDE 
DOWN FROM A TREE.  

JACK

Please don't tell my principal about 

this. 

AS HE FALLS INTO THE BUSHES, WE...

CUT TO:

Netflix Second Pilot pass      April 9

39.

INT. - PRINCIPAL'S OFFICE WAITING ROOM DAY (D3)
(Alexa, Katie, Lori, Dave, Jennifer) 

ALEXA, KATIE, LORI AND JENNIFER SIT ON CHAIRS. DAVE PACES.  

DAVE

I just hope Principal Trugly doesn't 

remember me. I was always such a model 

student. I hate to let her down.  

LORI

This is about Alexa, Dave.

DAVE

Right, right. Of course.

LORI

I just don't understand what would 

make you do something like this, 

Alexa.

JENNIFER

And with Jack! This is not like you, 

Katie.

LORI

(TO JENNIFER) I'm so sorry Alexa 

dragged Katie into this.

JENNIFER

No, no, Lori. I don't feel that way at 

all. Katie's in charge of her own 

behavior.

Netflix Second Pilot pass      April 9

40.

LORI

We can deal with the girls ourselves, 

but I think it's important we present 

a united front in front of Trugly--it 

was a case of mistaken identity, an 

innocent prank. We don't want the 

girls starting off with a bad 

reputation at school.

Absolutely.

JENNIFER

LORI

I know Trugly can be intimidating--

JENNIFER

I'm not afraid of her.

PRINCIPAL TRUGLY OPENS THE DOOR.

PRINCIPAL TRUGLY

Alright-

JENNIFER

It was all her daughter!

LORI

(WHISPERS) Jennifer!

JENNIFER

(WHISPERS) Sorry! She still scares me.

PRINCIPAL TRUGLY

Please come in. Parents first. Then 

I'll speak to the girls. 

Netflix Second Pilot pass      April 9

41.

AS LORI, JENNIFER, AND DAVE FILE IN, DAVE TRIES TO LOOK AWAY 
AND HIDE HIS FACE AS HE GOES BY.

PRINCIPAL TRUGLY (CONT’D)

David?

CAUGHT, DAVE LOWERS HIS HAND FROM HIS FACE.

DAVE

Yes, Principal Trugly.

PRINCIPAL TRUGLY

One of the perpetrators was yours?

DAVE BRIEFLY CONSIDERS DENYING IT, BUT REALIZES HE HAS NO 
RECOURSE.

DAVE

I'm so, so sorry.

INT. ALEXA'S GREAT ROOM/KITCHEN - AFTERNOON
(Alexa, Dave, Lori, Katie, Jennifer, Jack)

ALEXA, KATIE, LORI, DAVE, AND DONNA ARE SITTING IN THE GREAT 
ROOM. 

ALEXA

Suspended for three days! It's so 

unfair!

DAVE

It was just one day at first. But then 

she called you little felons and your 

mom took exception.  Words were 

exchanged.  So the way it worked out, 

you got three days, and your mother is 

banned from school for a month. 

LORI

A month!  It's so unfair!

Netflix Second Pilot pass      April 9

42.

KATIE

School doesn't start until Monday. How 

can we be suspended? 

JENNIFER

You'll start on Thursday.  

KATIE

(FIGHTING TEARS) This can't be 

happening. We're never going to have a 

first day of high school. 

ALEXA

This is so over the top. Trugly is 

crazy. Is this what we're going to be 

dealing with for the next four years? 

(THEN) I have an idea! Why don't Katie 

and I go to Crawford High instead? 

KATIE

That's across town. We don't know 

anyone there.

ALEXA

Perfect. We can start on Monday, and 

nobody there knows we got in trouble.

LORI

We're not doing that.

Netflix Second Pilot pass      April 9

43.

ALEXA

Okay, then, what about this? Mom, you 

could home school Katie and me. 

KATIE

You mean, we wouldn't even go to high 

school?! That's crazy.

ALEXA

Home schooling is really big right 

now. And, we can learn in our pajamas.  

KATIE

But we've been waiting to start high 

school since we were ten!  

LORI

And I have a job.

ALEXA

I know, but, this summer whenever I 

played you in Words With Friends, you 

played words like right away. I don't 

think they'll miss you.

LORI

That was when I was-- I don't have to 

defend myself to you. Alexa, what's 

going on?

DAVE PULLS A PIECE OF PAPER OUT OF ALEXA'S JACKET.  

DAVE

Did you know that was Trugly's house?

*

*

*

Netflix Second Pilot pass      April 9

44.

ALEXA

How would I know where the principal 

lives?

DAVE HOLDS UP THE ENVELOPE FROM THE NOTE TRUGLY MAILED TO THE 
HOUSE.

DAVE

By having her address in your pocket?  

KATIE GETS UP AND TAKES THE ENVELOPE FROM DAVE.

KATIE

You lied about Gwenny. You wanted us 

to get in big trouble. Why?

ALEXA SAYS NOTHING. SHE TURNS AND HURRIES OUT OF THE ROOM. 
THE MOMENT HANGS IN THE AIR. THEN... 

JACK (O.S.)

You're out of toilet paper! Hello! 

Little help? 

CUT TO:

Netflix Second Pilot pass      April 9

45.

SCENE K

INT. KATIE'S BEDROOM - DAY (D3)
(Katie, Jennifer)

KATIE IS LYING IN HER BED STARING AT THE CEILING. THERE IS A 
KNOCK ON HER DOOR. 

KATIE

Go away, Alexa.

THE DOOR OPENS AND JENNIFER ENTERS, CARRYING KATIE'S SKI CAP.

KATIE (CONT’D)

(DISAPPOINTED) Oh, it's you.

JENNIFER

Hoping it was the person you just told 

to go away? (THEN) Lori brought your 

ski cap back. She said Alexa was 

really upset.  

KATIE

Good.  She should be.

JENNIFER

Why don't you go talk to her?

KATIE

No way. She should be coming to me!

JENNIFER

So, you're going to put pride before 

*

friendship? 

KATIE

She lied to me!

JENNIFER

Alright. Suit yourself. Here.

Netflix Second Pilot pass      April 9

46.

JENNIFER HANDS HER THE CAP AND EXITS. KATIE, UNHAPPY WITH 
HERSELF, FIDGETS WITH THE HAT. SUDDENLY SHE REALIZES THERE IS 
A BUNCH OF ALEXA'S HAIR IN THE CAP. KATIE HOLDS THE HAIR IN 
HER HAND TRYING NOT TO CRY.

KATIE

...Oh, Alexa. 

INT. - ALEXA'S GREAT ROOM/KITCHEN - A MOMENT LATER (D3)
(Alexa, Katie, Lucas, Lori, Dave)

ALEXA IS ON HER BED, STARING AT THE CEILING.  KATIE COMES 
STORMING IN. 

CUT TO:

KATIE

(TO ALEXA) Why didn't you tell me?

ALEXA

Tell you what?

My ski hat? 

KATIE

ALEXA

I gave it back. Let it go.

KATIE

No, I saw a lot of your hair in it. A 

lot.

ALEXA

...Oh. Why are you so mad? I'm the one 

losing my hair. 

KATIE

I'm mad because you lied to me! I'm 

your best friend. 

(MORE)

Netflix Second Pilot pass      April 9

47.

KATIE (CONT'D)

You should have told me what was going 

on. Like I would care if you have hair 

or not.

ALEXA GETS UP.

ALEXA

All right. I don't want my stupid hair 

to fall out. And all of a sudden it 

is. Just as school is starting. 

KATIE

That's why you wanted us to get 

suspended.  

ALEXA

Actually I was going for expelled. I 

don't want to go to school like this.  

But I don't want to not go to school 

without you. 

KATIE

That's... sweet. And really awful.  

It's swaful.  

ALEXA

I'm sorry. I got out of the hospital, 

got through all those treatments, got 

okay'd for school, then... this. 

ALEXA MOVES SOME OF HER HAIR AND REVEALS WHERE A LOT OF HAIR 
HAS COME OUT.

Netflix Second Pilot pass      April 9

48.

ALEXA (CONT’D)

And as much as losing the hair sucks, 

it's more about--

KATIE

People knowing. So what if they know?

ALEXA

Then I'm the sick girl. I want things 

to be like they were. I want to go out 

for basketball, and not keep things 

from my friends, and take Gwenny down. 

Just be my normal awesome self. But 

now, when school starts instead of 

seeing me everyone's going to see 

cancer. Everyone is going to know me 

as the sick girl. And I'm so much more 

than that.  

KATIE

They'll figure it out. You'll do 

something really funny and dumb and 

wrong, and they'll get to know who you 

really are. 

ALEXA

...Yeah. It's just sometimes it 

really, really sucks. And when the 

hair started coming out I panicked. 

And I took you down with me.  

Netflix Second Pilot pass      April 9

49.

KATIE

I have an idea. 

KATIE EXITS. ALEXA STARES AFTER HER CONFUSED. A MOMENT LATER 
SHE RETURNS WITH AN ELECTRIC RAZOR. 

ALEXA

You think I should just go for it and 

shave it off? Then everyone will 

definitely notice.  

KATIE

What if you're not the only one 

they're noticing?

ALEXA

You've always hated how I get more 

attention. Ever since Ms. Papadakis 

moved me to the front row in 

kindergarten.

KATIE

She moved you from the back because 

you were making fun of her mustache.

ALEXA

Come on. She could twirl that thing.

KATIE

This time I'm going to share the 

attention with you, Alexa.

KATIE PLUGS IN THE RAZOR AND STARTS IT UP.

Netflix Second Pilot pass      April 9

50.

ALEXA

Okay, Katie. Put it away. I get it. 

You are the best friend anyone could--

KATIE BRINGS THE RAZOR TO HER HEAD, SHAVING OFF A HUGE CHUNK 
OF HAIR. 

ALEXA (CONT’D)

Oh my gosh, Katie! 

ALEXA STARES AT HER. 

ALEXA (CONT’D)

(AMAZED) You just did that. 

KATIE

As long as you're my best friend, 

you're never going through anything 

alone.

ALEXA

(TEARY) Seriously?

KATIE

Yeah, I'm kind of committed now.

ALEXA HUGS HER. KATIE SHAVES ANOTHER BIG CHUNK OF HAIR.

KATIE (CONT’D)

How do I look?

ALEXA

So, so bad.

ALEXA TAKES THE ELECTRIC RAZOR FROM KATIE AND SHAVES OFF A 
BIG CHUNK OF HER OWN HAIR.  

ALEXA (CONT’D)

How about me?

Netflix Second Pilot pass      April 9

51.

KATIE

You look ridiculous. 

THE GIRLS START TO GIGGLE. THEY CONTINUE TAKING TURNS BUZZING 
OFF THEIR HAIR AS:

KATIE (CONT’D)

If you promise to tell me how you're 

feeling about things, I promise I'll 

personally take Gwenny down for you.  

ALEXA

Oh, that's a deal. (THEN) You know, I 

can really pull off this no hair look 

thanks to my boobs. 

What boobs?

KATIE

ALEXA

You've never noticed because I've 

always had such great hair. 

LAUGHING, ALEXA OPENS THE CLOSET DOOR THAT HAS A MIRROR 
HANGING ON THE BACK TO CHECK OUT THEIR HAIR. AS THEY STEP IN 
FRONT OF THE MIRROR...

KATIE

AHHHHHHHHHH. What have I done?

ALEXA HUGS HER AND THEY BOTH SORT OF LAUGH AND CRY, AS THEY 
CONTINUE TO LOOK AT THEIR HEADS IN THE MIRROR. 

ALEXA (V.O.)

I have the best friend in the world.  

I've always wondered if I would've 

done the same thing for her.  

(MORE)

*

*
*

*

*

*

*

Netflix Second Pilot pass      April 9

52.

ALEXA (V.O.) (CONT'D)

Katie said I would have, but I told 

her I wasn't so sure because I have 

way better hair than her. And then she 

hit me. One thing I am sure of, the 

times in my life that have sucked have 

been way less sucky thanks to her.

FADE OUT.

END OF SHOW

*

*

*

*

*

*

*

*

*
*
*

