#!/bin/bash

PDF_DIR=./PDFs/*.pdf
TXT_DIR=./TXTs/

for file in $PDF_DIR
do
    echo "OCR'ing $file..."
    $(ocrmypdf "$file" "$file")
    echo "pdf2txt on $file..."
    $(pdf2txt.py -t text -o "$file".txt "$file")
done
