import os
import requests
import time
from urllib.parse import urljoin
from bs4 import BeautifulSoup

# Adding Selenium

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def DownloadSimplyScripts():
    url = "https://www.simplyscripts.com/tv_all.html"

    #If there is no such folder, the script will create one automatically
    folder_location = r'./PDFs'
    if not os.path.exists(folder_location):os.mkdir(folder_location)

    response = requests.get(url)
    soup= BeautifulSoup(response.text, "html.parser")     
    # For direct PDF link fun
    for link in soup.select("a[href$='.pdf']"):
        #Name the pdf files using the last portion of each link which are unique in this case
        filename = os.path.join(folder_location,link['href'].split('/')[-1])
        with open(filename, 'wb') as f:
            f.write(requests.get(urljoin(url,link['href'])).content)

def DownTvWritingGoogleSite():
    # For https://sites.google.com/site/tvwriting/

    url = "https://sites.google.com/site/tvwriting/"

    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    for link in soup.findAll("a", {"class": "dhtgD"}): #https://stackoverflow.com/questions/5041008/how-to-find-elements-by-class
        filename = os.path.join(folder_location,link['href'].split('/')[-1])
        with open(filename, 'wb') as f:
            f.write(requests.get(urljoin(url,link['href'])).content)

def TestSelenium():
    options = Options()
    ffprofile = FirefoxProfile()
    ffprofile.set_preference("browser.helperApps.neverAsk.saveToDisk","application/pdf")
    ffprofile.set_preference("browser.download.folderList", 2)
    #ffprofile.set_preference("browser.download.manager.showWhenStarting", "false")
    ffprofile.set_preference("browser.download.dir", "/home/fprime/Dev/tv-gpt2/PDFs")
    #ffprofile.set_preference("plugin.scan.plid.all","false")
    #ffprofile.set_preference("plugin.scan.Acrobat","99.0")
    ffprofile.set_preference("pdfjs.disabled", True)
    ffprofile.set_preference("browser.download.useDownloadDir", True)
    options.headless = True
    firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
    firefox_capabilities['marioneete'] = True
    driver = webdriver.Firefox(capabilities = firefox_capabilities, options=options, firefox_profile=ffprofile)
    driver.get("https://sites.google.com/site/tvwriting/")
    print(driver.title)
    downloadLinks = driver.find_elements_by_link_text("READ NOW")
    try:
        for link in downloadLinks:
            link.click()
            driver.implicitly_wait(5)
    #link = driver.find_element_by_link_text("READ NOW")
    #print(f'href of link is {link.get_attribute("href")}')
    #link.click()
    #try:
    #    element = WebDriverWait(driver, 10).until(
    #            EC.title_contains("Redirecting"))
    #    print(driver.title)
    finally:
        driver.quit()
        print("Leaving, please check for errors or phat loot")

def DownloadTVCalling(driver):
    # Download for scripts.tv-calling.com/archives
    url = "https://scripts.tv-calling.com/archives"

    driver.get(url)
    print(f'Now downloading from {driver.title}')
    # Steps: Click download links on the screen, then click next. Repeat through maximum number
    # Let's go through and collect links first. Then download them from the links one at a time.

    element = driver.find_element_by_xpath('//div[@class="text-center"]/ul/li[last() - 1]')
    print(f'Last number is {element.text}')

    wait = WebDriverWait(driver, 20)

    for page in range(0,int(element.text)):
        for link in driver.find_elements_by_xpath("//div[@class='media-body']//a[@class='wpdm-download-link wpdm-download-locked [btnclass]']"):
            #wait.until(EC.visibility_of(link))
            #link.click()
            # Click isn't working, so trying javascript
            link_url = link.get_attribute('href')
            # Maybe open in new window?!
            #driver.execute_script(f"window.open('{link_url}', 'new_window')")
            #print(driver.window_handles)
            #driver.switch_to_window(driver.window_handles[0])
            # Nope was just missing a mime type, use javascript click since it's "not visible" by link.click()
            driver.execute_script("arguments[0].click()", link)
            #link.click()
            time.sleep(0.5)
        nextButton = None
        nextButton = driver.find_element_by_class_name('next')
        print(f'Next takes us to: {nextButton.get_attribute("href")}')
        nextButton.click()
        time.sleep(5)
    print('Complete')

def TheTelevisionPilot(driver):
    # Down from The Television Pilot
    url_a_b = 'http://thetelevisionpilot.com/a-b/'
    url_c_f = 'http://thetelevisionpilot.com/script-database/c-f/'
    url_g_l = 'http://thetelevisionpilot.com/script-database/g-l/'
    url_m_r = 'http://thetelevisionpilot.com/script-database/m-r/'
    url_s_z = 'http://thetelevisionpilot.com/script-database/s-z/'

    urls = ['http://thetelevisionpilot.com/script-database/c-f/', 'http://thetelevisionpilot.com/script-database/g-l/', 'http://thetelevisionpilot.com/script-database/m-r/', 'http://thetelevisionpilot.com/script-database/s-z/']
    print(f'Going to {url_a_b}')
    driver.get(url_a_b)
    for link in driver.find_elements_by_xpath('/html/body/div[1]/div[1]/section/main/article/div/div/div/div/section/div/div/div/div/div/div/div/div/table/tbody/tr/td/div/div/h8/p/strong/a'):
        print(link.get_attribute('href'))
        link.click()
    # Each one is different, oh great
    print(f'Going to {url_c_f}')
    driver.get(url_c_f)
    for link in driver.find_elements_by_xpath('/html/body/div[1]/div[1]/section/main/article/div/div/div/div/section/div/div/div/div/div/div/div/div/div/table/tbody/tr/td/a[contains(@href,"pdf")]'):
        print (link.get_attribute('href'))
        link.click()
    # Except these last 3, so someone had consistency and quit lol
    print(f'Going to {url_g_l}')
    driver.get(url_g_l)
    for link in driver.find_elements_by_xpath('/html/body/div[1]/div[1]/section/main/article/div/div/table/tbody/tr/td/a[contains(@href,"pdf")]'):
        print (link.get_attribute('href'))
        link.click()
    print(f'Going to {url_m_r}')
    driver.get(url_m_r)
    for link in driver.find_elements_by_xpath('/html/body/div[1]/div[1]/section/main/article/div/div/table/tbody/tr/td/a[contains(@href,"pdf")]'):
        print (link.get_attribute('href'))
        link.click()
    print(f'Going to {url_s_z}')
    driver.get(url_s_z)
    for link in driver.find_elements_by_xpath('/html/body/div[1]/div[1]/section/main/article/div/div/table/tbody/tr/td/a[contains(@href,"pdf")]'):
        print (link.get_attribute('href'))
        link.click()

    print('Complete')

def SetupWebDriver():
    # Setup the driver for downloading PDFs
    options = Options()
    ffprofile = FirefoxProfile()
    # application/x-download used for TV Calling
    ffprofile.set_preference("browser.helperApps.neverAsk.saveToDisk","application/pdf,application/x-download")
    ffprofile.set_preference("browser.download.folderList", 2)
    ffprofile.set_preference("browser.download.dir", "/home/fprime/Dev/tv-gpt2/PDFs")
    ffprofile.set_preference("pdfjs.disabled", True)
    ffprofile.set_preference("browser.download.useDownloadDir", True)
    ffprofile.set_preference("browser.download.manager.showWhenStarting", False)
    ffprofile.set_preference("plugin.disable_full_page_plugin_for_types","application/pdf,application/x-download")
    #options.headless = True
    firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
    firefox_capabilities['marioneete'] = True
    driver = webdriver.Firefox(capabilities = firefox_capabilities, options=options, firefox_profile=ffprofile)
    return driver
    

if __name__ == "__main__":
    try:
        driver = SetupWebDriver()
        # TODO: Clean up this shit to where we can control more easily where we downloading from...
        #TestSelenium()
        #DownloadTVCalling(driver)
        #TheTelevisionPilot(driver)
    except (Exception, KeyboardInterrupt) as ex:
        driver.quit()
        print(f"Error {str(ex)}")
    finally:
        driver.quit()
