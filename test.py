import os
import requests
from urllib.parse import urljoin
from bs4 import BeautifulSoup
import re
from pathlib import Path

folder_location = Path('/mnt/backup/Datasets/TVScriptCorpus/test2/')
#if not os.path.exists(folder_location):os.mkdir(folder_location)

# For https://sites.google.com/site/tvwriting/

url = "https://sites.google.com/site/tvwriting/"

headers = {
	'User-Agent': 'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/71.0',
	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	'Accept-Language': 'en-US,en;q=0.5'
}

response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, "html.parser")
for link in soup.findAll("a", {"target" : "_blank"}): #https://stackoverflow.com/questions/5041008/how-to-find-elements-by-class
	if link.string == "READ NOW":
		filename = Path(folder_location / link.previous_element.string[0:len(link.previous_element.string) - 1]).with_suffix('.pdf')
		print(f'Writing out: {filename.name}')
		#filename = os.path.join(folder_location,link.previous_element.string[0:len(link.previous_element.string) - 1].join('.pdf'))
		with open(filename, 'wb') as f:
			try:
				downloadRequestToBeContinued = requests.get(urljoin(url,link['href']), allow_redirects=True, headers=headers)
				# Thanks to google, always returns HTTP 200
				redirectSoup = BeautifulSoup(downloadRequestToBeContinued.text, "html.parser")
				metas = redirectSoup.findAll("meta")
				stringWeWant = metas[1]['content']
				firstEqualSign = stringWeWant.index('=')
				lengthOfString = len(stringWeWant)
				mainURL = stringWeWant[firstEqualSign + 1: lengthOfString]
				print(mainURL)
				downloadResponse = requests.get(mainURL, headers=headers)
				print(downloadResponse.status_code)
				f.write(downloadResponse.content)
			except requests.exceptions.InvalidSchema:
				print("No emails or other type of links pls")
			finally:
				f.close()